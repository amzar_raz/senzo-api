<?php
session_start();
ob_start();
ini_set('max_execution_time', 30);
include '././configuration/serverConfig.php';
include './header/headerAll.php';
?>
<!DOCTYPE html>
<html>

<head>
    <title>Setting</title>
    <link href="css/Setting.css" rel="stylesheet">
    <link rel="stylesheet" href="css/Sidebar.css">
    <script type="text/javascript" src="SidebarHandler.js"></script>
    <link rel="stylesheet" href="css/checkboxes.min.css">
</head>
<script type="text/javascript">
    $(function() {
        $('#toggle-GoogleHome').change(function() {
            var loader = document.getElementById("loader");
            if ($(this).prop('checked')) {
                var url = baseURL + "/googleEnableGoogleHome.php";
                loader.style.display = "block";
                $.getJSON(url, function(data) {
                    console.log(data);
                    loader.style.display = "none";
                    if (data.Status == false) {
                        $("#toggle-GoogleHome").prop('checked', false);
                        Swal.fire({
                            icon: 'error',
                            title: 'Failed',
                            text: data.Message
                        })
                    } else {
                        iziToast.show({
                            title: 'GOOGLE HOME',
                            message: 'Your gateway has enable Google Home',
                            theme: 'dark',
                            position: 'bottomCenter',
                            icon: 'icon-person'
                        });
                    }
                });
            } else {
                var url = baseURL + "/googleDisableGoogleHome.php";
                loader.style.display = "block";
                $.getJSON(url, function(data) {
                    console.log(data);
                    loader.style.display = "none";
                    if (data.Status == false) {
                        $('#toggle-GoogleHome').prop('checked', true);
                        Swal.fire({
                            icon: 'error',
                            title: 'Failed',
                            text: data.Message
                        })
                    } else {
                        iziToast.show({
                            title: 'GOOGLE HOME',
                            message: 'Your gateway has disable Google Home',
                            theme: 'dark',
                            position: 'bottomCenter',
                            icon: 'icon-person'
                        });
                    }
                });
            }

        });
    })

    $(function() {
        $('#toggle-Analytic').change(function() {
            if ($(this).prop('checked')) {
                console.log("check");
            } else {
                console.log("uncheck");
            }
        });
    })
</script>

<body>
    <?php
    $SerialGateway = $_SESSION['serialGateway'];
    $Password = $_SESSION['password'];
    $LocationListArray = $_SESSION['location'];
    $SmartSwitchStatus = $_SESSION['SmartSwitchStatus'];
    $CurtainStatus = $_SESSION['CurtainStatus'];
    $AlarmStatus = $_SESSION['AlarmStatus'];
    $ShutterStatus = $_SESSION['ShutterStatus'];
    $IRBlasterStatus = $_SESSION['IRBlasterStatus'];
    $LockStatus = $_SESSION['LockStatus'];
    $TotalDevice = $_SESSION['TotalDevice'];
    $SceneCount = $_SESSION['SceneCount'];
    $AdminPassword = $_SESSION['AdminPassword'];

    echo "<input style='display:none;' id='SerialGateway' value='$SerialGateway'>";
    echo "<input style='display:none;' id='Password' value='$Password'>";
    echo "<input style='display:none;' id='AdminPassword' value='$AdminPassword'>";

    if (isset($_POST['Logout'])) {
        header("Location: ChooseGateway.php");
        header("Refresh:0");
        ob_flush();
        session_destroy();
        exit();
    }

    if (isset($_POST['Home'])) {
        // header("Refresh:0");
        header("Location: Dashboard.php");
        ob_flush();
        exit();
    }

    $opts = array('http' => array('header' => 'Cookie: ' . $_SERVER['HTTP_COOKIE'] . "\r\n"));
    $context = stream_context_create($opts);
    session_write_close(); // unlock the file
    $url = $baseURL . "/googleGetGoogleHome.php";
    $GoogleJson = file_get_contents($url, false, $context);
    $msgJson = json_decode($GoogleJson);
    if (!empty($msgJson->Message)) {
        $Error = $msgJson->Message;
    ?>
        <script type="text/javascript">
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: '<?php echo $Error; ?>',
            }).then(function() {
                window.location.href = "ChooseGateway.php";
            });
        </script>
    <?php
        die();
    }

    function getList($msgJson)
    {
    }

    ?>

    <input style='display:none;' id='IntervalId' value='none'>

    <!-- Form for sign Door Lock -->
    <div class="form-popup" id="mySiginDoorLockForm">
        <div class="form-container" style="width:400px;padding:0px;">
            <div class="modal-header" style='width:100%;height:70px;'>
                <!-- <a class='back' onclick='closeFormSigin()'><span class='fa fa-arrow-left fa-2x'></a> -->
                <h3 class='h2form'>Manage Door Lock </br> Password</h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <fieldset style='width:100%;'>
                    <label for="name">Enter Admin Password:</label>
                    <input type="password" id="password" name="DoorlockPassword" style='width:100%;'>
                </fieldset>
            </div>
            <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
                <button type="submit" class="all" onclick='openGetLock()' name="signin">Ok</button>
                <button type="button" class="all" onclick="cancelFormSigin()">Cancel</button>
            </div>
        </div>
    </div>

    <!-- SideBar + Navbar -->
    <div class="wrapper">
        <!-- Sidebar Holder -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>Senzo</h3>
                <strong>SZ</strong>
            </div>
            <ul class="list-unstyled components">
                <li>
                    <form action='SmartCamera.php' method='POST' id='NavHome' style='margin-block-end: 0'>
                        <input style='display:none;' name='Home'>
                        <a style='cursor:pointer;' class='Home'>
                            <i class="glyphicon glyphicon-home"></i>
                            Home
                        </a>
                    </form>
                </li>
                <li>
                    <a href="#pageDevice" data-toggle="collapse" aria-expanded="false">
                        <i class="glyphicon glyphicon-tasks"></i>
                        <?php echo "Device"; ?>
                    </a>
                    <ul class="collapse list-unstyled" id="pageDevice">
                        <?php
                        if ($SmartSwitchStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectSwitch()">Smart Switch</a></li>';
                        }
                        if ($CurtainStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectCurtain()">Smart Curtain</a></li>';
                        }
                        if ($ShutterStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectShutter()">Smart Shutter</a></li>';
                        }
                        if ($LockStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectDoorLock()">Smart Door Lock</a></li>';
                        }
                        if ($IRBlasterStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectIRBlaster()">IR Blaster</a></li>';
                        }
                        ?>
                    </ul>
                </li>
                <li>
                    <a style="cursor:pointer;" onclick='openScenesForm()'>
                        <i class="glyphicon glyphicon-film"></i>
                        <?php echo "Scene ($SceneCount)"; ?>
                    </a>
                </li>
                <li>
                    <a href="#pageSecurity" data-toggle="collapse" aria-expanded="false">
                        <i class="glyphicon glyphicon-lock"></i>
                        <?php echo "Security"; ?>
                    </a>
                    <ul class="collapse list-unstyled" id="pageSecurity">
                        <?php
                        if ($AlarmStatus) {
                            echo "<li><a style='cursor:pointer;' onclick='selectAlarm(&#39;True&#39;)'>
                            Smart Alarm <img src='Images/Action/security_armed_icon.png' alt='scenes' style='width:20px;height:20px;'>";
                        } else {
                            echo "<li><a style='cursor:pointer;' onclick='selectAlarm(&#39;False&#39;)'>
                            Smart Alarm";
                        }
                        ?>
                        </a>
                </li>
                <li><a style="cursor:pointer;" onclick="selectCamera()">Camera</a></li>
            </ul>
            </li>
            <li>
                <a style='cursor:pointer;' onclick='openAnalyticForm()'>
                    <i class='glyphicon glyphicon-stats'></i>
                    Analytic
                </a>
            </li>
            <li>
                <a id='cube' style='cursor:pointer;' onclick='openSettingForm()'>
                    <i class='glyphicon glyphicon-cog'></i>
                    Setting
                </a>
            </li>
            </ul>

            <ul class="list-unstyled CTAs">
                <li>
                    <form method="POST" action="SceneList.php" id='NavLogout' style='margin-block-end: 0'>
                        <input style='display:none;' name='Logout'>
                        <a style="cursor:pointer;" class="Logout">
                            <i class="glyphicon glyphicon-log-out"></i>
                            Logout
                        </a>
                    </form>
                </li>
            </ul>
        </nav>

        <!-- Page Content Holder -->
        <div id="content" style="width: -webkit-fill-available;">

            <nav class="navbar navbar-default">
                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                            <i class="glyphicon glyphicon-align-left"></i>
                            <span></span>
                        </button>

                        <button class="btn btn-dark navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <i class="glyphicon glyphicon-align-justify"></i>
                        </button>
                    </div>

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <h2>Account Setting</h2>
            <?php
            $GatewayLevel = $_SESSION['level'];
            echo "<h4 class='Level' >Gateway $GatewayLevel</h4>";
            ?>
            <div class="line"></div>
            <h3>Edit Profile</h3>
            <div class="container">
                <div class="row">
                    <div>
                        <form action="Setting.php" method="post" enctype="multipart/form-data" style="text-align: center;">
                            <label for="fileToUpload">
                                <div class="profile-pic" style="background-image: url('Images/avatar_img.png')">
                                    <span class="glyphicon glyphicon-camera"></span>
                                    <span>Change Image</span>
                                </div>
                            </label>
                            <input type="File" name="fileToUpload" id="fileToUpload">
                        </form>
                        <label for="email"><b>Username</b></label>
                        <input type="text" name="username" placeholder="Enter Username" required>
                        <label for="psw"><b>Password</b></label>
                        <input type="password" placeholder="Enter Password" name="psw" required>
                        <input type="submit" value="Update Setting" class="btn btn-primary upd_profile" />
                    </div>
                </div>
            </div>

            <div class="line"></div>
            <h3>Feature Options</h3>
            <div class="container">
                <div class="row">
                    <div class="feature-flag">
                        <div class="row-fluid feature high_contrast">
                            <div style="display:inline-block">
                                <div class="flex-container">
                                    <div><img src="Images/google-home.svg" alt='google_home' style='width:50px;height:50px;'></div>
                                    <div class="feature-title"><strong>Google Home </strong></br>Enable this to access features voice recognition, and home automation support</div>
                                </div>
                            </div>
                            <div style="float:right;margin:10px;">
                                <div class="ckbx-style-15">
                                    <?php
                                    $GoogleHome = $msgJson->GoogleHome == true ? "checked" : "";
                                    echo "<input type='checkbox' $GoogleHome id='toggle-GoogleHome' name='toggle-GoogleHome'>";
                                    ?>
                                    <label for="toggle-GoogleHome"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="feature-flag">
                        <div class="row-fluid feature high_contrast">
                            <div style="display:inline-block">
                                <div class="flex-container">
                                    <div><img src="Images/statistics.svg" alt='statistics' style='width:50px;height:50px;'></div>
                                    <div class="feature-title"><strong>Analytic </strong></br>Enable this to track your data analysis</div>
                                </div>
                            </div>
                            <div style="float:right;margin:10px;">
                                <div class="ckbx-style-15">
                                    <input type="checkbox" id="toggle-Analytic" value="0" name="toggle-Analytic">
                                    <label for="toggle-Analytic"></label>
                                </div>
                            </div>
                        </div>
                        </li>
                    </div>
                </div>
            </div>

            <div class="line"></div>

        </div>

        <!-- Auto Refresh dropdown -->
        <div class='autorefresh'>
            <div class="dropdown">
                <button class="btnRefresh dropdown-toggle" type="button" data-toggle="dropdown">
                    <img src='Images/Switch/settings_gear.png' alt='switchon' style='width:30px;height:30px;animation: spin 3s linear infinite;'>
                </button>
                <ul class="dropdown-menu">
                    <!-- <li><a href="#">Phantom</a></li> -->
                    <li>
                        <input type='checkbox' id='enable' name='fooby[2][]'>
                        <label> Enable</label><br>
                        <div id='timeRefresh' style='display:none;margin-left:10px;'>
                            <input type='checkbox' value='1-minute' class="slectOne" id='1-minute'>
                            <label> 1 minute</label><br>
                            <input type='checkbox' value='5-minutes' class="slectOne" id='5-minutes'>
                            <label> 5 minutes</label><br>
                            <input type='checkbox' value='10-minutes' class="slectOne" id='10-minutes'>
                            <label> 10 minutes</label><br>
                            <input type='checkbox' value='20-minutes' class="slectOne" id='20-minutes'>
                            <label> 20 minutes</label><br>
                            <input type='checkbox' value='25-minutes' class="slectOne" id='25-minutes'>
                            <label> 25 minutes</label><br>
                            <input type='checkbox' value='1-hour' class="slectOne" id='1-hour'>
                            <label> 1 hour</label><br>
                        </div>
                    </li>
                    <li>
                        <input type='checkbox' id='disable' value='disable'>
                        <label> Disable</label><br>
                    </li>
                </ul>
            </div>
        </div>

        <div class="form-popup" id='loader'>
            <!-- <div id="loadering" class="loader"></div> -->
            <div class="loading-container">
                <div class="loading"></div>
                <div class="loading-text">loading</div>
            </div>
        </div>
</body>

</html>