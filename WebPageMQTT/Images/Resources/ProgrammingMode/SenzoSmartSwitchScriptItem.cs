using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using SenzoProApp.Resources;
using Xamarin.Forms;


namespace SenzoProApp
{
    public class SenzoSmartSwitchScriptItem : SenzoScriptItem
    {

        //        //test variable is fit here
        private int firelevel;

        public int Firelevel
        {
            get {
                return firelevel;
            }
            set {
                firelevel = value;
                if ( SmartSwitch.SwitchMode == 1 ) {
                    OnPropertyChanged(nameof(DimmerFirelevelValue));
                    OnPropertyChanged(nameof(DimmerTextColor));   
                }
            }
        }

        public int PrevFirelevel { get; set; }
        //public bool IsDelayEnabled { get; set; }
        public bool HasBeep { get; set; }

        public bool? DelayOnOff { get; set; }
        public int? TotalMinutes { get; set; }
		
        public SenzoSwitch SmartSwitch {
            get { return SenzoDevice as SenzoSwitch; }
            set {
                if (SenzoDevice == value ) {
                    return;
                }
                SenzoDevice = value;
                OnPropertyChanged (  );
                OnPropertyChanged ( "Label" );
            }
        }        
        private EScriptIconState switchState;
        public EScriptIconState SwitchState {
            get { return switchState; }
            set {
                if ( switchState == value ) {
                    return;
                }
                switchState = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(ImageSource));
                if ( SmartSwitch.SwitchMode == 1 ) {
                    OnPropertyChanged(nameof(DimmerVisibility));
                }
            }
        }
		
        public new string Label
        {
            get {
                if ( string.IsNullOrEmpty (label) ) {
                    label = $"{SmartSwitch.Name} ({SmartSwitch.Owner.ModelName})";
                }
                return label;
            }
            set {
                if ( label == value ) {
                    return;
                }
                label = value;
                OnPropertyChanged (  );
            }
        }


        private string imageSource;
        public string ImageSource {
            get {
                switch (SmartSwitch.SwitchMode ) {
                    case 1 :
                        switch ( SwitchState ) {
                            case EScriptIconState.NoAction:
                                imageSource = "senzo_switch_dimmerNoAction.png";
                                break;
                            case EScriptIconState.On:
                                imageSource = "senzo_switch_dimmerOnTimerOff.png";
                                break;
                            case EScriptIconState.Off:
                                imageSource = "senzo_switch_dimmerOffTimerOff.png";
                                break;
                            case EScriptIconState.AlarmLight:
                                imageSource = "senzo_switch_dimmerAlarm.png";
                                break;
                            case EScriptIconState.Press:
                                SwitchState = EScriptIconState.NoAction;
                                imageSource = "senzo_switch_dimmerNoAction.png";
                                break;
                            default :
                                throw new ArgumentOutOfRangeException ();
                        }
                        break;
                    case 4 :
                        switch ( SwitchState ) {
                            case EScriptIconState.NoAction:
                                imageSource = "senzo_switch_autogateNoAction.png";
                                break;
                            case EScriptIconState.On:
                                imageSource = "senzo_switch_autogateOnTimerOff.png";
                                break;
                            case EScriptIconState.Off:
                                imageSource = "senzo_switch_autogateOffTimerOff.png";
                                break;
                            case EScriptIconState.AlarmLight:
                                imageSource = "senzo_switch_autogateAlarm.png";
                                break;
                            case EScriptIconState.Press:
                                //SwitchState = EScriptIconState.NoAction;
                                imageSource = "senzo_switch_autogateOnTimerOff.png";
                                break;
                            default :
                                throw new ArgumentOutOfRangeException ();
                        }
                        break;
                    default :
                        switch ( SwitchState ) {
                            case EScriptIconState.NoAction:
                                imageSource = "senzo_switch_buttonNoAction.png";
                                break;
                            case EScriptIconState.On:
                                imageSource = "senzo_switch_buttonOnTimerOff.png";
                                break;
                            case EScriptIconState.Off:
                                imageSource = "senzo_switch_buttonOffTimerOff.png";
                                break;
                            case EScriptIconState.AlarmLight:
                                imageSource = "senzo_switch_buttonAlarm.png";
                                break;
                            case EScriptIconState.Press:
                                SwitchState = EScriptIconState.NoAction;
                                imageSource = "senzo_switch_buttonNoAction.png";
                                break;
                            default :
                                throw new ArgumentOutOfRangeException ();
                        }
                        break;
                }

                return imageSource;
            }
            set { imageSource = value; OnPropertyChanged (); }
        }

		
        private EBooleanStatus isDelayEnabled;
        public EBooleanStatus IsDelayEnabled {
            get { return isDelayEnabled; }
            set {
                if ( isDelayEnabled == value ) {
                    return;
                }
                isDelayEnabled = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(DelayImageSource));
            }
        }

        private string delayImageSource;
        public string DelayImageSource {
            get {
                switch ( IsDelayEnabled ) {
                    case EBooleanStatus.Unknown :
                        delayImageSource = "senzo_switch_delayTimerOff.png";
                        break;
                    case EBooleanStatus.False :
                        delayImageSource = "senzo_switch_delayTimerCancel.png";
                        break;
                    case EBooleanStatus.True :
                        delayImageSource = "senzo_switch_delayTimerOn.png";
                        break;
                    default :
                        throw new ArgumentOutOfRangeException ();
                }
                return delayImageSource;
            }
            set {
                delayImageSource = value;
                OnPropertyChanged ();
            }
        }
		
        private string dimmerFirelevelValue;

        public string DimmerFirelevelValue
        {
            get {
                string tempValue = string.Format ("{0}%", Firelevel.ToString ());
                return dimmerFirelevelValue = tempValue;
            }
            set {
                if ( dimmerFirelevelValue == value ) {
                    return;
                }
                dimmerFirelevelValue = value;
                OnPropertyChanged ();
                OnPropertyChanged (nameof(DimmerTextColor));
            }
        }

        public Color DimmerTextColor
        {
            get {
                if (SmartSwitch.SwitchMode == 1 /*&& SwitchState != EScriptIconState.NoAction*/ && Firelevel > 0 ) {
                    return Resource.AppColor.Primary;
                } else {
                    return Resource.FlatColor.Asbestos;
                }
            }
        }

        public bool DimmerVisibility
        {
            get {
                
                //[0:] DimmerVisibility:: Timer 2 | SwitchMode: 0 | SwitchState: On
                if ( this.SmartSwitch.SwitchMode != 1 ) {
                    return false;
                }
                if ( SwitchState == EScriptIconState.NoAction ) {
                    return false;
                } else {
                    //Debug.WriteLine($"DimmerVisibility:: {SmartSwitch.Name} | SwitchMode: {SmartSwitch.SwitchMode} | SwitchState: {SwitchState}");
                    return true;
                }
            }
        }

        public SenzoSmartSwitchScriptItem(SenzoSwitch smartSwitch, List<SenzoProgrammingMode> commands) : base(smartSwitch, commands) {
            SmartSwitch = smartSwitch;
            SwitchState = EScriptIconState.NoAction;
            IsDelayEnabled = EBooleanStatus.Unknown;


        }

        public override void UpdateCommands () {
            base.UpdateCommands ();

            UpdateDeviceFromCommandsByCategory <SenzoProgrammingMode_Beep> ();
            UpdateDeviceFromCommandsByCategory <SenzoProgrammingMode_SetSwitchDelayTimer> ();
            UpdateDeviceFromCommandsByCategory <SenzoProgrammingMode_ToggleAllSwitches> ();
            UpdateDeviceFromCommandsByCategory <SenzoProgrammingMode_SetAllSwitch> ();
            UpdateDeviceFromCommandsByCategory <SenzoProgrammingMode_Set1Switch> ();
            UpdateDeviceFromCommandsByCategory <SenzoProgrammingMode_SetAllSwitchAlarmTriggerFirelevel> ();
            UpdateDeviceFromCommandsByCategory <SenzoProgrammingMode_CancelSwitchDelayTimer> ();

        }
        protected override void UpdateDeviceFromCommandsByCategory <T> (  ) {
            Predicate <SenzoProgrammingMode> condition = item => item is T;
            var commands = Commands.ToList ().FindAll ( condition );
            if ( commands == null || !commands.Any() ) {
                return;
            }
			
            foreach (var command in commands) {
                if ( command == null ) {
                    continue;
                }
                if ( command is SenzoProgrammingMode_SetAllSwitch ) {
                    if ( SmartSwitch.SerialNo == ( ( SenzoProgrammingMode_SetAllSwitch ) command ).SerialNo ) {
                        //
                        Firelevel = ( ( SenzoProgrammingMode_SetAllSwitch ) command ).Firelevels [ SmartSwitch.SwitchNo - 1 ];
                        SwitchState = Firelevel > 0 ? EScriptIconState.On : EScriptIconState.Off;
                    }
                    continue;
                }
                if ( command is SenzoProgrammingMode_ToggleAllSwitches ) {
                    if ( SmartSwitch.SerialNo == ( ( SenzoProgrammingMode_ToggleAllSwitches ) command ).SerialNo ) {
                        var firelevelState = ((SenzoProgrammingMode_ToggleAllSwitches)command).FirelevelStates[SmartSwitch.SwitchNo - 1];
                        switch ( firelevelState ) {
                            case 1:
                                Firelevel = 100;
                                SwitchState = SmartSwitch.SwitchMode == 4 ? EScriptIconState.Press : EScriptIconState.On;
                                break;
                            case 2:
                                Firelevel = 0;
                                SwitchState = SmartSwitch.SwitchMode == 4
                                    ? EScriptIconState.Press
                                    : EScriptIconState.Off;
                                break;
                            case 255:
                                Firelevel = -1;
                                SwitchState = EScriptIconState.NoAction;
                                break;
                        }
                    }
                    continue;
                }
                if ( command is SenzoProgrammingMode_Set1Switch ) {
                    if ( SmartSwitch.SerialNo == ( ( SenzoProgrammingMode_Set1Switch ) command ).SerialNo ) {
                        if ( SmartSwitch.SwitchNo == ( ( SenzoProgrammingMode_Set1Switch ) command ).SwitchNo ) {
                            Firelevel = ( ( SenzoProgrammingMode_Set1Switch ) command ).Firelevel;
                            if ( Firelevel > 0 ) {
                                SwitchState = EScriptIconState.On;
                            } else {
                                SwitchState = EScriptIconState.Off;
                            }
                        }
                    }
                    continue;
                }
                if ( command is SenzoProgrammingMode_SetAllSwitchAlarmTriggerFirelevel ) {
                    if ( SmartSwitch.SerialNo == ( ( SenzoProgrammingMode_SetAllSwitchAlarmTriggerFirelevel ) command ).SerialNo ) {
                        HasBeep = ((SenzoProgrammingMode_SetAllSwitchAlarmTriggerFirelevel)command).EnableBeep;
                        Firelevel = ((SenzoProgrammingMode_SetAllSwitchAlarmTriggerFirelevel)command).Firelevels[SmartSwitch.SwitchNo - 1];
                        if ( Firelevel <= 100 && Firelevel > 0 ) {
                            SwitchState = EScriptIconState.AlarmLight;
                        } 
						
                    }
                    continue;
                }
                if ( command is SenzoProgrammingMode_SetSwitchDelayTimer ) {
                    if ( SmartSwitch.SerialNo == ( ( SenzoProgrammingMode_SetSwitchDelayTimer ) command ).SerialNo ) {
                        if ( SmartSwitch.SwitchNo == ( ( SenzoProgrammingMode_SetSwitchDelayTimer ) command ).SwitchNo ) {
                            IsDelayEnabled = EBooleanStatus.True;
                            DelayOnOff = ( ( SenzoProgrammingMode_SetSwitchDelayTimer ) command ).OnOff;
                            TotalMinutes = ((SenzoProgrammingMode_SetSwitchDelayTimer)command).Minutes;
//                            var onoff = ((SenzoProgrammingMode_SetSwitchDelayTimer)command).OnOff;
//                            var minutes = ((SenzoProgrammingMode_SetSwitchDelayTimer)command).Minutes;
                        }
                    }
                    continue;
                }
                if ( command is SenzoProgrammingMode_CancelSwitchDelayTimer ) {
                    if ( SmartSwitch.SerialNo == ( ( SenzoProgrammingMode_CancelSwitchDelayTimer ) command ).SerialNo ) {
                        if ( SmartSwitch.SwitchNo == ( ( SenzoProgrammingMode_CancelSwitchDelayTimer ) command ).SwitchNo ) {
                            IsDelayEnabled = EBooleanStatus.False;
                        } 
                    }
                    continue;
                }
                if ( command is SenzoProgrammingMode_Beep ) {
                    HasBeep = false || SmartSwitch.SerialNo == ( ( SenzoProgrammingMode_Beep ) command ).SerialNo;
                    continue;
                }
            }     
        }
    }
}