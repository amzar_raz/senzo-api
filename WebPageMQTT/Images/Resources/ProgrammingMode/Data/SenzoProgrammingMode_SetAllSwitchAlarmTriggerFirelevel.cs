using System;
using System.Collections.Generic;

namespace SenzoProApp
{
    public class SenzoProgrammingMode_SetAllSwitchAlarmTriggerFirelevel : SenzoProgrammingMode
    {
        public string SerialNo { get; set; }
        public int TotalSwitch { get; set; }
        public List<int> Firelevels { get; set; }
        public bool EnableBeep { get; set; }

        public SenzoProgrammingMode_SetAllSwitchAlarmTriggerFirelevel() { }
        public override bool Parse(List<string> commandParams) {
            if ( commandParams.Count < 4 ) {
                return false;
            }
            SerialNo = commandParams [ 1 ];
            TotalSwitch = int.Parse ( commandParams [ 2 ] );
            Firelevels = new List <int> ();
            Firelevels.Clear ();
            for ( var i = 0; i < TotalSwitch; i++ ) {
                Firelevels.Add ( -1 );
                Firelevels [ i ] = int.Parse ( commandParams [ 3 + i ] );
            }

            if ( commandParams.Count > 3 + TotalSwitch ) {
                var beepStr = commandParams[3 + TotalSwitch];
                EnableBeep = Convert.ToBoolean ( int.Parse ( beepStr ) );
            } else {
                EnableBeep = false;
            }
            return true;
        }

        public override string ToScript() {
            var commandBuilder = string.Empty;
            commandBuilder = string.Format ( "{0} {1} {2}" ,
                                             SenzoScriptParser.CommandList.SetAllSwitchAlarmTriggerFirelevel , 
                                             SerialNo ,
                                             TotalSwitch );
            for ( var i = 0; i < TotalSwitch; i++ ) {
                commandBuilder += string.Format ( " {0}" , Firelevels [ i ] );
            }

            commandBuilder += string.Format ( " {0}" , Convert.ToInt32 ( EnableBeep ) );
            return commandBuilder;
        }

        public override string ToDesc() {
            var onSwitchDesc = string.Empty;
            var offSwitchDesc = string.Empty;
            var dimmerSwitchDesc = string.Empty;
            var noChangeSwitchDesc = string.Empty;
            for ( var i = 0; i < TotalSwitch; i++ ) {
                if ( Firelevels [ i ] > 100 ) {
                    noChangeSwitchDesc = string.IsNullOrEmpty ( noChangeSwitchDesc )
                                                 ? ( i + 1 ).ToString ()
                                                 : string.Format ( "{0}, {1}" , noChangeSwitchDesc , ( i + 1 ) );
                } else {
                    if ( Firelevels [ i ] > 0 ) {
                        if ( Firelevels [ i ] < 100 ) {
                            dimmerSwitchDesc = string.IsNullOrEmpty ( dimmerSwitchDesc )
                                                       ? string.Format ( "Switch : {0} to {1}%" , ( i + 1 ) , Firelevels [ i ] )
                                                       : string.Format ( "{0}, {1} to {2}%" , dimmerSwitchDesc , ( i + 1 ) , Firelevels [ i ] );
                        } else {
                            onSwitchDesc = string.IsNullOrEmpty ( onSwitchDesc )
                                                   ? ( i + 1 ).ToString ()
                                                   : string.Format ( "{0}, {1}" , onSwitchDesc , ( i + 1 ) );
                        }
                    } else {
                        offSwitchDesc = string.IsNullOrEmpty ( offSwitchDesc )
                                                ? ( i + 1 ).ToString ()
                                                : string.Format ( "{0}, {1}" , offSwitchDesc , ( i + 1 ) );
                    }
                }
            }
            var commandDesc = string.Empty;

            var smartSwitch = SenzoSmartSwitch.FindSmartSwitchBySerialNo ( SerialNo );
            if ( smartSwitch == null ) {
                commandDesc = string.Format ( "Turn Smart Switch for" );
                if ( !string.IsNullOrEmpty ( onSwitchDesc ) ) {
                    commandDesc = string.Format ( "Switch : {0} to 100%" , onSwitchDesc );
                }
                if ( !string.IsNullOrEmpty ( offSwitchDesc ) ) {
                    commandDesc = string.Format ( "Switch : {0} to 0%" , offSwitchDesc );
                }
                if ( !string.IsNullOrEmpty ( dimmerSwitchDesc ) ) {
                    commandDesc = commandDesc + dimmerSwitchDesc;
                }
                return string.Format ( "{0} (!)" , commandDesc );
            }

            var switchList = smartSwitch.SwitchList;
            if ( switchList [ 0 ] == null ) {
                commandDesc = string.Format ( "Turn Smart Switch of Serial Number {0} for" , smartSwitch.UserSerialNo );
                if ( !string.IsNullOrEmpty ( onSwitchDesc ) ) {
                    commandDesc = string.Format ( "Switch : {0} to 100%" , onSwitchDesc );
                }
                if ( !string.IsNullOrEmpty ( offSwitchDesc ) ) {
                    commandDesc = string.Format ( "Switch : {0} to 0%" , offSwitchDesc );
                }
                if ( !string.IsNullOrEmpty ( dimmerSwitchDesc ) ) {
                    commandDesc = commandDesc + dimmerSwitchDesc;
                }
                return commandDesc;
            }
            
            for ( var i = 0; i < TotalSwitch; i++ ) {
                if ( Firelevels [ i ] >= 0 && Firelevels [ i ] <= 100 ) {
                    if ( !string.IsNullOrEmpty ( commandDesc ) ) {
                        commandDesc += ", ";
                    }
                    switch ( switchList[i].SwitchMode ) {
                        case 4: //momentary
                            commandDesc += string.Format("Autogate ON {0} (Channel {3}, Zone {4})",/*{1}, {2}, ...Channel*/
                                                           switchList [ i ].Name , 
                                                           switchList [ i ].FloorName ,
                                                           switchList [ i ].LocationName , 
                                                           switchList [ i ].ChannelNo ,
                                                           switchList [ i ].ZoneNo );
                            break;
                        case 0: 
                        case 128:
                            var switchOnOffDesc = Firelevels [ i ] > 0 ? "on" : "off";
                            commandDesc += string.Format("Turn {0} {1} (Channel {4}, Zone {5})",/*{1}, {2}, ...Channel*/
                                                           switchOnOffDesc , 
                                                           switchList [ i ].Name ,
                                                           switchList [ i ].FloorName , 
                                                           switchList [ i ].LocationName ,
                                                           switchList [ i ].ChannelNo , switchList [ i ].ZoneNo );
                            break;
                        case 1: //Dimmer
                            var dimmerOnOffDesc = Firelevels[i] > 0 ? "on" : "off";
                            commandDesc += string.Format("Dimmer {0} to {1}% brightness {2} (Channel {5}, Zone {6})",/*{1}, {2}, ...Channel*/
                                                           dimmerOnOffDesc,
                                                           Firelevels[i],
                                                           switchList [ i ].Name ,
                                                           switchList [ i ].FloorName , 
                                                           switchList [ i ].LocationName ,
                                                           switchList [ i ].ChannelNo , 
                                                           switchList [ i ].ZoneNo );
                            break;
                        case 2: // Fan
                            commandDesc += string.Format("Turn {0} to speed {1} (Channel {4}, Zone {5})",/*{1}, {2}, ...Channel*/
                                                           switchList[i].Name,
                                                           Firelevels[i],
                                                           switchList[i].FloorName,
                                                           switchList[i].LocationName,
                                                           switchList[i].ChannelNo,
                                                           switchList[i].ZoneNo);
                            break;
                        default:
                            commandDesc += string.Format("Unknown switch mode for Switch {0}({1}).", (i + 1), switchList[i].Name);
                            break;
                    }
                }
            }
            return commandDesc;
        }

        public override string ToDescLess() {
            var onSwitchDesc = string.Empty;
            var offSwitchDesc = string.Empty;
            var dimmerSwitchDesc = string.Empty;
            var noChangeSwitchDesc = string.Empty;
            for ( var i = 0; i < TotalSwitch; i++ ) {
                if ( Firelevels [ i ] > 100 ) {
                    noChangeSwitchDesc = string.IsNullOrEmpty ( noChangeSwitchDesc )
                                                 ? ( i + 1 ).ToString ()
                                                 : string.Format ( "{0}, {1}" , noChangeSwitchDesc , ( i + 1 ) );
                } else {
                    if ( Firelevels [ i ] > 0 ) {
                        if ( Firelevels [ i ] < 100 ) {
                            dimmerSwitchDesc = string.IsNullOrEmpty ( dimmerSwitchDesc )
                                                       ? string.Format ( "Switch : {0} to {1}%" , ( i + 1 ) , Firelevels [ i ] )
                                                       : string.Format ( "{0}, {1} to {2}%" , dimmerSwitchDesc , ( i + 1 ) , Firelevels [ i ] );
                        } else {
                            onSwitchDesc = string.IsNullOrEmpty ( onSwitchDesc )
                                                   ? ( i + 1 ).ToString ()
                                                   : string.Format ( "{0}, {1}" , onSwitchDesc , ( i + 1 ) );
                        }
                    } else {
                        offSwitchDesc = string.IsNullOrEmpty ( offSwitchDesc )
                                                ? ( i + 1 ).ToString ()
                                                : string.Format ( "{0}, {1}" , offSwitchDesc , ( i + 1 ) );
                    }
                }
            }
            var commandDesc = string.Empty;

            var smartSwitch = SenzoSmartSwitch.FindSmartSwitchBySerialNo ( SerialNo );
            if ( smartSwitch == null ) {
                commandDesc = string.Format ( "Turn Smart Switch for" );
                if ( !string.IsNullOrEmpty ( onSwitchDesc ) ) {
                    commandDesc = string.Format ( "Switch : {0} to 100%" , onSwitchDesc );
                }
                if ( !string.IsNullOrEmpty ( offSwitchDesc ) ) {
                    commandDesc = string.Format ( "Switch : {0} to 0%" , offSwitchDesc );
                }
                if ( !string.IsNullOrEmpty ( dimmerSwitchDesc ) ) {
                    commandDesc = commandDesc + dimmerSwitchDesc;
                }
                return string.Format ( "{0} (!)" , commandDesc );
            }

            var switchList = smartSwitch.SwitchList;
            if ( switchList [ 0 ] == null ) {
                commandDesc = string.Format ( "Turn Smart Switch of Serial Number {0} for" , smartSwitch.UserSerialNo );
                if ( !string.IsNullOrEmpty ( onSwitchDesc ) ) {
                    commandDesc = string.Format ( "Switch : {0} to 100%" , onSwitchDesc );
                }
                if ( !string.IsNullOrEmpty ( offSwitchDesc ) ) {
                    commandDesc = string.Format ( "Switch : {0} to 0%" , offSwitchDesc );
                }
                if ( !string.IsNullOrEmpty ( dimmerSwitchDesc ) ) {
                    commandDesc = commandDesc + dimmerSwitchDesc;
                }
                return commandDesc;
            }
            
            for ( var i = 0; i < TotalSwitch; i++ ) {
                if ( Firelevels [ i ] >= 0 && Firelevels [ i ] <= 100 ) {
                    if ( !string.IsNullOrEmpty ( commandDesc ) ) {
                        commandDesc += ", ";
                    }
                    switch ( switchList[i].SwitchMode ) {
                        case 4: //momentary
                            commandDesc += string.Format("Autogate ON {0} (Channel {3}, Zone {4})",/*{1}, {2}, ...Channel*/
                                                           switchList [ i ].Name , 
                                                           switchList [ i ].FloorName ,
                                                           switchList [ i ].LocationName , 
                                                           switchList [ i ].ChannelNo ,
                                                           switchList [ i ].ZoneNo );
                            break;
                        case 0: 
                        case 128:
                            var switchOnOffDesc = Firelevels [ i ] > 0 ? "on" : "off";
                            commandDesc += string.Format("Turn {0} {1} (Channel {4}, Zone {5})",/*{1}, {2}, ...Channel*/
                                                           switchOnOffDesc , 
                                                           switchList [ i ].Name ,
                                                           switchList [ i ].FloorName , 
                                                           switchList [ i ].LocationName ,
                                                           switchList [ i ].ChannelNo , switchList [ i ].ZoneNo );
                            break;
                        case 1: //Dimmer
                            var dimmerOnOffDesc = Firelevels[i] > 0 ? "on" : "off";
                            commandDesc += string.Format("Dimmer {0} to {1}% brightness {2} (Channel {5}, Zone {6})",/*{1}, {2}, ...Channel*/
                                                           dimmerOnOffDesc,
                                                           Firelevels[i],
                                                           switchList [ i ].Name ,
                                                           switchList [ i ].FloorName , 
                                                           switchList [ i ].LocationName ,
                                                           switchList [ i ].ChannelNo , 
                                                           switchList [ i ].ZoneNo );
                            break;
                        case 2: // Fan
                            commandDesc += string.Format("Turn {0} to speed {1} (Channel {4}, Zone {5})",/*{1}, {2}, ...Channel*/
                                                           switchList[i].Name,
                                                           Firelevels[i],
                                                           switchList[i].FloorName,
                                                           switchList[i].LocationName,
                                                           switchList[i].ChannelNo,
                                                           switchList[i].ZoneNo);
                            break;
                        default:
                            commandDesc += string.Format("Unknown switch mode for Switch {0}({1}).", (i + 1), switchList[i].Name);
                            break;
                    }
                }
            }
            return commandDesc;
        }

        public override bool Undo() {
            return false;
        }

        public override void Send () {
            throw new NotImplementedException ();
        }

        public override SenzoProgrammingMode Clone() {
            return new SenzoProgrammingMode_SetAllSwitchAlarmTriggerFirelevel {
                SerialNo = SerialNo,
                TotalSwitch = TotalSwitch,
                Firelevels = Firelevels
            };
        }

        public override SenzoProgrammingMode Invert() {
            var command = Clone () as SenzoProgrammingMode_SetAllSwitchAlarmTriggerFirelevel;
            for ( var i = 0; i < TotalSwitch; i++ ) {
                if ( command == null ) {
                    return null;
                }
                if ( Firelevels [ i ] > 100 ) {
                    command.Firelevels [ i ] = Firelevels [ i ];
                } else {
                    command.Firelevels [ i ] = Firelevels [ i ] > 0 ? 0 : 100;
                }
            }
            return command;
        }
    }
}