﻿using System;
using System.Collections.Generic;

namespace SenzoProApp {
    public class SenzoProgrammingMode_RunCurtainMacro : SenzoProgrammingMode {
        public int ZoneNo { get; private set; }

        public SenzoProgrammingMode_RunCurtainMacro() { }

        public override bool Parse(List<string> commandParams) {
            return true;
        }

        public override string ToScript() {
            return SenzoScriptCurtainParser.CommandList.RunMacro;
        }

        public override string ToDesc() {
            return string.Format("Run Macro {0}", ZoneNo);
        }

        public override string ToDescLess() {
            return string.Format("Run Macro {0}", ZoneNo);
        }

        public override bool Undo() {
            return false;
        }

        public override void Send() { }

        public override SenzoProgrammingMode Clone() {
            throw new NotImplementedException();
        }

        public override SenzoProgrammingMode Invert() {
            return null;
        }
    }
}