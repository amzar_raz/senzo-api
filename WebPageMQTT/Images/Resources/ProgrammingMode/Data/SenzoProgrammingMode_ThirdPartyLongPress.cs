using System;
using System.Collections.Generic;


namespace SenzoProApp
{
    public class SenzoProgrammingMode_ThirdPartyLongPress : SenzoProgrammingMode {

        //50ms = 1
        public string SerialNo { get; set; }
        public int TicksPer50ms { get; set; }
        public int KeyPress { get;set; }
        
        public SenzoProgrammingMode_ThirdPartyLongPress () {}

        public override bool Parse ( List <string> commandParams ) {
            //"thirdpartylongpress C0000001 60 13"
            if ( commandParams.Count > 4 ) {
                return false;
            }
            SerialNo = commandParams[1];
            TicksPer50ms = int.Parse (commandParams[2]);
            KeyPress = int.Parse (commandParams[3]);
            return true;
        }

        public override string ToScript () {
            return $"{SenzoScriptParser.CommandList.ThirdPartyLongPress} {SerialNo} {TicksPer50ms} {KeyPress}";
        }

        public override string ToDesc () {
            return "Long Press the Third Party Product";
        }

        public override string ToDescLess () {
            return "Long Press the Third Party Product";
        }

        public override bool Undo () {
            return false;
        }

        public override void Send () {
            throw new System.NotImplementedException ();
        }

        public override SenzoProgrammingMode Clone () {
            throw new System.NotImplementedException ();
        }
        
        public override SenzoProgrammingMode Invert () {
            throw new System.NotImplementedException();
        }

    }
}