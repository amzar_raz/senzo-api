using System;
using System.Collections.Generic;

namespace SenzoProApp
{
    public class SenzoProgrammingMode_NOP : SenzoProgrammingMode
    {
        public SenzoProgrammingMode_NOP () {}

        public override bool Parse ( List <string> commandParams ) {
            return true;
        }

        public override string ToScript() {
            return SenzoScriptParser.CommandList.Nop;
        }

        public override string ToDesc()
        {
            return "No Operation";
        }

        public override string ToDescLess()
        {
            return "No Operation";
        }

        public override bool Undo()
        {
            return false;
        }

        public override void Send () {
            throw new NotImplementedException();
        }

        public override SenzoProgrammingMode Clone () {
            return new SenzoProgrammingMode_NOP ();
        }

        public override SenzoProgrammingMode Invert() {
            return null;
        }
    }
}