using System;
using System.Collections.Generic;

namespace SenzoProApp
{
    public class SenzoProgrammingMode_RunFavorite : SenzoProgrammingMode
    {
        public string SerialNo { get; private set; }
        public int ZoneNo { get; private set; }
        public int FavoriteNo { get; private set; }

        public SenzoProgrammingMode_RunFavorite() { }
        public override bool Parse(List<string> commandParams) {
            if ( commandParams.Count < 4 ) {
                return false;
            }
            ZoneNo = int.Parse ( commandParams [ 1 ] );
            SerialNo = commandParams [ 2 ];
            FavoriteNo = int.Parse ( commandParams [ 3 ] );
            if ( FavoriteNo < 0 || FavoriteNo > 9 ) {
                FavoriteNo = 0;
            }
            return true;
        }

        public override string ToScript() {
            return string.Format ( "{0} {1} {2} {3}" , 
                                    SenzoScriptParser.CommandList.RunFavorite , 
                                    ZoneNo , 
                                    SerialNo ,
                                    FavoriteNo );
        }

        public override string ToDesc() {
            switch ( ZoneNo ) {
                case 0:
                    var smartSwitch = SenzoSmartSwitch.FindSmartSwitchBySerialNo ( SerialNo );
                    return smartSwitch == null
                                   ? string.Format ( "Run Favorite {0} of the Smart Switch" , FavoriteNo )
                                   : string.Format("Run Favorite {0} of the Smart Switch"/* at {1} on {2}*/, FavoriteNo, smartSwitch.LocationName, smartSwitch.FloorName);
                case 255:
                    return string.Format ( "Run Favorite {0} of all Smart Switches" , FavoriteNo );
                default:
                    return string.Format ( "Run Favorite {0} of all Smart Switches in Zone {1}" , FavoriteNo , ZoneNo );
            }
        }

        public override string ToDescLess () {
            switch ( ZoneNo ) {
                case 0:
                    var smartSwitch = SenzoSmartSwitch.FindSmartSwitchBySerialNo ( SerialNo );
                    return smartSwitch == null
                                   ? string.Format ( "Run Favorite {0} of the Smart Switch" , FavoriteNo )
                                   : string.Format("Run Favorite {0} of the Smart Switch"/* at {1} on {2}*/, FavoriteNo, smartSwitch.LocationName, smartSwitch.FloorName);
                case 255:
                    return string.Format ( "Run Favorite {0} of all Smart Switches" , FavoriteNo );
                default:
                    return string.Format ( "Run Favorite {0} of all Smart Switches in Zone {1}" , FavoriteNo , ZoneNo );
            }
        }

        public override bool Undo() {
            return false;
        }

        public override void Send () {
            //PCK.SingleCommand ReplyObj;
            //string Pin = "0000";
            //Global.RFSend.
        }

        public override SenzoProgrammingMode Clone() {
            throw new NotImplementedException ();
        }

        public override SenzoProgrammingMode Invert() {
            return null;
        }
    }
}