using System;
using System.Collections.Generic;

namespace SenzoProApp
{
    public class SenzoProgrammingMode_MR_SelfDelay : SenzoProgrammingMode
    {
        public int Duration { get; private set; }
        public SenzoProgrammingMode_MR_SelfDelay() { }
        public override bool Parse(List<string> commandParams) {
            if ( commandParams.Count < 2 ) {
                return false;
            }
            Duration = int.Parse ( commandParams [ 1 ] );
            return true;
        }

        public override string ToScript() {
            return string.Format ( "{0} {1}" , SenzoScriptParser.CommandList.Delay , Duration );
        }

        public override string ToDesc () {
            var seconds = Duration / 1000M;
            return string.Format ( "Delay {0} second(s)" , seconds.ToString ( "f1" ) );
        }

        public override string ToDescLess () {
            var seconds = Duration / 1000M;
            return string.Format ( "Delay {0} second(s)" , seconds.ToString ( "f1" ) );
        }

        public override bool Undo() {
            return false;
        }

        public override void Send () {
            throw new NotImplementedException();
        }

        public override SenzoProgrammingMode Clone() {
            throw new NotImplementedException ();
        }

        public override SenzoProgrammingMode Invert() {
            return null;
        }
    }
}