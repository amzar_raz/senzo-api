﻿using System;
using System.Collections.Generic;

namespace SenzoProApp {
    public class SenzoProgrammingMode_PR_RepeatSendCurtainButtonPress : SenzoProgrammingMode {
        public int RepeatCount { get; private set; }
        public SenzoProgrammingMode_PR_RepeatSendCurtainButtonPress() { }

        public override bool Parse(List<string> commandParams) {
            if (commandParams.Count < 2) {
                return false;
            }
            RepeatCount = int.Parse(commandParams[1]);
            return true;
        }

        public override string ToScript() {
            return string.Format("{0} {1}", SenzoScriptCurtainParser.CommandList.PR_RepeatSendButtonPress, RepeatCount);
        }

        public override string ToDesc() {
            return string.Format("Repeat Send MR_ButtonPress for {0} times ", RepeatCount);
        }

        public override string ToDescLess() {
            return string.Format("Repeat Send MR_ButtonPress for {0} times ", RepeatCount);
        }

        public override bool Undo() {
            return false;
        }

        public override void Send() {
            throw new NotImplementedException();
        }

        public override SenzoProgrammingMode Clone() {
            throw new NotImplementedException();
        }

        public override SenzoProgrammingMode Invert() {
            return null;
        }
    }
}