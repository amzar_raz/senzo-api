using System;
using System.Collections.Generic;
using System.Globalization;

namespace SenzoProApp
{
    public class SenzoProgrammingMode_MR_SetCarrier : SenzoProgrammingMode
    {
        public int RFChannel { get; private set; }
        public bool OnOff { get; private set; }

        public SenzoProgrammingMode_MR_SetCarrier() { }

        public override bool Parse ( List <string> commandParams ) {
            if ( commandParams.Count < 2 ) {
                return false;
            }
            OnOff = Convert.ToBoolean ( int.Parse ( commandParams [ 1 ] ) );
            if ( OnOff ) {
                if ( commandParams.Count >= 3 ) {
                    RFChannel = int.Parse ( commandParams [ 2 ] );
                }
            }
            return true;
        }

        public override string ToScript() {
            return string.Format ( "{0} {1} {2}" ,
                                   SenzoScriptParser.CommandList.PR_SetCarrier ,
                                   Convert.ToInt32 ( OnOff ) ,
                                   OnOff ? RFChannel.ToString ( CultureInfo.InvariantCulture ) : string.Empty );
        }

        public override string ToDesc() {
            return OnOff
                           ? string.Format ( "Set Carrier on at RFChannel {0}" , RFChannel )
                           : "Set Carrier off";
        }

        public override string ToDescLess () {
            return OnOff
                           ? string.Format ( "Set Carrier on at RFChannel {0}" , RFChannel )
                           : "Set Carrier off";
        }

        public override bool Undo() {
            return false;
        }

        public override void Send () {
            throw new NotImplementedException();
        }

        public override SenzoProgrammingMode Clone() {
            throw new NotImplementedException ();
        }

        public override SenzoProgrammingMode Invert() {
            return null;
        }
    }
}