﻿using System;
using System.Collections.Generic;

namespace SenzoProApp {
    public class SenzoProgrammingMode_CurtainBeep : SenzoProgrammingMode {
        public string SerialNo { get; set; }
        public int Seconds { get; set; }

        public SenzoProgrammingMode_CurtainBeep() { }
        public override bool Parse(List<string> commandParams) {
            if (commandParams.Count < 3) {
                return false;
            }
            SerialNo = commandParams[1];
            Seconds = int.Parse(commandParams[2]);
            return true;
        }

        public override string ToScript() {
            return string.Format("{0} {1} {2}", SenzoScriptCurtainParser.CommandList.Beep, SerialNo, Seconds);
        }

        public override string ToDesc() {
            return string.Format("Beep Smart Switch for {0} seconds.", Seconds);
        }

        public override string ToDescLess() {
            return string.Format("Beep Smart Switch for {0} seconds.", Seconds);
        }

        public override bool Undo() {
            return false;
        }

        public override void Send() {
            throw new NotImplementedException();
        }

        public override SenzoProgrammingMode Clone() {
            return new SenzoProgrammingMode_Beep {
                SerialNo = SerialNo,
                Seconds = Seconds
            };
        }

        public override SenzoProgrammingMode Invert() {
            var command = Clone() as SenzoProgrammingMode_Beep;
            if (command == null) {
                return null;
            }
            command.Seconds = command.Seconds == 0 ? 3 : 0;
            return command;
        }
    }
}