using System;
using System.Collections.Generic;

namespace SenzoProApp
{
    public class SenzoProgrammingMode_MPUSendInfrared : SenzoProgrammingMode
    {
        public string SerialNo { get; private set; }
        public int SlotNo { get; private set; }
        public int PackagesToSend { get; private set; }
        public bool BeepMPU { get; private set; }

        public SenzoProgrammingMode_MPUSendInfrared() { }
        public override bool Parse(List<string> commandParams)
        {
            if ( commandParams.Count < 5 ) {
                return false;
            }
            SerialNo = commandParams [ 1 ];
            SlotNo = int.Parse ( commandParams [ 2 ] );
            PackagesToSend = int.Parse ( commandParams [ 3 ] );
            BeepMPU = Convert.ToBoolean ( int.Parse ( commandParams [ 4 ] ) );
            return true;
        }

        public override string ToScript() {
            return string.Format ( "{0} {1} {2} {3} {4}" ,
                                   SenzoScriptParser.CommandList.MPUSendInfrared ,
                                   SerialNo ,
                                   SlotNo ,
                                   PackagesToSend ,
                                   Convert.ToInt32 ( BeepMPU ) );
        }

        public override string ToDesc() {
            return string.Format ( "Send Infrared Slot {0} from unknown MPU", SlotNo );
        }

        public override string ToDescLess() {
            return string.Format ( "Send Infrared Slot {0} from unknown MPU" , SlotNo );
        }
        
        public override bool Undo() {
            return false;
        }

        public override void Send() {
            throw new NotImplementedException ();
        }

        public override SenzoProgrammingMode Clone() {
            throw new NotImplementedException ();
        }

        public override SenzoProgrammingMode Invert() {
            return null;
        }
    }
}