using System;
using System.Collections.Generic;

namespace SenzoProApp
{
    public class SenzoProgrammingMode_AwayArm : SenzoProgrammingMode
    {
        public SenzoProgrammingMode_AwayArm() { }
        public string EncryptedPinNumber { get; private set; }
        public string PinNumber {
            get {
                return SenzoScriptParser.DecryptPin(EncryptedPinNumber);
            }
            set { EncryptedPinNumber = SenzoScriptParser.EncryptPin ( value );
            }
        }
        public override bool Parse ( List <string> commandParams ) {
            EncryptedPinNumber = commandParams.Count > 1
                                         ? commandParams [ 1 ]
                                         : SenzoScriptParser.EncryptPin ( "0000" );
            return true;
        }

        public override string ToScript() {
            return string.Format ( "{0} {1}", SenzoScriptParser.CommandList.AwayArm, EncryptedPinNumber );
        }

        public override string ToDesc() {
            return "Away Arm the Wireless Security System";
        }

        public override string ToDescLess() {
            return "Away Arm the Wireless Security System";
        }

        public override bool Undo() {
            return false;
        }

        public override bool NeedAlarmPassword () {
            return true;
        }

        public override void Send () {
            throw new NotImplementedException();
        }

        public override SenzoProgrammingMode Clone () {
            throw new NotImplementedException ();
        }

        public override SenzoProgrammingMode Invert() {
            return new SenzoProgrammingMode_Disarm ();
        }
    }
}