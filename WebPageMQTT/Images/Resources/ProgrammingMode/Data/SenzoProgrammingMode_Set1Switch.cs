using System;
using System.Collections.Generic;

namespace SenzoProApp
{
    public class SenzoProgrammingMode_Set1Switch : SenzoProgrammingMode
    {
        public string SerialNo { get; set; }
        public int SwitchNo { get; set; }
        public int Firelevel { get; set; }

        public SenzoProgrammingMode_Set1Switch() { }
        public override bool Parse(List<string> commandParams) {
            if ( commandParams.Count < 4 ) {
                return false;
            }
            SerialNo = commandParams [ 1 ];
            SwitchNo = int.Parse ( commandParams [ 2 ] );
            Firelevel = int.Parse ( commandParams [ 3 ] );
            return true;
        }

        public override string ToScript() {
            return string.Format ( "{0} {1} {2} {3}" ,
                                   SenzoScriptParser.CommandList.Set1Switch ,
                                   SerialNo ,
                                   SwitchNo ,
                                   Firelevel );
        }

        public override string ToDesc() {
            var smartSwitch = SenzoSmartSwitch.FindSmartSwitchBySerialNo ( SerialNo );
            if ( smartSwitch == null ) {
                return string.Format ( "Turn Switch {0} of Serial Number {1} to {2} %" , SwitchNo , SerialNo , Firelevel );
            }
            var smartButton = smartSwitch.SwitchList [ SwitchNo - 1 ];
            if ( smartButton == null ) {
                return string.Format ( "Turn Switch {0} of Serial Number {1} to {2} %" , SwitchNo , SerialNo , Firelevel );
            }

            switch ( smartButton.SwitchMode ) {
                case 4:
                    return string.Format("Autogate ON {0} (Channel {3}, Zone {4})",/*{1}, {2}, ...Channel*/
                                           smartButton.Name ,
                                           smartButton.FloorName ,
                                           smartButton.LocationName ,
                                           smartButton.ChannelNo ,
                                           smartButton.ZoneNo );
                case 0:
                case 128:
                    var switchOnOffDesc = Firelevel > 0 ? "on" : "off";
                    return string.Format("Turn {0} {1} (Channel {4}, Zone {5})",/*{1}, {2}, ...Channel*/
                                           switchOnOffDesc ,
                                           smartButton.Name ,
                                           smartButton.FloorName ,
                                           smartButton.LocationName ,
                                           smartButton.ChannelNo ,
                                           smartButton.ZoneNo );
                case 1:
                    var dimmerOnOffDesc = Firelevel > 0 ? "on" : "off";
                    return string.Format("Dimmer {0} to {1} % brightness {2} (Channel {5}, Zone {6})",/*{1}, {2}, ...Channel*/
                                           dimmerOnOffDesc ,
                                           Firelevel ,
                                           smartButton.Name ,
                                           smartButton.FloorName ,
                                           smartButton.LocationName ,
                                           smartButton.ChannelNo ,
                                           smartButton.ZoneNo );
                case 2:
                    return string.Format("Turn {0} to speed {1} (Channel {4}, Zone {5})",/*{1}, {2}, ...Channel*/
                                           smartButton.Name , 
                                           Firelevel , 
                                           smartButton.FloorName ,
                                           smartButton.LocationName , 
                                           smartButton.ChannelNo , 
                                           smartButton.ZoneNo );
            }

            return "Unknown switch mode.";
        }

        public override string ToDescLess() {
            var smartSwitch = SenzoSmartSwitch.FindSmartSwitchBySerialNo(SerialNo);
            if ( smartSwitch == null ) {
                return string.Format ( "Turn Switch {0} of Serial Number {1} to {2} %" , SwitchNo , SerialNo , Firelevel );
            }
            var smartButton = smartSwitch.SwitchList[SwitchNo - 1];
            if (smartButton == null)
            {
                return string.Format("Turn Switch {0} of Serial Number {1} to {2} %", SwitchNo, SerialNo, Firelevel);
            }

            switch (smartButton.SwitchMode)
            {
                case 4:
                    return string.Format("Autogate ON {0} (Channel {3}, Zone {4})",/*{1}, {2}, ...Channel*/
                                           smartButton.Name,
                                           smartButton.FloorName,
                                           smartButton.LocationName,
                                           smartButton.ChannelNo,
                                           smartButton.ZoneNo);
                case 0:
                case 128:
                    var switchOnOffDesc = Firelevel > 0 ? "on" : "off";
                    return string.Format("Turn {0} {1} (Channel {4}, Zone {5})",/*{1}, {2}, ...Channel*/
                                           switchOnOffDesc,
                                           smartButton.Name,
                                           smartButton.FloorName,
                                           smartButton.LocationName,
                                           smartButton.ChannelNo,
                                           smartButton.ZoneNo);
                case 1:
                    var dimmerOnOffDesc = Firelevel > 0 ? "on" : "off";
                    return string.Format("Dimmer {0} to {1} % brightness {2} (Channel {5}, Zone {6})",/*{1}, {2}, ...Channel*/
                                           dimmerOnOffDesc,
                                           Firelevel,
                                           smartButton.Name,
                                           smartButton.FloorName,
                                           smartButton.LocationName,
                                           smartButton.ChannelNo,
                                           smartButton.ZoneNo);
                case 2:
                    return string.Format("Turn {0} to speed {1} (Channel {4}, Zone {5})",/*{1}, {2}, ...Channel*/
                                           smartButton.Name,
                                           Firelevel,
                                           smartButton.FloorName,
                                           smartButton.LocationName,
                                           smartButton.ChannelNo,
                                           smartButton.ZoneNo);
            }

            return "Unknown switch mode.";
        }

        public override bool Undo() {
            return false;
        }

        public override void Send () {
            throw new NotImplementedException();
        }

        public override SenzoProgrammingMode Clone() {
            return new SenzoProgrammingMode_Set1Switch {
                    SerialNo = SerialNo ,
                    SwitchNo = SwitchNo ,
                    Firelevel = Firelevel
            };
        }

        public override SenzoProgrammingMode Invert() {
            var command = Clone () as SenzoProgrammingMode_Set1Switch;
            if ( command == null ) {
                return null;
            }
            command.Firelevel = Firelevel > 0 ? 0 : 100;
            return command;
        }
    }
}