using System;
using System.Collections.Generic;

namespace SenzoProApp
{
    public class SenzoProgrammingMode_HomeArm : SenzoProgrammingMode
    {
        public string EncryptedPinNumber { get; private set; }
        public string PinNumber {
            get {
                return SenzoScriptParser.DecryptPin(EncryptedPinNumber);
            }
            set { EncryptedPinNumber = SenzoScriptParser.EncryptPin ( value );
            }
        }
        public SenzoProgrammingMode_HomeArm() { }
        public override bool Parse(List<string> commandParams) {
            EncryptedPinNumber = commandParams.Count > 1
                                         ? commandParams [ 1 ]
                                         : SenzoScriptParser.EncryptPin("0000");
            return true;
        }

        public override string ToScript() {
            return string.Format ( "{0} {1}" , SenzoScriptParser.CommandList.HomeArm , EncryptedPinNumber );
        }

        public override string ToDesc() {
            return "Home Arm the Wireless Security System";
        }

        public override string ToDescLess() {
            return "Home Arm the Wireless Security System";
        }

        public override bool Undo() {
            return false;
        }

        public override void Send () {
            throw new NotImplementedException ();
        }

        public override SenzoProgrammingMode Clone() {
            throw new NotImplementedException ();
        }

        public override SenzoProgrammingMode Invert() {
            return new SenzoProgrammingMode_Disarm ();
        }

        public override bool NeedAlarmPassword() {
            return true;
        }
    }
}