﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace SenzoProApp {
    /// <summary>
    /// This Command uses the 0x93 toggle all firelevel command.
    /// The data byte is: TotalSwitch, Switch 1, Switch 2, Switch 3, Switch 4
    /// Switch1 to Switch4: 1 Means On, 2 Means Off, others means ignore
    /// </summary>
    public class SenzoProgrammingMode_ToggleAllCurtains : SenzoProgrammingMode {
        public string SerialNo { get; set; }
        public int TotalSwitch { get; set; }
        public List<int> FirelevelStates { get; set; }


        public SenzoProgrammingMode_ToggleAllCurtains() { }
        public override bool Parse(List<string> commandParams) {
            if (commandParams.Count < 4) {
                return false;
            }
            SerialNo = commandParams[1];
            TotalSwitch = int.Parse(commandParams[2]);
            FirelevelStates = new List<int>();
            FirelevelStates.Clear();
            for (var i = 0; i < TotalSwitch; i++) {
                FirelevelStates.Add(-1);
                if (commandParams.Count <= (3 + i)) {
                    FirelevelStates[i] = 0;
                }
                else {
                    try {
                        FirelevelStates[i] = int.Parse(commandParams[3 + i]);
                    }
                    catch {
                        FirelevelStates[i] = -1;
                    }

                    if (FirelevelStates[i] == -1) {
                        try {
                            FirelevelStates[i] = int.Parse(commandParams[3 + i], NumberStyles.AllowHexSpecifier);
                        }
                        catch {
                            FirelevelStates[i] = -1;
                        }
                    }
                }
            }
            return true;
        }

        public override string ToScript() {
            var command = string.Format("{0} {1} {2}", SenzoScriptParser.CommandList.ToggleAllSwitches, SerialNo, TotalSwitch);
            for (var i = 0; i < TotalSwitch; i++) {
                command += string.Format(" {0}", FirelevelStates[i]);
            }
            return command;
        }

        public override string ToDesc() {
            var onSwitchDesc = string.Empty;
            var offSwitchDesc = string.Empty;
            var dimmerSwitchDesc = string.Empty;
            var noChangeSwitchDesc = string.Empty;
            for (var i = 0; i < TotalSwitch; i++) {
                switch (FirelevelStates[i]) {
                    case 1:
                        onSwitchDesc = string.IsNullOrEmpty(onSwitchDesc)
                                               ? (i + 1).ToString()
                                               : string.Format("{0}, {1}", onSwitchDesc, (i + 1));
                        break;
                    case 2:
                        offSwitchDesc = string.IsNullOrEmpty(offSwitchDesc)
                                                ? (i + 1).ToString()
                                                : string.Format("{0}, {1}", offSwitchDesc, (i + 1));
                        break;
                    case 255:
                        noChangeSwitchDesc = string.IsNullOrEmpty(noChangeSwitchDesc)
                                                     ? (i + 1).ToString()
                                                     : string.Format("{0}, {1}", noChangeSwitchDesc, (i + 1));
                        break;
                }
            }

            var commandDesc = string.Empty;

            var smartSwitch = SenzoSmartCurtain.FindSmartSwitchBySerialNo(SerialNo);
            if (smartSwitch == null) {
                commandDesc = string.Format("Toggle Smart Switch for");
                if (string.IsNullOrEmpty(noChangeSwitchDesc)) {
                    commandDesc = string.Format("Switch: {0} Remain Unchanged", noChangeSwitchDesc);
                }
                else {
                    if (!string.IsNullOrEmpty(onSwitchDesc)) {
                        commandDesc = string.Format("Switch : {0} to On", onSwitchDesc);
                    }
                    if (!string.IsNullOrEmpty(offSwitchDesc)) {
                        commandDesc = string.Format("Switch : {0} to Off", offSwitchDesc);
                    }
                }
                return string.Format("{0} (!)", commandDesc);
            }

            var switchList = smartSwitch.CurtainList;
            if (switchList[0] == null) {
                commandDesc = string.Format("Toggle Smart Switch of Serial Number {0} for", smartSwitch.UserSerialNo);
                if (string.IsNullOrEmpty(noChangeSwitchDesc)) {
                    commandDesc = string.Format("Switch: {0} Remain Unchanged", noChangeSwitchDesc);
                }
                else {
                    if (!string.IsNullOrEmpty(onSwitchDesc)) {
                        commandDesc = string.Format("Switch : {0} to On", onSwitchDesc);
                    }
                    if (!string.IsNullOrEmpty(offSwitchDesc)) {
                        commandDesc = string.Format("Switch : {0} to Off", offSwitchDesc);
                    }
                }
                return commandDesc;
            }

            for (var i = 0; i < TotalSwitch; i++) {
                if (!string.IsNullOrEmpty(commandDesc)) {
                    commandDesc += ", ";
                }
                switch (FirelevelStates[i]) {
                    case 1:
                        onSwitchDesc = "on";
                        commandDesc += string.Format("Toggle {0} {1} (Channel {4}, Zone {5})",/*{1}, {2}, ...Channel*/
                                                       onSwitchDesc,
                                                       switchList[i].Name,
                                                       switchList[i].FloorName,
                                                       switchList[i].LocationName,
                                                       switchList[i].ChannelNo,
                                                       switchList[i].ZoneNo);
                        break;
                    case 2:
                        offSwitchDesc = "off";
                        commandDesc += string.Format("Toggle {0} {1} (Channel {4}, Zone {5})",/*{1}, {2}, ...Channel*/
                                                       offSwitchDesc,
                                                       switchList[i].Name,
                                                       switchList[i].FloorName,
                                                       switchList[i].LocationName,
                                                       switchList[i].ChannelNo,
                                                       switchList[i].ZoneNo);
                        break;
                    case 255:
                        commandDesc += string.Format("Ignore {0} (Channel {3}, Zone {4})", /*{1}, {2}, ...Channel*/
                                                             switchList[i].Name,
                                                             switchList[i].FloorName,
                                                             switchList[i].LocationName,
                                                             switchList[i].ChannelNo,
                                                             switchList[i].ZoneNo);
                        break;
                    default:
                        commandDesc += string.Format("Unknown Toggle Mode for Switch {0}({1}).", (i + 1), switchList[i].Name);
                        break;
                }
            }
            return commandDesc;
        }

        public override string ToDescLess() {
            var onSwitchDesc = string.Empty;
            var offSwitchDesc = string.Empty;
            var dimmerSwitchDesc = string.Empty;
            var noChangeSwitchDesc = string.Empty;
            for (var i = 0; i < TotalSwitch; i++) {
                switch (FirelevelStates[i]) {
                    case 1:
                        onSwitchDesc = string.IsNullOrEmpty(onSwitchDesc)
                                               ? (i + 1).ToString()
                                               : string.Format("{0}, {1}", onSwitchDesc, (i + 1));
                        break;
                    case 2:
                        offSwitchDesc = string.IsNullOrEmpty(offSwitchDesc)
                                                ? (i + 1).ToString()
                                                : string.Format("{0}, {1}", offSwitchDesc, (i + 1));
                        break;
                    case 255:
                        noChangeSwitchDesc = string.IsNullOrEmpty(noChangeSwitchDesc)
                                                     ? (i + 1).ToString()
                                                     : string.Format("{0}, {1}", noChangeSwitchDesc, (i + 1));
                        break;
                }
            }

            var commandDesc = string.Empty;

            var smartSwitch = SenzoSmartCurtain.FindSmartSwitchBySerialNo(SerialNo);
            if (smartSwitch == null) {
                commandDesc = string.Format("Toggle Smart Switch for");
                if (string.IsNullOrEmpty(noChangeSwitchDesc)) {
                    commandDesc = string.Format("Switch: {0} Remain Unchanged", noChangeSwitchDesc);
                }
                else {
                    if (!string.IsNullOrEmpty(onSwitchDesc)) {
                        commandDesc = string.Format("Switch : {0} to On", onSwitchDesc);
                    }
                    if (!string.IsNullOrEmpty(offSwitchDesc)) {
                        commandDesc = string.Format("Switch : {0} to Off", offSwitchDesc);
                    }
                }
                return string.Format("{0} (!)", commandDesc);
            }

            var switchList = smartSwitch.CurtainList;
            if (switchList[0] == null) {
                commandDesc = string.Format("Toggle Smart Switch of Serial Number {0} for", smartSwitch.UserSerialNo);
                if (string.IsNullOrEmpty(noChangeSwitchDesc)) {
                    commandDesc = string.Format("Switch: {0} Remain Unchanged", noChangeSwitchDesc);
                }
                else {
                    if (!string.IsNullOrEmpty(onSwitchDesc)) {
                        commandDesc = string.Format("Switch : {0} to On", onSwitchDesc);
                    }
                    if (!string.IsNullOrEmpty(offSwitchDesc)) {
                        commandDesc = string.Format("Switch : {0} to Off", offSwitchDesc);
                    }
                }
                return commandDesc;
            }

            for (var i = 0; i < TotalSwitch; i++) {
                if (!string.IsNullOrEmpty(commandDesc)) {
                    commandDesc += ", ";
                }
                switch (FirelevelStates[i]) {
                    case 1:
                        onSwitchDesc = "on";
                        commandDesc += string.Format("Toggle {0} {1} (Channel {4}, Zone {5})", /*{1}, {2}, ...Channel*/
                                                       onSwitchDesc,
                                                       switchList[i].Name,
                                                       switchList[i].FloorName,
                                                       switchList[i].LocationName,
                                                       switchList[i].ChannelNo,
                                                       switchList[i].ZoneNo);
                        break;
                    case 2:
                        offSwitchDesc = "off";
                        commandDesc += string.Format("Toggle {0} {1} (Channel {4}, Zone {5})",/*{1}, {2}, ...Channel*/
                                                       offSwitchDesc,
                                                       switchList[i].Name,
                                                       switchList[i].FloorName,
                                                       switchList[i].LocationName,
                                                       switchList[i].ChannelNo,
                                                       switchList[i].ZoneNo);
                        break;
                    case 255:
                        commandDesc += string.Format("Ignore {0} (Channel {3}, Zone {4})", /*{1}, {2}, ...Channel*/
                                                             switchList[i].Name,
                                                             switchList[i].FloorName,
                                                             switchList[i].LocationName,
                                                             switchList[i].ChannelNo,
                                                             switchList[i].ZoneNo);
                        break;
                    default:
                        commandDesc += string.Format("Unknown Toggle Mode for Switch {0}({1}).", (i + 1), switchList[i].Name);
                        break;
                }
            }
            return commandDesc;
        }

        public override bool Undo() {
            return false;
        }

        public override void Send() {
            throw new NotImplementedException();
        }

        public override SenzoProgrammingMode Clone() {
            return new SenzoProgrammingMode_ToggleAllCurtains {
                SerialNo = SerialNo,
                TotalSwitch = TotalSwitch,
                FirelevelStates = FirelevelStates
            };
        }

        public override SenzoProgrammingMode Invert() {
            var command = Clone() as SenzoProgrammingMode_ToggleAllCurtains;
            if (command == null) {
                return null;
            }
            for (var i = 0; i < TotalSwitch; i++) {
                switch (FirelevelStates[i]) {
                    case 1:
                        command.FirelevelStates[i] = 2;
                        break;
                    case 2:
                        command.FirelevelStates[i] = 1;
                        break;
                    default:
                        command.FirelevelStates[i] = 255;
                        break;
                }
            }
            return command;
        }
    }
}