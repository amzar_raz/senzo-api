using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace SenzoProApp
{
    public class SenzoProgrammingModeTemplate : SenzoProgrammingMode
    {

        public override bool Parse ( List <string> commandParams ) {
            return true;
        }

        public override string ToScript () {
            return "TODO: ToScript() - TEMPLATE";
        }

        public override string ToDesc () {
            return "TODO: ToDesc() - TEMPLATE";
        }

        public override string ToDescLess () {
            return "TODO: ToDescLess() - TEMPLATE";
        }

        public override bool Undo () {
            return true;
        }

        public override void Send () {}

        public override SenzoProgrammingMode Clone () {
            return this;
        }

        public override SenzoProgrammingMode Invert () {
            return this;
        }
    }
}