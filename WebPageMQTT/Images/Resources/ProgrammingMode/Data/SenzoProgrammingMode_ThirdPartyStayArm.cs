using System;
using System.Collections.Generic;

namespace SenzoProApp
{
    public class SenzoProgrammingMode_ThirdPartyStayArm : SenzoProgrammingMode
    {
        public string EncryptedPinNumber { get; private set; }
        public int Partition { get; set; }
        public string PinNumber {
            get {
                return SenzoScriptParser.DecryptPin(EncryptedPinNumber);
            }
            set { EncryptedPinNumber = SenzoScriptParser.EncryptPin ( value );
            }
        }

        public SenzoProgrammingMode_ThirdPartyStayArm() { }
        public override bool Parse(List<string> commandParams)
        {
            if ( commandParams.Count < 2 ) {
                SenzoScriptParser.EncryptPin ( "0000" );
            }
            else if ( commandParams.Count > 2 ) {
                Partition = int.Parse ( commandParams [ 2 ] );
            }
            return true;
        }

        public override string ToScript()
        {
            return string.Format("{0} {1} {2}", SenzoScriptParser.CommandList.ThirdPartyStayArm, EncryptedPinNumber, Partition);
        }

        public override string ToDesc() {
            return string.Format ( "Stay Arm the Security System " + "(Partition {0})", Partition );
        }

        public override string ToDescLess () {
            return string.Format ( "Stay Arm the Security System " + "(Partition {0})" , Partition );
        }

        public override bool Undo () {
            return false;
        }

        public override void Send () {
            throw new NotImplementedException ();
        }
        
        public override bool NeedAlarmPassword() {
            return true;
        }
        
        public override SenzoProgrammingMode Clone() {
            throw new NotImplementedException ();
        }
        
        public override SenzoProgrammingMode Invert() {
            return new SenzoProgrammingMode_ThirdPartyDisarm {
                    Partition = Partition
            };
        }
    }
}