﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using NLibraryForms;
using Plugin.NewictLib;


namespace SenzoProApp
{
    
    [DefaultValue(None)]
    public enum ESmartAlarmArmStatus
    {

        None = -1,
        HomeArm = 13,
        AwayArm = 14,
        Disarm = 10,
        Panic = 15,
        Partition1Arm = 1,
        Partition2Arm = 2,
        PartitionBoth = 0
            
    }
    public class SenzoSmartAlarmScriptItem : SenzoScriptItem
    {

        private ESmartAlarmArmStatus alarmArmStatus = ESmartAlarmArmStatus.None;

        public SenzoSmartAlarm SmartAlarm
        {
            get { return SenzoDevice as SenzoSmartAlarm; }
            set { SenzoDevice = value; }
        }

        public override string Label
        {
            get {
                if ( string.IsNullOrEmpty (label) ) {
                    label = $"{SenzoDevice.Name} ({SenzoDevice.ModelName})";
                }
                return label;
            }
            set {
                if ( label == value ) {
                    return;
                }
                label = value;
                OnPropertyChanged ();
            }
        }

        public string Status
        {
            get
            {
                switch ( AlarmArmStatus ) {

                    case ESmartAlarmArmStatus.None:
                        return $"Action: None";
                    case ESmartAlarmArmStatus.HomeArm:
                        return $"Action: Home Arm";
                    case ESmartAlarmArmStatus.AwayArm:
                        return $"Action: Away Arm";
                    case ESmartAlarmArmStatus.Disarm:
                        return $"Action: Disarm";
                    case ESmartAlarmArmStatus.Panic:
                        return $"Action: Panic";
                    case ESmartAlarmArmStatus.Partition1Arm:
                        return $"Action: Partition Arm 1";
                    case ESmartAlarmArmStatus.Partition2Arm:
                        return $"Action: Partition Arm 2";
                    case ESmartAlarmArmStatus.PartitionBoth:
                        return $"Action: Partition Arm 1 and 2";
                    default :
                        throw new ArgumentOutOfRangeException ();
                }
            }
        }

//        public int TicksPer50ms { get; set; }
//
//        public int KeyPress { get; set; }
//
        public List <int> KeySeqAction { get; set; }

        public ESmartAlarmArmStatus AlarmArmStatus
        {
            get { return alarmArmStatus; }
            set {
                switch ( value ) {

                    case ESmartAlarmArmStatus.None :
                        CrossLog.WriteLine ("Breakpoint None");
                        break;
                    case ESmartAlarmArmStatus.HomeArm :
                        CrossLog.WriteLine ("Breakpoint HomeArm");
                        break;
                    case ESmartAlarmArmStatus.AwayArm :
                        CrossLog.WriteLine ("Breakpoint AwayArm");
                        break;
                    case ESmartAlarmArmStatus.Disarm :
                        CrossLog.WriteLine ("Breakpoint Disarm");
                        break;
                    case ESmartAlarmArmStatus.Panic:
                        CrossLog.WriteLine("Breakpoint Panic");
                        break;
                    case ESmartAlarmArmStatus.Partition1Arm :
                        CrossLog.WriteLine ("Breakpoint Partition1Arm");
                        break;
                    case ESmartAlarmArmStatus.Partition2Arm :
                        CrossLog.WriteLine ("Breakpoint Partition2Arm");
                        break;
                    case ESmartAlarmArmStatus.PartitionBoth :
                        CrossLog.WriteLine ("Breakpoint PartitionBoth");
                        break;
                    default :
                        throw new ArgumentOutOfRangeException (nameof (value), value, null);
                }
                alarmArmStatus = value;
                OnPropertyChanged ();
                OnPropertyChanged (nameof (Status));
            }
        }

        public SenzoSmartAlarmScriptItem ( SenzoDevice senzoDevice, IEnumerable <SenzoProgrammingMode> commands ) : base (senzoDevice, commands) {
            KeySeqAction = new List <int> ();
        }

        public override void UpdateCommands () {
            base.UpdateCommands ();
            UpdateDeviceFromCommandsByCategory <SenzoProgrammingMode_ThirdPartyPartitionArm> ();
            UpdateDeviceFromCommandsByCategory <SenzoProgrammingMode_ThirdPartyLongPress> ();
            UpdateDeviceFromCommandsByCategory <SenzoProgrammingMode_ThirdPartyDisarmPress> ();
        }

        protected override void UpdateDeviceFromCommandsByCategory <T> () {

            Predicate <SenzoProgrammingMode> condition = item => item is T;
            var commands = Commands.ToList ().FindAll (condition);
            if ( commands == null || !commands.Any () ) {
                return;
            }

            foreach ( var command in commands ) {
                if ( command == null ) {
                    continue;
                }
                if ( command is SenzoProgrammingMode_ThirdPartyPartitionArm ) {

                    if ( SmartAlarm.SerialNo == ( (SenzoProgrammingMode_ThirdPartyPartitionArm) command ).SerialNo ) {
                        //KeySeqAction = ( (SenzoProgrammingMode_ThirdPartyPartitionArm) command ).KeySeqAction;
                        KeySeqAction = ( (SenzoProgrammingMode_ThirdPartyPartitionArm) command ).KeySeqAction.GetRange (0, ( (SenzoProgrammingMode_ThirdPartyPartitionArm) command ).KeySeqAction.Count - 1);
                        AlarmArmStatus = (ESmartAlarmArmStatus) ( (SenzoProgrammingMode_ThirdPartyPartitionArm) command ).KeySeqAction.Last ();
                        OnPropertyChanged (nameof (Status)); //TODO TEMP

                    }
                    continue;
                }
                if ( command is SenzoProgrammingMode_ThirdPartyLongPress ) {
                    ////
                    if ( SmartAlarm.SerialNo == ( (SenzoProgrammingMode_ThirdPartyLongPress) command ).SerialNo ) {
                        //KeyPress = ( (SenzoProgrammingMode_ThirdPartyLongPress) command ).KeyPress;
                        AlarmArmStatus = (ESmartAlarmArmStatus) ( (SenzoProgrammingMode_ThirdPartyLongPress) command ).KeyPress;

                        //TicksPer50ms = ( (SenzoProgrammingMode_ThirdPartyLongPress) command ).TicksPer50ms;
                        OnPropertyChanged (nameof (Status)); //TODO TEMP
                    }
                    continue;
                }
                if ( command is SenzoProgrammingMode_ThirdPartyDisarmPress ) {
                    if ( SmartAlarm.SerialNo == ( (SenzoProgrammingMode_ThirdPartyDisarmPress) command ).SerialNo ) {
                        KeySeqAction = ( (SenzoProgrammingMode_ThirdPartyDisarmPress) command ).KeySeqAction.GetRange (0, ( (SenzoProgrammingMode_ThirdPartyDisarmPress) command ).KeySeqAction.Count - 1);
                        AlarmArmStatus = (ESmartAlarmArmStatus) ( (SenzoProgrammingMode_ThirdPartyDisarmPress) command ).KeySeqAction.Last ();
                    }
                    OnPropertyChanged (nameof (Status)); //TODO TEMP
                    continue;
                }
            }
        }

    }
}