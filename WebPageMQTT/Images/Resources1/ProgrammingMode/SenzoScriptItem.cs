﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Bson;


namespace SenzoProApp
{
    public abstract class SenzoScriptItem : INotifyPropertyChanged
	{

		private SenzoDevice senzoDevice;
		public SenzoDevice SenzoDevice
		{
			get { return senzoDevice; }
			protected set {
				if (senzoDevice == value ) {
					return;
				}
				senzoDevice = value;
				OnPropertyChanged (  );
				OnPropertyChanged ( "Label" );
			}
		}

		public IEnumerable <SenzoProgrammingMode> Commands { get; set; }

		//public int ScriptDelayIcon { get; set; }

		public bool HasParsedCommands { get; set; }

		protected string label;

		public virtual string Label
		{
			get { return label; }
			set {
				if ( label == value ) {
					return;
				}
				label = value;
				OnPropertyChanged ();
			}
		}

		protected SenzoScriptItem ( SenzoDevice senzoDevice, IEnumerable <SenzoProgrammingMode> commands ) {
			SenzoDevice = senzoDevice;
			Commands = commands;
			HasParsedCommands = false;
		}

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged ( [ CallerMemberName ] string propertyName = null ) {
			PropertyChanged?.Invoke (this, new PropertyChangedEventArgs (propertyName));
		}

		public virtual void UpdateCommands () {
			HasParsedCommands = true;
		}

		protected abstract void UpdateDeviceFromCommandsByCategory <T> () where T : SenzoProgrammingMode;

	}
}
