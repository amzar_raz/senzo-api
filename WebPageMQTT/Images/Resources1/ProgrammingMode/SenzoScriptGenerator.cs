﻿using SenzoProApp.Resources.ProgrammingMode;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace SenzoProApp
{
    public class SenzoScriptGenerator {
        public static string GenerateCommands ( ObservableCollection <SenzoScriptItem> scriptItems ) {
            var output = string.Empty;
            if ( SGlobal.SmartAlarms != null && SGlobal.SmartAlarms.Any () ) {
                var smartAlarmScriptItems = scriptItems.ToList ().Find (item => item is SenzoSmartAlarmScriptItem) as SenzoSmartAlarmScriptItem;
                var smartAlarm = SGlobal.SmartAlarms.First ();
                output += GenerateSmartAlarmCommands (smartAlarm, smartAlarmScriptItems);
            }
            if ( SGlobal.SmartSwitches != null && SGlobal.SmartSwitches.Any () ) {
                ObservableCollection<SenzoSmartSwitchScriptItem> smartSwitchScriptItems = new ObservableCollection <SenzoSmartSwitchScriptItem> (scriptItems.ToList().FindAll(item => item is SenzoSmartSwitchScriptItem).Cast<SenzoSmartSwitchScriptItem>());
//                for ( int i = 0; i < UPPER; i++ ) {
//                    
//                }
                //ObservableCollection<SenzoSmartSwitchScriptItem> smartSwitchScriptItems = scriptItems.ToList ().FindAll (item => item is SenzoSmartSwitchScriptItem).Cast<SenzoSmartSwitchScriptItem> () as ObservableCollection <SenzoSmartSwitchScriptItem>;
                for (var i = 0; i < SGlobal.SmartSwitches.Count; i++)
                {
                    var senzoSwitch = SGlobal.SmartSwitches[i];
                    output += GenerateSmartSwitchCommands(senzoSwitch, smartSwitchScriptItems);
                }   
            }
            if (SGlobal.Smartcurtains != null && SGlobal.Smartcurtains.Any()) {
                ObservableCollection<SenzoSmartCurtainScriptItem> smartCurtainScriptItems = new ObservableCollection<SenzoSmartCurtainScriptItem>(scriptItems.ToList().FindAll(item => item is SenzoSmartCurtainScriptItem).Cast<SenzoSmartCurtainScriptItem>());
                //                for ( int i = 0; i < UPPER; i++ ) {
                //                    
                //                }
                //ObservableCollection<SenzoSmartSwitchScriptItem> smartSwitchScriptItems = scriptItems.ToList ().FindAll (item => item is SenzoSmartSwitchScriptItem).Cast<SenzoSmartSwitchScriptItem> () as ObservableCollection <SenzoSmartSwitchScriptItem>;
                for (var i = 0; i < SGlobal.Smartcurtains.Count; i++) {
                    var senzoCurtain = SGlobal.Smartcurtains[i];
                    output += GenerateSmartCurtainCommands(senzoCurtain, smartCurtainScriptItems);
                }
            }
            if (SGlobal.Smartshutters != null && SGlobal.Smartshutters.Any())
            {
                ObservableCollection<SenzoSmartShutterScriptItem> smartShutterScriptItems = new ObservableCollection<SenzoSmartShutterScriptItem>(scriptItems.ToList().FindAll(item => item is SenzoSmartShutterScriptItem).Cast<SenzoSmartShutterScriptItem>());
                //                for ( int i = 0; i < UPPER; i++ ) {
                //                    
                //                }
                //ObservableCollection<SenzoSmartSwitchScriptItem> smartSwitchScriptItems = scriptItems.ToList ().FindAll (item => item is SenzoSmartSwitchScriptItem).Cast<SenzoSmartSwitchScriptItem> () as ObservableCollection <SenzoSmartSwitchScriptItem>;
                for (var i = 0; i < SGlobal.Smartshutters.Count; i++)
                {
                    var senzoShutter = SGlobal.Smartshutters[i];
                    output += GenerateSmartShutterCommands(senzoShutter, smartShutterScriptItems);
                }
            }
            return $"{output}\n";
        }

        private static string GenerateSmartAlarmCommands ( SenzoSmartAlarm smartAlarm, SenzoSmartAlarmScriptItem smartAlarmScriptItems ) {
            
            /*
             Home Arm ="thirdpartylongpress C0000001 60 13"
Away Arm ="thirdpartylongpress C0000001 60 14"
Partition 1&2 Arm="thirdpartypartitionarm C0000001 5 1 2 3 4 0"
Partition 1 Arm ="thirdpartypartitionarm C0000001 5 1 2 3 4 1"
Partition 2 Arm ="thirdpartypartitionarm C0000001 5 1 2 3 4 2"
Disarm ="thirdpartydisarmpress C0000001 5 1 2 3 4 10"

             */
            var output = string.Empty;
            SenzoProgrammingMode command;
            if ( smartAlarmScriptItems == null ) {
                return output;
            }
            switch (smartAlarmScriptItems.AlarmArmStatus) {

                case ESmartAlarmArmStatus.None :
                    break;
                case ESmartAlarmArmStatus.HomeArm :
                    command = new SenzoProgrammingMode_ThirdPartyLongPress () {
                        SerialNo = smartAlarm.SerialNo,
                        TicksPer50ms = 16,
                        KeyPress = (int)smartAlarmScriptItems.AlarmArmStatus
                    };
                    output += command.ToScript ();
                    break;
                case ESmartAlarmArmStatus.AwayArm :
                    command = new SenzoProgrammingMode_ThirdPartyLongPress () {
                        SerialNo = smartAlarm.SerialNo,
                        TicksPer50ms = 16,
                        KeyPress = (int)smartAlarmScriptItems.AlarmArmStatus
                    };
                    output += command.ToScript ();
                    break;
                case ESmartAlarmArmStatus.Panic:
                    command = new SenzoProgrammingMode_ThirdPartyLongPress () {
                        SerialNo = smartAlarm.SerialNo,
                        TicksPer50ms = 16,
                        KeyPress = (int) smartAlarmScriptItems.AlarmArmStatus
                    };
                    output += command.ToScript ();
                    break;
                case ESmartAlarmArmStatus.Disarm :
                    command = new SenzoProgrammingMode_ThirdPartyDisarmPress () {
                        SerialNo = smartAlarm.SerialNo,
                    };
                    ((SenzoProgrammingMode_ThirdPartyDisarmPress)command).KeySeqAction.AddRange (smartAlarmScriptItems.KeySeqAction);
                    ((SenzoProgrammingMode_ThirdPartyDisarmPress)command).KeySeqAction.Add ((int) smartAlarmScriptItems.AlarmArmStatus);
                    output += command.ToScript ();
                    break;
                case ESmartAlarmArmStatus.Partition1Arm:
                case ESmartAlarmArmStatus.Partition2Arm:
                    command = new SenzoProgrammingMode_ThirdPartyPartitionArm()
                    {
                        SerialNo = smartAlarm.SerialNo,
                    };
                    ((SenzoProgrammingMode_ThirdPartyPartitionArm)command).KeySeqAction.AddRange(smartAlarmScriptItems.KeySeqAction);
                    ((SenzoProgrammingMode_ThirdPartyPartitionArm)command).KeySeqAction.Add((int)smartAlarmScriptItems.AlarmArmStatus);
                    output += command.ToScript();
                    break;
                case ESmartAlarmArmStatus.PartitionBoth:
                    command = new SenzoProgrammingMode_ThirdPartyPartitionArm () {
                        SerialNo = smartAlarm.SerialNo,
                    };
                    ((SenzoProgrammingMode_ThirdPartyPartitionArm)command).KeySeqAction.AddRange(smartAlarmScriptItems.KeySeqAction);
                    ((SenzoProgrammingMode_ThirdPartyPartitionArm)command).KeySeqAction.Add(10);
                    output += command.ToScript ();
                    break;
                default :
                    throw new ArgumentOutOfRangeException ();
            }
            return $"{output}\n";
        }


        private static string GenerateSmartSwitchCommands ( SenzoSmartSwitch senzoSwitch , ObservableCollection <SenzoSmartSwitchScriptItem> scriptItems ) {
            
            
	        var itemList = new ObservableCollection <SenzoSmartSwitchScriptItem> ( scriptItems.ToList ().FindAll ( item => item.SmartSwitch.SerialNo == senzoSwitch.SerialNo ) );

            var cancelCommands = GenerateCancelCommands(senzoSwitch, itemList);
            var alarmCommands = GenerateAlarmCommands(senzoSwitch, itemList);
            var firelevelCommands = GenerateFirelevelCommands(senzoSwitch, itemList);
            var setdelayCommands = GenerateSetDelayCommands(senzoSwitch, itemList);
            var beepCommands = GenerateBeepCommands(senzoSwitch, itemList);

            return $"{cancelCommands}{alarmCommands}{firelevelCommands}{setdelayCommands}{beepCommands}";
        }
        private static string GenerateSmartCurtainCommands(SenzoSmartCurtain curtainSwitch, ObservableCollection<SenzoSmartCurtainScriptItem> scriptItems) {


            var itemList = new ObservableCollection<SenzoSmartCurtainScriptItem>(scriptItems.ToList().FindAll(item => item.CurtainSwitch.SerialNo == curtainSwitch.SerialNo));

            var cancelCommands = GenerateCurtainCancelCommands(curtainSwitch, itemList);
            var alarmCommands = GenerateAlarmCurtainCommands(curtainSwitch, itemList);
            var firelevelCommands = GenerateCurtainFirelevelCommands(curtainSwitch, itemList);
            var setdelayCommands = GenerateSetCurtainDelayCommands(curtainSwitch, itemList);
            var beepCommands = GenerateCurtainBeepCommands(curtainSwitch, itemList);

            return $"{cancelCommands}{alarmCommands}{firelevelCommands}{setdelayCommands}{beepCommands}";
        }
        private static string GenerateSmartShutterCommands(SenzoSmartShutter shutterSwitch, ObservableCollection<SenzoSmartShutterScriptItem> scriptItems)
        {


            var itemList = new ObservableCollection<SenzoSmartShutterScriptItem>(scriptItems.ToList().FindAll(item => item.ShutterSwitch.SerialNo == shutterSwitch.SerialNo));

            var cancelCommands = GenerateShutterCancelCommands(shutterSwitch, itemList);
            var alarmCommands = GenerateAlarmShutterCommands(shutterSwitch, itemList);
            var firelevelCommands = GenerateShutterFirelevelCommands(shutterSwitch, itemList);
            var setdelayCommands = GenerateSetShutterDelayCommands(shutterSwitch, itemList);
            var beepCommands = GenerateShutterBeepCommands(shutterSwitch, itemList);

            return $"{cancelCommands}{alarmCommands}{firelevelCommands}{setdelayCommands}{beepCommands}";
        }
        private static object GenerateCancelCommands ( SenzoSmartSwitch senzoSwitch , ObservableCollection <SenzoSmartSwitchScriptItem> scriptItems ) {
	        var output = string.Empty;

	        foreach ( var item in scriptItems ) {
	            if ( item == null ) {
	                continue;
	            }
	            int switchNo = item.SmartSwitch.SwitchNo;
	            if ( item.IsDelayEnabled != EBooleanStatus.False ) {
	                continue;
	            }
	            var cancelcommand = new SenzoProgrammingMode_CancelSwitchDelayTimer {
	                    SerialNo = item.SmartSwitch.SerialNo ,
	                    SwitchNo = switchNo
	            };
	            output += string.Format ( "{0}\n" , cancelcommand.ToScript () );
	        }
	        return output;
        }
        private static object GenerateCurtainCancelCommands(SenzoSmartCurtain senzoSwitch, ObservableCollection<SenzoSmartCurtainScriptItem> scriptItems) {
            var output = string.Empty;

            foreach (var item in scriptItems) {
                if (item == null) {
                    continue;
                }
                int switchNo = item.CurtainSwitch.SwitchNo;
                if (item.IsDelayEnabled != EBooleanStatus.False) {
                    continue;
                }
                var cancelcommand = new SenzoProgrammingMode_CancelSwitchDelayTimer {
                    SerialNo = item.CurtainSwitch.SerialNo,
                    SwitchNo = switchNo
                };
                output += string.Format("{0}\n", cancelcommand.ToScript());
            }
            return output;
        }
        private static object GenerateShutterCancelCommands(SenzoSmartShutter senzoSwitch, ObservableCollection<SenzoSmartShutterScriptItem> scriptItems)
        {
            var output = string.Empty;

            foreach (var item in scriptItems)
            {
                if (item == null)
                {
                    continue;
                }
                int switchNo = item.ShutterSwitch.SwitchNo;
                if (item.IsDelayEnabled != EBooleanStatus.False)
                {
                    continue;
                }
                var cancelcommand = new SenzoProgrammingMode_CancelSwitchDelayTimer
                {
                    SerialNo = item.ShutterSwitch.SerialNo,
                    SwitchNo = switchNo
                };
                output += string.Format("{0}\n", cancelcommand.ToScript());
            }
            return output;
        }
        private static object GenerateAlarmCommands ( SenzoSmartSwitch senzoSwitch, ObservableCollection <SenzoSmartSwitchScriptItem> scriptItems ) {
            //IF ALARM LIGHT FALSE RETURN EMPTY
            var output = string.Empty;

            var setAlarmLightCommand = new SenzoProgrammingMode_SetAllSwitchAlarmTriggerFirelevel {
                SerialNo = senzoSwitch.SerialNo,
                EnableBeep = false,
                TotalSwitch = senzoSwitch.SwitchList.Count,
                Firelevels = new int[senzoSwitch.SwitchList.Count].ToList ()
            };
            var hasAnyFirelevelAction = false;
            for ( var i = 0; i < scriptItems.Count; i++ ) {
                var item = scriptItems[i];
                if ( item == null ) {
                    continue;
                }
                int firelevel;
                switch ( item.SwitchState ) {
                    default :
                    case EScriptIconState.NoAction :
                        firelevel = 255;
                        break;
                    case EScriptIconState.AlarmLight :
                        switch ( item.SmartSwitch.SwitchMode ) {
                            case 4 :
                                firelevel = 255;
                                break;
                            default :
                                firelevel = 100;
                                hasAnyFirelevelAction = true;
                                break;
                            case 1 :
                                firelevel = item.Firelevel;
                                hasAnyFirelevelAction = true;
                                break;
                        }
                        break;
                }
                setAlarmLightCommand.Firelevels[i] = firelevel;
            }

            if ( hasAnyFirelevelAction ) {
                output = string.Format ("{0}\n", setAlarmLightCommand.ToScript ());
            }
            return output;
        }
        private static object GenerateAlarmCurtainCommands(SenzoSmartCurtain senzoSwitch, ObservableCollection<SenzoSmartCurtainScriptItem> scriptItems) {
            //IF ALARM LIGHT FALSE RETURN EMPTY
            var output = string.Empty;

            var setAlarmLightCommand = new SenzoProgrammingMode_SetAllSwitchAlarmTriggerFirelevel {
                SerialNo = senzoSwitch.SerialNo,
                EnableBeep = false,
                TotalSwitch = senzoSwitch.CurtainList.Count,
                Firelevels = new int[senzoSwitch.CurtainList.Count].ToList()
            };
            var hasAnyFirelevelAction = false;
            for (var i = 0; i < scriptItems.Count; i++) {
                var item = scriptItems[i];
                if (item == null) {
                    continue;
                }
                int firelevel;
                switch (item.SwitchState) {
                    default:
                    case EScriptIconState.NoAction:
                        firelevel = 255;
                        break;
                    case EScriptIconState.AlarmLight:
                        switch (item.CurtainSwitch.SwitchMode) {
                            case 4:
                                firelevel = 255;
                                break;
                            default:
                                firelevel = 100;
                                hasAnyFirelevelAction = true;
                                break;
                            case 1:
                                firelevel = item.Firelevel;
                                hasAnyFirelevelAction = true;
                                break;
                        }
                        break;
                }
                setAlarmLightCommand.Firelevels[i] = firelevel;
            }

            if (hasAnyFirelevelAction) {
                output = string.Format("{0}\n", setAlarmLightCommand.ToScript());
            }
            return output;
        }
        private static object GenerateAlarmShutterCommands(SenzoSmartShutter senzoSwitch, ObservableCollection<SenzoSmartShutterScriptItem> scriptItems)
        {
            //IF ALARM LIGHT FALSE RETURN EMPTY
            var output = string.Empty;

            var setAlarmLightCommand = new SenzoProgrammingMode_SetAllSwitchAlarmTriggerFirelevel
            {
                SerialNo = senzoSwitch.SerialNo,
                EnableBeep = false,
                TotalSwitch = senzoSwitch.ShutterList.Count,
                Firelevels = new int[senzoSwitch.ShutterList.Count].ToList()
            };
            var hasAnyFirelevelAction = false;
            for (var i = 0; i < scriptItems.Count; i++)
            {
                var item = scriptItems[i];
                if (item == null)
                {
                    continue;
                }
                int firelevel;
                switch (item.SwitchState)
                {
                    default:
                    case EScriptIconState.NoAction:
                        firelevel = 255;
                        break;
                    case EScriptIconState.AlarmLight:
                        switch (item.ShutterSwitch.SwitchMode)
                        {
                            case 4:
                                firelevel = 255;
                                break;
                            default:
                                firelevel = 100;
                                hasAnyFirelevelAction = true;
                                break;
                            case 1:
                                firelevel = item.Firelevel;
                                hasAnyFirelevelAction = true;
                                break;
                        }
                        break;
                }
                setAlarmLightCommand.Firelevels[i] = firelevel;
            }

            if (hasAnyFirelevelAction)
            {
                output = string.Format("{0}\n", setAlarmLightCommand.ToScript());
            }
            return output;
        }
        private static object GenerateFirelevelCommands ( SenzoSmartSwitch senzoSwitch, ObservableCollection <SenzoSmartSwitchScriptItem> scriptItems ) {
            var hasAnyNonDimmerFirelevelAction = false;

            var output = string.Empty;

            var firelevelstates = new int[senzoSwitch.SwitchList.Count].ToList ();
            for ( var i = 0; i < scriptItems.Count; i++ ) {
                var item = scriptItems[i];
                if ( item == null ) {
                    continue;
                }
                int nonDimmerToggleFlags;
                switch ( item.SwitchState ) {
                    default :
                    case EScriptIconState.NoAction :
                        nonDimmerToggleFlags = 255;
                        break;
                    case EScriptIconState.Off :
                        switch ( item.SmartSwitch.SwitchMode ) {
                            case 4 :
                                hasAnyNonDimmerFirelevelAction = true;
                                nonDimmerToggleFlags = 255;
                                break;
                            default :
                                hasAnyNonDimmerFirelevelAction = true;
                                nonDimmerToggleFlags = 2;
                                break;
                            case 1 :
                                nonDimmerToggleFlags = 255;
                                var set1SwitchCommand = new SenzoProgrammingMode_Set1Switch () {
                                    SerialNo = item.SmartSwitch.SerialNo,
                                    SwitchNo = item.SmartSwitch.SwitchNo,
                                    Firelevel = item.Firelevel
                                };
                                output += string.Format ("{0}\n", set1SwitchCommand.ToScript ());
                                break;
                        }
                        break;
                    case EScriptIconState.On :
                        switch ( item.SmartSwitch.SwitchMode ) {
                            default :
                            case 4 :
                                nonDimmerToggleFlags = 1;
                                hasAnyNonDimmerFirelevelAction = true;
                                break;
                            case 1 :
                                if ( item.Firelevel > 100 ) {
                                    item.Firelevel = 100;
                                }
                                nonDimmerToggleFlags = 255;
                                var set1SwitchCommand = new SenzoProgrammingMode_Set1Switch {
                                    SerialNo = item.SmartSwitch.SerialNo,
                                    SwitchNo = item.SmartSwitch.SwitchNo,
                                    Firelevel = item.Firelevel
                                };
                                output += string.Format ("{0}\n", set1SwitchCommand.ToScript ());
                                break;
                        }
                        break;
                    case EScriptIconState.Press :
                        nonDimmerToggleFlags = 1;
                        hasAnyNonDimmerFirelevelAction = true;
                        break;
                }
                firelevelstates[i] = nonDimmerToggleFlags;
            }

            if ( hasAnyNonDimmerFirelevelAction ) {
                var toggleAllSwitchesCommand = new SenzoProgrammingMode_ToggleAllSwitches {
                    SerialNo = senzoSwitch.SerialNo,
                    TotalSwitch = senzoSwitch.SwitchList.Count,
                    FirelevelStates = firelevelstates
                };
                output += string.Format ("{0} \n", toggleAllSwitchesCommand.ToScript ());
            }

            return output;
        }
        private static object GenerateCurtainFirelevelCommands(SenzoSmartCurtain senzoSwitch, ObservableCollection<SenzoSmartCurtainScriptItem> scriptItems) {
            var hasAnyNonDimmerFirelevelAction = false;

            var output = string.Empty;

            var firelevelstates = new int[senzoSwitch.CurtainList.Count].ToList();
            for (var i = 0; i < scriptItems.Count; i++) {
                var item = scriptItems[i];
                if (item == null) {
                    continue;
                }
                int nonDimmerToggleFlags;
                switch (item.SwitchState) {
                    default:
                    case EScriptIconState.NoAction:
                        nonDimmerToggleFlags = 255;
                        break;
                    case EScriptIconState.Off:
                        switch (item.CurtainSwitch.SwitchMode) {
                            case 4:
                                hasAnyNonDimmerFirelevelAction = true;
                                nonDimmerToggleFlags = 255;
                                break;
                            default:
                                hasAnyNonDimmerFirelevelAction = true;
                                nonDimmerToggleFlags = 2;
                                break;
                            case 1:
                                nonDimmerToggleFlags = 255;
                                var set1SwitchCommand = new SenzoProgrammingMode_Set1Switch() {
                                    SerialNo = item.CurtainSwitch.SerialNo,
                                    SwitchNo = item.CurtainSwitch.SwitchNo,
                                    Firelevel = item.Firelevel
                                };
                                output += string.Format("{0}\n", set1SwitchCommand.ToScript());
                                break;
                        }
                        break;
                    case EScriptIconState.On:
                        switch (item.CurtainSwitch.SwitchMode) {
                            default:
                            case 4:
                                nonDimmerToggleFlags = 1;
                                hasAnyNonDimmerFirelevelAction = true;
                                break;
                            case 1:
                                if (item.Firelevel > 100) {
                                    item.Firelevel = 100;
                                }
                                nonDimmerToggleFlags = 255;
                                var set1SwitchCommand = new SenzoProgrammingMode_Set1Switch {
                                    SerialNo = item.CurtainSwitch.SerialNo,
                                    SwitchNo = item.CurtainSwitch.SwitchNo,
                                    Firelevel = item.Firelevel
                                };
                                output += string.Format("{0}\n", set1SwitchCommand.ToScript());
                                break;
                        }
                        break;
                    case EScriptIconState.Press:
                        nonDimmerToggleFlags = 1;
                        hasAnyNonDimmerFirelevelAction = true;
                        break;
                }
                firelevelstates[i] = nonDimmerToggleFlags;
            }

            if (hasAnyNonDimmerFirelevelAction) {
                var toggleAllSwitchesCommand = new SenzoProgrammingMode_ToggleAllSwitches {
                    SerialNo = senzoSwitch.SerialNo,
                    TotalSwitch = senzoSwitch.CurtainList.Count,
                    FirelevelStates = firelevelstates
                };
                output += string.Format("{0} \n", toggleAllSwitchesCommand.ToScript());
            }

            return output;
        }
        private static object GenerateShutterFirelevelCommands(SenzoSmartShutter senzoSwitch, ObservableCollection<SenzoSmartShutterScriptItem> scriptItems)
        {
            var hasAnyNonDimmerFirelevelAction = false;

            var output = string.Empty;

            var firelevelstates = new int[senzoSwitch.ShutterList.Count].ToList();
            for (var i = 0; i < scriptItems.Count; i++)
            {
                var item = scriptItems[i];
                if (item == null)
                {
                    continue;
                }
                int nonDimmerToggleFlags;
                switch (item.SwitchState)
                {
                    default:
                    case EScriptIconState.NoAction:
                        nonDimmerToggleFlags = 255;
                        break;
                    case EScriptIconState.Off:
                        switch (item.ShutterSwitch.SwitchMode)
                        {
                            case 4:
                                hasAnyNonDimmerFirelevelAction = true;
                                nonDimmerToggleFlags = 255;
                                break;
                            default:
                                hasAnyNonDimmerFirelevelAction = true;
                                nonDimmerToggleFlags = 2;
                                break;
                            case 1:
                                nonDimmerToggleFlags = 255;
                                var set1SwitchCommand = new SenzoProgrammingMode_Set1Switch()
                                {
                                    SerialNo = item.ShutterSwitch.SerialNo,
                                    SwitchNo = item.ShutterSwitch.SwitchNo,
                                    Firelevel = item.Firelevel
                                };
                                output += string.Format("{0}\n", set1SwitchCommand.ToScript());
                                break;
                        }
                        break;
                    case EScriptIconState.On:
                        switch (item.ShutterSwitch.SwitchMode)
                        {
                            default:
                            case 4:
                                nonDimmerToggleFlags = 1;
                                hasAnyNonDimmerFirelevelAction = true;
                                break;
                            case 1:
                                if (item.Firelevel > 100)
                                {
                                    item.Firelevel = 100;
                                }
                                nonDimmerToggleFlags = 255;
                                var set1SwitchCommand = new SenzoProgrammingMode_Set1Switch
                                {
                                    SerialNo = item.ShutterSwitch.SerialNo,
                                    SwitchNo = item.ShutterSwitch.SwitchNo,
                                    Firelevel = item.Firelevel
                                };
                                output += string.Format("{0}\n", set1SwitchCommand.ToScript());
                                break;
                        }
                        break;
                    case EScriptIconState.Press:
                        nonDimmerToggleFlags = 1;
                        hasAnyNonDimmerFirelevelAction = true;
                        break;
                }
                firelevelstates[i] = nonDimmerToggleFlags;
            }

            if (hasAnyNonDimmerFirelevelAction)
            {
                var toggleAllSwitchesCommand = new SenzoProgrammingMode_ToggleAllSwitches
                {
                    SerialNo = senzoSwitch.SerialNo,
                    TotalSwitch = senzoSwitch.ShutterList.Count,
                    FirelevelStates = firelevelstates
                };
                output += string.Format("{0} \n", toggleAllSwitchesCommand.ToScript());
            }

            return output;
        }
        private static object GenerateSetDelayCommands ( SenzoSmartSwitch senzoSwitch, ObservableCollection <SenzoSmartSwitchScriptItem> scriptItems ) {
            var output = string.Empty;

            foreach ( SenzoSmartSwitchScriptItem item in scriptItems ) {
                if ( item == null ) {
                    continue;
                }
                var switchNo = item.SmartSwitch.SwitchNo;
                switch ( item.IsDelayEnabled ) {
                    default :
                    case EBooleanStatus.Unknown :
                        break;
                    case EBooleanStatus.False :
                    case EBooleanStatus.True :
                        if ( !item.DelayOnOff.HasValue ) {
                            continue;
                        }
                        if ( !item.TotalMinutes.HasValue ) {
                            continue;
                        }
                        var setdelayCommand = new SenzoProgrammingMode_SetSwitchDelayTimer {
                            SerialNo = item.SmartSwitch.SerialNo,
                            SwitchNo = switchNo,
                            Minutes = item.TotalMinutes.Value,
                            OnOff = item.DelayOnOff.Value,
                        };
                        output += string.Format ("{0}\n", setdelayCommand.ToScript ());
                        break;
                }
            }
            return output;

            //if allow delay timer, then return
        }
        private static object GenerateSetCurtainDelayCommands(SenzoSmartCurtain senzoSwitch, ObservableCollection<SenzoSmartCurtainScriptItem> scriptItems) {
            var output = string.Empty;

            foreach (SenzoSmartCurtainScriptItem item in scriptItems) {
                if (item == null) {
                    continue;
                }
                var switchNo = item.CurtainSwitch.SwitchNo;
                switch (item.IsDelayEnabled) {
                    default:
                    case EBooleanStatus.Unknown:
                        break;
                    case EBooleanStatus.False:
                    case EBooleanStatus.True:
                        if (!item.DelayOnOff.HasValue) {
                            continue;
                        }
                        if (!item.TotalMinutes.HasValue) {
                            continue;
                        }
                        var setdelayCommand = new SenzoProgrammingMode_SetSwitchDelayTimer {
                            SerialNo = item.CurtainSwitch.SerialNo,
                            SwitchNo = switchNo,
                            Minutes = item.TotalMinutes.Value,
                            OnOff = item.DelayOnOff.Value,
                        };
                        output += string.Format("{0}\n", setdelayCommand.ToScript());
                        break;
                }
            }
            return output;

            //if allow delay timer, then return
        }
        private static object GenerateSetShutterDelayCommands(SenzoSmartShutter senzoSwitch, ObservableCollection<SenzoSmartShutterScriptItem> scriptItems)
        {
            var output = string.Empty;

            foreach (SenzoSmartShutterScriptItem item in scriptItems)
            {
                if (item == null)
                {
                    continue;
                }
                var switchNo = item.ShutterSwitch.SwitchNo;
                switch (item.IsDelayEnabled)
                {
                    default:
                    case EBooleanStatus.Unknown:
                        break;
                    case EBooleanStatus.False:
                    case EBooleanStatus.True:
                        if (!item.DelayOnOff.HasValue)
                        {
                            continue;
                        }
                        if (!item.TotalMinutes.HasValue)
                        {
                            continue;
                        }
                        var setdelayCommand = new SenzoProgrammingMode_SetSwitchDelayTimer
                        {
                            SerialNo = item.ShutterSwitch.SerialNo,
                            SwitchNo = switchNo,
                            Minutes = item.TotalMinutes.Value,
                            OnOff = item.DelayOnOff.Value,
                        };
                        output += string.Format("{0}\n", setdelayCommand.ToScript());
                        break;
                }
            }
            return output;

            //if allow delay timer, then return
        }
        private static object GenerateBeepCommands ( SenzoSmartSwitch senzoSwitch, ObservableCollection <SenzoSmartSwitchScriptItem> scriptItem ) {
            return string.Empty;
        }
        private static object GenerateCurtainBeepCommands(SenzoSmartCurtain senzoSwitch, ObservableCollection<SenzoSmartCurtainScriptItem> scriptItem) {
            return string.Empty;
        }
        private static object GenerateShutterBeepCommands(SenzoSmartShutter senzoSwitch, ObservableCollection<SenzoSmartShutterScriptItem> scriptItem)
        {
            return string.Empty;
        }

    }
}