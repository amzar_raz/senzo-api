﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SenzoProApp {
    public class SenzoScriptCurtainParser {
        public struct CommandList {
            public const string AwayArm = "awayarm";
            public const string HomeArm = "homearm";
            public const string DuressArm = "duressarm";
            public const string Disarm = "disarm";
            public const string TriggerAlarm = "triggeralarm";
            public const string ThirdPartyArm = "thirdpartyarm"; // regular arm
            public const string ThirdPartySleepArm = "thirdpartysleeparm";
            public const string ThirdPartyStayArm = "thirdpartystayarm";
            public const string ThirdPartyDisarm = "thirdpartydisarm";
            public const string ThirdPartyTriggerAlarm = "thirdpartytriggeralarm";
            public const string ThirdPartyPartitionArm = "thirdpartypartitionarm";
            public const string ThirdPartyLongPress = "thirdpartylongpress";
            public const string ThirdPartyDisarmPress = "thirdpartydisarmpress";
            public const string Nop = "nop";
            public const string UnknownCommand = "unknowncommand";
            public const string Set1Switch = "set1switchfirelevel";
            public const string SetAllSwitch = "setallfirelevel";
            public const string ToggleAllSwitches = "toggleallswitches";
            public const string SetAllSwitchAlarmTriggerFirelevel = "setallalarmtriggerfirelevel";
            public const string Beep = "beep";
            public const string SetZone = "setzonefirelevel";
            public const string RunFavorite = "runfavorite";
            public const string DimUpDown = "dimupdown";
            public const string ToggleOneSwitch = "toggleoneswitch";
            public const string SetSwitchDelayTimer = "setswitchdelaytimer";
            public const string CancelSwitchDelayTimer = "cancelswitchdelaytimer";
            public const string RunMacro = "runmacro";
            public const string RunProgram = "runprogram";
            public const string SMS = "sms";
            public const string MPUSendInfrared = "mpu_sendinfrared";
            public const string Delay = "delay";
            public const string PR_SetCarrier = "pr_setcarrier";
            public const string PR_RepeatSendButtonPress = "pr_repeatsend_buttonpress";
        }

        public SenzoScriptCurtainParser() { }

        /// <summary>
        /// Parse the Programming mode data into commands
        /// </summary>
        /// <param name="scripta">The script.</param>
        /// <returns></returns>
        public static List<SenzoProgrammingMode> ParseScript(string script) {

            /*SAMPLE: "set1switchfirelevel 0A31A130 1 30\nsetswitchdelaytimer 0A31A130 1 1 0\n\n"*/
            var scriptLines = script.Split(new[] { '\n', '\r' });

            /*
             * SAMPLE: 
             * [0]"set1switchfirelevel 0A31A130 1 30"
             * [1]"setswitchdelaytimer 0A31A130 1 1 0"
             * [2]""
             * [3]""
             */
            var currentCommands = new List<SenzoProgrammingMode>();
            foreach (var scriptLine in scriptLines) {
                SenzoProgrammingMode command = ParseLine(scriptLine);
                currentCommands.Add(command);
            }

            return currentCommands;
        }

        private static SenzoProgrammingMode ParseLine(string script) {
            if (script.Contains("\n") || script.Contains("\r")) {
                throw new NotSupportedException("Please Parse the script in SenzoScriptCurtainParser::ParseScript first");
            }
            SenzoProgrammingMode command;

            script = script.Trim();
            /*[0]"set1switchfirelevel 0A31A130 1 30"*/
            var scriptFields = ParseFields(script);
            switch (scriptFields.First()) {
                case CommandList.AwayArm:
                    command = new SenzoProgrammingMode_AwayArm();
                    break;
                case CommandList.HomeArm:
                    command = new SenzoProgrammingMode_HomeArm();
                    break;
                case CommandList.DuressArm:
                    command = new SenzoProgrammingMode_DuressArm();
                    break;
                case CommandList.Disarm:
                    command = new SenzoProgrammingMode_Disarm();
                    break;
                case CommandList.TriggerAlarm:
                    command = new SenzoProgrammingMode_TriggerAlarm();
                    break;
                case CommandList.ThirdPartyArm:
                    command = new SenzoProgrammingMode_ThirdPartyArm();
                    break;
                case CommandList.ThirdPartySleepArm:
                    command = new SenzoProgrammingMode_ThirdPartySleepArm();
                    break;
                case CommandList.ThirdPartyStayArm:
                    command = new SenzoProgrammingMode_ThirdPartyStayArm();
                    break;
                case CommandList.ThirdPartyDisarm:
                    command = new SenzoProgrammingMode_ThirdPartyDisarm();
                    break;
                case CommandList.ThirdPartyPartitionArm:
                    command = new SenzoProgrammingMode_ThirdPartyPartitionArm();
                    break;
                case CommandList.ThirdPartyLongPress:
                    command = new SenzoProgrammingMode_ThirdPartyLongPress();
                    break;
                case CommandList.ThirdPartyDisarmPress:
                    command = new SenzoProgrammingMode_ThirdPartyDisarmPress();
                    break;
                case CommandList.ThirdPartyTriggerAlarm:
                    command = new SenzoProgrammingMode_ThirdPartyTriggerAlarm();
                    break;
                case CommandList.Nop:
                    command = new SenzoProgrammingMode_NOP();
                    break;
                case CommandList.UnknownCommand:
                    command = new SenzoProgrammingMode_UnknownCommand();
                    break;
                case CommandList.Set1Switch:
                    command = new SenzoProgrammingMode_Set1Curtain();
                    break;
                case CommandList.SetAllSwitch:
                    command = new SenzoProgrammingMode_SetAllCurtain();
                    break;
                case CommandList.ToggleAllSwitches:
                    command = new SenzoProgrammingMode_ToggleAllCurtains();
                    break;
                case CommandList.SetAllSwitchAlarmTriggerFirelevel:
                    command = new SenzoProgrammingMode_SetAllCurtainAlarmTriggerFirelevel();
                    break;
                case CommandList.Beep:
                    command = new SenzoProgrammingMode_Beep();
                    break;
                case CommandList.SetZone:
                    command = new SenzoProgrammingMode_SetZone();
                    break;
                case CommandList.RunFavorite:
                    command = new SenzoProgrammingMode_RunFavorite();
                    break;
                case CommandList.DimUpDown:
                    command = new SenzoProgrammingMode_DimUpDown();
                    break;
                case CommandList.ToggleOneSwitch:
                    command = new SenzoProgrammingMode_ToggleOneCurtain();
                    break;
                case CommandList.SetSwitchDelayTimer:
                    command = new SenzoProgrammingMode_SetCurtainDelayTimer();
                    break;
                case CommandList.CancelSwitchDelayTimer:
                    command = new SenzoProgrammingMode_CancelCurtainDelayTimer();
                    break;
                case CommandList.RunMacro:
                    command = new SenzoProgrammingMode_RunCurtainMacro();
                    break;
                case CommandList.RunProgram:
                    command = new SenzoProgrammingMode_RunCurtainProgram();
                    break;
                case CommandList.SMS:
                    command = new SenzoProgrammingMode_SMS();
                    break;
                case CommandList.MPUSendInfrared:
                    command = new SenzoProgrammingMode_MPUSendInfrared();
                    break;
                case CommandList.Delay:
                    command = new SenzoProgrammingMode_MR_SelfDelay();
                    break;
                case CommandList.PR_SetCarrier:
                    command = new SenzoProgrammingMode_MR_SetCarrier();
                    break;
                case CommandList.PR_RepeatSendButtonPress:
                    command = new SenzoProgrammingMode_PR_RepeatSendCurtainButtonPress();
                    break;
                default:
                    command = null;
                    break;
            }
            if (command != null) {
                //do awesome here
                command.Parse(scriptFields);
            }

            return command;
        }

        private static List<string> ParseFields(string data) {
            var result = new List<string>();
            var pos = -1;
            data = data.Replace(@"\r", "\r");
            data = data.Replace(@"\n", "\n");

            while (pos < data.Length) {
                var scriptField = ParseField(data, ref pos);
                result.Add(scriptField);
            }
            return result;
        }

        private static string ParseField(string data, ref int delimPos) {
            if (delimPos == data.Length - 1) {
                delimPos++;
                return string.Empty;
            }

            var startPos = delimPos + 1;

            if (data[startPos] == '"') {
                if (startPos == data.Length - 1) {
                    return "\"";
                }

                //check FindSingleQuote()
                var nextSingleQuote = data.IndexOf('"', startPos + 1);
                delimPos = nextSingleQuote + 1;
                return data.Substring(startPos + 1, nextSingleQuote - startPos - 1).Replace("\"\"", "\"");
            }

            var delimIndex = data.IndexOf(' ', startPos);
            if (delimIndex == -1) {
                delimPos = data.Length;
                return data.Substring(startPos);
            }
            delimPos = delimIndex;
            return data.Substring(startPos, delimIndex - startPos);
        }

        internal static string EncryptPin(string decryptedPinNo) {

            //NotImplemented function. will return as it is.
            var result = decryptedPinNo;
            return result;
        }

        internal static string DecryptPin(string encryptedPinNo) {
            //NotImplemented function. will return as it is.
            var result = encryptedPinNo;
            return result;
        }
    }
}
