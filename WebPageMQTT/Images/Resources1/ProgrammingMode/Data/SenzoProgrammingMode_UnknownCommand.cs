using System;
using System.Collections.Generic;

namespace SenzoProApp
{
    public class SenzoProgrammingMode_UnknownCommand : SenzoProgrammingMode
    {
        public SenzoProgrammingMode_UnknownCommand() { }

        public override bool Parse ( List <string> commandParams ) {
            return true;
        }

        public override string ToScript() {
            return string.Empty;
        }

        public override string ToDesc() {
            return "Unknown Command";
        }

        public override string ToDescLess() {
            return "Unknown Command";
        }

        public override bool Undo() {
            return false;
        }

        public override void Send () {
            throw new NotImplementedException();
        }

        public override SenzoProgrammingMode Clone() {
            return new SenzoProgrammingMode_UnknownCommand ();
        }

        public override SenzoProgrammingMode Invert() {
            return null;
        }
    }
}