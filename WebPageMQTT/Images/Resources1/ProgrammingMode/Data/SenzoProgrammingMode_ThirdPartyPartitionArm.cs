using System.Collections.Generic;


namespace SenzoProApp
{
    public class SenzoProgrammingMode_ThirdPartyPartitionArm : SenzoProgrammingMode
    {

        public string SerialNo { get; set; }
        public List <int> KeySeqAction { get; set; }
        

        public SenzoProgrammingMode_ThirdPartyPartitionArm () {
            KeySeqAction = new List <int> ();
        }
        public override bool Parse ( List <string> commandParams ) {
            //thirdpartyshortpress C0000001 5 12 1 2 3 4
            ////"thirdpartypartitionarm C0000001 5 1 2 3 4 0"
            if (commandParams.Count != int.Parse(commandParams[2]) + 3) {
                return false;
            }
            SerialNo = commandParams[1];
            KeySeqAction.Clear ();
            for ( var i = 3; i < commandParams.Count; i++ ) {
                KeySeqAction.Add(int.Parse (commandParams[i]));
            }
            return true;
        }

        public override string ToScript () {
            var keyPressOutput = $"{KeySeqAction.Count} {string.Join (" ", KeySeqAction)}";

            return $"{SenzoScriptParser.CommandList.ThirdPartyPartitionArm} {SerialNo} {keyPressOutput}";
        }

        public override string ToDesc () {
            return "Third Party Product Partition Arm";
        }

        public override string ToDescLess () {
            return "Third Party Product Partition Arm";
        }

        public override bool Undo () {
            return false;
        }

        public override void Send () {
            throw new System.NotImplementedException ();
        }

        public override SenzoProgrammingMode Clone () {
            throw new System.NotImplementedException ();
        }

        public override SenzoProgrammingMode Invert () {
            throw new System.NotImplementedException();
        }

    }
}