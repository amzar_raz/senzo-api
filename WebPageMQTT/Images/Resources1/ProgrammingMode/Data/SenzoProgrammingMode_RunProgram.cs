using System;
using System.Collections.Generic;

namespace SenzoProApp
{
    public class SenzoProgrammingMode_RunProgram : SenzoProgrammingMode
    {
        public string ProgramName { get; private set; }

        public SenzoProgrammingMode_RunProgram() { }

        public override bool Parse ( List <string> commandParams ) {
            if ( commandParams.Count < 2 ) {
                return false;
            }
            ProgramName = commandParams [ 1 ];
            return true;
        }

        public override string ToScript() {
            return string.Format ( "{0} \"{1}\"" , SenzoScriptParser.CommandList.RunProgram , ProgramName );
        }

        public override string ToDesc() {
            return string.Format ( "Run Program {0}" , ProgramName );
        }

        public override string ToDescLess() {
            return string.Format ( "Run Program {0}" , ProgramName );
        }

        public override bool Undo() {
            return false;
        }

        public override void Send() { }

        public override SenzoProgrammingMode Clone() {
            throw new NotImplementedException ();
        }

        public override SenzoProgrammingMode Invert() {
            return null;
        }
    }
}