using System;
using System.Collections.Generic;

namespace SenzoProApp
{
    public class SenzoProgrammingMode_ToggleOneSwitch : SenzoProgrammingMode
    {
        public string SerialNo { get; private set; }
        public int SwitchNo { get; private set; }

        public SenzoProgrammingMode_ToggleOneSwitch() { }
        public override bool Parse(List<string> commandParams) {
            if ( commandParams.Count < 3 ) {
                return false;
            }
            SerialNo = commandParams [ 1 ];
            SwitchNo = int.Parse ( commandParams [ 2 ] );
            return true;
        }

        public override string ToScript() {
            return string.Format ( "{0} {1} {2}" , SenzoScriptParser.CommandList.ToggleOneSwitch , SerialNo , SwitchNo );
        }

        public override string ToDesc () {
            var smartSwitch = SenzoSmartSwitch.FindSmartSwitchBySerialNo ( SerialNo );
            if ( smartSwitch == null ) {
                return string.Format ( "Toggle switch {0}" , SwitchNo );
            }
            var smartButton = smartSwitch.SwitchList [ SwitchNo - 1 ];
            return smartButton == null
                           ? string.Format ( "Toggle switch {0}" , SwitchNo )
                           : string.Format("Toggle {0} (Channel {3}, Zone {4})", /*{1}, {2}, ... Channel*/
                                             smartButton.Name ,
                                             smartButton.LocationName,
                                             smartButton.FloorName,
                                             smartButton.ChannelNo ,
                                             smartButton.ZoneNo );
        }

        public override string ToDescLess () {
            var smartSwitch = SenzoSmartSwitch.FindSmartSwitchBySerialNo ( SerialNo );
            if ( smartSwitch == null ) {
                return string.Format ( "Toggle switch {0}" , SwitchNo );
            }
            var smartButton = smartSwitch.SwitchList [ SwitchNo - 1 ];
            return smartButton == null
                           ? string.Format ( "Toggle switch {0}" , SwitchNo )
                           : string.Format ( "Toggle {0} (Channel {3}, Zone {4})" , /*{1}, {2}, ...Channel*/
                                             smartButton.Name ,
                                             smartButton.LocationName,
                                             smartButton.FloorName,
                                             smartButton.ChannelNo ,
                                             smartButton.ZoneNo );
        }

        public override bool Undo() {
            return false;
        }

        public override void Send () {
            //PCK.SingleCommand ReplyObj;
            //string Pin = "0000";
            //Global.RFSend. (GlobalProp.RFID, GlobalProp.HostID, GlobalProp.MasterKeypad.SerialNo, out ReplyObj, Pin);
        }

        public override SenzoProgrammingMode Clone () {
            throw new NotImplementedException ();
        }

        public override SenzoProgrammingMode Invert() {
            return null;
        }
    }
}