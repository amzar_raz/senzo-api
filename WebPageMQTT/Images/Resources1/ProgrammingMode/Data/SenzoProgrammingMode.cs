﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SenzoProApp
{
    public abstract class SenzoProgrammingMode
    {
        protected SenzoProgrammingMode () {
            Debug.WriteLine ( "{0} Created", GetType ().Name );
        }
        public abstract bool Parse ( List <string> commandParams );
        public abstract string ToScript ();
        public abstract string ToDesc ();
        public abstract string ToDescLess ();
        public abstract bool Undo ();
        public abstract void Send ();

        public virtual bool NeedAlarmPassword () {
            return false;
        }


        /// <summary>
        /// Enforce user to send password through Pocket Remote
        /// <para>Reason: If user lost her pocket remote, she only need to change her Security System password. 
        /// The one who pick up the pocket remote will not be able to disarm the security system even if the RFID and HostID are not changed.</para>
        /// </summary>
        /// <returns></returns>
        protected static bool DoesPocketRemoteSecurityCommandNeedPassword () {
            return true;
        }

        /// <summary>
        /// Create a copy of this command
        /// </summary>
        /// <returns></returns>
        public abstract SenzoProgrammingMode Clone ();

        /// <summary>
        /// Inverts the specified Command
        /// </summary>
        /// <returns></returns>
        public abstract SenzoProgrammingMode Invert ();
    }
}
