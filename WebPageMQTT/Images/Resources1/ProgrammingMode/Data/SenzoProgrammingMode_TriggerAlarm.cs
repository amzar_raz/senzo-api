using System;
using System.Collections.Generic;

namespace SenzoProApp
{
    public class SenzoProgrammingMode_TriggerAlarm : SenzoProgrammingMode
    {
        public string EncryptedPinNumber { get; private set; }
        public string PinNumber {
            get {
                return SenzoScriptParser.DecryptPin(EncryptedPinNumber);
            }
            set { EncryptedPinNumber = SenzoScriptParser.EncryptPin ( value );
            }
        }
        public SenzoProgrammingMode_TriggerAlarm() { }
        public override bool Parse(List<string> commandParams) {
            EncryptedPinNumber = commandParams.Count > 1
                                         ? commandParams[1]
                                         : SenzoScriptParser.EncryptPin("0000");
            return true;
        }

        public override string ToScript() {
            return string.Format ( "{0} {1}" , SenzoScriptParser.CommandList.TriggerAlarm , EncryptedPinNumber );
        }

        public override string ToDesc() {
            return "Trigger the Wireless Security System";
        }

        public override string ToDescLess() {
            return "Trigger the Wireless Security System";
        }

        public override bool Undo() {
            return false;
        }

        public override void Send () {
            //            PCK.SingleCommand ReplyObj;
            //            string Pin = "0000";
            //            Global.RFSend.MPUThirdParty_ManualTriggerAlarm(GlobalProp.RFID, GlobalProp.HostID, GlobalProp.MasterKeypad.SerialNo, out ReplyObj, Pin);
        }

        public override SenzoProgrammingMode Clone() {
            throw new NotImplementedException ();
        }

        public override SenzoProgrammingMode Invert() {
            return null;
        }
    }
}