using System;
using System.Collections.Generic;
using System.Linq;

namespace SenzoProApp
{
    public class SenzoProgrammingMode_SMS : SenzoProgrammingMode
    {
        public string Message { get; private set; }

        public struct PhoneItem
        {
            public string Name { get; set; }
            public string Phone { get; set; }
        }

        public List<PhoneItem> PhoneLists { get; private set; }
        public SenzoProgrammingMode_SMS() { }
        public override bool Parse(List<string> commandParams)
        {
            if ( commandParams.Count < 3 ) {
                return false;
            }

            Message = commandParams [ 1 ];

            if ( PhoneLists == null ) {
                PhoneLists = new List <PhoneItem> ();
            }

            for ( var i = 0; i < commandParams.Count; i++ ) {
                if ( ( i + 1 ) >= commandParams.Count ) {
                    continue;
                }
                var phonelist = new PhoneItem { Name = commandParams [ i ] , Phone = commandParams [ i + 1 ] };
                PhoneLists.Add ( phonelist );
            }

            return true;
        }

        public override string ToScript () {
            var escapedMessage = Message;
            escapedMessage = escapedMessage.Replace("\r", @"\r");
            escapedMessage = escapedMessage.Replace("\n", @"\n");
            escapedMessage = string.Format("\"{0}\"", escapedMessage);
            var phones = PhoneLists.Aggregate ( string.Empty , ( current , phoneList ) => current + string.Format ( "\"{0}\" \"{1}\" " , phoneList.Name , phoneList.Phone ) );
            return string.Format ( "{0} {1} {2}", SenzoScriptParser.CommandList.SMS, escapedMessage, phones );
        }

        public override string ToDesc() {
            return "SMS";
        }

        public override string ToDescLess() {
            return "SMS";
        }

        public override bool Undo () {
            return false;
        }

        public override void Send () {
            throw new NotImplementedException ();
        }


        public override SenzoProgrammingMode Clone() {
            throw new NotImplementedException ();
        }

        public override SenzoProgrammingMode Invert () {
            return null;
        }
    }
}