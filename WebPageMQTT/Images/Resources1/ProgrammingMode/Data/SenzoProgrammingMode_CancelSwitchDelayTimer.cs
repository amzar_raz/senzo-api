using System;
using System.Collections.Generic;

namespace SenzoProApp
{
    public class SenzoProgrammingMode_CancelSwitchDelayTimer : SenzoProgrammingMode
    {
        public string SerialNo { get; set; }
        public int SwitchNo { get; set; }

        public SenzoProgrammingMode_CancelSwitchDelayTimer() { }

        public override bool Parse ( List <string> commandParams ) {
            if ( commandParams.Count < 3 ) {
                return false;
            }
            SerialNo = commandParams [ 1 ];
            SwitchNo = int.Parse ( commandParams [ 2 ] );
            return true;
        }

        public override string ToScript() {
            return string.Format ( "{0} {1} {2}" , 
                                    SenzoScriptParser.CommandList.CancelSwitchDelayTimer , 
                                    SerialNo ,
                                    SwitchNo );
        }

        public override string ToDesc() {
            var smartswitch = SenzoSmartSwitch.FindSmartSwitchBySerialNo ( SerialNo );
            if ( smartswitch == null ) {
                return string.Format ( "Cancel delay timer of switch {0}" , SwitchNo );
            }
            var smartButton = smartswitch.SwitchList [ SwitchNo - 1 ];
            if ( smartButton == null ) {
                return string.Format ( "Cancel delay timer of switch {0}" , SwitchNo );
            }
            return string.Format ( "Cancel delay timer of {0} ({1}, {2}, Channel {3}, Zone {4})" ,
                                   smartButton.Name ,
                                   smartButton.LocationName ,
                                   smartButton.FloorName ,
                                   smartButton.ChannelNo ,
                                   smartButton.ZoneNo );
        }

        public override string ToDescLess() {
            var smartswitch = SenzoSmartSwitch.FindSmartSwitchBySerialNo ( SerialNo );
            if ( smartswitch == null ) {
                return string.Format ( "Cancel delay timer of switch {0}" , SwitchNo );
            }
            var smartButton = smartswitch.SwitchList [ SwitchNo - 1 ];
            if ( smartButton == null ) {
                return string.Format ( "Cancel delay timer of switch {0}" ,
                                       SwitchNo );
            }
            return string.Format ( "Cancel delay timer of {0} ({1}, {2}, Channel {3}, Zone {4})" ,
                                   smartButton.Name ,
                                   smartButton.LocationName ,
                                   smartButton.FloorName ,
                                   smartButton.ChannelNo ,
                                   smartButton.ZoneNo );
        }

        public override bool Undo() {
            return false;
        }

        public override void Send () {
//            PCK.SingleCommand ReplyObj;
//            Global.RFSend.SetTimerDelay(GlobalProp.RFID, GlobalProp.HostID, SerialNo, true, out ReplyObj, SwitchNo, false, false, 0);
        }

        public override SenzoProgrammingMode Clone() {
            throw new NotImplementedException ();
        }

        public override SenzoProgrammingMode Invert() {
            return null;
        }
    }
}