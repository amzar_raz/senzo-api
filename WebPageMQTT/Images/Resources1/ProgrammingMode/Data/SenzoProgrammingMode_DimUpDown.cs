using System.Collections.Generic;

namespace SenzoProApp
{
    public class SenzoProgrammingMode_DimUpDown : SenzoProgrammingMode
    {
        public string SerialNo { get; private set; }
        public int ZoneNo { get; private set; }
        public int Direction { get; private set; }
        public int Step { get; private set; }
        public int [] dimmerAction = new int[ 2 ];

        public SenzoProgrammingMode_DimUpDown() { }

        public override bool Parse ( List <string> commandParams ) {
            if ( commandParams.Count < 7 ) {
                return false;
            }
            ZoneNo = int.Parse ( commandParams [ 1 ] );
            SerialNo = commandParams [ 2 ];
            Direction = int.Parse ( commandParams [ 3 ] );
            Step = int.Parse ( commandParams [ 4 ] );
            dimmerAction [ 0 ] = int.Parse ( commandParams [ 5 ] );
            dimmerAction [ 1 ] = int.Parse ( commandParams [ 6 ] );
            return true;
        }

        public override string ToScript() {
            return string.Format ( "{0} {1} {2} {3} {4} {5} {6}" ,
                                   SenzoScriptParser.CommandList.DimUpDown ,
                                   ZoneNo ,
                                   SerialNo ,
                                   Direction ,
                                   Step ,
                                   dimmerAction [ 0 ] ,
                                   dimmerAction [ 1 ] );
        }

        public override string ToDesc() {
            var dimUpDownDesc = string.Empty;
            switch ( Direction ) {
                case 253:
                    dimUpDownDesc = "down";
                    break;
                case 254: //dim up
                    dimUpDownDesc = "up";
                    break;
            }

            string strSteps;
            switch ( Step ) {
                case 255: //stop
                    strSteps = "continuously";
                    break;
                case 0:
                    strSteps = "stop";
                    break;
                default:
                    strSteps = string.Format ( "for {0} {1}" , Step , Step == 1 ? "step" : "steps" );
                    break;
            }

            var strWhichDimmer = string.Empty;
            if ( dimmerAction [ 0 ] == 255 ) {
                strWhichDimmer = dimmerAction [ 1 ] == 255
                                      ? "(Both Dimmer)"
                                      : "(Dimmer 1 only)";
            } else if ( dimmerAction [ 1 ] == 255 ) {
                strWhichDimmer = "(Dimmer 2 only)";
            }

            switch ( ZoneNo ) {
                case 0://ThisSwitchPanelOnly
                    var smartSwitch = SenzoSmartSwitch.FindSmartSwitchBySerialNo ( SerialNo );
                    return smartSwitch == null
                                   ? string.Format ( "Dim {0} {1}" , dimUpDownDesc , strSteps )
                                   : string.Format("Dim {0} {1} of the Smart Switch {4}"/* at {2} on {3}*/ , dimUpDownDesc, strSteps, smartSwitch.LocationName, smartSwitch.FloorName, strWhichDimmer);
                case 255:
                    return string.Format ( "Dim {0} all dimmers {1}" , dimUpDownDesc , strSteps );
                default:
                    return string.Format ( "Dim {0} all dimmers in Zone {1} {2}" , dimUpDownDesc , ZoneNo , strSteps );
            }
        }

        public override string ToDescLess () {
            var dimUpDownDesc = string.Empty;
            switch ( Direction ) {
                case 253:
                    dimUpDownDesc = "down";
                    break;
                case 254: //dim up
                    dimUpDownDesc = "up";
                    break;
            }

            string strSteps;
            switch ( Step ) {
                case 255: //stop
                    strSteps = "continuously";
                    break;
                case 0:
                    strSteps = "stop";
                    break;
                default:
                    strSteps = string.Format ( "for {0} {1}" , Step , Step == 1 ? "step" : "steps" );
                    break;
            }

            var strWhichDimmer = string.Empty;
            if ( dimmerAction [ 0 ] == 255 ) {
                strWhichDimmer = dimmerAction [ 1 ] == 255
                                      ? "(Both Dimmer)"
                                      : "(Dimmer 1 only)";
            } else if ( dimmerAction [ 1 ] == 255 ) {
                strWhichDimmer = "(Dimmer 2 only)";
            }

            switch ( ZoneNo ) {
                case 0://ThisSwitchPanelOnly
                    var smartSwitch = SenzoSmartSwitch.FindSmartSwitchBySerialNo ( SerialNo );
                    return smartSwitch == null
                                   ? string.Format ( "Dim {0} {1}" , dimUpDownDesc , strSteps )
                                   : string.Format("Dim {0} {1} of the Smart Switch{4}"/* at {2} on {3}*/ , dimUpDownDesc, strSteps, smartSwitch.LocationName, smartSwitch.FloorName, strWhichDimmer);
                case 255:
                    return string.Format ( "Dim {0} all dimmers {1}" , dimUpDownDesc , strSteps );
                default:
                    return string.Format ( "Dim {0} all dimmers in Zone {1} {2}" , dimUpDownDesc , ZoneNo , strSteps );
            }
        }

        public override bool Undo() {
            return false;
        }

        public override void Send () {
            //PCK.SingleCommand ReplyObj;
            //string Pin = "0000";
            //Global.RFSend.(GlobalProp.RFID, GlobalProp.HostID, GlobalProp.MasterKeypad.SerialNo, out ReplyObj, Pin);
        }

        public override SenzoProgrammingMode Clone () {
            return new SenzoProgrammingMode_DimUpDown {
                    SerialNo = SerialNo ,
                    ZoneNo = ZoneNo ,
                    Direction = Direction ,
                    Step = Step ,
                    dimmerAction = new [] { dimmerAction [ 0 ] , dimmerAction [ 1 ] } ,
            };
        }

        public override SenzoProgrammingMode Invert () {
            var command = Clone () as SenzoProgrammingMode_DimUpDown;
            if ( command == null ) {
                return null;
            }
            command.Direction = ( Direction == 253 ) ? 254 : 253;
            return command;
        }
    }
}