using System;
using System.Collections.Generic;

namespace SenzoProApp
{
    public class SenzoProgrammingMode_SetZone : SenzoProgrammingMode
    {
        public int ZoneNo { get; private set; }
        public bool OnOff { get; private set; }
        public int DimmerFireLevel { get; private set; }
        public int FanSpeed { get; private set;}
        public bool IsDimmerInPreviousState { get; private set; }
        
        public SenzoProgrammingMode_SetZone() { }
        public override bool Parse(List<string> commandParams) {
            if ( commandParams.Count < 5 ) {
                return false;
            }
            ZoneNo = int.Parse ( commandParams [ 1 ] );
            OnOff = Convert.ToBoolean ( int.Parse ( commandParams [ 2 ] ) );
            DimmerFireLevel = int.Parse ( commandParams [ 3 ] );
            FanSpeed = int.Parse ( commandParams [ 4 ] );

            IsDimmerInPreviousState = commandParams.Count > 5 && Convert.ToBoolean ( int.Parse ( commandParams [ 5 ] ) );
            return true;
        }

        public override string ToScript() {
            return string.Format ( "{0} {1} {2} {3} {4} {5}" , 
                                    SenzoScriptParser.CommandList.SetZone , 
                                    ZoneNo ,
                                    Convert.ToInt32 ( OnOff ) , 
                                    DimmerFireLevel , 
                                    FanSpeed ,
                                    Convert.ToInt32 ( IsDimmerInPreviousState ) );
        }

        public override string ToDesc() {
            var dimmerLevelDesc = IsDimmerInPreviousState
                                             ? "previous brightness"
                                             : string.Format ( "{0} %" , DimmerFireLevel );
            return string.Format ( "For Zone {0}, turn all switches {1} and adjust all dimmers to {2}", ZoneNo, OnOff, dimmerLevelDesc );
        }

        public override string ToDescLess () {
            var dimmerLevelDesc = IsDimmerInPreviousState
                                          ? "previous brightness"
                                          : string.Format ( "{0} %" , DimmerFireLevel );
            return string.Format ( "For Zone {0}, turn all switches {1} and adjust all dimmers to {2}" , ZoneNo , OnOff , dimmerLevelDesc );
        }

        public override bool Undo() {
            return false;
        }

        public override void Send() {
            //PCK.SingleCommand ReplyObj;
            //Global.RFSend.SetZone(GlobalProp.RFID, GlobalProp.HostID, mSerialNo, true, out ReplyObj, mSwitchNo, false, false, 0);
        }

        public override SenzoProgrammingMode Clone() {
            return new SenzoProgrammingMode_SetZone {
                    ZoneNo = ZoneNo ,
                    OnOff = OnOff ,
                    DimmerFireLevel = DimmerFireLevel ,
                    FanSpeed = FanSpeed
            };
        }

        public override SenzoProgrammingMode Invert() {
            var command = Clone () as SenzoProgrammingMode_SetZone;
            if ( command == null ) {
                return null;
            }
            command.OnOff = !OnOff;
            command.DimmerFireLevel = DimmerFireLevel > 0 ? 0 : 100;
            command.FanSpeed = FanSpeed > 0 ? 0 : 100;
            return command;
        }
    }
}