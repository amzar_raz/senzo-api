using System.Collections.Generic;


namespace SenzoProApp
{
    public class SenzoProgrammingMode_ThirdPartyDisarmPress : SenzoProgrammingMode
    {

        public string SerialNo { get; set; }
        public List<int> KeySeqAction { get; set; }

        public SenzoProgrammingMode_ThirdPartyDisarmPress () {
            KeySeqAction = new List <int> ();
        }

        public override bool Parse ( List <string> commandParams ) {
            //thirdpartydisarmpress C0000001 5 1 2 3 4 10
            if (commandParams.Count != int.Parse(commandParams[2]) + 3) {
                return false;
            }
            SerialNo = commandParams[1];
            KeySeqAction.Clear();
            for (var i = 3; i < commandParams.Count; i++)
            {
                KeySeqAction.Add(int.Parse(commandParams[i]));
            }
            return true;
        }
         
        public override string ToScript () {
            //Disarm ="thirdpartydisarmpress C0000001 5 1 2 3 4 10"
            var keyPressOutput = $"{KeySeqAction.Count} {string.Join (" ", KeySeqAction)}";

            return $"{SenzoScriptParser.CommandList.ThirdPartyDisarmPress} {SerialNo} {keyPressOutput}";
        }

        public override string ToDesc () {
            return "Third Party Product Disarm press";
        }

        public override string ToDescLess () {
            return "Third Party Product Disarm press";
        }

        public override bool Undo () {
            return false;
        }

        public override void Send () {
            throw new System.NotImplementedException ();
        }

        public override SenzoProgrammingMode Clone () {
            throw new System.NotImplementedException ();
        }

        public override SenzoProgrammingMode Invert () {
            throw new System.NotImplementedException ();
        }

    }
}