using System;
using System.Collections.Generic;

namespace SenzoProApp
{
    public class SenzoProgrammingMode_SetSwitchDelayTimer : SenzoProgrammingMode
    {
        public string SerialNo { get; set; }
        public int SwitchNo { get; set; }
        public int Minutes { get; set; }
        public bool OnOff { get; set; }

        public SenzoProgrammingMode_SetSwitchDelayTimer() { }
        public override bool Parse(List<string> commandParams)
        {
            if ( commandParams.Count < 5 ) {
                return false;
            }
            SerialNo = commandParams [ 1 ];
            SwitchNo = int.Parse ( commandParams [ 2 ] );
            Minutes = int.Parse ( commandParams [ 3 ] );
            OnOff = Convert.ToBoolean ( int.Parse ( commandParams [ 4 ] ) );
            return true;
        }

        public override string ToScript() {
            return string.Format ( "{0} {1} {2} {3} {4}" ,
                                   SenzoScriptParser.CommandList.SetSwitchDelayTimer ,
                                   SerialNo ,
                                   SwitchNo ,
                                   Minutes ,
                                   Convert.ToInt32 ( OnOff ) );
        }

        public override string ToDesc() {
            var smartSwitch = SenzoSmartSwitch.FindSmartSwitchBySerialNo ( SerialNo );
            if ( smartSwitch == null ) {
                return string.Format ( "Turn {0} switch {1} after {2} minutes" , OnOff ? "on" : "off" , SwitchNo , Minutes );
            }
            var smartButton = smartSwitch.SwitchList [ SwitchNo - 1 ];
            return smartButton == null
                           ? string.Format ( "Turn {0} switch {1} after {2} minutes" ,
                                             OnOff ? "on" : "off" ,
                                             SwitchNo ,
                                             Minutes )
                           : string.Format("Turn {0} {1} after {2} minutes (Channel {5}, Zone {6})",/*{1}, {2}, ...Channel*/
                                             OnOff ? "on" : "off" ,
                                             smartButton.Name ,
                                             Minutes ,
                                             smartButton.LocationName ,
                                             smartButton.FloorName ,
                                             smartButton.ChannelNo ,
                                             smartButton.ZoneNo );
        }

        public override string ToDescLess() {
            var smartSwitch = SenzoSmartSwitch.FindSmartSwitchBySerialNo ( SerialNo );
            if ( smartSwitch == null ) {
                return string.Format ( "Turn {0} switch {1} after {2} minutes" , OnOff ? "on" : "off" , SwitchNo , Minutes );
            }
            var smartButton = smartSwitch.SwitchList [ SwitchNo - 1 ];
            return smartButton == null
                           ? string.Format ( "Turn {0} switch {1} after {2} minutes" ,
                                             OnOff ? "on" : "off" ,
                                             SwitchNo ,
                                             Minutes )
                           : string.Format("Turn {0} {1} after {2} minutes (Channel {5}, Zone {6})",/*{1}, {2}, ...Channel*/
                                             OnOff ? "on" : "off" ,
                                             smartButton.Name ,
                                             Minutes ,
                                             smartButton.LocationName ,
                                             smartButton.FloorName ,
                                             smartButton.ChannelNo ,
                                             smartButton.ZoneNo );
        }

        public override bool Undo() {
            return false;
        }

        public override void Send() {
//            PCK.SingleCommand ReplyObj;
//            Global.RFSend.SetTimerDelay(GlobalProp.RFID, GlobalProp.HostID, SerialNo, true, out ReplyObj, SwitchNo, true, On, Minutes);
        }

        public override SenzoProgrammingMode Clone () {
            return new SenzoProgrammingMode_SetSwitchDelayTimer {
                    SerialNo = SerialNo ,
                    SwitchNo = SwitchNo ,
                    Minutes = Minutes ,
                    OnOff = OnOff ,
            };
        }

        public override SenzoProgrammingMode Invert() {
            var command = Clone () as SenzoProgrammingMode_SetSwitchDelayTimer;
            if ( command == null ) {
                return null;
            }
            command.OnOff = !OnOff;
            return command;
        }
    }
}