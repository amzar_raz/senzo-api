﻿using System;
using Xamarin.Forms;

namespace SenzoProApp.Resources
{
    public static class Resource
    {
        public static class AppColor
        {
            public static Xamarin.Forms.Color Primary { get { return Xamarin.Forms.Color.FromHex("1A97D4"); } }
            public static Xamarin.Forms.Color PrimaryDark { get { return Xamarin.Forms.Color.FromHex("195890"); } }
            public static Xamarin.Forms.Color PrimaryLight { get { return Xamarin.Forms.Color.FromHex("B3E5FC"); } }
            public static Xamarin.Forms.Color PrimaryText { get { return Xamarin.Forms.Color.FromHex("212121"); } }
            public static Xamarin.Forms.Color WindowBackground { get { return Xamarin.Forms.Color.FromHex("3498DB"); } }

            //public static Xamarin.Forms.Color Accent { get { return Xamarin.Forms.Color.FromHex("3684d0"); } }
            //public static Xamarin.Forms.Color SecondaryText { get { return Xamarin.Forms.Color.FromHex("727272"); } }
            //public static Xamarin.Forms.Color Icons { get { return Xamarin.Forms.Color.FromHex("FFFFFF"); } }
            //public static Xamarin.Forms.Color Divider { get { return Xamarin.Forms.Color.FromHex("B6B6B6"); } }
        }

        public static class FlatColor
        {
            //Flat white / grey
            public static Xamarin.Forms.Color Clouds { get { return Xamarin.Forms.Color.FromHex("ecf0f1"); } }
            //public static Xamarin.Forms.Color Silver { get { return Xamarin.Forms.Color.FromHex("bdc3c7"); } }
            public static Xamarin.Forms.Color Concrete { get { return Xamarin.Forms.Color.FromHex("95a5a6"); } }
            public static Xamarin.Forms.Color Asbestos { get { return Xamarin.Forms.Color.FromHex("7f8c8d"); } }

            //Flat Red
            public static Xamarin.Forms.Color Alizarin { get { return Xamarin.Forms.Color.FromHex("e74c3c"); } }//"?attr/colorPrimary"
            public static Xamarin.Forms.Color Pomegranate { get { return Xamarin.Forms.Color.FromHex("c0392b"); } }

            //Flat Orange
            //public static Xamarin.Forms.Color Carrot { get { return Xamarin.Forms.Color.FromHex("e67e22"); } }
            public static Xamarin.Forms.Color Pumpkin { get { return Xamarin.Forms.Color.FromHex("d35400"); } }

            //Flat Yellow
            public static Xamarin.Forms.Color Sunflower { get { return Xamarin.Forms.Color.FromHex("f1c40f"); } }
            //public static Xamarin.Forms.Color Orange { get { return Xamarin.Forms.Color.FromHex("f39c12"); } }


            //Flat Turquiose
            public static Xamarin.Forms.Color Turquoise { get { return Xamarin.Forms.Color.FromHex("1abc9c"); } }
            public static Xamarin.Forms.Color GreenSea { get { return Xamarin.Forms.Color.FromHex("16a085"); } }

            //Flat Green
            public static Xamarin.Forms.Color Emerald { get { return Xamarin.Forms.Color.FromHex("2ecc71"); } }
            public static Xamarin.Forms.Color Nephritis { get { return Xamarin.Forms.Color.FromHex("16a085"); } }

            //Flat Blue
            public static Xamarin.Forms.Color PeterRiver { get { return Xamarin.Forms.Color.FromHex("3498db"); } }
            //public static Xamarin.Forms.Color BelizeHole { get { return Xamarin.Forms.Color.FromHex("2980b9"); } }

            //Flat Violet
            //public static Xamarin.Forms.Color Amethyst { get { return Xamarin.Forms.Color.FromHex("9b59b6"); } }
            //public static Xamarin.Forms.Color Wisteria { get { return Xamarin.Forms.Color.FromHex("8e44ad"); } }

            //Flat Black / Dark Grey Blue
            public static Xamarin.Forms.Color WetAsphalt { get { return Xamarin.Forms.Color.FromHex("34495e"); } }
            public static Xamarin.Forms.Color MidnightBlue { get { return Xamarin.Forms.Color.FromHex("2c3e50"); } }
        }



        //public static string Icon {get;} = "Icon" ;
        //public static string SplashScreenImage { get; set; } = "splashLoading.png";
        //Paik Wai testing only
        //public static string BG { get; set; } = "splash.png";



        //private static string iOS_GetDefault()
        //{
        //    if (Device.RuntimePlatform != Device.iOS)
        //    {
        //        return string.Empty;
        //    }
        //    //"Images.xcassets/LaunchImage.launchimage/Default.png"
        //    //"It is my understanding that the -568h@2x, etc., naming scheme only applies to bitmaps used for splashscreens."
        //    switch (Device.Idiom)
        //    {
        //        case TargetIdiom.Phone:
        //            if (NLibraryForms.NDevice.DeviceWidth == 640 && NLibraryForms.NDevice.DeviceHeight == 1136)
        //            {
        //                //iPhone 5, iPhone5S
        //                return "Images.xcassets/LaunchImage.launchimage/Default-568h@2x.png";
        //            }
        //            else if (NLibraryForms.NDevice.DeviceWidth == 750 && NLibraryForms.NDevice.DeviceHeight == 1334)
        //            {
        //                //iPhone 6
        //                return "Images.xcassets/LaunchImage.launchimage/Default-667h@2x.png";
        //            }
        //            else if (NLibraryForms.NDevice.DeviceWidth == 1242 && NLibraryForms.NDevice.DeviceHeight == 2208)
        //            {
        //                //iPhone 6s Plus
        //                return "Images.xcassets/LaunchImage.launchimage/Default-736h@3x.png";
        //            }
        //            break;
        //        case TargetIdiom.Tablet:
        //            if (NLibraryForms.NDevice.DeviceWidth == 768 && NLibraryForms.NDevice.DeviceHeight == 1024)
        //            {
        //                //iPad
        //                return "Images.xcassets/LaunchImage.launchimage/Default-Portrait.png";
        //            }
        //            if (NLibraryForms.NDevice.DeviceWidth == 1536 && NLibraryForms.NDevice.DeviceHeight == 2048)
        //            {
        //                //iPad @2x
        //                return "Images.xcassets/LaunchImage.launchimage/Default-Portrait@2x.png";
        //            }
        //            if (NLibraryForms.NDevice.DeviceWidth == 1024 && NLibraryForms.NDevice.DeviceHeight == 768)
        //            {
        //                //iPad
        //                return "Images.xcassets/LaunchImage.launchimage/Default-Landscape.png";
        //            }
        //            if (NLibraryForms.NDevice.DeviceWidth == 2048 && NLibraryForms.NDevice.DeviceHeight == 1536)
        //            {
        //                //iPad @2x
        //                return "Images.xcassets/LaunchImage.launchimage/Default-Landscape@2x.png";
        //            }
        //            if (NLibraryForms.NDevice.DeviceWidth == 2048 && NLibraryForms.NDevice.DeviceHeight == 2732)
        //            {
        //                //iPad Pro
        //                return "Images.xcassets/LaunchImage.launchimage/Default-Portrait-1336h.png";
        //            }
        //            if (NLibraryForms.NDevice.DeviceWidth == 2732 && NLibraryForms.NDevice.DeviceHeight == 2048)
        //            {
        //                //iPad Pro
        //                return "Images.xcassets/LaunchImage.launchimage/Default-Landscape-1336h.png";
        //            }
        //            break;
        //    }
        //    return "Images.xcassets/LaunchImage.launchimage/Default.png";
        //}

        public static readonly ImageSource IconPlaceholder = "ic_temp.png";
        public static readonly ImageSource RadioButtonEnable = ImageSource.FromFile("senzotheme_btn_radio_on_holo_light"); //TEST: 
        public static readonly ImageSource RadioButtonDisable = ImageSource.FromFile("senzotheme_btn_radio_on_disabled_holo_light");
        public static ImageSource UnknownStatus = ImageSource.FromFile("senzo_sensor_generic_unknown.png");
        public static ImageSource RSSI1 = ImageSource.FromFile("RSSI1.png");
        public static ImageSource RSSI2 = ImageSource.FromFile("RSSI2.png");
        public static ImageSource RSSI3 = ImageSource.FromFile("RSSI3.png");
        public static ImageSource RSSI4 = ImageSource.FromFile("RSSI4.png");
        public static ImageSource RSSI5 = ImageSource.FromFile("RSSI5.png");
        public static ImageSource RSSI6 = ImageSource.FromFile("RSSI6.png");
        public static ImageSource RSSI7 = ImageSource.FromFile("RSSI7.png");
        public static ImageSource RSSI8 = ImageSource.FromFile("RSSI8.png");
        public static ImageSource RSSI9 = ImageSource.FromFile("RSSI9.png");
        public static ImageSource RSSI10 = ImageSource.FromFile("RSSI10.png");
        public static ImageSource UnknownRSSI = ImageSource.FromFile("RSSI_Unkown.png");
        public static ImageSource DimmerOnTimerOff = ImageSource.FromFile("senzo_switch_dimmerOnTimerOff.png");
        public static ImageSource DimmerOffTimerOff = ImageSource.FromFile("senzo_switch_dimmerOffTimerOff.png");
        public static ImageSource AutogateOnTimerOff = ImageSource.FromFile("senzo_switch_autogateOnTimerOff.png");
        public static ImageSource AutogateOffTimerOff = ImageSource.FromFile("senzo_switch_autogateOffTimerOff.png");
        public static ImageSource CurtainOnTimerOff = ImageSource.FromFile("senzo_curtain_ON.png");
        public static ImageSource CurtainOffTimerOff = ImageSource.FromFile("senzo_curtain_OFF.png");
        public static ImageSource Shutter_ON_icon = ImageSource.FromFile("Shutter_icon.png");
        public static ImageSource Shutter_OFF_icon = ImageSource.FromFile("Shutter_Grey_icon.png");
        public static ImageSource SwitchOnTimerOff = ImageSource.FromFile("senzo_switch_buttonOnTimerOff.png");
        public static ImageSource SwitchOffTimerOff = ImageSource.FromFile("senzo_switch_buttonOffTimerOff.png");
        public static ImageSource DelayTimerOn = ImageSource.FromFile("senzo_switch_delayTimerOn.png");
        public static ImageSource DelayTimerOff = ImageSource.FromFile("senzo_switch_delayTimerOff.png");
        public static ImageSource Timer24HOn = ImageSource.FromFile("senzo_switch_24HourTimerOn.png");
        public static ImageSource Timer24HOff = ImageSource.FromFile("senzo_switch_24HourTimerOff.png");
        public static ImageSource SensorTamperOn = ImageSource.FromFile("senzo_sensor_tamper_open.png");
        public static ImageSource SensorLowBattery = ImageSource.FromFile("senzo_sensor_battery.png");
        public static ImageSource SensorUnknownStatus = ImageSource.FromFile("senzo_sensor_generic_unknown.png");
        public static ImageSource SensorMagneticOpen = ImageSource.FromFile("senzo_sensor_magnetic_open.png");
        public static ImageSource SensorMagneticClose = ImageSource.FromFile("senzo_sensor_magnetic_close.png");
        public static ImageSource SensorVibrationOpen = ImageSource.FromFile("senzo_sensor_vibration_open.png");
        public static ImageSource SensorVibrationClose = ImageSource.FromFile("senzo_sensor_vibration_close.png");
        public static ImageSource SensorPIRClose = ImageSource.FromFile("senzo_sensor_PIR_close.png");
        public static ImageSource SensorPIROpen = ImageSource.FromFile("senzo_sensor_PIR_open.png");
        public static ImageSource SensorSmokeDetectorClose = ImageSource.FromFile("senzo_sensor_smokeDetector_close.png");
        public static ImageSource SensorSmokeDetectorOpen = ImageSource.FromFile("senzo_sensor_smokeDetector_open.png");
        public static ImageSource SensorSirenClose = ImageSource.FromFile("senzo_sensor_siren_close.png");
        public static ImageSource SensorSirenOpen = ImageSource.FromFile("senzo_sensor_siren_open.png");
        public static ImageSource SensorMotionLightClose = ImageSource.FromFile("senzo_sensor_motionLight_close.png");
        public static ImageSource SensorMotionLightOpen = ImageSource.FromFile("senzo_sensor_motionLight_open.png");
        public static ImageSource SensorStatusOffline = ImageSource.FromFile("senzo_sensor_status_offline.png");
        public static ImageSource SensorStatusOnline = ImageSource.FromFile("senzo_sensor_status_online.png");
        public static ImageSource SensorStatusSetupMode = ImageSource.FromFile("senzo_sensor_status_setupmode.png");
        //Pocket remote short press image
        public static ImageSource PocketRemoteFrameBlue = ImageSource.FromFile("senzo_pocketremote_frame_blue.png");
        public static ImageSource PocketRemoteFrameGray = ImageSource.FromFile("senzo_pocketremote_frame.png");
        public static ImageSource PocketRemoteBlueButton1 = ImageSource.FromFile("senzo_pocketremote_button_1_normal.png");
        public static ImageSource PocketRemoteGrayButton1 = ImageSource.FromFile("senzo_pocketremote_button_1_disabled.png");
        public static ImageSource PocketRemoteBlueButton2 = ImageSource.FromFile("senzo_pocketremote_button_2_normal.png");
        public static ImageSource PocketRemoteGrayButton2 = ImageSource.FromFile("senzo_pocketremote_button_2_disabled.png");
        public static ImageSource PocketRemoteBlueButton3 = ImageSource.FromFile("senzo_pocketremote_button_3_normal.png");
        public static ImageSource PocketRemoteGrayButton3 = ImageSource.FromFile("senzo_pocketremote_button_3_disabled.png");
        public static ImageSource PocketRemoteBlueButton4 = ImageSource.FromFile("senzo_pocketremote_button_4_normal.png");
        public static ImageSource PocketRemoteGrayButton4 = ImageSource.FromFile("senzo_pocketremote_button_4_disabled.png");
        public static ImageSource PocketRemoteBlueButton5 = ImageSource.FromFile("senzo_pocketremote_button_5_normal.png");
        public static ImageSource PocketRemoteGrayButton5 = ImageSource.FromFile("senzo_pocketremote_button_5_disabled.png");

        //Pocket remote long press image
        public static ImageSource PocketRemoteBlueButton1LongPress = ImageSource.FromFile("senzo_pocketremote_button_1_normal_LP.png");
        public static ImageSource PocketRemoteGrayButton1LongPress = ImageSource.FromFile("senzo_pocketremote_button_1_disabled_LP.png");
        public static ImageSource PocketRemoteBlueButton2LongPress = ImageSource.FromFile("senzo_pocketremote_button_2_normal_LP.png");
        public static ImageSource PocketRemoteGrayButton2LongPress = ImageSource.FromFile("senzo_pocketremote_button_2_disabled_LP.png");
        public static ImageSource PocketRemoteBlueButton3LongPress = ImageSource.FromFile("senzo_pocketremote_button_3_normal_LP.png");
        public static ImageSource PocketRemoteGrayButton3LongPress = ImageSource.FromFile("senzo_pocketremote_button_3_disabled_LP.png");
        public static ImageSource PocketRemoteBlueButton4LongPress = ImageSource.FromFile("senzo_pocketremote_button_4_normal_LP.png");
        public static ImageSource PocketRemoteGrayButton4LongPress = ImageSource.FromFile("senzo_pocketremote_button_4_disabled_LP.png");
        public static ImageSource PocketRemoteBlueButton5LongPress = ImageSource.FromFile("senzo_pocketremote_button_5_normal_LP.png");
        public static ImageSource PocketRemoteGrayButton5LongPress = ImageSource.FromFile("senzo_pocketremote_button_5_disabled_LP.png");

        //Smart Alarm Controller status image
        public static ImageSource SmartAlarmArmDisabled = ImageSource.FromFile("ic_alert_arm_disabled.png");
        public static ImageSource SmartAlarmHomeDisabled = ImageSource.FromFile("ic_alert_home_disabled.png");
        public static ImageSource SmartAlarmReadyDisabled = ImageSource.FromFile("ic_alert_ready_disabled.png");
        public static ImageSource SmartAlarmAwayDisabled = ImageSource.FromFile("ic_alert_away_disabled.png");
        public static ImageSource SmartAlarmBypassDisabled = ImageSource.FromFile("ic_alert_bypass_disabled.png");
        public static ImageSource SmartAlarmMemoryDisabled = ImageSource.FromFile("ic_alert_memory_disabled.png");

        //Paik Wai 23042018
        public static ImageSource LockUserCardType = ImageSource.FromFile("doorlock_type_icon_card.png");
        public static ImageSource LockUserPasswordType = ImageSource.FromFile("doorlock_type_icon_password.png");
        public static ImageSource LockUserThumbType = ImageSource.FromFile("doorlock_type_icon_thumb.png");
        public static ImageSource LockSceneEnable = ImageSource.FromFile("scene_icon_color.png");
        public static ImageSource LockScenedisable = ImageSource.FromFile("scene_icon_grey.png");
        public static ImageSource SPExpired = ImageSource.FromFile("DoorLock_UI_v1_20180426_06.png");
        public static ImageSource SPStandby = ImageSource.FromFile("DoorLock_UI_v1_20180426_15.png");
        public static ImageSource SPCancel = ImageSource.FromFile("DoorLock_UI_v1_20180426_24.png");
        public static ImageSource SPInProgress = ImageSource.FromFile("doorlock_ui_v1inprogress.png");
        public static ImageSource LockFullBattery = ImageSource.FromFile("door_lock_full_battery.png");
        public static ImageSource LockEmptyBattery = ImageSource.FromFile("door_lock_low_battery.png");
        public static ImageSource TPUsed = ImageSource.FromFile("DoorLock_UI_v1_20180426_09.png");
        public static ImageSource TPUnused = ImageSource.FromFile("DoorLock_UI_v1_20180426_12.png");
        public static ImageSource LockAdminPassword = ImageSource.FromFile("door_lock_ui_icons_03.png");
        public static ImageSource LockAdminPasswordEmpty = ImageSource.FromFile("door_lock_ui_icons_02.png");


        public static ImageSource SmartAlarmArmEnabled = ImageSource.FromFile("ic_alert_arm_enabled.png");
        public static ImageSource SmartAlarmHomeEnabled = ImageSource.FromFile("ic_alert_home_enabled.png");
        public static ImageSource SmartAlarmReadyEnabled = ImageSource.FromFile("ic_alert_ready_enabled.png");
        public static ImageSource SmartAlarmAwayEnabled = ImageSource.FromFile("ic_alert_away_enabled.png");
        public static ImageSource SmartAlarmBypassEnabled = ImageSource.FromFile("ic_alert_bypass_enabled.png");
        public static ImageSource SmartAlarmMemoryEnabled = ImageSource.FromFile("ic_alert_memory_enabled.png");
        public static ImageSource SmartAlarmImage = ImageSource.FromFile("senzo_ic_group_alert_normal.png");
        public static ImageSource SmartAlarmOffline = ImageSource.FromFile("senzo_ic_group_alarm_unknown.png");

        //Overflow button
        public static ImageSource OverFlowButton = ImageSource.FromFile("senzotheme_ic_list_moreoverflow_normal.png");

        //public static Color GetRandomColors(bool randomAlpha = false)
        //{
        //    var random = new Random();
        //    return Color.FromRgba(
        //        random.Next(0, 255),
        //        random.Next(0, 255),
        //        random.Next(0, 255),
        //        randomAlpha ? random.Next(0, 255) : 255);
        //}

    }
}
