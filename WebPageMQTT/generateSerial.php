<?php

function generateUserSerial($serialNo)
{
$userSerialNo = $serialNo;

$userSerialNo = str_replace('-','',$userSerialNo);

$strModel = substr($userSerialNo,0,4);
$strChecksum = substr($userSerialNo,4,2);
$strYear = substr($userSerialNo,6,2);
$strWeek = substr($userSerialNo,8,2);
$strCounter = substr($userSerialNo,10,4);

// echo "$strCounter";

$Model = 229;

$binModel = decbin($Model);
$binYear = decbin((int)$strYear);
$binWeek = decbin((int)$strWeek);
$binCounter = decbin((int)$strCounter);

// echo $binModel.'<br>';
// echo $binYear.'<br>';
// echo $binWeek.'<br>';
// echo $binCounter.'<br>';

//$binSerialNo = $binModel.$binYear.$binWeek.$binCounter;

//$binSerialNo = $binModel.'00'.$binYear.'0'.$binWeek.'0'.$binCounter;

$binSerialNo = sprintf( '%08d',  $binModel ).sprintf( '%06d',  $binYear ).sprintf( '%06d',  $binWeek ).sprintf( '%012d',  $binCounter );

// echo $binSerialNo.'<br>';

$SerialNo = bindec($binSerialNo);

// echo $SerialNo.'<br>';

// echo 'SerialNo in Hex '.strtoupper(dechex($SerialNo)).'<br>';

$userSerial = strtoupper(dechex($SerialNo));

// $result = $userSerial . ' ' . $strCounter;

return $userSerial;

//echo sprintf( '%012d',  $binCounter );
}