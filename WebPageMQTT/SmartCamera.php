<?php
session_start();
ob_start();
ini_set('max_execution_time', 30);
include '././configuration/serverConfig.php';
include './header/headerAll.php';
?>
<!DOCTYPE html>
<html>

<head>
    <title>Camera</title>
    <link href="css/SmartCamera.css" rel="stylesheet">
    <link rel="stylesheet" href="css/Sidebar.css">
    <script type="text/javascript" src="SidebarHandler.js"></script>
</head>
<script type="text/javascript">
    function openaddCamera(GatewayCameraList) {
        var CameraArray = JSON.parse(GatewayCameraList);
        document.getElementsByClassName("CamerayArray").value = CameraArray;
        document.getElementById("addCameraForm").style.display = "block";
    }

    function addCamera() {
        var Name = document.getElementById("cameraName").value;
        var LocationName = document.getElementById("cameraLocationName").value;
        var Url = document.getElementById("cameraUrl").value;
        var Port = document.getElementById("cameraPort").value;
        var CameraArray = document.getElementsByClassName("CamerayArray").value;

        var boolSameName = false;
        var boolSameUrlPort = false;
        if (CameraArray.length != 0) {
            for (i = 0; i < CameraArray.length; i++) {
                if (Name == CameraArray[i].Name) {
                    boolSameName = true;
                    var sameName = CameraArray[i].Name;
                    break;
                } else if (Url == CameraArray[i].URL && Port == CameraArray[i].Port) {
                    boolSameUrlPort = true;
                    break;
                }
            }
        }

        var boolEmpty = false
        if (Name == null || Name == '' ||
            LocationName == null || LocationName == '' ||
            Url == null || Url == '' ||
            Port == null || Port == '') {
            boolEmpty = true;
        }

        var patternUrl = /^((http):\/\/)/;

        if (!boolSameName && !boolSameUrlPort && !boolEmpty && patternUrl.test(Url)) {
            hrefAddCamera = baseURL + "/cameraAddCamera.php/?Name=" + Name + "&LocationName=" + LocationName + "&URL=" + Url + "&Port=" + Port;
            var loader = document.getElementById("loader");
            loader.style.display = "block";
            $.getJSON(hrefAddCamera, function(data) {
                console.log(data);
                //data is the JSON string
                loader.style.display = "none";
                if (data.Command == 'AddGatewayCamera' && data.Status == true) {
                    Swal.fire(
                        'Good job!',
                        'You have create a camera',
                        'success'
                    )
                    setTimeout(function() {
                        window.location.href = "SmartCamera.php";
                    }, 2000);

                } else if (data.Status == false) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: data.Message
                    })
                }
            });
        } else if (boolEmpty) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Input cannot be empty'
            })
        } else if (boolSameName) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: sameName + ' already exist in Camera Management List'
            })
        } else if (boolSameUrlPort) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Both URL and Port already exist in one Camera Management'
            })
        } else if (!patternUrl.test(Url)) {
            Url = "http://" + Url;
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Invalid form URL. Please put your URL like this ' + Url
            })
        }
    }

    function openeditCamera(CameraInfo, GatewayCameraArray) {
        var CameraArray = JSON.parse(GatewayCameraArray);
        CameraInfo = JSON.parse(CameraInfo);
        document.getElementsByClassName("CamerayArray").value = CameraArray;
        document.getElementById("cameraEditDeviceId").value = CameraInfo.DeviceId;
        document.getElementById("cameraEditName").value = CameraInfo.Name;
        document.getElementById("cameraEditLocationName").value = CameraInfo.LocationName;
        document.getElementById("cameraEditUrl").value = CameraInfo.URL;
        document.getElementById("cameraEditPort").value = CameraInfo.Port;
        document.getElementById("editCameraForm").style.display = "block";
    }

    function editCamera() {
        var DeviceId = document.getElementById("cameraEditDeviceId").value;
        var Name = document.getElementById("cameraEditName").value;
        var LocationName = document.getElementById("cameraEditLocationName").value;
        var Url = document.getElementById("cameraEditUrl").value;
        var Port = document.getElementById("cameraEditPort").value;
        var CameraArray = document.getElementsByClassName("CamerayArray").value;

        var boolSameName = false;
        var boolSameUrlPort = false;
        if (CameraArray.length != 0) {
            for (i = 0; i < CameraArray.length; i++) {

                if (DeviceId != CameraArray[i].DeviceId) {
                    if (Name == CameraArray[i].Name) {
                        boolSameName = true;
                        var sameName = CameraArray[i].Name;
                        break;
                    } else if (Url == CameraArray[i].URL && Port == CameraArray[i].Port) {
                        boolSameUrlPort = true;
                        break;
                    }
                }
            }
        }

        var boolEmpty = false
        if (Name == null || Name == '' ||
            LocationName == null || LocationName == '' ||
            Url == null || Url == '' ||
            Port == null || Port == '') {
            boolEmpty = true;
        }

        var patternUrl = /^((http):\/\/)/;

        if (!boolSameName && !boolSameUrlPort && !boolEmpty && patternUrl.test(Url)) {
            hrefUpdateCamera = baseURL + "/cameraUpdateCamera.php/?DeviceId=" + DeviceId + "&Name=" + Name + "&LocationName=" + LocationName + "&URL=" + Url + "&Port=" + Port;
            var loader = document.getElementById("loader");
            loader.style.display = "block";
            $.getJSON(hrefUpdateCamera, function(data) {
                console.log(data);
                //data is the JSON string
                loader.style.display = "none";
                if (data.Command == 'UpdateGatewayCamera' && data.Status == true) {
                    Swal.fire(
                        'Good job!',
                        'Gateway Camera has been successfully updated',
                        'success'
                    )
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
                } else if (data.Status == false) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: data.Message
                    })
                }
            });
        } else if (boolEmpty) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Input cannot be empty'
            })
        } else if (boolSameName) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: sameName + ' already exist in Camera Management List'
            })
        } else if (boolSameUrlPort) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Both URL and Port already exist in one Camera Management'
            })
        } else if (!patternUrl.test(Url)) {
            Url = "http://" + Url;
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Invalid form URL. Please put your URL like this ' + Url
            })
        }
    }

    function deleteCamera(CameraId) {
        var hrefDeleteCamera = baseURL + "/cameraDeleteCamera.php/?DeviceId=" + CameraId;
        var loader = document.getElementById("loader");

        Swal.fire({
            title: 'Are you sure want to delete this Camera?',
            text: "Note: All information belong to This Camera will be deleted if you proceed",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                loader.style.display = "block";
                $.getJSON(hrefDeleteCamera, function(data) {
                    console.log(data);
                    //data is the JSON string
                    loader.style.display = "none";
                    if (data.Command == 'DeleteGatewayCamera' && data.Status == true) {
                        Swal.fire(
                            'Good job!',
                            'Gateway Camera Successfully Deleted',
                            'success'
                        )
                        setTimeout(function() {
                            window.location.href = "SmartCamera.php";
                        }, 2000);

                    } else if (data.Status == false) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Failed',
                            text: data.Message
                        })
                    }
                });
            }
        })
    }

    function closeForm(id) {
        document.getElementById(id).style.display = "none";
    }

    //For validation hours and minutes
    $(document).ready(function() {
        function setInputFilter(textbox, inputFilter) {
            ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop", "number"].forEach(function(event) {
                textbox.addEventListener(event, function() {
                    if (inputFilter(this.value)) {
                        this.oldValue = this.value;
                        this.oldSelectionStart = this.selectionStart;
                        this.oldSelectionEnd = this.selectionEnd;
                    } else if (this.hasOwnProperty("oldValue")) {
                        this.value = this.oldValue;
                        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                    } else {
                        this.value = "";
                    }
                });
            });
        }
        setInputFilter(document.getElementById("cameraPort"), function(value) {
            return /^\d*$/.test(value);
        });

        setInputFilter(document.getElementById("cameraEditPort"), function(value) {
            return /^\d*$/.test(value);
        });

        setInputFilter(document.getElementById("cameraName"), function(value) {
            return /^[0-9a-zA-Z \_]*$/i.test(value);
        });

        setInputFilter(document.getElementById("cameraEditName"), function(value) {
            return /^[0-9a-zA-Z \_]*$/i.test(value);
        });

        setInputFilter(document.getElementById("cameraLocationName"), function(value) {
            return /^[0-9a-zA-Z \_]*$/i.test(value);
        });

        setInputFilter(document.getElementById("cameraEditLocationName"), function(value) {
            return /^[0-9a-zA-Z \_]*$/i.test(value);
        });
    });
</script>

<body>
    <?php
    $SerialGateway = $_SESSION['serialGateway'];
    $Password = $_SESSION['password'];
    $LocationListArray = $_SESSION['location'];
    $SmartSwitchStatus = $_SESSION['SmartSwitchStatus'];
    $CurtainStatus = $_SESSION['CurtainStatus'];
    $AlarmStatus = $_SESSION['AlarmStatus'];
    $ShutterStatus = $_SESSION['ShutterStatus'];
    $IRBlasterStatus = $_SESSION['IRBlasterStatus'];
    $LockStatus = $_SESSION['LockStatus'];
    $TotalDevice = $_SESSION['TotalDevice'];
    $SceneCount = $_SESSION['SceneCount'];
    $AdminPassword = $_SESSION['AdminPassword'];

    echo "<input style='display:none;' id='SerialGateway' value='$SerialGateway'>";
    echo "<input style='display:none;' id='Password' value='$Password'>";
    echo "<input style='display:none;' id='AdminPassword' value='$AdminPassword'>";

    if (isset($_POST['Logout'])) {
        header("Location: ChooseGateway.php");
        header("Refresh:0");
        ob_flush();
        session_destroy();
        exit();
    }

    if (isset($_POST['Home'])) {
        // header("Refresh:0");
        header("Location: Dashboard.php");
        ob_flush();
        exit();
    }

    $opts = array('http' => array('header' => 'Cookie: ' . $_SERVER['HTTP_COOKIE'] . "\r\n"));
    $context = stream_context_create($opts);
    session_write_close(); // unlock the file
    $url = $baseURL . "/cameraGetCamera.php";
    $CameraJson = file_get_contents($url, false, $context);
    $msgJson = json_decode($CameraJson);
    if (!empty($msgJson->Message)) {
        $Error = $msgJson->Message;
    ?>
        <script type="text/javascript">
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: '<?php echo $Error; ?>',
            }).then(function() {
                window.location.href = "ChooseGateway.php";
            });
        </script>
    <?php
        die();
    }

    function getList($msgJson)
    {
        if ($msgJson->Command == 'GetGatewayCamera') {

            $GatewayCameraListArray = $msgJson->GatewayCameraList;
            if (!empty($GatewayCameraListArray)) {
                echo "<div class='container mt-2'>";
                echo "<div class='row'>";
                $counter = 1;
                $GatewayCameraArray = json_encode($GatewayCameraListArray);
                foreach ($GatewayCameraListArray as $GatewayCameraList) {
                    $CameraInfo = json_encode($GatewayCameraList);
                    echo "<div class='col-lg-4 col-md-6 col-sm-6 item'>
                        <div class='custom-card text-center'>
                            <div class='card item-card card-block'>
                                <!-- <h4 class='item-card-title text-right'><i class='material-icons'></i></h4> -->
                                <img src='Images/DevicesView/cctv.svg' alt='Photo of sunset'>
                                <h3 class='card-title  mt-3 mb-3'>$GatewayCameraList->Name</h3>
                                <h4 class='card-text'>$GatewayCameraList->LocationName</h4>
                                <a href='$GatewayCameraList->URL:$GatewayCameraList->Port' class='btn btn-warning' target='_blank'><span class='fas fa-external-link-alt'></a>
                                <button class='btn btn-primary' onclick='openeditCamera(&#39;$CameraInfo&#39, &#39;$GatewayCameraArray&#39)'><span class='fa fa-edit'></button>
                                <button class='btn btn-danger' onclick='deleteCamera(&#39;$GatewayCameraList->DeviceId&#39)'><span class='fa fa-trash'></button>
                            </div>
                        </div>
                    </div>";
                    if ($counter % 4 == 0) {
                        echo "</div>";
                        echo "<div class='container mt-2'>";
                    }
                }
                echo "</div>";
                echo "</div>";
            } else {
                echo "<h2 class='error'><i class='fas fa-info-circle fa-5x' style='color:#45aaf2;'></i> </br></br>
                Camera Not Available</br>
                Click + at the top to add a camera</h2>";
            }
        }
    }

    ?>

    <input style='display:none;' id='IntervalId' value='none'>
    <input style='display:none;' class='CameraArray'>

    <!-- Add Camera Form -->
    <div class='form-popup' id='addCameraForm'>
        <div class='form-container' style='width:400px;padding:0px;'>
            <div class="modal-header" style='width:100%;height:60px;'>
                <button class='close' onclick='closeForm("addCameraForm")'></button>
                <h3 class='h2form'>Camera Management</h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <fieldset style='width:100%;'>
                    <label for="name">Name:</label>
                    <input type="text" id="cameraName" name="cameraName" style='width:100%;' require>
                    <label for="name">Location Name:</label>
                    <input type="text" id="cameraLocationName" name="cameraLocationName" style='width:100%;'>
                    <label for="name">Url:</label>
                    <input type="text" id="cameraUrl" name="cameraURL" style='width:100%;'>
                    <label for="name">Port:</label>
                    <input type="text" id="cameraPort" name="cameraPort" style='width:100%;'>
                </fieldset>
            </div>
            <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
                <button type="button" class="all" onclick="addCamera()">Add Camera</button>
            </div>
        </div>
    </div>

    <!-- Edit Camera Form -->
    <div class='form-popup' id='editCameraForm'>
        <div class='form-container' style='width:400px;padding:0px;'>
            <div class="modal-header" style='width:100%;height:60px;'>
                <button class='close' onclick='closeForm("editCameraForm")'></button>
                <h3 class='h2form'>Camera Management</h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <fieldset style='width:100%;'>
                    <label for="name">Name:</label>
                    <input type="text" id="cameraEditName" name="cameraName" style='width:100%;'>
                    <label for="name">Location Name:</label>
                    <input type="text" id="cameraEditLocationName" name="cameraLocationName" style='width:100%;'>
                    <label for="name">Url:</label>
                    <input type="text" id="cameraEditUrl" name="cameraURL" style='width:100%;'>
                    <label for="name">Port:</label>
                    <input type="text" id="cameraEditPort" name="cameraPort" style='width:100%;'>

                    <input type="text" style="display:none;" id="cameraEditDeviceId" name="cameraEditDeviceId">
                </fieldset>
            </div>
            <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
                <button type="button" class="all" onclick="editCamera()">Update Camera</button>
            </div>
        </div>
    </div>

    <!-- Form for sign Door Lock -->
    <div class="form-popup" id="mySiginDoorLockForm">
        <div class="form-container" style="width:400px;padding:0px;">
            <div class="modal-header" style='width:100%;height:70px;'>
                <!-- <a class='back' onclick='closeFormSigin()'><span class='fa fa-arrow-left fa-2x'></a> -->
                <h3 class='h2form'>Manage Door Lock </br> Password</h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <fieldset style='width:100%;'>
                    <label for="name">Enter Admin Password:</label>
                    <input type="password" id="password" name="DoorlockPassword" style='width:100%;'>
                </fieldset>
            </div>
            <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
                <button type="submit" class="all" onclick='openGetLock()' name="signin">Ok</button>
                <button type="button" class="all" onclick="cancelFormSigin()">Cancel</button>
            </div>
        </div>
    </div>

    <!-- SideBar + Navbar -->
    <div class="wrapper">
        <!-- Sidebar Holder -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>Senzo</h3>
                <strong>SZ</strong>
            </div>
            <ul class="list-unstyled components">
                <!-- Home Section -->
                <li>
                    <form action='SmartCamera.php' method='POST' id='NavHome' style='margin-block-end: 0'>
                        <input style='display:none;' name='Home'>
                        <a style='cursor:pointer;' class='Home'>
                            <i class="glyphicon glyphicon-home"></i>
                            Home
                        </a>
                    </form>
                </li>
                <li>
                    <!-- Device Section -->
                    <a href="#pageDevice" data-toggle="collapse" aria-expanded="false">
                        <i class="glyphicon glyphicon-tasks"></i>
                        <?php echo "Device"; ?>
                    </a>
                    <ul class="collapse list-unstyled" id="pageDevice">
                        <?php
                        if ($SmartSwitchStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectSwitch()">Smart Switch</a></li>';
                        }
                        if ($CurtainStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectCurtain()">Smart Curtain</a></li>';
                        }
                        if ($ShutterStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectShutter()">Smart Shutter</a></li>';
                        }
                        if ($LockStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectDoorLock()">Smart Door Lock</a></li>';
                        }
                        if ($IRBlasterStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectIRBlaster()">IR Blaster</a></li>';
                        }
                        ?>
                    </ul>
                </li>
                <li>
                    <!-- Scene Section -->
                    <a style="cursor:pointer;" onclick='openScenesForm()'>
                        <i class="glyphicon glyphicon-film"></i>
                        <?php echo "Scene ($SceneCount)"; ?>
                    </a>
                </li>
                <li>
                    <!-- Security Section -->
                    <a href="#pageSecurity" data-toggle="collapse" aria-expanded="false">
                        <i class="glyphicon glyphicon-lock"></i>
                        <?php echo "Security"; ?>
                    </a>
                    <ul class="collapse list-unstyled in" id="pageSecurity">
                        <?php
                        // If smart alarm connected to gateway
                        if ($AlarmStatus) {
                            echo "<li><a style='cursor:pointer;' onclick='selectAlarm(&#39;True&#39;)'>
                            Smart Alarm <img src='Images/Action/security_armed_icon.png' alt='scenes' style='width:20px;height:20px;'>";
                        } else {
                            echo "<li><a style='cursor:pointer;' onclick='selectAlarm(&#39;False&#39;)'>
                            Smart Alarm";
                        }
                        ?>
                        </a>
                </li>
                <li><a id="cube" style="cursor:pointer;" onclick="selectCamera()">Camera</a></li>
            </ul>
            </li>
            <li>
                <!-- Analytic Section -->
                <a style='cursor:pointer;' onclick='openAnalyticForm()'>
                    <i class='glyphicon glyphicon-stats'></i>
                    Analytic
                </a>
            </li>
            <li>
                <!-- Setting Section -->
                <a style='cursor:pointer;' onclick='openSettingForm()'>
                    <i class='glyphicon glyphicon-cog'></i>
                    Setting
                </a>
            </li>
            </ul>

            <!-- Logout button -->
            <ul class="list-unstyled CTAs">
                <li>
                    <form method="POST" action="SceneList.php" id='NavLogout' style='margin-block-end: 0'>
                        <input style='display:none;' name='Logout'>
                        <a style="cursor:pointer;" class="Logout">
                            <i class="glyphicon glyphicon-log-out"></i>
                            Logout
                        </a>
                    </form>
                </li>
            </ul>
        </nav>

        <!-- Page Content Holder -->
        <div id="content" style="width: -webkit-fill-available;">

            <nav class="navbar navbar-default">
                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                            <i class="glyphicon glyphicon-align-left"></i>
                            <span></span>
                        </button>

                        <!-- For responsive part -->
                        <!-- Show and hide content that been targeted-->
                        <button class="btn btn-dark navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <i class="glyphicon glyphicon-align-justify"></i>
                        </button>
                    </div>

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <?php
                                $GatewayCameraListArray = $msgJson->GatewayCameraList;
                                $GatewayCameraArray = json_encode($GatewayCameraListArray);
                                echo "<a style='cursor:pointer;' onclick='openaddCamera(&#39;$GatewayCameraArray&#39;)'><i class='fas fa-plus'></i> Add Camera</a>";
                                ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <h2>List of Available Camera</h2>
            <?php
            $GatewayLevel = $_SESSION['level'];
            echo "<h4 class='Level' >Gateway $GatewayLevel</h4>";
            getList($msgJson)
            ?>
            <div class="line"></div>
        </div>
    </div>

    <!-- Auto Refresh dropdown -->
    <div class='autorefresh'>
        <div class="dropdown">
            <button class="btnRefresh dropdown-toggle" type="button" data-toggle="dropdown">
                <img src='Images/Switch/settings_gear.png' alt='switchon' style='width:30px;height:30px;animation: spin 3s linear infinite;'>
            </button>
            <ul class="dropdown-menu">
                <!-- <li><a href="#">Phantom</a></li> -->
                <li>
                    <input type='checkbox' id='enable' name='fooby[2][]'>
                    <label> Enable</label><br>
                    <div id='timeRefresh' style='display:none;margin-left:10px;'>
                        <input type='checkbox' value='1-minute' class="slectOne" id='1-minute'>
                        <label> 1 minute</label><br>
                        <input type='checkbox' value='5-minutes' class="slectOne" id='5-minutes'>
                        <label> 5 minutes</label><br>
                        <input type='checkbox' value='10-minutes' class="slectOne" id='10-minutes'>
                        <label> 10 minutes</label><br>
                        <input type='checkbox' value='20-minutes' class="slectOne" id='20-minutes'>
                        <label> 20 minutes</label><br>
                        <input type='checkbox' value='25-minutes' class="slectOne" id='25-minutes'>
                        <label> 25 minutes</label><br>
                        <input type='checkbox' value='1-hour' class="slectOne" id='1-hour'>
                        <label> 1 hour</label><br>
                    </div>
                </li>
                <li>
                    <input type='checkbox' id='disable' value='disable'>
                    <label> Disable</label><br>
                </li>
            </ul>
        </div>
    </div>

    <!-- Loading animation -->
    <div class="form-popup" id='loader'>
        <!-- <div id="loadering" class="loader"></div> -->
        <div class="loading-container">
            <div class="loading"></div>
            <div class="loading-text">loading</div>
        </div>
    </div>
</body>

</html>