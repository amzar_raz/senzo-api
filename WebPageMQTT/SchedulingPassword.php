<?php
ini_set('max_execution_time', 30);
session_start();
ob_start();
//Include all file needed to use in this page
include '././configuration/serverConfig.php';
include './header/headerAll.php';
?>

<!DOCTYPE html>
<html>

<head>
    <title>Smart Door Lock</title>
    <link href="css/ScheduleTimePassword.css" rel="stylesheet">
    <link rel="stylesheet" href="css/Sidebar.css">
    <!-- Sidebar and auto refresh function -->
    <script type="text/javascript" src="SidebarHandler.js"></script>
</head>
<script type="text/javascript">
    function openScheduleTimeForm(SchedulingInfoWithId) {

        var fields = SchedulingInfoWithId.split(" ");
        var id = fields[0];
        var ScheduleInfo = fields[1];

        var temp = JSON.parse(ScheduleInfo);
        var tempLength = temp.length;
        //To find number of available schedule password 
        var avail = tempLength == 0 ? "3" : tempLength == 1 ? "2" : tempLength == 2 ? "1" : tempLength == 3 ? "0" : "";
        var bool = true;
        if (avail == 0) {
            bool = false;
        }

        if (bool == true) {
            document.getElementsByClassName("ScheduleTimePass").value = ScheduleInfo;
            document.getElementsByClassName("ScheduleTimeId").value = id;
            document.getElementById("myScheduleTimeForm").style.display = "block";
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Full',
                text: 'All password utilised'
            })
        }

    }

    function addScheduleTimePassword() {

        var tempInfo = document.getElementsByClassName("ScheduleTimePass").value;
        var LockId = document.getElementsByClassName("ScheduleTimeId").value;
        var password = document.getElementById("pass").value;
        var dateFrom = document.getElementById("dateFrom").value;
        var timeFrom = document.getElementById("timeFrom").value;
        var dateTo = document.getElementById("dateTo").value;
        var timeTo = document.getElementById("timeTo").value;

        //Find password id
        parsedtempInfo = JSON.parse(tempInfo);
        var PIdArray = [];
        if (parsedtempInfo.length == 0) {
            var PId = 1;
        } else {
            for (i = 0; i < parsedtempInfo.length; i++) {
                for (j = 1; j <= 3; j++) {
                    if (parsedtempInfo[i].PId == j) {
                        PIdArray.push(j);
                    }
                }

            }

            for (i = 1; i <= 3; i++) {
                if (!PIdArray.includes(i)) {
                    var PId = i;
                    break;
                }
            }
        }

        //Find empty password
        var boolEmpty = false;
        if (password.length == 0) {
            boolEmpty = true;
        }

        //Find password must not less than 6 number
        var boolLess = false;
        if (password.length > 0 && password.length < 6) {
            boolLess = true;
        }

        //Find password must numeric character only
        var numbers = /^[0-9]+$/;
        var boolNum = false;
        if (!password.match(numbers)) {
            boolNum = true;
        }

        //Find password overlapping
        var boolPass = false;
        for (i = 0; i < parsedtempInfo.length; i++) {
            if (parsedtempInfo[i].SP == password) {
                boolPass = true;
                break;
            }
        }

        //Input start date and time
        var fields = dateFrom.split("-")
        //Convert to mm/dd/yyyy for comparison
        var fromDate = fields[1] + "/" + fields[0] + "/" + fields[2];
        //Convert to dd/mm/yyyy to send into url
        var newFromDate = fields[0] + "/" + fields[1] + "/" + fields[2];

        var index = timeFrom.split(":");
        var fromhours = index[0];
        var fromminutes = index[1];
        var inputFromDate = new Date(fromDate);
        inputFromDate.setHours(fromhours);
        inputFromDate.setMinutes(fromminutes);

        //Input end date and time
        var fieldsTo = dateTo.split("-")
        //Convert to mm/dd/yyyy for comparison
        var toDate = fieldsTo[1] + "/" + fieldsTo[0] + "/" + fieldsTo[2];
        //Convert to dd/mm/yyyy to send into url
        var newtoDate = fieldsTo[0] + "/" + fieldsTo[1] + "/" + fieldsTo[2];

        var indexTo = timeTo.split(":");
        var tohours = indexTo[0];
        var tominutes = indexTo[1];
        var inputToDate = new Date(toDate);
        inputToDate.setHours(tohours);
        inputToDate.setMinutes(tominutes);

        //Current date
        var today = new Date();

        //Find date overlapping
        var boolError = false;
        for (i = 1; i <= parsedtempInfo.length; i++) {
            window['moment-range'].extendMoment(moment);

            var dateStart = parsedtempInfo[i - 1].SPDateStart;
            var timeStart = parsedtempInfo[i - 1].SPTimeStart;
            var dateEnd = parsedtempInfo[i - 1].SPDateEnd;
            var timeEnd = parsedtempInfo[i - 1].SPTimeEnd;

            var d1 = dateStart.split("/");
            var d1Date = d1[1] + "/" + d1[0] + "/" + d1[2];
            var indexd1 = timeStart.split(":");
            var d1hours = indexd1[0];
            var d1minutes = indexd1[1];
            var getd1Date = new Date(d1Date);
            getd1Date.setHours(d1hours);
            getd1Date.setMinutes(d1minutes);

            var d2 = dateEnd.split("/");
            var d2Date = d2[1] + "/" + d2[0] + "/" + d2[2];
            var indexd2 = timeEnd.split(":");
            var d2hours = indexd2[0];
            var d2minutes = indexd2[1];
            var getd2Date = new Date(d2Date);
            getd2Date.setHours(d2hours);
            getd2Date.setMinutes(d2minutes);

            const range1 = moment.range(inputFromDate, inputToDate);
            const range2 = moment.range(getd1Date, getd2Date);
            if (range1.overlaps(range2)) {
                // window.alert("Date Overlapping with no : " + i);
                boolError = true;
            }

        }

        var loader = document.getElementById("loader");
        if (inputFromDate >= today && inputToDate >= today && inputToDate > inputFromDate && !boolError && !boolPass && !boolEmpty && !boolLess && !boolNum) {
            var hrefSetLockSchedulingPassword = baseURL + "/doorSetLockSchedule.php/?LockId=" + LockId + "&PId=" + PId +
                "&SP=" + password + "&SPDateStart=" + newFromDate + "&SPTimeStart=" + timeFrom + "&SPDateEnd=" + newtoDate + "&SPTimeEnd=" + timeTo;
            loader.style.display = "block";
            $.getJSON(hrefSetLockSchedulingPassword, function(data) {
                loader.style.display = "none";
                if (data.Command == 'SetLockSchedulingPassword' && data.Status == false) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: data.Message
                    })
                } else {
                    Swal.fire(
                        'Good job!',
                        'Scheduling password is successfully set',
                        'success'
                    )
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
                }
            });
        } else if (boolEmpty) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Please input your password'
            })
        } else if (boolLess) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Your password must be 6 digit'
            })
        } else if (boolPass) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Scheduling password cannot be same'
            })
        } else if (boolError) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Scheduling date is overlapping'
            })
        } else if (boolNum) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Password must numeric character only'
            })
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Please check your time correctly'
            })
        }

    }

    function editScheduleTimeForm(LockListInfo) {
        var fields = LockListInfo.split(" ");
        var LockId = fields[0];
        var PId = fields[1];
        var ScheduleInfo = fields[2];

        parsedtempInfo = JSON.parse(ScheduleInfo);
        for (i = 0; i < parsedtempInfo.length; i++) {
            if (LockId == parsedtempInfo[i].LockId && PId == parsedtempInfo[i].PId) {
                //Input start date
                var fields = parsedtempInfo[i].SPDateStart.split("/")
                //Convert to mm/dd/yyyy
                var fromDate = fields[0] + "-" + fields[1] + "-" + fields[2];

                //Input end date
                var fields = parsedtempInfo[i].SPDateEnd.split("/")
                //Convert to mm/dd/yyyy
                var toDate = fields[0] + "-" + fields[1] + "-" + fields[2];

                document.getElementById("passEdit").value = parsedtempInfo[i].SP;
                document.getElementById("dateFromEdit").value = fromDate;
                document.getElementById("timeFromEdit").value = parsedtempInfo[i].SPTimeStart;
                document.getElementById("dateToEdit").value = toDate;
                document.getElementById("timeToEdit").value = parsedtempInfo[i].SPTimeEnd;

            }
        }

        document.getElementsByClassName("ScheduleTimePId").value = PId;
        document.getElementsByClassName("ScheduleTimeId").value = LockId;
        document.getElementsByClassName("ScheduleTimePass").value = ScheduleInfo;
        document.getElementById("editScheduleTimeForm").style.display = "block";
    }

    function editScheduleTimePassword() {

        var tempInfo = document.getElementsByClassName("ScheduleTimePass").value;
        var LockId = document.getElementsByClassName("ScheduleTimeId").value;
        var PId = document.getElementsByClassName("ScheduleTimePId").value;
        var password = document.getElementById("passEdit").value;
        var dateFrom = document.getElementById("dateFromEdit").value;
        var timeFrom = document.getElementById("timeFromEdit").value;
        var dateTo = document.getElementById("dateToEdit").value;
        var timeTo = document.getElementById("timeToEdit").value;

        parsedtempInfo = JSON.parse(tempInfo);

        //Find empty password
        var boolEmpty = false;
        if (password.length == 0) {
            boolEmpty = true;
        }

        //Find password must not less than 6 number
        var boolLess = false;
        if (password.length > 0 && password.length < 6) {
            boolLess = true;
        }

        //Find password must numeric character only
        var numbers = /^[0-9]+$/;
        var boolNum = false;
        if (!password.match(numbers)) {
            boolNum = true;
        }

        //Find password overlapping
        var boolPass = false;
        for (i = 0; i < parsedtempInfo.length; i++) {
            if (parsedtempInfo[i].SP == password) {
                boolPass = true;
                break;
            }
        }

        //Input start date and time
        var fields = dateFrom.split("-")
        //Convert to mm/dd/yyyy for comparison
        var fromDate = fields[1] + "/" + fields[0] + "/" + fields[2];
        //Convert to dd/mm/yyyy to send into url
        var newFromDate = fields[0] + "/" + fields[1] + "/" + fields[2];

        var index = timeFrom.split(":");
        var fromhours = index[0];
        var fromminutes = index[1];
        var inputFromDate = new Date(fromDate);
        inputFromDate.setHours(fromhours);
        inputFromDate.setMinutes(fromminutes);

        //Input end date and time
        var fieldsTo = dateTo.split("-")
        //Convert to mm/dd/yyyy for comparison
        var toDate = fieldsTo[1] + "/" + fieldsTo[0] + "/" + fieldsTo[2];
        //Convert to dd/mm/yyyy to send into url
        var newtoDate = fieldsTo[0] + "/" + fieldsTo[1] + "/" + fieldsTo[2];

        var indexTo = timeTo.split(":");
        var tohours = indexTo[0];
        var tominutes = indexTo[1];
        var inputToDate = new Date(toDate);
        inputToDate.setHours(tohours);
        inputToDate.setMinutes(tominutes);


        //Current date
        var today = new Date();

        //Find date overlapping
        var boolError = false;
        for (i = 1; i <= parsedtempInfo.length; i++) {
            window['moment-range'].extendMoment(moment);

            var dateStart = parsedtempInfo[i - 1].SPDateStart;
            var timeStart = parsedtempInfo[i - 1].SPTimeStart;
            var dateEnd = parsedtempInfo[i - 1].SPDateEnd;
            var timeEnd = parsedtempInfo[i - 1].SPTimeEnd;

            var d1 = dateStart.split("/");
            var d1Date = d1[1] + "/" + d1[0] + "/" + d1[2];
            var indexd1 = timeStart.split(":");
            var d1hours = indexd1[0];
            var d1minutes = indexd1[1];
            var getd1Date = new Date(d1Date);
            getd1Date.setHours(d1hours);
            getd1Date.setMinutes(d1minutes);

            var d2 = dateEnd.split("/");
            var d2Date = d2[1] + "/" + d2[0] + "/" + d2[2];
            var indexd2 = timeEnd.split(":");
            var d2hours = indexd2[0];
            var d2minutes = indexd2[1];
            var getd2Date = new Date(d2Date);
            getd2Date.setHours(d2hours);
            getd2Date.setMinutes(d2minutes);

            if (parsedtempInfo[i - 1].PId != PId) {
                const range1 = moment.range(inputFromDate, inputToDate);
                const range2 = moment.range(getd1Date, getd2Date);
                if (range1.overlaps(range2)) {
                    // window.alert("Date Overlapping with no : " + i);
                    boolError = true;
                }
            }

        }

        var loader = document.getElementById("loader");
        if (inputFromDate >= today && inputToDate >= today && inputToDate > inputFromDate && !boolError && !boolPass && !boolEmpty && !boolLess && !boolNum) {
            var hrefSetLockSchedulingPassword = baseURL + "/doorSetLockSchedule.php/?LockId=" + LockId + "&PId=" + PId +
                "&SP=" + password + "&SPDateStart=" + newFromDate + "&SPTimeStart=" + timeFrom + "&SPDateEnd=" + newtoDate + "&SPTimeEnd=" + timeTo;
            loader.style.display = "block";
            $.getJSON(hrefSetLockSchedulingPassword, function(data) {
                loader.style.display = "none";
                if (data.Command == 'SetLockSchedulingPassword' && data.Status == false) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: data.Message
                    })
                } else {
                    Swal.fire(
                        'Good job!',
                        'Scheduling password is successfully set',
                        'success'
                    )
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
                }
            });
        } else if (boolEmpty) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Please input your password'
            })
        } else if (boolLess) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Your password must be 6 digit'
            })
        } else if (boolPass) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Scheduling password cannot be same'
            })
        } else if (boolError) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Scheduling date is overlapping'
            })
        } else if (boolNum) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Password must numeric character only'
            })
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Please check your time correctly'
            })
        }
    }

    function deleteScheduleTimePassword(LockListInfo) {
        var fields = LockListInfo.split(" ");
        var LockId = fields[0];
        var PId = fields[1];

        var hrefDeleteSchedulePassword = baseURL + "/doorDeleteSchedule.php/?LockId=" + LockId + "&PId=" + PId;
        var loader = document.getElementById("loader");

        Swal.fire({
            title: 'Confirmation',
            text: "Are you sure you want to delete this password?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                loader.style.display = "block";
                $.getJSON(hrefDeleteSchedulePassword, function(data) {
                    loader.style.display = "none";
                    if (data.Command == 'DeleteSchedulingPassword' && data.Status == false) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Failed',
                            text: data.Message
                        })
                    } else {
                        Swal.fire(
                            'Good job!',
                            'Password Successfully Deleted',
                            'success'
                        )
                        setTimeout(function() {
                            location.reload();
                        }, 2000);
                    }

                });
            }
        })
    }

    function openHistoryForm() {
        var historyForm = document.getElementById("historyForm");
        if (historyForm) {
            historyForm.style.display = "block";
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Error while retrieving scheduling history'
            })
        }
    }

    //Add Start Timepicker function
    $(function() {
        $('#datetimepickerFrom').datetimepicker({
            format: 'HH:mm'

        });
    });

    //Add End Timepicker function
    $(function() {
        $('#datetimepickerTo').datetimepicker({
            format: 'HH:mm'

        });
    });

    //Add Start Datepicker function
    $(function() {
        $("#datepickerFrom").datepicker({
            autoclose: true,
            todayHighlight: true,
            // format: 'mm-dd-yyyy'
            format: 'dd-mm-yyyy'
        }).datepicker('update', new Date());
    });

    //Add Emd Datepicker function
    $(function() {
        $("#datepickerTo").datepicker({
            autoclose: true,
            todayHighlight: true,
            // format: 'mm-dd-yyyy'
            format: 'dd-mm-yyyy'
        }).datepicker('update', new Date());
    });

    //Edit Start Timepicker function
    $(function() {
        $('#datetimepickerFromEdit').datetimepicker({
            format: 'HH:mm'

        });
    });

    //Edit End Timepicker function
    $(function() {
        $('#datetimepickerToEdit').datetimepicker({
            format: 'HH:mm'

        });
    });

    //Edit Start Datepicker function
    $(function() {
        $("#datepickerFromEdit").datepicker({
            autoclose: true,
            todayHighlight: true,
            // format: 'mm-dd-yyyy'
            format: 'dd-mm-yyyy'
        }).datepicker('update', new Date());
    });

    //Edit End Datepicker function
    $(function() {
        $("#datepickerToEdit").datepicker({
            autoclose: true,
            todayHighlight: true,
            // format: 'mm-dd-yyyy'
            format: 'dd-mm-yyyy'
        }).datepicker('update', new Date());
    });

    function closeForm(id) {
        document.getElementById(id).style.display = "none";
    }

    //For additional info in the beginning
    $(function() {
        var myLock = document.getElementById("lockEmpty");
        var myHistory = document.getElementById("historyEmpty");

        if (myHistory) {
            $("#historyEmpty").append(
                '<div class="alert alert-info" role="alert" style="text-align:center;">' +
                '<h4 class="alert-heading">Additional Info!</h4>' +
                '<p>To create new password, click on ADD PASSWORD</p>' +
                '</div>'
            );
        } else if (myLock) {
            $("#lockEmpty").append(
                '<div class="alert alert-info" role="alert" style="text-align:center;">' +
                '<h4 class="alert-heading">Additional Info!</h4>' +
                '<p>The used password now located in PASSWORD HISTORY</p>' +
                '</div>'
            );
        }
    });
</script>

<body>
    <?php
    //Get all the session variable from Dashboard.php
    $SerialGateway = $_SESSION['serialGateway'];
    $Password = $_SESSION['password'];
    $LocationListArray = $_SESSION['location'];
    $SmartSwitchStatus = $_SESSION['SmartSwitchStatus'];
    $CurtainStatus = $_SESSION['CurtainStatus'];
    $AlarmStatus = $_SESSION['AlarmStatus'];
    $ShutterStatus = $_SESSION['ShutterStatus'];
    $IRBlasterStatus = $_SESSION['IRBlasterStatus'];
    $LockStatus = $_SESSION['LockStatus'];
    $TotalDevice = $_SESSION['TotalDevice'];
    $SceneCount = $_SESSION['SceneCount'];
    $AdminPassword = $_SESSION['AdminPassword'];

    //Get Serial and Lock id from SmartDoorLock page
    $SerialNo = ($_POST['SerialNo']);
    $LockId = ($_POST['LockId']);

    echo "<input style='display:none;' id='SerialGateway' value='$SerialGateway'>";
    echo "<input style='display:none;' id='Password' value='$Password'>";
    echo "<input style='display:none;' id='AdminPassword' value='$AdminPassword'>";

    if (isset($_POST['Logout'])) {
        header("Location: ChooseGateway.php");
        header("Refresh:0");
        ob_flush();
        session_destroy();
        exit();
    }

    if (isset($_POST['Home'])) {
        // header("Refresh:0");
        header("Location: Dashboard.php");
        ob_flush();
        exit();
    }

    $opts = array('http' => array('header' => 'Cookie: ' . $_SERVER['HTTP_COOKIE'] . "\r\n"));  //Create array to send session id in the cookie
    $context = stream_context_create($opts);     //Creates and returns a stream context
    session_write_close();  // Write session data and end session 
    $url = $baseURL . "/doorSchedulePassword.php/?LockId=$LockId&SerialNo=$SerialNo";   //Create API url to get schedule password and temporary history
    $SchedulingString = file_get_contents($url, false, $context);   //Reads entire file into a string
    $SchedulingString = explode("&#&", $SchedulingString);      //Split the string
    $msgJsonSchedulingPassword = json_decode($SchedulingString[0]); //Convert schedule password or error message from json string 
    $msgJson = $msgJsonSchedulingPassword;

    if (!empty($msgJson->Message)) {
        $Error = $msgJson->Message;
    ?>
        <script type="text/javascript">
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: '<?php echo $Error; ?>',
            }).then(function() {
                window.location.href = "ChooseGateway.php";
            });
        </script>
    <?php
        die();
    } else {
        $msgJsonSchedulingHistory = json_decode($SchedulingString[1]);
        //To decompress all the data from gateway
        // $Compression = $msgJsonSchedulingHistory->Compression;
        // $decode = base64_decode($Compression);
        // $Decompress = gzdecode($decode);
        // $msgJsonSchedulingHistory =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));
    }

    function getList($msgJsonPassword, $TotalSchedulingHistoryLog)
    {
        //To decompress the whole schedule password data
        $Compression = $msgJsonPassword->Compression;
        $decode = base64_decode($Compression);
        $Decompress = gzdecode($decode);
        $msgJsonPassword =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));

        if ($msgJsonPassword->Command == 'GetLockSchedulingPasswordAPI' && $msgJsonPassword->Reply == true) {

            //To decompress the schedule password list array
            $Compress = $msgJsonPassword->Compress;
            $decode = base64_decode($Compress);
            $Decompress = gzdecode($decode);
            $msgJsonPassword =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));

            $SerialNo = ($_POST['SerialNo']);
            $LockId = ($_POST['LockId']);
            $Name = ($_POST['LockName']);

            $SchedulingPasswordListArray = $msgJsonPassword->LockSchedulingPasswordList;

            echo "<h4 class='title nameDevice'>Device : $Name</h4>";
            $count = count($SchedulingPasswordListArray);
            if ($count == 0) {
                echo "<h4 class='title' style='margin-top: 30px;'>Available : 3 </h4>";
            } else if ($count == 1) {
                echo "<h4 class='title' style='margin-top: 30px;'>Available : 2 </h4>";
            } else if ($count == 2) {
                echo "<h4 class='title' style='margin-top: 30px;'>Available : 1 </h4>";
            } else {
                echo "<h4 class='title' style='margin-top: 30px;'>Available : 0 </h4>";
            }

            $SchedulingInfo = json_encode($SchedulingPasswordListArray);
            $SchedulingInfoWithId = $LockId . ' ' . $SchedulingInfo;


            foreach ($SchedulingPasswordListArray as $SchedulingPassword) {

                $LockListInfo = $SchedulingPassword->LockId . ' ' . $SchedulingPassword->PId;
                $LockListInfoEdit = $SchedulingPassword->LockId . ' ' . $SchedulingPassword->PId . ' ' . $SchedulingInfo;

                $DateTime = new DateTime;
                $DateTime->setTimeZone(new DateTimeZone("Asia/Kuala_Lumpur"));
                $dateCurrent = $DateTime->format("d/m/Y H:i");
                $dateStart = $SchedulingPassword->SPDateStart . ' ' . $SchedulingPassword->SPTimeStart;
                $dateEnd = $SchedulingPassword->SPDateEnd . ' ' . $SchedulingPassword->SPTimeEnd;

                if ($dateCurrent > $dateStart && $dateCurrent < $dateEnd) {
                    $status = 'InProgress';
                } else if ($dateCurrent > $dateStart && $dateCurrent > $dateEnd) {
                    $status = 'InProgress';
                } else {
                    $status = 'StandBy';
                }
                echo "<div class='container'>
            <div style='margin-left:auto;margin-right:auto;margin-top: 30px;'>
            <p class='pass'><img src='Images/Door/$status.png' alt='used' height='30px' width='120px'></p>
            <p class='pass'>Password : $SchedulingPassword->SP</p>
            <div class='grid-container'>
            <div class='center'><p style='color:green;'>Start Date : </p><p>$SchedulingPassword->SPDateStart</p></div>
            <div style='width:100px;'></div>
            <div class='center'><p style='color:green;'>Start Time : </p><p>$SchedulingPassword->SPTimeStart</p></div>
            </div>
            <div class='grid-container'>
            <div class='center'><p style='color:red;'>End Date : </p><p>$SchedulingPassword->SPDateEnd</p></div>
            <div style='width:100px;'></div>
            <div class='center'><p style='color:red;'>End Time : </p><p>$SchedulingPassword->SPTimeEnd</p></div>
            </div>
            </div>
            <div class='wrap1'>
            <button class='btn3' style='background:#00a8ff;' onclick='editScheduleTimeForm(&#39;$LockListInfoEdit&#39;)'><span class='fa fa-edit'></button>
            </div>
            <div class='wrap2'>
            <button class='btn3' style='background:#eb4d4b;' onclick='deleteScheduleTimePassword(&#39;$LockListInfo&#39;)'><span class='fa fa-trash'></button>
            </div>
            </div>";
            }


            echo "<div style='text-align:center;'>";
            echo "<button onclick='openScheduleTimeForm(&#39;$SchedulingInfoWithId&#39;)' class='all gap' id='btnAddPassword'>Add Password</button>";
            echo "<div class='notification-badges'>";
            echo "<button data-badge='$TotalSchedulingHistoryLog' onclick='openHistoryForm()' class='all gap' id='btnHistory' >Password History </button>";
            echo "</div></div>";

            if ($count == 0) {
                echo "<div id='historyEmpty'></div>";
            } else if ($count == 1) {
                echo "<div id='lockEmpty'></div>";
            } else if ($count == 2) {
                echo "<div id='lockEmpty'></div>";
            } else {
                echo "<div id='lockEmpty'></div>";
            }
        }
    }

    function getHistory($msgJson)
    {
        if ($msgJson->Command == 'GetLockSchedulingHistoryAPI') {

            //To decompress the schedule history list array
            $Compress = $msgJson->Compress;
            $decode = base64_decode($Compress);
            $Decompress = gzdecode($decode);
            $msgJson =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));

            $DataSchedulingPasswordListArray = $msgJson->DataSchedulingPasswordList;
            $count = 1;

            echo "<div class='form-popup' id='historyForm'>
                    <div class='form-container' style='width: 600px;padding:0px;'>
                        <div class='modal-header' style='width:100%;height:60px;'>
                            <button class='close' onclick='closeForm(&#39;historyForm&#39;)'></button>
                            <h3 class='h2form'>Schedule Password History</h3>
                        </div>
                        <div class='modal-body' style='width:100%;max-height: calc(80vh - 210px);overflow-y: auto;'>";

            foreach ($DataSchedulingPasswordListArray as $DataScheduling) {

                echo "<div class='container' style='width:max-content;'>
                        <div class='numHistory'>
                            <span class='step'>$count</span>
                        </div>
                        <div style='margin-left:auto;margin-right:auto;margin-top: 30px;'>
                            <div class='grid-container'>
                                <div class='center'><p>Password : </p><p>$DataScheduling->Password</p></div>
                                <div style='width:50px;'></div>";
                if ($DataScheduling->SPStatus == 1) {
                    echo "<div class='right'><img src='Images/Door/Used.png' alt='used' height='30px' width='120px'></div>";
                } else if ($DataScheduling->SPStatus == 2) {
                    echo "<div class='right'><img src='Images/Door/Expired.png' alt='expired' height='30px' width='120px'></div>";
                } else {
                    echo "<div class='right'><img src='Images/Door/Cancel.png' alt='cancel' height='30px' width='120px'></div>";
                }
                echo "</div>";
                echo "<div class='grid-container'>
                            <div class='center'><p style='color:green;'>Start Date : </p><p>$DataScheduling->SPStartDate</p></div>
                            <div style='width:50px;'></div>
                            <div class='center'><p style='color:green;'>Start Time : </p><p>$DataScheduling->SPStartTime</p></div>
                    </div>
                    <div class='grid-container'>
                            <div class='center'><p style='color:red;'>End Date : </p><p>$DataScheduling->SPEndDate</p></div>
                            <div style='width:50px;'></div>
                            <div class='center'><p style='color:red;'>End Time : </p><p>$DataScheduling->SPEndTime</p></div>
                    </div>
                </div>
            </div>";
                $count++;
            }
            echo "
                    </div>
                </div>
            </div>";
        }
    }

    ?>
    <input style='display:none;' id='IntervalId' value='none'>
    <input style='display:none;' class='ScheduleTimePass'>
    <input style='display:none;' class='ScheduleTimeId'>
    <input style='display:none;' class='ScheduleTimePId'>

    <!-- Schedule Time Password Form -->
    <div class="form-popup" id="myScheduleTimeForm">
        <div class="form-container" style='width: 550px;padding:0px;'>
            <div class="modal-header" style='width:100%;height:60px;'>
                <h3 class='h2form'>Create Schedule Time Use Password</h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <!-- <form> -->
                <div class='omrs-input-group'>
                    <label class='omrs-input-underlined' style='width:100%;'>
                        <label for='password'>Password :</label>
                        <input type="text" id="pass" name="password" maxlength="6" required />
                    </label>
                </div>

                <!-- From Date and Time -->
                <div class='grid-containerForm'>
                    <div class='omrs-input-group'>
                        <div class='row'>
                            <div class='col-xs-12 col-md-10'>
                                <div class=settings>
                                    <label for='quantityDate'>From </label>
                                    <div id="datepickerFrom" class="input-group date" data-date-format="mm-dd-yyyy">
                                        <input id="dateFrom" class="form-control" type="text" readonly />
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='omrs-input-group'>
                        <div class='row'>
                            <div class='col-xs-12 col-md-10'>
                                <div class='form-group'>
                                    <div class='settings'>
                                        <label for='quantityHours'>Time </label>
                                        <div class='input-group date' id='datetimepickerFrom'>
                                            <input placeholder="00:00" type='text' class='form-control' id='timeFrom' />
                                            <span class='input-group-addon'>
                                                <span class='glyphicon glyphicon-time'></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- To Date and Time -->
                <div class='grid-containerForm'>
                    <div class='omrs-input-group'>
                        <div class='row'>
                            <div class='col-xs-12 col-md-10'>
                                <div class=settings>
                                    <label for='quantityDate'>To </label>
                                    <div id="datepickerTo" class="input-group date" data-date-format="mm-dd-yyyy">
                                        <input id="dateTo" class="form-control" type="text" readonly />
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='omrs-input-group'>
                        <div class='row'>
                            <div class='col-xs-12 col-md-10'>
                                <div class='form-group'>
                                    <div class='settings'>
                                        <label for='quantityHours'>Time </label>
                                        <div class='input-group date' id='datetimepickerTo'>
                                            <input placeholder="00:00" type='text' class='form-control' id='timeTo' />
                                            <span class='input-group-addon'>
                                                <span class='glyphicon glyphicon-time'></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- </form> -->
            </div>
            <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
                <button type='button' class='all' onclick='addScheduleTimePassword()'>Ok</button>
                <button type='button' class='all' onclick='closeForm("myScheduleTimeForm")'>Cancel</button>
            </div>
        </div>
    </div>

    <!-- Edit Schedule Time Password Form -->
    <div class="form-popup" id="editScheduleTimeForm">
        <div class="form-container" style='width: 550px;padding:0px;'>
            <div class="modal-header" style='width:100%;height:60px;'>
                <h3 class='h2form'>Edit Schedule Time Use Password</h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <!-- <form> -->
                <div class='omrs-input-group'>
                    <label class='omrs-input-underlined' style='width:100%;'>
                        <label for='password'>Password :</label>
                        <input type="text" id="passEdit" name="password" maxlength="6" required />
                    </label>
                </div>

                <!-- From Date and Time -->
                <div class='grid-containerForm'>
                    <div class='omrs-input-group'>
                        <div class='row'>
                            <div class='col-xs-12 col-md-10'>
                                <div class=settings>
                                    <label for='quantityDate'>From </label>
                                    <div id="datepickerFromEdit" class="input-group date" data-date-format="mm-dd-yyyy">
                                        <input id="dateFromEdit" class="form-control" type="text" readonly />
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='omrs-input-group'>
                        <div class='row'>
                            <div class='col-xs-12 col-md-10'>
                                <div class='form-group'>
                                    <div class='settings'>
                                        <label for='quantityHours'>Time </label>
                                        <div class='input-group date' id='datetimepickerFromEdit'>
                                            <input placeholder="00:00" type='text' class='form-control' id='timeFromEdit' />
                                            <span class='input-group-addon'>
                                                <span class='glyphicon glyphicon-time'></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- To Date and Time -->
                <div class='grid-containerForm'>
                    <div class='omrs-input-group'>
                        <div class='row'>
                            <div class='col-xs-12 col-md-10'>
                                <div class=settings>
                                    <label for='quantityDate'>To </label>
                                    <div id="datepickerToEdit" class="input-group date" data-date-format="mm-dd-yyyy">
                                        <input id="dateToEdit" class="form-control" type="text" readonly />
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='omrs-input-group'>
                        <div class='row'>
                            <div class='col-xs-12 col-md-10'>
                                <div class='form-group'>
                                    <div class='settings'>
                                        <label for='quantityHours'>Time </label>
                                        <div class='input-group date' id='datetimepickerToEdit'>
                                            <input placeholder="00:00" type='text' class='form-control' id='timeToEdit' />
                                            <span class='input-group-addon'>
                                                <span class='glyphicon glyphicon-time'></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- </form> -->
            </div>
            <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
                <button type='button' class='all' onclick='editScheduleTimePassword()'>Ok</button>
                <button type='button' class='all' onclick='closeForm("editScheduleTimeForm")'>Cancel</button>
            </div>
        </div>
    </div>

    <!-- Form for sign Door Lock -->
    <div class="form-popup" id="mySiginDoorLockForm">
        <div class="form-container" style="width:400px;padding:0px;">
            <div class="modal-header" style='width:100%;height:70px;'>
                <!-- <a class='back' onclick='closeFormSigin()'><span class='fa fa-arrow-left fa-2x'></a> -->
                <h3 class='h2form'>Manage Door Lock </br> Password</h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <fieldset style='width:100%;'>
                    <label for="name">Enter Admin Password:</label>
                    <input type="password" id="password" name="DoorlockPassword" style='width:100%;'>
                </fieldset>
            </div>
            <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
                <button type="submit" class="all" onclick='openGetLock()' name="signin">Ok</button>
                <button type="button" class="all" onclick="cancelFormSigin()">Cancel</button>
            </div>
        </div>
    </div>

    <!-- SideBar + Navbar -->
    <div class="wrapper">
        <!-- Sidebar Holder -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>Senzo</h3>
                <strong>SZ</strong>
            </div>
            <ul class="list-unstyled components">
                <!-- Home Section -->
                <li>
                    <form action='SchedulingPassword.php' method='POST' id='NavHome' style='margin-block-end: 0'>
                        <input style='display:none;' name='Home'>
                        <a style='cursor:pointer;' class='Home'>
                            <i class="glyphicon glyphicon-home"></i>
                            Home
                        </a>
                    </form>
                </li>
                <li>
                    <!-- Device Section -->
                    <a href="#pageDevice" data-toggle="collapse" aria-expanded="false">
                        <i class="glyphicon glyphicon-tasks"></i>
                        <?php echo "Device"; ?>
                    </a>
                    <ul class="collapse list-unstyled in" id="pageDevice">
                        <?php
                        if ($SmartSwitchStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectSwitch()">Smart Switch</a></li>';
                        }
                        if ($CurtainStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectCurtain()">Smart Curtain</a></li>';
                        }
                        if ($ShutterStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectShutter()">Smart Shutter</a></li>';
                        }
                        if ($LockStatus) {
                            echo '<li><a id="cube" style="cursor:pointer;" onclick="openSmartDoorLock()">Smart Door Lock</a></li>';
                        }
                        if ($IRBlasterStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectIRBlaster()">IR Blaster</a></li>';
                        }
                        ?>
                    </ul>
                </li>
                <li>
                    <!-- Scene Section -->
                    <a style="cursor:pointer;" onclick='openScenesForm()'>
                        <i class="glyphicon glyphicon-film"></i>
                        <?php echo "Scene ($SceneCount)"; ?>
                    </a>
                </li>
                <li>
                    <!-- Security Section -->
                    <a href="#pageSecurity" data-toggle="collapse" aria-expanded="false">
                        <i class="glyphicon glyphicon-lock"></i>
                        <?php echo "Security"; ?>
                    </a>
                    <ul class="collapse list-unstyled" id="pageSecurity">
                        <?php
                        // If smart alarm connected to gateway
                        if ($AlarmStatus) {
                            echo "<li><a style='cursor:pointer;' onclick='selectAlarm(&#39;True&#39;)'>
                            Smart Alarm <img src='Images/Action/security_armed_icon.png' alt='scenes' style='width:20px;height:20px;'>";
                        } else {
                            echo "<li><a style='cursor:pointer;' onclick='selectAlarm(&#39;False&#39;)'>
                            Smart Alarm";
                        }
                        ?>
                        </a>
                </li>
                <li><a style="cursor:pointer;" onclick="selectCamera()">Camera</a></li>
            </ul>
            </li>
            <li>
                <!-- Analytic Section -->
                <a style='cursor:pointer;' onclick='openAnalyticForm()'>
                    <i class='glyphicon glyphicon-stats'></i>
                    Analytic
                </a>
            </li>
            <li>
                <!-- Setting Section -->
                <a style='cursor:pointer;' onclick='openSettingForm()'>
                    <i class='glyphicon glyphicon-cog'></i>
                    Setting
                </a>
            </li>
            </ul>

            <!-- Logout button -->
            <ul class="list-unstyled CTAs">
                <li>
                    <form method="POST" action="SchedulingPassword.php" id='NavLogout' style='margin-block-end: 0'>
                        <input style='display:none;' name='Logout'>
                        <a style="cursor:pointer;" class="Logout">
                            <i class="glyphicon glyphicon-log-out"></i>
                            Logout
                        </a>
                    </form>
                </li>
            </ul>
        </nav>

        <!-- Page Content Holder -->
        <div id="content" style="width: -webkit-fill-available;">

            <nav class="navbar navbar-default">
                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                            <i class="glyphicon glyphicon-align-left"></i>
                            <span></span>
                        </button>

                        <!-- For responsive part -->
                        <!-- Show and hide content that been targeted-->
                        <button class="btn btn-dark navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <i class="glyphicon glyphicon-align-justify"></i>
                        </button>
                    </div>

                    <!-- Content that have been targeted -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a style="cursor:pointer;" onclick='openSmartDoorLock()'>Smart Door Lock</a></li>
                        </ul>
                    </div>
                </div>
            </nav>

            <h2>List of Schedule Password</h2>
            <?php
            $GatewayLevel = $_SESSION['level'];
            echo "<h4 class='Level' >Gateway $GatewayLevel</h4>";
            $TotalSchedulingHistoryLog = $msgJsonSchedulingHistory->TotalSchedulingHistoryLog;
            getList($msgJsonSchedulingPassword, $TotalSchedulingHistoryLog);
            getHistory($msgJsonSchedulingHistory);
            ?>
            <div class="line"></div>

        </div>
    </div>

    <!-- Auto Refresh dropdown -->
    <div class='autorefresh'>
        <div class="dropdown">
            <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">
                <img src='Images/Switch/settings_gear.png' alt='switchon' style='width:30px;height:30px;animation: spin 3s linear infinite;'>
            </button>
            <ul class="dropdown-menu refreshdrop">
                <!-- <li><a href="#">Phantom</a></li> -->
                <li>
                    <input type='checkbox' id='enable' name='fooby[2][]'>
                    <label> Enable</label><br>
                    <div id='timeRefresh' style='display:none;margin-left:10px;'>
                        <input type='checkbox' value='1-minute' class="slectOne" id='1-minute'>
                        <label> 1 minute</label><br>
                        <input type='checkbox' value='5-minutes' class="slectOne" id='5-minutes'>
                        <label> 5 minutes</label><br>
                        <input type='checkbox' value='10-minutes' class="slectOne" id='10-minutes'>
                        <label> 10 minutes</label><br>
                        <input type='checkbox' value='20-minutes' class="slectOne" id='20-minutes'>
                        <label> 20 minutes</label><br>
                        <input type='checkbox' value='25-minutes' class="slectOne" id='25-minutes'>
                        <label> 25 minutes</label><br>
                        <input type='checkbox' value='1-hour' class="slectOne" id='1-hour'>
                        <label> 1 hour</label><br>
                    </div>
                </li>
                <li>
                    <input type='checkbox' id='disable' value='disable'>
                    <label> Disable</label><br>
                </li>
            </ul>
        </div>
    </div>

     <!-- Loading animation -->
    <div class="form-popup" id='loader'>
        <!-- <div id="loadering" class="loader"></div> -->
        <div class="loading-container">
            <div class="loading"></div>
            <div class="loading-text">loading</div>
        </div>
    </div>
</body>

</html>