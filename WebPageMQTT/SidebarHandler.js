function selectSwitch() {
  window.location.href = "SmartSwitch.php";
}

function selectCurtain() {
  window.location.href = "SmartCurtain.php";
}

function selectShutter() {
  window.location.href = "SmartShutter.php";
}

function selectDoorLock() {
  document.getElementById("mySiginDoorLockForm").style.display = "block";
}

function selectIRBlaster() {
  window.location.href = "SmartIRBlaster.php";
}

function selectCamera() {
  window.location.href = "SmartCamera.php";
}

function openScenesForm() {
  window.location.href = "SceneList.php";
}

function selectAlarm(Bool) {
  if (Bool == "True") {
    window.location.href = "SmartAlarm.php";
  } else {
    Swal.fire({
      icon: "error",
      title: "Failed",
      text: "There is no Smart Alarm connected to Gateway",
    });
  }
}

function openGetLock() {
  var AdminPassword = document.getElementById("AdminPassword").value;
  var Password = document.getElementById("password").value;

  if (Password == AdminPassword) {
    window.location.href = "SmartDoorLock.php";
  } else {
    Swal.fire({
      icon: "error",
      title: "Password Not Match",
      text: "Please re-enter admin password",
    });
  }
}

function cancelFormSigin() {
  document.getElementById("mySiginDoorLockForm").style.display = "none";
}

function openSmartDoorLock() {
  window.location.href = "SmartDoorLock.php";
}

function openAnalyticForm() {
  window.location.href = "Analytic.php";
}

function openSettingForm() {
  window.location.href = "Setting.php";
}

//For auto refresh
$(document).ready(function () {
  $("#enable").click(function () {
    var checkBox = document.getElementById("enable");
    var text = document.getElementById("timeRefresh");
    if (checkBox.checked == true) {
      text.style.display = "block";
      document.getElementById("disable").checked = false;
    } else {
      text.style.display = "none";
    }
  });
});

$(document).ready(function () {
  $("#disable").click(function () {
    var checkBox = document.getElementById("disable");
    var text = document.getElementById("timeRefresh");
    var refreshIntervalId = document.getElementById("IntervalId").value;
    if (checkBox.checked == true) {
      document.getElementById("enable").checked = false;
      text.style.display = "none";
      $(".slectOne").prop("checked", false);
      if (refreshIntervalId != 1) {
        clearInterval(refreshIntervalId);
      }
      // localStorage.input = 'disable';
      localStorage.setItem("autoRefresh", "disable");
    }
  });
});

$(document).ready(function () {
  $(".slectOne").on("change", function () {
    $(".slectOne").not(this).prop("checked", false);
    if ($(this).is(":checked")) {
      var time = $(this).val();
      var refreshIntervalId = document.getElementById("IntervalId").value;
      if (refreshIntervalId != "none") {
        clearInterval(refreshIntervalId);
      }
      if (time == "1-minute") {
        // localStorage.input = time;
        localStorage.setItem("autoRefresh", time);
        var refreshIntervalId = setInterval(function () {
          location.reload();
        }, 60000);
      } else if (time == "5-minutes") {
        // localStorage.input = time;
        localStorage.setItem("autoRefresh", time);
        var refreshIntervalId = setInterval(function () {
          location.reload();
        }, 300000);
      } else if (time == "10-minutes") {
        // localStorage.input = time;
        localStorage.setItem("autoRefresh", time);
        var refreshIntervalId = setInterval(function () {
          location.reload();
        }, 600000);
      } else if (time == "20-minutes") {
        // localStorage.input = time;
        localStorage.setItem("autoRefresh", time);
        var refreshIntervalId = setInterval(function () {
          location.reload();
        }, 1200000);
      } else if (time == "25-minutes") {
        // localStorage.input = time;
        localStorage.setItem("autoRefresh", time);
        var refreshIntervalId = setInterval(function () {
          location.reload();
        }, 1500000);
      } else if (time == "1-hour") {
        // localStorage.input = time;
        localStorage.setItem("autoRefresh", time);
        var refreshIntervalId = setInterval(function () {
          location.reload();
        }, 3600000);
      }
      document.getElementById("IntervalId").value = refreshIntervalId;
    }
  });
});

$(function () {
  var check = document.getElementsByClassName("slectOne");
  var time = localStorage.getItem("autoRefresh");

  if (time == null) {
    time == "disable";
  }

  if (time == "disable") {
    $("#disable").prop("checked", true);
  } else {
    $("#enable").prop("checked", true);
    document.getElementById("timeRefresh").style.display = "block";
    $("#" + time).prop("checked", true);
    if (time == "1-minute") {
      // localStorage.input = time;
      localStorage.setItem("autoRefresh", time);
      var refreshIntervalId = setInterval(function () {
        location.reload();
      }, 60000);
    } else if (time == "5-minutes") {
      // localStorage.input = time;
      localStorage.setItem("autoRefresh", time);
      var refreshIntervalId = setInterval(function () {
        location.reload();
      }, 300000);
    } else if (time == "10-minutes") {
      // localStorage.input = time;
      localStorage.setItem("autoRefresh", time);
      var refreshIntervalId = setInterval(function () {
        location.reload();
      }, 600000);
    } else if (time == "20-minutes") {
      // localStorage.input = time;
      localStorage.setItem("autoRefresh", time);
      var refreshIntervalId = setInterval(function () {
        location.reload();
      }, 1200000);
    } else if (time == "25-minutes") {
      // localStorage.input = time;
      localStorage.setItem("autoRefresh", time);
      var refreshIntervalId = setInterval(function () {
        location.reload();
      }, 1500000);
    } else if (time == "1-hour") {
      // localStorage.input = time;
      localStorage.setItem("autoRefresh", time);
      var refreshIntervalId = setInterval(function () {
        location.reload();
      }, 3600000);
    }
    document.getElementById("IntervalId").value = refreshIntervalId;
  }
});

//Sidebar Collapse
$(document).ready(function () {
  $("#sidebarCollapse").on("click", function () {
    $("#sidebar").toggleClass("active");
  });
});

//Home
$(document).ready(function () {
  $(".Home").click(function () {
    document.getElementById("NavHome").submit();

    // window.alert($(this).text());
  });
});

//Logout
$(document).ready(function () {
  $(".Logout").click(function () {
    Swal.fire({
      title: "Confirmation",
      text: "Are you sure you want to logout?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes",
    }).then((result) => {
      if (result.value) {
        document.getElementById("NavLogout").submit();
      }
    });
    // window.alert($(this).text());
  });
});
