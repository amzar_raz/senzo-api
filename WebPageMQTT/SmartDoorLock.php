<?php
session_start();
ob_start();
ini_set('max_execution_time', 30);
include '././configuration/serverConfig.php';
include './header/headerAll.php';
?>
<!DOCTYPE html>
<html>

<head>
    <title>Smart Door Lock</title>
    <link href="css/SmartDoorLock.css" rel="stylesheet">
    <link rel="stylesheet" href="css/Sidebar.css">
    <script type="text/javascript" src="SidebarHandler.js"></script>
</head>
<script type="text/javascript">
    function openLogForm(param) {
        var paramSplit = param.split("&&"); //---- [0]= SN, [2]=switchNo ----//
        var url = baseURL + "/doorReadLog.php/?SerialNo=" + paramSplit[0] + "&LockId=" + paramSplit[1];
        var loader = document.getElementById("loader");
        document.getElementById("logName").innerHTML = "Device: " + paramSplit[2] + "(" + paramSplit[3] + ")";
        loader.style.display = "block";
        console.log(url);
        $.getJSON(url, function(data) {
            loader.style.display = "none";
            var LogList = data.DataLockLogList;
            var logNo = 1;

            if (LogList.length == 0) {
                $("#emptyLog").
                append(
                    '<div class="divNoLog">' +
                    '<img src="Images/smiling_bell.png" alt="smiling bell" width="100" height="100" class="smiley">' +
                    '<h4 style="text-align:center;">No log yet !</h4>' +
                    '<p style="text-align:center;">Check this section to know the activity of this device.</p>' +
                    '</div>'
                );
                document.getElementById("tableLog").style.display = "none";
            } else {
                for (i = LogList.length - 1; i >= 0; i--) {
                    var timeSplit = LogList[i].DateTime.split("T");
                    var statusSplit = LogList[i].Status;

                    //Find user info
                    if (LogList[i].UserId == 0) {

                        var userInfo = 'Door Lock';
                    } else if (LogList[i].UserId == 201 || LogList[i].UserId == 201) {

                        var userInfo = 'Temporary Password';
                    } else if (LogList[i].UserId == 203 || LogList[i].UserId == 204 || LogList[i].UserId == 205) {

                        var userInfo = 'Scheduling Password';
                    } else if (LogList[i].UserId >= 1 && LogList[i].UserId <= 200) {
                        if (LogList[i].UserName.length != 0) {
                            var userInfo = LogList[i].UserName;
                        } else {
                            var userInfo = 'User : ' + LogList[i].UserId;
                        }
                    }

                    $("#DoorLogTable").
                    append(
                        '<tr class="trLogTable">' +
                        '<td>' + logNo + '</td>' +
                        '<td>' + timeSplit[0] + "<br/>" + timeSplit[1] + '</td>' +
                        '<td>' + userInfo + "<br/>" + '</td>' +
                        '<td>' + '<b>' + statusSplit + '</b>' + '</td>' +
                        '</tr>'
                    );
                    logNo++;
                }
                document.getElementById("tableLog").style.display = "block";
            }

        });
        $("#DoorLogForm").fadeIn("fast", "swing");
    }

    function backLog(id) {
        document.getElementById(id).style.display = 'none';
        if (id == "DoorLogForm") {
            if ($(".trLogTable")[0]) {
                // Do something if class exists
                $(".trLogTable").remove();
            } else {
                // Do something if class does not exist
                $(".divNoLog").remove();
            }
        }
        if (id == "UserLockForm") {
            $(".trUserLockTable").remove();
        }
        if (id == "SceneForm") {
            $(".trSceneTable").remove();
            $(".divNoScene").remove();
            document.getElementById("UserLockForm").style.display = 'block';
        }
    }

    function openRSSI(Rssi) {
        // Swal.fire(
        //     'RSSI RESULT',
        //     'The RSSI value for the switch is :' + Rssi,
        //     'info'
        // )
        iziToast.show({
            title: 'RSSI RESULT',
            message: 'The RSSI value for the switch is :' + Rssi,
            theme: 'dark',
            position: 'topCenter',
            icon: 'icon-person'
        });
    }

    function openUserLock(UserInfo) {
        var field = UserInfo.split("&&");
        var url = baseURL + "/doorGetLockUser.php/?LockId=" + field[0];
        var loader = document.getElementById("loader");
        document.getElementById("lockName").innerHTML = "Door Lock Name: " + field[1];
        document.getElementById("lockLocation").innerHTML = "Location: " + field[2];
        loader.style.display = "block";
        $.getJSON(url, function(data) {
            loader.style.display = 'none';
            var LockUserList = data.LockUserList;
            for (i = 0; i < LockUserList.length; i++) {

                if (LockUserList[i].UserType == 1) {
                    var Type = "<img src='Images/Door/icon_card.png' alt='type' style='width:50px;height:50px;'>"
                } else if (LockUserList[i].UserType == 2) {
                    var Type = "<img src='Images/Door/icon_password.png' alt='type' style='width:50px;height:50px;'>"
                } else {
                    var Type = "<img src='Images/Door/icon_thumb.png' alt='type' style='width:50px;height:50px;'>"
                }

                if (LockUserList[i].Scene == true) {
                    var Scene = '<div class="dropdown">' +
                        '<button class="btn dropdown-toggle" type="button" data-toggle="dropdown">' +
                        "<img src='Images/Scene/scene_icon_color.png' alt='scene' style='width:50px;height:50px;'>" +
                        '</button>' +
                        '<ul class="dropdown-menu">' +
                        '<li><a style="cursor:pointer;" onclick="openAddScene(&#39;' + LockUserList[i].UserId + '&#39;,' + '&#39;' + field[0] + '&#39;,' + '&#39;' + LockUserList[i].UserName + '&#39;)">Add Scene</a></li>' +
                        '<li><a style="cursor:pointer;" onclick="openDeleteScene(&#39;' + LockUserList[i].UserId + '&#39;,' + '&#39;' + field[0] + '&#39;,' + '&#39;' + LockUserList[i].UserName + '&#39;)">Delete Scene</a></li>' +
                        '</ul>' +
                        '</div>'
                } else {
                    var Scene = '<div class="dropdown">' +
                        '<button class="btn dropdown-toggle" type="button" data-toggle="dropdown">' +
                        "<img src='Images/Scene/scene_icon_grey.png' alt='scene' style='width:50px;height:50px;'>" +
                        '</button>' +
                        '<ul class="dropdown-menu">' +
                        '<li><a style="cursor:pointer;" onclick="openAddScene(&#39;' + LockUserList[i].UserId + '&#39;,' + '&#39;' + field[0] + '&#39;,' + '&#39;' + LockUserList[i].UserName + '&#39;)">Add Scene</a></li>' +
                        '<li><a style="cursor:pointer;" onclick="openDeleteScene(&#39;' + LockUserList[i].UserId + '&#39;,' + '&#39;' + field[0] + '&#39;,' + '&#39;' + LockUserList[i].UserName + '&#39;)">Delete Scene</a></li>' +
                        '</ul>' +
                        '</div>'


                }
                $("#UserLockTable").
                append(
                    '<tr class="trUserLockTable">' +
                    '<td>' + LockUserList[i].UserId + '</td>' +
                    '<td>' + Type + '</td>' +
                    '<td>' + LockUserList[i].UserName + '</td>' +
                    '<td>' + Scene + '</td>' +
                    '</tr>'
                );
            }

        });

        document.getElementById("UserLockForm").style.display = 'block';
    }

    function openAddScene(UserId, LockId, UserName) {
        document.getElementById("UserLockForm").style.display = 'none';
        var SerialGateway = document.getElementById('SerialGateway').value;
        var Password = document.getElementById('Password').value;
        var url = baseURL + "/doorGetUserScene.php";
        var loader = document.getElementById("loader");
        loader.style.display = 'block';
        console.log(url);
        $.getJSON(url, function(data) {
            console.log(data);
            loader.style.display = 'none';
            if (data.Command == 'GetScene' && data.Reply == false) {
                $("#sceneForm-body").
                append(
                    '<div class="divNoScene">' +
                    '<h4 style="text-align:center;">There is no scene detected</h4>' +
                    '<h4 style="text-align:center;">Click button below to create a Scene</h4>' +
                    '<div style="display: flex;justify-content: center;align-items: center;">' +
                    '<button class="all" onclick="openScenesForm()">Create Scene</button' +
                    '</div>' +
                    '</div>'
                );
                document.getElementById("sceneForm-table").style.display = "none";
            } else {
                var SceneList = data.SceneList;
                for (i = 0; i < SceneList.length; i++) {
                    var SceneInfo = LockId + '&&' + UserId + '&&' + UserName + '&&' + SceneList[i].Id + '&&' + SceneList[i].Name;
                    $("#SceneTable").
                    append(
                        '<tr class="trSceneTable">' +
                        '<td onclick="openConfirmation(&#39;' + SceneInfo + '&#39;)">' + SceneList[i].Name + '</td>' +
                        '</tr>'
                    );
                }
            }

        });
        document.getElementById("SceneForm").style.display = 'block';
    }

    function openConfirmation(SceneInfo) {
        var fields = SceneInfo.split('&&')
        Swal.fire({
            title: 'Confirmation',
            text: "Are you sure you want to set " + fields[4] + "?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                loader.style.display = "block";
                url = baseURL + "/doorUpdateLockUser.php/?LockId=" + fields[0] + "&UserId=" + fields[1] + "&SceneId=" + fields[3] + "&SceneEnable=1";
                $.getJSON(url, function(data) {
                    console.log(data);
                    if (data.Command == 'UpdateLockUserScene' && data.Status == false) {
                        loader.style.display = "none";
                        Swal.fire({
                            icon: 'error',
                            title: 'Failed',
                            text: data.Message
                        })
                    } else {
                        Swal.fire(
                            'Good job!',
                            data.Message,
                            'success'
                        )
                        setTimeout(function() {
                            location.reload();
                        }, 2000);
                    }

                });
            }
        })
    }

    function openDeleteScene(UserId, LockId, UserName) {
        Swal.fire({
            title: 'Confirmation',
            text: "Do you want to disable the scene for UserID: " + UserId,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                loader.style.display = "block";
                url = baseURL + "/doorDisableLockUser.php/?LockId=" + LockId + "&UserId=" + UserId + "&SceneEnable=0";
                $.getJSON(url, function(data) {
                    loader.style.display = "none";
                    console.log(data);
                    if (data.Command == 'DisableLockUserScene' && data.Status == false) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Failed',
                            text: data.Message
                        })
                    } else {
                        Swal.fire(
                            'Good job!',
                            data.Message,
                            'success'
                        )
                        setTimeout(function() {
                            location.reload();
                        }, 2000);
                    }

                });
            }
        })
    }

    //For Show/Hide SN
    $(document).ready(function() {
        $("#showSn").click(function() {

            if ($(this).prop("checked") == true) {
                $(".gatewaySNText").fadeIn("fast", "swing");
                localStorage.setItem("displayGatewaySN", $(this).val());
            } else {
                $(".gatewaySNText").fadeOut("fast", "swing");
                localStorage.setItem("displayGatewaySN", "hideSn");
            }
        });
    });

    $(function() {
        var displayGatewaySN = localStorage.getItem("displayGatewaySN");
        if (displayGatewaySN == null) {
            displayGatewaySN = 'hideSn'
        }

        if (displayGatewaySN == 'showSn') {
            $('#showSn').prop('checked', true);
            $(".gatewaySNText").css("display", "block");
        } else {
            $('#showSn').prop('checked', false);
            $(".gatewaySNText").css("display", "none");
        }
    });

    //For table
    $(document).ready(function() {
        $('#myTable').DataTable();
    });

    //For location
    $(document).ready(function() {
        $('.LocationListItem').click(function() {
            // document.getElementById("LocationListForm" + $(this).text()).submit();
            var table = $('#myTable').DataTable();
            var value = document.getElementById("Location" + $(this).text()).value;
            document.getElementsByClassName("LocationName").value = value;
            table.draw();

            if ($(".data-location").contents().length > 0) {
                $(".data-location").remove();
            }
            $("#collapseFilter").
            append(
                '<div class="data-tag data-location" id="tag' + value + '">' +
                '<span class="data-tag-text" style="margin:0px 5px;">' + value + '</span>' +
                '<span onclick="removeFilter(&#39;' + value + '&#39;)"><i class="fas fa-times"></i></span>' +
                '</div>'
            );
            document.getElementById("collapseFilter").style.display = 'block';
            // window.alert($(this).text());
        });
    });

    function removeFilter(id) {
        var tagId = document.getElementById("tag" + id);
        var table = $('#myTable').DataTable();

        document.getElementsByClassName("LocationName").value = 'Null';

        tagId.remove();
        table.draw();

        if ($(".data-tag").contents().length == 0) {
            document.getElementById("collapseFilter").style.display = "none";
        }
    }

    //For search based on location
    $.fn.dataTable.ext.search.push(
        function(settings, data, dataIndex) {
            var LocationName = document.getElementsByClassName("LocationName").value;
            if (LocationName == null) {
                LocationName = 'Null';
            }
            var Location = data[2];

            if (LocationName == Location || LocationName == 'Null') {
                return true;
            }

            return false;

        }
    );
</script>

<body>
    <?php
    $SerialGateway = $_SESSION['serialGateway'];
    $Password = $_SESSION['password'];
    $LocationListArray = $_SESSION['location'];
    $SmartSwitchStatus = $_SESSION['SmartSwitchStatus'];
    $CurtainStatus = $_SESSION['CurtainStatus'];
    $AlarmStatus = $_SESSION['AlarmStatus'];
    $ShutterStatus = $_SESSION['ShutterStatus'];
    $IRBlasterStatus = $_SESSION['IRBlasterStatus'];
    $LockStatus = $_SESSION['LockStatus'];
    $TotalDevice = $_SESSION['TotalDevice'];
    $SceneCount = $_SESSION['SceneCount'];
    $AdminPassword = $_SESSION['AdminPassword'];

    echo "<input style='display:none;' id='SerialGateway' value='$SerialGateway'>";
    echo "<input style='display:none;' id='Password' value='$Password'>";
    echo "<input style='display:none;' id='AdminPassword' value='$AdminPassword'>";

    if (isset($_POST['Logout'])) {
        header("Location: ChooseGateway.php");
        header("Refresh:0");
        ob_flush();
        session_destroy();
        exit();
    }

    if (isset($_POST['Home'])) {
        // header("Refresh:0");
        header("Location: Dashboard.php");
        ob_flush();
        exit();
    }

    $opts = array('http' => array('header' => 'Cookie: ' . $_SERVER['HTTP_COOKIE'] . "\r\n"));
    $context = stream_context_create($opts);
    session_write_close(); // unlock the file
    $url = $baseURL . "/doorSmartDoorLock.php";
    $SmartDoorLockJson = file_get_contents($url, false, $context);
    $msgJson = json_decode($SmartDoorLockJson);

    if (!empty($msgJson->Message)) {
        $Error = $msgJson->Message;
    ?>
        <script type="text/javascript">
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: '<?php echo $Error; ?>',
            }).then(function() {
                window.location.href = "ChooseGateway.php";
            });
        </script>
    <?php
        die();
    }

    function getList($msgJson)
    {
        //To decompress all the data from gateway
        $Compression = $msgJson->Compression;
        $decode = base64_decode($Compression);
        $Decompress = gzdecode($decode);
        $msgJson =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));

        if ($msgJson->Command == 'GetLockAPI' && $msgJson->Reply == true) {

            //To decompress all the data from gateway
            $Compress = $msgJson->Compress;
            $decode = base64_decode($Compress);
            $Decompress = gzdecode($decode);
            $msgJson =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));

            $SmartDoorListArray = $msgJson->LockList;
            if (!empty($SmartDoorListArray)) {

                echo "<div class='table-responsive'>";
                echo "<table id='myTable' class='table-bordered' style='width:100%;margin-top:50px;'>";
                echo "<thead>";
                echo "<tr>";
                echo "<th scope='col'>#</th>";
                echo "<th scope='col'>Name</th>";
                echo "<th scope='col'>Location Name</th>";
                echo "<th scope='col'>1 Time Password</th>";
                echo "<th scope='col'>Scheduling Password</th>";
                echo "<th scope='col'>User</th>";
                echo "<th scope='col'>Log</th>";
                echo "<th scope='col'>Battery</th>";
                echo "<th scope='col'>RSSI</th>";
                echo "</tr>";
                echo "</thead>";
                echo "<tbody id='myBody' >";

                $counter = 1;

                foreach ($SmartDoorListArray as $SmartLockList) {

                    $LockId = $SmartLockList->LockId;
                    $LockSerialNo = $SmartLockList->SerialNo;
                    $LockName = $SmartLockList->Name;

                    $LockInfo = $LockId . " " . $LockSerialNo . " " . $LockName;


                    echo "<tr class='DoorLockTable' >";
                    echo "<td>$counter</td>";
                    echo "<td>
                        <p style='margin:0px;'>$SmartLockList->Name</p>
                        <p style='margin-top:5px;' class='gatewaySNText'>$SmartLockList->UserSerialNo</p>
                        </td>";
                    echo "<td>$SmartLockList->LocationName</td>";
                    echo "<td>
                    <form action='1TimePassword.php' method='post'>
                    <input style='display:none;' value='$SmartLockList->SerialNo' name='SerialNo'  **/>
                    <input style='display:none;' value='$SmartLockList->LockId' name='LockId' **/>
                    <input style='display:none;' value='$SmartLockList->Name' name='LockName' **/>
                    <button type='submit' value='$LockInfo' name='LockInfoTemporary' class='btnForm'><img class='responsive' src='Images/Door/1TimePassword.png' alt='1Time' style='width:160px;'></button></td>
                    </form>";
                    echo "<td>
                    <form action='SchedulingPassword.php' method='post'>
                    <input style='display:none;' value='$SmartLockList->SerialNo' name='SerialNo'  **/>
                    <input style='display:none;' value='$SmartLockList->LockId' name='LockId' **/>
                    <input style='display:none;' value='$SmartLockList->Name' name='LockName' **/>
                    <button type='submit' value='$LockInfo' name='LockInfoSchedule' class='btnForm'><img class='responsive' src='Images/Door/SchedulePassword.png' alt='1Time' style='width:160px;'></button></td>
                    </form>";

                    $UserInfo = $SmartLockList->LockId . '&&' . $SmartLockList->Name . '&&' . $SmartLockList->LocationName;
                    echo "<td>
                        <button class='btn3' style='' type='button' onclick='openUserLock(&#39;$UserInfo&#39;)'><span class='fa fa-play'></button>
                        </td>";

                    $LogInfo = $SmartLockList->SerialNo . '&&' . $SmartLockList->LockId . '&&' . $SmartLockList->Name . '&&' . $SmartLockList->UserSerialNo;
                    echo "<td><button class='btn3' type='button' onclick='openLogForm(&#39;$LogInfo&#39;)'><span class='fa fa-book-open'></button></td>";

                    if ($SmartLockList->BatteryStatus == true) {
                        echo "<td>
                        <img src='Images/Door/low_battery.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    } else {
                        echo "<td>
                        <img src='Images/Door/full_battery.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    }

                    $RSSI = 100 + $SmartLockList->RSSI;
                    if ($RSSI >= 91 && $RSSI <= 99) {
                        echo "<td>
                      <input type='image' onclick='openRSSI(&#39;$SmartLockList->RSSI&#39;)' src='Images/RSSI/RSSI10.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    } else if ($RSSI >= 81 && $RSSI <= 90) {
                        echo "<td>
                      <input type='image' onclick='openRSSI(&#39;$SmartLockList->RSSI&#39;)' src='Images/RSSI/RSSI9.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    } else if ($RSSI >= 71 && $RSSI <= 80) {
                        echo "<td>
                      <input type='image' onclick='openRSSI(&#39;$SmartLockList->RSSI&#39;)' src='Images/RSSI/RSSI8.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    } else if ($RSSI >= 61 && $RSSI <= 70) {
                        echo "<td>
                      <input type='image' onclick='openRSSI(&#39;$SmartLockList->RSSI&#39;)' src='Images/RSSI/RSSI7.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    } else if ($RSSI >= 51 && $RSSI <= 60) {
                        echo "<td>
                      <input type='image' onclick='openRSSI(&#39;$SmartLockList->RSSI&#39;)' src='Images/RSSI/RSSI6.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    } else if ($RSSI >= 41 && $RSSI <= 50) {
                        echo "<td>
                      <input type='image' onclick='openRSSI(&#39;$SmartLockList->RSSI&#39;)'src='Images/RSSI/RSSI5.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    } else if ($RSSI >= 31 && $RSSI <= 40) {
                        echo "<td>
                      <input type='image' onclick='openRSSI(&#39;$SmartLockList->RSSI&#39;)' src='Images/RSSI/RSSI4.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    } else if ($RSSI >= 21 && $RSSI <= 30) {
                        echo "<td>
                      <input type='image' onclick='openRSSI(&#39;$SmartLockList->RSSI&#39;)' src='Images/RSSI/RSSI3.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    } else if ($RSSI >= 11 && $RSSI <= 20) {
                        echo "<td>
                      <input type='image' onclick='openRSSI(&#39;$SmartLockList->RSSI&#39;)'src='Images/RSSI/RSSI2.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    } else if ($RSSI >= 00 && $RSSI <= 10) {
                        echo "<td>
                      <input type='image' onclick='openRSSI(&#39;$SmartLockList->RSSI&#39;)' src='Images/RSSI/RSSI1.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    } else {
                        echo "<td>
                      <input type='image' onclick='openRSSI(&#39;$SmartLockList->RSSI&#39;)' src='Images/RSSI/Unknown.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    }
                    echo "</tr>";
                    $counter++;
                }
                echo "</tbody>";
                echo "</table>";
                echo "</div>";
            } else {
                echo "<h2 class='error'><i class='fas fa-exclamation-circle fa-5x' style='color:red;'></i> </br></br>
                Smart Door Lock Not Available For This Selected Gateway </br>
                Please Back To Home Page</h2>";
            }
        } else {
            echo "<h2 class='error'><i class='fas fa-exclamation-circle fa-5x' style='color:red;'></i> </br></br>
            Smart Door Lock Not Available For This Selected Gateway </br>
            Please Back To Home Page</h2>";
        }
    }

    ?>
    <input style='display:none;' id='IntervalId' value='none'>
    <input style='display:none;' class='LocationName'>

    <!--Log Form -->
    <div class='form-popup' id='DoorLogForm'>
        <div class='form-container' style='width:650px;padding:0px;'>
            <div class="modal-header" style='width:100%;height:60px;'>
                <button class='close' onclick='backLog("DoorLogForm")'></button>
                <h3 class="h2form">Logs</h3>
            </div>
            <div class="modal-body" style='width:100%;max-height: calc(80vh - 210px);overflow-y: auto;'>
                <h4 class="h2form" id='logName'></h4>
                <div id="emptyLog">

                </div>
                <div id="tableLog">
                    <table class="table table-bordered table-hover" style="text-align:center;">
                        <thead>
                            <th>#</th>
                            <th>Date Time</th>
                            <th>User Info</th>
                            <th>Status</th>
                        </thead>
                        <tbody id="DoorLogTable">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!--User Lock Form -->
    <div class='form-popup' id='UserLockForm'>
        <div class='form-container' style='width:600px;padding:0px;'>
            <div class="modal-header" style='width:100%;height:60px;'>
                <button class='close' onclick='backLog("UserLockForm")'></button>
                <h3 class="h2form">Smart Door Lock Users</h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <h4 class="h2form" id='lockName'></h4>
                <h4 class="h2form" id='lockLocation'></h4>
                <h4 style='margin-top:30px;text-align: center;'><b>Door Lock User Information</b></h4>
                <div class='table-wrapper-scroll-y my-custom-scrollbar'>
                    <table class="table table-bordered table-hover" style="text-align:center;">
                        <thead>
                            <th>Id</th>
                            <th>Type</th>
                            <th>User Name</th>
                            <th>Scene</th>
                        </thead>
                        <tbody id="UserLockTable">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!--Scene Form -->
    <div class='form-popup' id='SceneForm'>
        <div class='form-container' style='width:600px;padding:0px;'>
            <div class="modal-header" style='width:100%;height:60px;'>
                <a class='back' onclick='backLog("SceneForm")'><span class='fa fa-arrow-left fa-2x'></a>
                <h3 class="h2form">Select Scene</h3>
            </div>
            <div class="modal-body" id="sceneForm-body" style='width:100%;'>
                <div class='table-wrapper-scroll-y my-custom-scrollbar' id='sceneForm-table'>
                    <table class="table table-bordered table-hover" style="text-align:center;">
                        <thead>
                            <th>Name</th>
                        </thead>
                        <tbody id="SceneTable">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Form for sign Door Lock -->
    <div class="form-popup" id="mySiginDoorLockForm">
        <div class="form-container" style="width:400px;padding:0px;">
            <div class="modal-header" style='width:100%;height:70px;'>
                <!-- <a class='back' onclick='closeFormSigin()'><span class='fa fa-arrow-left fa-2x'></a> -->
                <h3 class='h2form'>Manage Door Lock </br> Password</h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <fieldset style='width:100%;'>
                    <label for="name">Enter Admin Password:</label>
                    <input type="password" id="password" name="DoorlockPassword" style='width:100%;'>
                </fieldset>
            </div>
            <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
                <button type="submit" class="all" onclick='openGetLock()' name="signin">Ok</button>
                <button type="button" class="all" onclick="cancelFormSigin()">Cancel</button>
            </div>
        </div>
    </div>

    <!-- SideBar + Navbar -->
    <div class="wrapper">
        <!-- Sidebar Holder -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>Senzo</h3>
                <strong>SZ</strong>
            </div>
            <ul class="list-unstyled components">
                <!-- Home Section -->
                <li>
                    <form action='SmartDoorLock.php' method='POST' id='NavHome' style='margin-block-end: 0'>
                        <input style='display:none;' name='Home'>
                        <a style='cursor:pointer;' class='Home'>
                            <i class="glyphicon glyphicon-home"></i>
                            Home
                        </a>
                    </form>
                </li>
                <li>
                    <!-- Device Section -->
                    <a href="#pageDevice" data-toggle="collapse" aria-expanded="false">
                        <i class="glyphicon glyphicon-tasks"></i>
                        <?php echo "Device"; ?>
                    </a>
                    <ul class="collapse list-unstyled in" id="pageDevice">
                        <?php
                        if ($SmartSwitchStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectSwitch()">Smart Switch</a></li>';
                        }
                        if ($CurtainStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectCurtain()">Smart Curtain</a></li>';
                        }
                        if ($ShutterStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectShutter()">Smart Shutter</a></li>';
                        }
                        if ($LockStatus) {
                            echo '<li><a id="cube" style="cursor:pointer;" onclick="openSmartDoorLock()">Smart Door Lock</a></li>';
                        }
                        if ($IRBlasterStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectIRBlaster()">IR Blaster</a></li>';
                        }
                        ?>
                    </ul>
                </li>
                <li>
                    <!-- Scene Section -->
                    <a style="cursor:pointer;" onclick='openScenesForm()'>
                        <i class="glyphicon glyphicon-film"></i>
                        <?php echo "Scene ($SceneCount)"; ?>
                    </a>
                </li>
                <li>
                    <!-- Security Section -->
                    <a href="#pageSecurity" data-toggle="collapse" aria-expanded="false">
                        <i class="glyphicon glyphicon-lock"></i>
                        <?php echo "Security"; ?>
                    </a>
                    <ul class="collapse list-unstyled" id="pageSecurity">
                        <?php
                        // If smart alarm connected to gateway
                        if ($AlarmStatus) {
                            echo "<li><a style='cursor:pointer;' onclick='selectAlarm(&#39;True&#39;)'>
                            Smart Alarm <img src='Images/Action/security_armed_icon.png' alt='scenes' style='width:20px;height:20px;'>";
                        } else {
                            echo "<li><a style='cursor:pointer;' onclick='selectAlarm(&#39;False&#39;)'>
                            Smart Alarm";
                        }
                        ?>
                        </a>
                </li>
                <li><a style="cursor:pointer;" onclick="selectCamera()">Camera</a></li>
            </ul>
            </li>
            <li>
                <!-- Analytic Section -->
                <a style='cursor:pointer;' onclick='openAnalyticForm()'>
                    <i class='glyphicon glyphicon-stats'></i>
                    Analytic
                </a>
            </li>
            <li>
                <!-- Setting Section -->
                <a style='cursor:pointer;' onclick='openSettingForm()'>
                    <i class='glyphicon glyphicon-cog'></i>
                    Setting
                </a>
            </li>
            </ul>

            <!-- Logout button -->
            <ul class="list-unstyled CTAs">
                <li>
                    <form method="POST" action="SmartDoorLock.php" id='NavLogout' style='margin-block-end: 0'>
                        <input style='display:none;' name='Logout'>
                        <a style="cursor:pointer;" class="Logout">
                            <i class="glyphicon glyphicon-log-out"></i>
                            Logout
                        </a>
                    </form>
                </li>
            </ul>
        </nav>

        <!-- Page Content Holder -->
        <div id="content" style="width: -webkit-fill-available;">

            <nav class="navbar navbar-default">
                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                            <i class="glyphicon glyphicon-align-left"></i>
                            <span></span>
                        </button>

                        <!-- For responsive part -->
                        <!-- Show and hide content that been targeted-->
                        <button class="btn btn-dark navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <i class="glyphicon glyphicon-align-justify"></i>
                        </button>
                    </div>

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a data-toggle="dropdown" class="dropdown-toggle" style='background: #fff;cursor:pointer;'>Location</a>
                                <ul class="dropdown-menu">
                                    <?php
                                    $LocationListArray = $_SESSION['location'];
                                    if (is_array($LocationListArray) || is_object($LocationListArray)) {
                                        foreach ($LocationListArray as $location) {
                                            echo "<li>";
                                            echo "<input style='display:none;' value='$location->LocationName' id='Location$location->LocationName'>";
                                            echo "<a style='cursor:pointer;' class='LocationListItem' >$location->LocationName</a>";
                                            echo "</li>";
                                        }
                                    }
                                    ?>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div class='filter' id='collapseFilter'>
                <span class='filer-title'> Filtered By : </span>
            </div>

            <h2>List of Available Door Lock</h2>
            <?php
            $GatewayLevel = $_SESSION['level'];
            echo "<h4 class='Level' >Gateway $GatewayLevel</h4>";
            getList($msgJson);
            ?>
            <div class="line"></div>

        </div>
    </div>

    <!-- Auto Refresh dropdown -->
    <div class='autorefresh'>
        <div class="dropdown">
            <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">
                <img src='Images/Switch/settings_gear.png' alt='switchon' style='width:30px;height:30px;animation: spin 3s linear infinite;'>
            </button>
            <ul class="dropdown-menu">
                <li>
                    <input type='checkbox' id='showSn' value='showSn'>
                    <label> Show SN</label><br>
                </li>
                <li>
                    <input type='checkbox' id='enable' name='fooby[2][]'>
                    <label> Enable</label><br>
                    <div id='timeRefresh' style='display:none;margin-left:10px;'>
                        <input type='checkbox' value='1-minute' class="slectOne" id='1-minute'>
                        <label> 1 minute</label><br>
                        <input type='checkbox' value='5-minutes' class="slectOne" id='5-minutes'>
                        <label> 5 minutes</label><br>
                        <input type='checkbox' value='10-minutes' class="slectOne" id='10-minutes'>
                        <label> 10 minutes</label><br>
                        <input type='checkbox' value='20-minutes' class="slectOne" id='20-minutes'>
                        <label> 20 minutes</label><br>
                        <input type='checkbox' value='25-minutes' class="slectOne" id='25-minutes'>
                        <label> 25 minutes</label><br>
                        <input type='checkbox' value='1-hour' class="slectOne" id='1-hour'>
                        <label> 1 hour</label><br>
                    </div>
                </li>
                <li>
                    <input type='checkbox' id='disable' value='disable'>
                    <label> Disable</label><br>
                </li>
            </ul>
        </div>
    </div>

    <!-- Loading animation -->
    <div class="form-popup" id='loader'>
        <!-- <div id="loadering" class="loader"></div> -->
        <div class="loading-container">
            <div class="loading"></div>
            <div class="loading-text">loading</div>
        </div>
    </div>
</body>

</html>