<?php
ini_set('max_execution_time', 30);
session_start();
ob_start();
//Include all file needed to use in this page
include '././configuration/serverConfig.php';
include './header/headerAll.php';
?>
<!DOCTYPE HTML>
<html>

<head>
    <title>Senzo Analytic</title>
    <link href="css/Analytic.css" rel="stylesheet">
    <link rel="stylesheet" href="css/Sidebar.css">
    <!-- Sidebar and auto refresh function -->
    <script type="text/javascript" src="SidebarHandler.js"></script>
</head>
<script type="text/javascript">
    $(document).ready(function() {
        showAnalytic();
    });

    //Search the data table
    function search() {
        var table = $('#myTable').DataTable();
        var min = document.getElementById('startDate').value;
        var max = document.getElementById('endDate').value;
        var location = document.getElementById('selectLocation').value;

        document.getElementsByClassName("start-date").value = min;
        document.getElementsByClassName("end-date").value = max;
        document.getElementsByClassName("LocationName").value = location;
        table.draw();
        $(".chart").remove();
        showAnalytic();
    }

    //Reset the data table
    function reset() {
        var table = $('#myTable').DataTable();
        document.getElementById('startDate').value = 'mm/dd/yyyy';
        document.getElementById('endDate').value = 'mm/dd/yyyy';
        document.getElementById('selectLocation').value = 'All';

        document.getElementsByClassName("start-date").value = '';
        document.getElementsByClassName("end-date").value = '';
        document.getElementsByClassName("LocationName").value = 'All';
        table.draw();
        $(".chart").remove();
        showAnalytic();
    }

    function showAnalytic() {
        var myTable = $('#myTable').DataTable();
        var form_data = get_filtered_datatable();

        var DataOnSwitch = [];
        var DataOffSwitch = [];

        //Filter the data based on ON/OFF status
        $.each(form_data, function(index, item) {
            var obj = new Object();
            obj.No = item[0];
            obj.SerialNumber = item[1];
            obj.DeviceName = item[2];
            obj.Status = item[3];
            obj.Time = item[4];
            obj.Location = item[5];
            obj.Duration = item[6];
            obj.Date = item[7];
            if (item[3] == 'ON') {
                DataOnSwitch.push(obj);
            } else {
                DataOffSwitch.push(obj);
            }

        });
        console.log(DataOnSwitch);
        // -----------------------------------------------Start Active Time(Minutes)-----------------------------------------------------
        //Combine serial number and device name for each data
        var SwitchArray = [];
        for (i = 0; i < DataOnSwitch.length; i++) {
            SwitchArray.push(DataOnSwitch[i].SerialNumber + "%%" + DataOnSwitch[i].DeviceName);
        }

        //Remove duplicate data
        var uniqueSwitchArray = [];
        $.each(SwitchArray, function(i, el) {
            if ($.inArray(el, uniqueSwitchArray) === -1) uniqueSwitchArray.push(el);
        });

        //Get the serial number and device name
        var newSwitchArray = [];
        for (i = 0; i < uniqueSwitchArray.length; i++) {
            var fields = uniqueSwitchArray[i].split("%%");
            var obj = new Object();
            obj.SerialNumber = fields[0];
            obj.DeviceName = fields[1];
            newSwitchArray.push(obj);
        }

        //Find the total duration
        var dataAnalytic = [];
        for (i = 0; i < newSwitchArray.length; i++) {
            var obj = new Object();
            obj.DeviceName = newSwitchArray[i].DeviceName;
            obj.TotalTime = 0;
            for (j = 0; j < DataOnSwitch.length; j++) {
                if (DataOnSwitch[j].SerialNumber == newSwitchArray[i].SerialNumber && DataOnSwitch[j].DeviceName == newSwitchArray[i].DeviceName) {
                    obj.TotalTime += parseInt(DataOnSwitch[j].Duration, 10);
                }
            }
            dataAnalytic.push(obj);
        }

        //Convert second to minutes
        var TotalTimeArray = [];
        var DeviceNameArray = [];
        for (i = 0; i < dataAnalytic.length; i++) {
            if (Math.round(dataAnalytic[i].TotalTime / 60) != 0) {
                TotalTimeArray.push(Math.round(dataAnalytic[i].TotalTime / 60));
                DeviceNameArray.push(dataAnalytic[i].DeviceName);
            }
        }

        $("#activeTimeMinutesChart").append(
            '<div class="chart">' +
            '<canvas id="myActiveTimeBarChart"></canvas>' +
            '</div>'
        );
        drawActiveTimeMinutesBarChart(DeviceNameArray, TotalTimeArray);
        // -----------------------------------------------End Active Time(Minutes)-----------------------------------------------------
        // -----------------------------------------------Start Active Time(Percentage)-----------------------------------------------------
        //Find the total duration
        var sum = TotalTimeArray.reduce(function(a, b) {
            return a + b;
        }, 0);

        //Convert to percentage
        var TotalPercentageArray = [];
        for (i = 0; i < TotalTimeArray.length; i++) {
            if (Math.round((TotalTimeArray[i] / sum) * 100) != 0) {
                TotalPercentageArray[i] = Math.round((TotalTimeArray[i] / sum) * 100);
            }
        }

        $("#activeTimePercentagesChart").append(
            '<div class="chart">' +
            '<canvas id="myActiveTimePieChart"></canvas>' +
            '</div>'
        );
        drawActiveTimeMinutesPieChart(DeviceNameArray, TotalPercentageArray);
        // -----------------------------------------------End Active Time(Percentage)-----------------------------------------------------
        // -----------------------------------------------Start Inactive Time(Minutes)-----------------------------------------------------
        //Combine serial number and device name for each data
        var SwitchArray = [];
        for (i = 0; i < DataOffSwitch.length; i++) {
            SwitchArray.push(DataOffSwitch[i].SerialNumber + "%%" + DataOffSwitch[i].DeviceName);
        }

        //Remove duplicate data
        var uniqueSwitchArray = [];
        $.each(SwitchArray, function(i, el) {
            if ($.inArray(el, uniqueSwitchArray) === -1) uniqueSwitchArray.push(el);
        });

        //Get the serial number and device name
        var newSwitchArray = [];
        for (i = 0; i < uniqueSwitchArray.length; i++) {
            var fields = uniqueSwitchArray[i].split("%%");
            var obj = new Object();
            obj.SerialNumber = fields[0];
            obj.DeviceName = fields[1];
            newSwitchArray.push(obj);
        }

        //Find the total duration
        var dataAnalytic = [];
        for (i = 0; i < newSwitchArray.length; i++) {
            var obj = new Object();
            obj.DeviceName = newSwitchArray[i].DeviceName;
            obj.TotalTime = 0;
            for (j = 0; j < DataOffSwitch.length; j++) {
                if (DataOffSwitch[j].SerialNumber == newSwitchArray[i].SerialNumber && DataOffSwitch[j].DeviceName == newSwitchArray[i].DeviceName) {
                    obj.TotalTime += parseInt(DataOffSwitch[j].Duration, 10);
                }
            }
            dataAnalytic.push(obj);
        }

        //Convert second to minutes
        var TotalTimeArray = [];
        var DeviceNameArray = [];
        for (i = 0; i < dataAnalytic.length; i++) {
            if (Math.round(dataAnalytic[i].TotalTime / 60) != 0) {
                TotalTimeArray.push(Math.round(dataAnalytic[i].TotalTime / 60));
                DeviceNameArray.push(dataAnalytic[i].DeviceName);
            }
        }

        $("#inactiveTimeMinutesChart").append(
            '<div class="chart">' +
            '<canvas id="myInactiveTimeBarChart"></canvas>' +
            '</div>'
        );
        drawInactiveTimeMinutesBarChart(DeviceNameArray, TotalTimeArray);
        // -----------------------------------------------End Inactive Time(Minutes)-----------------------------------------------------
        // -----------------------------------------------Start Inactive Time(Percentages)-----------------------------------------------------
        //Find the total duration
        var sum = TotalTimeArray.reduce(function(a, b) {
            return a + b;
        }, 0);

        //Convert to percentage
        var TotalPercentageArray = [];
        for (i = 0; i < TotalTimeArray.length; i++) {
            if (Math.round((TotalTimeArray[i] / sum) * 100) != 0.00) {
                TotalPercentageArray[i] = Math.round((TotalTimeArray[i] / sum) * 100);
            }
        }

        $("#inactiveTimePercentagesChart").append(
            '<div class="chart">' +
            '<canvas id="myInactiveTimePieChart"></canvas>' +
            '</div>'
        );
        drawInactiveTimeMinutesPieChart(DeviceNameArray, TotalPercentageArray);
        // -----------------------------------------------Start Inactive Time(Percentages)-----------------------------------------------------
        // -----------------------------------------------Start Busy Time-----------------------------------------------------
        //Include moment.js
        window['moment-range'].extendMoment(moment);

        //If the active time overlap with time given
        //it will return true
        let overlap = (timeSegments) => {
            let ret = false;
            let i = 0;
            while (!ret && i < timeSegments.length - 1) {
                let seg1 = timeSegments[i];
                let seg2 = timeSegments[i + 1];
                let range1 = moment.range(moment(seg1[0], 'hh:mm:ss aa'), moment(seg1[1], 'hh:mm:ss aa'));
                let range2 = moment.range(moment(seg2[0], 'hh:mm:ss aa'), moment(seg2[1], 'hh:mm:ss aa'));
                if (range1.overlaps(range2)) {
                    ret = true;
                }
                i++;

                return ret;
            }
        };

        //Initialize the value for each hour (12am to 12pm)
        var BusyTimeArray = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

        for (i = 0; i < DataOnSwitch.length; i++) {
            var Duration = DataOnSwitch[i].Duration * 1000; //Convert data into miliseconds
            var DateTime = DataOnSwitch[i].Date + "T" + DataOnSwitch[i].Time; //Combine Date and Time
            var fromDateTime = new Date(DateTime); //Convert start time into datetime format
            var toDateTime = new Date(fromDateTime.getTime() + Duration); //Convert end time into datetime format

            var fromDate = fromDateTime.toLocaleDateString(); //Get start date only
            var fromTime = fromDateTime.toLocaleTimeString(); //Get start time only
            var toDate = toDateTime.toLocaleDateString(); //Get end date only
            var toTime = toDateTime.toLocaleTimeString(); //Get end time only


            var from = "12:00:00 AM"; //Initialize start time for overlapping
            var to = "12:59:00 AM"; //Initialize end time for overlapping
            let timeSegments1 = [];
            timeSegments1.push([from, to])
            timeSegments1.push([fromTime, toTime])
            //If overlap with time given, the value at index=0  will be increase by one
            if (overlap(timeSegments1)) {
                BusyTimeArray[0] += 1;
            }

            var from = "1:00:00 AM";
            var to = "1:59:00 AM";
            let timeSegments2 = [];
            timeSegments2.push([from, to])
            timeSegments2.push([fromTime, toTime])
            if (overlap(timeSegments2)) {
                BusyTimeArray[1] += 1;
            }

            var from = "2:00:00 AM";
            var to = "2:59:00 AM";
            let timeSegments3 = [];
            timeSegments3.push([from, to])
            timeSegments3.push([fromTime, toTime])
            if (overlap(timeSegments3)) {
                BusyTimeArray[2] += 1;
            }

            var from = "3:00:00 AM";
            var to = "3:59:00 AM";
            let timeSegments4 = [];
            timeSegments4.push([from, to])
            timeSegments4.push([fromTime, toTime])
            if (overlap(timeSegments4)) {
                BusyTimeArray[3] += 1;
            }

            var from = "4:00:00 AM";
            var to = "4:59:00 AM";
            let timeSegments5 = [];
            timeSegments5.push([from, to])
            timeSegments5.push([fromTime, toTime])
            if (overlap(timeSegments5)) {
                BusyTimeArray[4] += 1;
            }

            var from = "5:00:00 AM";
            var to = "5:59:00 AM";
            let timeSegments6 = [];
            timeSegments6.push([from, to])
            timeSegments6.push([fromTime, toTime])
            if (overlap(timeSegments6)) {
                BusyTimeArray[5] += 1;
            }

            var from = "6:00:00 AM";
            var to = "6:59:00 AM";
            let timeSegments7 = [];
            timeSegments7.push([from, to])
            timeSegments7.push([fromTime, toTime])
            if (overlap(timeSegments7)) {
                BusyTimeArray[6] += 1;
            }

            var from = "7:00:00 AM";
            var to = "7:59:00 AM";
            let timeSegments8 = [];
            timeSegments8.push([from, to])
            timeSegments8.push([fromTime, toTime])
            if (overlap(timeSegments8)) {
                BusyTimeArray[7] += 1;
            }

            var from = "8:00:00 AM";
            var to = "8:59:00 AM";
            let timeSegments9 = [];
            timeSegments9.push([from, to])
            timeSegments9.push([fromTime, toTime])
            if (overlap(timeSegments9)) {
                BusyTimeArray[8] += 1;
            }

            var from = "9:00:00 AM";
            var to = "9:59:00 AM";
            let timeSegments10 = [];
            timeSegments10.push([from, to])
            timeSegments10.push([fromTime, toTime])
            if (overlap(timeSegments10)) {
                BusyTimeArray[9] += 1;
            }

            var from = "10:00:00 AM";
            var to = "10:59:00 AM";
            let timeSegments11 = [];
            timeSegments11.push([from, to])
            timeSegments11.push([fromTime, toTime])
            if (overlap(timeSegments11)) {
                BusyTimeArray[10] += 1;
            }

            var from = "11:00:00 AM";
            var to = "11:59:00 AM";
            let timeSegments12 = [];
            timeSegments12.push([from, to])
            timeSegments12.push([fromTime, toTime])
            if (overlap(timeSegments12)) {
                BusyTimeArray[11] += 1;
            }

            var from = "12:00:00 PM";
            var to = "12:59:00 PM";
            let timeSegments13 = [];
            timeSegments13.push([from, to])
            timeSegments13.push([fromTime, toTime])
            if (overlap(timeSegments13)) {
                BusyTimeArray[12] += 1;
            }

            var from = "1:00:00 PM";
            var to = "1:59:00 PM";
            let timeSegments14 = [];
            timeSegments14.push([from, to])
            timeSegments14.push([fromTime, toTime])
            if (overlap(timeSegments14)) {
                BusyTimeArray[13] += 1;
            }

            var from = "2:00:00 PM";
            var to = "2:59:00 PM";
            let timeSegments15 = [];
            timeSegments15.push([from, to])
            timeSegments15.push([fromTime, toTime])
            if (overlap(timeSegments15)) {
                BusyTimeArray[14] += 1;
            }

            var from = "3:00:00 PM";
            var to = "3:59:00 PM";
            let timeSegments16 = [];
            timeSegments16.push([from, to])
            timeSegments16.push([fromTime, toTime])
            if (overlap(timeSegments16)) {
                BusyTimeArray[15] += 1;
            }

            var from = "4:00:00 PM";
            var to = "4:59:00 PM";
            let timeSegments17 = [];
            timeSegments17.push([from, to])
            timeSegments17.push([fromTime, toTime])
            if (overlap(timeSegments17)) {
                BusyTimeArray[16] += 1;
            }

            var from = "5:00:00 PM";
            var to = "5:59:00 PM";
            let timeSegments18 = [];
            timeSegments18.push([from, to])
            timeSegments18.push([fromTime, toTime])
            if (overlap(timeSegments18)) {
                BusyTimeArray[17] += 1;
            }

            var from = "6:00:00 PM";
            var to = "6:59:00 PM";
            let timeSegments19 = [];
            timeSegments19.push([from, to])
            timeSegments19.push([fromTime, toTime])
            if (overlap(timeSegments19)) {
                BusyTimeArray[18] += 1;
            }

            var from = "7:00:00 PM";
            var to = "7:59:00 PM";
            let timeSegments20 = [];
            timeSegments20.push([from, to])
            timeSegments20.push([fromTime, toTime])
            if (overlap(timeSegments20)) {
                BusyTimeArray[19] += 1;
            }

            var from = "8:00:00 PM";
            var to = "8:59:00 PM";
            let timeSegments21 = [];
            timeSegments21.push([from, to])
            timeSegments21.push([fromTime, toTime])
            if (overlap(timeSegments21)) {
                BusyTimeArray[20] += 1;
            }

            var from = "9:00:00 PM";
            var to = "9:59:00 PM";
            let timeSegments22 = [];
            timeSegments22.push([from, to])
            timeSegments22.push([fromTime, toTime])
            if (overlap(timeSegments22)) {
                BusyTimeArray[21] += 1;
            }

            var from = "10:00:00 PM";
            var to = "10:59:00 PM";
            let timeSegments23 = [];
            timeSegments23.push([from, to])
            timeSegments23.push([fromTime, toTime])
            if (overlap(timeSegments23)) {
                BusyTimeArray[22] += 1;
            }

            var from = "11:00:00 PM";
            var to = "11:59:00 PM";
            let timeSegments24 = [];
            timeSegments24.push([from, to])
            timeSegments24.push([fromTime, toTime])
            if (overlap(timeSegments24)) {
                BusyTimeArray[23] += 1;
            }
        }
        $("#busyTimeChart").append(
            '<div class="chart">' +
            '<canvas id="myChart"></canvas>' +
            '</div>'
        );
        drawBusyTimeBarChart(BusyTimeArray);
        // -----------------------------------------------End Busy Time-----------------------------------------------------
    }

    //For filter table
    $.fn.dataTable.ext.search.push(
        function(settings, data, dataIndex) {
            var min = document.getElementsByClassName("start-date").value;
            var max = document.getElementsByClassName("end-date").value;
            var LocationName = document.getElementsByClassName("LocationName").value

            if (min == null || min == '') {
                min = 'Null';

            }

            if (max == null || max == '') {
                max = 'Null';
            }

            if (LocationName == null || LocationName == 'All') {
                LocationName = 'Null';
            }

            var date = data[7];
            var location = data[5];

            if ((min == 'Null' && max == 'Null' && LocationName == 'Null') ||
                (min == 'Null' && date <= max && LocationName == 'Null') ||
                (min == 'Null' && date <= max && LocationName == location) ||
                (min <= date && max == 'Null' && LocationName == 'Null') ||
                (min <= date && max == 'Null' && LocationName == location) ||
                (min == 'Null' && max == 'Null' && LocationName == location) ||
                (min <= date && date <= max && LocationName == 'Null') ||
                (min <= date && date <= max && LocationName == location)) {
                return true;
            }
            return false;
        }
    );

    //Get the data from the table have been filter
    function get_filtered_datatable() {
        var filteredrows = $("#myTable").dataTable()._('tr', {
            "filter": "applied"
        });

        var filterArray = []
        for (var i = 0; i < filteredrows.length; i++) {
            filterArray.push(filteredrows[i])
        };
        return filterArray;
    }

    function drawActiveTimeMinutesBarChart(dataLabel, dataValue) {
        var ctx = document.getElementById('myActiveTimeBarChart').getContext('2d');

        var backgroundColor = [];
        for (i = 0; i < dataLabel.length; i++) {
            backgroundColor[i] = random_rgba();
        }
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: dataLabel,
                datasets: [{
                    label: 'Time(Minutes)',
                    data: dataValue,
                    backgroundColor: backgroundColor,
                    borderColor: backgroundColor,
                    borderSkipped: 'bottom',
                    borderWidth: 2

                }],
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    }

    function drawActiveTimeMinutesPieChart(dataLabel, dataValue) {
        var ctx = document.getElementById('myActiveTimePieChart').getContext('2d');

        var backgroundColor = [];
        for (i = 0; i < dataLabel.length; i++) {
            backgroundColor[i] = random_rgba();
        }
        var myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: dataLabel,
                datasets: [{
                    data: dataValue,
                    backgroundColor: backgroundColor,
                    borderColor: backgroundColor,
                    borderWidth: 2

                }],
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    }

    function drawBusyTimeBarChart(dataValue) {
        var ctx = document.getElementById('myChart').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'horizontalBar',
            data: {
                labels: ['12am', '1am', '2am', '3am', '4am', '5am', '6am', '7am', '8am', '9am', '10am', '11am', '12pm', '1pm', '2pm', '3pm', '4pm', '5pm', '6pm', '7pm', '8pm', '9pm', '10pm', '11pm'],
                datasets: [{
                    label: 'Devices',
                    data: dataValue,
                    backgroundColor: [
                        'rgba(206, 19, 89, 0.7)',
                        'rgba(205, 163, 193, 0.4)',
                        'rgba(127, 151, 1, 0.9)',
                        'rgba(232, 90, 48, 0.1)',
                        'rgba(184, 10, 218, 0.9)',
                        'rgba(254, 182, 126, 0.6)',
                        'rgba(149, 29, 193, 0.2)',
                        'rgba(105, 82, 201, 0.5)',
                        'rgba(164, 14, 214, 0.3)',
                        'rgba(12, 73, 78, 0.8)',
                        'rgba(129, 215, 164, 0.3)',
                        'rgba(129, 127, 94, 0.4)',
                        'rgba(40, 183, 106, 0.2)',
                        'rgba(29, 8, 252, 0.7)',
                        'rgba(173, 9, 216, 0)',
                        'rgba(107, 18, 64, 0.2)',
                        'rgba(211, 49, 18, 0.3)',
                        'rgba(34, 251, 77, 0)',
                        'rgba(94, 125, 63, 0.4)',
                        'rgba(38, 237, 186, 0.3)',
                        'rgba(113, 109, 248, 0.7)',
                        'rgba(122, 119, 80, 0.8)',
                        'rgba(26, 218, 87, 1)',
                        'rgba(9, 144, 130, 0.1)'
                    ],
                    borderColor: [
                        'rgba(206, 19, 89, 1)',
                        'rgba(205, 163, 193, 1)',
                        'rgba(127, 151, 1, 1)',
                        'rgba(232, 90, 48, 1)',
                        'rgba(184, 10, 218, 1)',
                        'rgba(254, 182, 126, 1)',
                        'rgba(149, 29, 193, 1)',
                        'rgba(105, 82, 201, 1)',
                        'rgba(164, 14, 214, 1)',
                        'rgba(12, 73, 78, 1)',
                        'rgba(129, 215, 164, 1)',
                        'rgba(129, 127, 94, 1)',
                        'rgba(40, 183, 106, 1)',
                        'rgba(29, 8, 252, 1)',
                        'rgba(173, 9, 216, 1)',
                        'rgba(107, 18, 64, 1)',
                        'rgba(211, 49, 18, 1)',
                        'rgba(34, 251, 77, 1)',
                        'rgba(94, 125, 63, 1)',
                        'rgba(38, 237, 186, 1)',
                        'rgba(113, 109, 248, 1)',
                        'rgba(122, 119, 80, 1)',
                        'rgba(26, 218, 87, 1)',
                        'rgba(9, 144, 130, 1)'
                    ],
                    borderSkipped: 'left',
                    borderWidth: 2

                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    }

    function drawInactiveTimeMinutesBarChart(dataLabel, dataValue) {
        var ctx = document.getElementById('myInactiveTimeBarChart').getContext('2d');

        var backgroundColor = [];
        for (i = 0; i < dataLabel.length; i++) {
            backgroundColor[i] = random_rgba();
        }
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: dataLabel,
                datasets: [{
                    label: 'Time(Minutes)',
                    data: dataValue,
                    backgroundColor: backgroundColor,
                    borderColor: backgroundColor,
                    borderSkipped: 'bottom',
                    borderWidth: 2

                }],
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    }

    function drawInactiveTimeMinutesPieChart(dataLabel, dataValue) {
        var ctx = document.getElementById('myInactiveTimePieChart').getContext('2d');

        var backgroundColor = [];
        for (i = 0; i < dataLabel.length; i++) {
            backgroundColor[i] = random_rgba();
        }
        var myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: dataLabel,
                datasets: [{
                    data: dataValue,
                    backgroundColor: backgroundColor,
                    borderColor: backgroundColor,
                    borderWidth: 2

                }],
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    }

    //Generate random rgba code
    function random_rgba() {
        var o = Math.round,
            r = Math.random,
            s = 255;
        return 'rgba(' + o(r() * s) + ',' + o(r() * s) + ',' + o(r() * s) + ',0.5' + ')';
    }
</script>

<body>
    <?php
    //Get all the session variable from Dashboard.php
    $SerialGateway = $_SESSION['serialGateway'];
    $Password = $_SESSION['password'];
    $LocationListArray = $_SESSION['location'];
    $SmartSwitchStatus = $_SESSION['SmartSwitchStatus'];
    $CurtainStatus = $_SESSION['CurtainStatus'];
    $AlarmStatus = $_SESSION['AlarmStatus'];
    $ShutterStatus = $_SESSION['ShutterStatus'];
    $IRBlasterStatus = $_SESSION['IRBlasterStatus'];
    $LockStatus = $_SESSION['LockStatus'];
    $TotalDevice = $_SESSION['TotalDevice'];
    $SceneCount = $_SESSION['SceneCount'];
    $AdminPassword = $_SESSION['AdminPassword'];
    $GatewaySerialNo = $_SESSION['serialNo'];

    echo "<input style='display:none;' id='SerialGateway' value='$SerialGateway'>";
    echo "<input style='display:none;' id='Password' value='$Password'>";
    echo "<input style='display:none;' id='AdminPassword' value='$AdminPassword'>";

    if (isset($_POST['Logout'])) {
        header("Location: ChooseGateway.php");
        header("Refresh:0");
        ob_flush();
        session_destroy();
        exit();
    }

    if (isset($_POST['Home'])) {
        // header("Refresh:0");
        header("Refresh:0");
        ob_flush();
        exit();
    }

    // Get the contents of Data Analytic JSON file 
    $url = "http://guardian.senzo.com.my/API/API-GetAnalytic.php?id=ID01-D10000-0001&key=lplqwekocmvfn89wkhg8238";
    $strJsonFileContents = file_get_contents($url);
    $GetDataAnalyticJsonObject = json_decode($strJsonFileContents); //Convert analytic data from json string 


    function getList($GetDataAnalyticJsonObject)
    {
        echo "<div class='table-responsive'>";
        echo "<table id='myTable' class='table-bordered' style='width:100%;margin-top:50px;'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>No</th>";
        echo "<th>Serial Number</th>";
        echo "<th>Device Name</th>";
        echo "<th>Status</th>";
        echo "<th>Time</th>";
        echo "<th>Location</th>";
        echo "<th>Duration(Seconds)</th>";
        echo "<th>Date</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody id='myBody' >";

        $counter = 1;
        //Display in table
        foreach ($GetDataAnalyticJsonObject as $data) {
            echo "<tr class='AnalyticTable' >";
            echo "<td>$counter</td>";
            echo "<td>$data->device_serial_no</td>";
            echo "<td>$data->device_name</td>";

            $Status = $data->status;
            if ($Status == 1) {
                $Status = 'ON';
            } else {
                $Status = 'OFF';
            }
            echo "<td>$Status</td>";
            echo "<td>$data->time</td>";
            echo "<td>$data->location</td>";
            echo "<td>$data->duration</td>";
            echo "<td>$data->date</td>";
            echo "</tr>";

            $counter++;
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
    }
    ?>


    <input style='display:none;' id='IntervalId' value='none'>
    <input style='display:none;' class='start-date'>
    <input style='display:none;' class='end-date'>
    <input style='display:none;' class='LocationName'>

    <!-- Form for sign Door Lock -->
    <div class="form-popup" id="mySiginDoorLockForm">
        <div class="form-container" style="width:400px;padding:0px;">
            <div class="modal-header" style='width:100%;height:70px;'>
                <!-- <a class='back' onclick='closeFormSigin()'><span class='fa fa-arrow-left fa-2x'></a> -->
                <h3 class='h2form'>Manage Door Lock </br> Password</h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <fieldset style='width:100%;'>
                    <label for="name">Enter Admin Password:</label>
                    <input type="password" id="password" name="DoorlockPassword" style='width:100%;'>
                </fieldset>
            </div>
            <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
                <button type="submit" class="all" onclick='openGetLock()' name="signin">Ok</button>
                <button type="button" class="all" onclick="cancelFormSigin()">Cancel</button>
            </div>
        </div>
    </div>

    <!-- SideBar + Navbar -->
    <div class="wrapper">
        <!-- Sidebar Holder -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>Senzo</h3>
                <strong>SZ</strong>
            </div>
            <ul class="list-unstyled components">
                <!-- Home Section -->
                <li>
                    <form action='Dashboard.php' method='POST' id='NavHome' style='margin-block-end: 0'>
                        <input style='display:none;' name='Home'>
                        <a style='cursor:pointer;' class='Home'>
                            <i class="glyphicon glyphicon-home"></i>
                            Home
                        </a>
                    </form>
                </li>
                <li>
                    <!-- Device Section -->
                    <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false">
                        <i class="glyphicon glyphicon-tasks"></i>
                        <?php echo "Device"; ?>
                    </a>
                    <ul class="collapse list-unstyled" id="pageSubmenu">
                        <?php
                        if ($SmartSwitchStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectSwitch()">Smart Switch</a></li>';
                        }
                        if ($CurtainStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectCurtain()">Smart Curtain</a></li>';
                        }
                        if ($ShutterStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectShutter()">Smart Shutter</a></li>';
                        }
                        if ($LockStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectDoorLock()">Smart Door Lock</a></li>';
                        }
                        if ($IRBlasterStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectIRBlaster()">IR Blaster</a></li>';
                        }
                        ?>
                    </ul>
                </li>
                <li>
                    <!-- Scene Section -->
                    <a style='cursor:pointer;' onclick='openScenesForm()'>
                        <i class='glyphicon glyphicon-film'></i>
                        <?php echo "Scene ($SceneCount)"; ?>
                    </a>
                </li>
                <li>
                    <!-- Security Section -->
                    <a href="#pageSecurity" data-toggle="collapse" aria-expanded="false">
                        <i class="glyphicon glyphicon-lock"></i>
                        <?php echo "Security"; ?>
                    </a>
                    <ul class="collapse list-unstyled" id="pageSecurity">
                        <?php
                        // If smart alarm connected to gateway
                        if ($AlarmStatus) {
                            echo "<li><a style='cursor:pointer;' onclick='selectAlarm(&#39;True&#39;)'>
                            Smart Alarm <img src='Images/Action/security_armed_icon.png' alt='scenes' style='width:20px;height:20px;'>";
                        } else {
                            echo "<li><a style='cursor:pointer;' onclick='selectAlarm(&#39;False&#39;)'>
                            Smart Alarm";
                        }
                        ?>
                        </a>
                </li>
                <li><a style="cursor:pointer;" onclick="selectCamera()">Camera</a></li>
            </ul>
            </li>
            <li>
                <!-- Analytic Section -->
                <a id='cube' style='cursor:pointer;' onclick='openAnalyticForm()'>
                    <i class='glyphicon glyphicon-stats'></i>
                    Analytic
                </a>
            </li>
            <li>
                <!-- Setting Section -->
                <a style='cursor:pointer;' onclick='openSettingForm()'>
                    <i class='glyphicon glyphicon-cog'></i>
                    Setting
                </a>
            </li>
            </ul>

            <!-- Logout button -->
            <ul class="list-unstyled CTAs">
                <li>
                    <form method="POST" action="Dashboard.php" id='NavLogout' style='margin-block-end: 0'>
                        <input style='display:none;' name='Logout'>
                        <a style="cursor:pointer;" class="Logout">
                            <i class="glyphicon glyphicon-log-out"></i>
                            Logout
                        </a>
                    </form>
                </li>
            </ul>
        </nav>

        <!-- Page Content Holder -->
        <div id="content" style="width: -webkit-fill-available;">

            <nav class="navbar navbar-default">
                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                            <i class="glyphicon glyphicon-align-left"></i>
                            <span></span>
                        </button>

                        <!-- For responsive part -->
                        <!-- Show and hide content that been targeted-->
                        <button class="btn btn-dark navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <i class="glyphicon glyphicon-align-justify"></i>
                        </button>
                    </div>

                    <!-- Content that have been targeted -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <h2>Analytic</h2>
            <?php
            $GatewayLevel = $_SESSION['level'];
            echo "<h4 class='Level' >Gateway $GatewayLevel</h4>";
            ?>
            <!-- Customize analytic form -->
            <div class="flex-container">
                <div>
                    <label for="start-date">Start Date:</label>
                    <input type="date" id="startDate" name="startDate">
                </div>
                <div>
                    <label for="end-date">End Date:</label>
                    <input type="date" id="endDate" name="endtDate">
                </div>
                <div>
                    <label id='labelLocation' for='locations'>Location:</label>
                    <select id='selectLocation' name='locations'>
                        <option value='All' selected>All</option>
                        <?php
                        $LocationListArray = $_SESSION['location'];
                        if (is_array($LocationListArray) || is_object($LocationListArray)) {
                            foreach ($LocationListArray as $location) {
                                echo "<option value='$location->LocationName'>$location->LocationName</option>";
                            }
                        }
                        ?>
                    </select>
                </div>
                <div>
                    <button type="button" class="btn btn-primary" onclick="search()">Search</button>
                    <button type="button" class="btn btn-primary" onclick="reset()">Reset</button>
                </div>
            </div>
            <?php
            getList($GetDataAnalyticJsonObject);
            ?>

            <!-- Analytic graph -->
            <div class="line"></div>
            <div class="row">
                <div class="col col-xs-12">
                    <h3>Busy Time</h3>
                    <div id='busyTimeChart'>

                    </div>
                </div>
            </div>
            <div class="line"></div>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>Active Time (Minutes)</h3>
                    <div id='activeTimeMinutesChart'>

                    </div>
                </div>
                <div class="col col-xs-6">
                    <h3>Active Time (Percentages)</h3>
                    <div id='activeTimePercentagesChart'>

                    </div>
                </div>
            </div>
            <div class="line"></div>
            <div class="row">
                <div class="col col-xs-6">
                    <h3>Inactive Time (Minutes)</h3>
                    <div id='inactiveTimeMinutesChart'>

                    </div>
                </div>
                <div class="col col-xs-6">
                    <h3>Inactive Time (Percentages)</h3>
                    <div id='inactiveTimePercentagesChart'>

                    </div>
                </div>
            </div>




        </div>
    </div>

    <!-- Auto Refresh dropdown -->
    <div class='autorefresh'>
        <div class="dropdown">
            <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">
                <img src='Images/Switch/settings_gear.png' alt='switchon' style='width:30px;height:30px;animation: spin 3s linear infinite;'>
            </button>
            <ul class="dropdown-menu">
                <!-- <li><a href="#">Phantom</a></li> -->
                <li>
                    <input type='checkbox' id='enable' name='fooby[2][]'>
                    <label> Enable</label><br>
                    <div id='timeRefresh' style='display:none;margin-left:10px;'>
                        <input type='checkbox' value='1-minute' class="slectOne" id='1-minute'>
                        <label> 1 minute</label><br>
                        <input type='checkbox' value='5-minutes' class="slectOne" id='5-minutes'>
                        <label> 5 minutes</label><br>
                        <input type='checkbox' value='10-minutes' class="slectOne" id='10-minutes'>
                        <label> 10 minutes</label><br>
                        <input type='checkbox' value='20-minutes' class="slectOne" id='20-minutes'>
                        <label> 20 minutes</label><br>
                        <input type='checkbox' value='25-minutes' class="slectOne" id='25-minutes'>
                        <label> 25 minutes</label><br>
                        <input type='checkbox' value='1-hour' class="slectOne" id='1-hour'>
                        <label> 1 hour</label><br>
                    </div>
                </li>
                <li>
                    <input type='checkbox' id='disable' value='disable'>
                    <label> Disable</label><br>
                </li>
            </ul>
        </div>
    </div>

    <!-- Loading animation -->
    <div class="form-popup" id='loader'>
        <!-- <div id="loadering" class="loader"></div> -->
        <div class="loading-container">
            <div class="loading"></div>
            <div class="loading-text">loading</div>
        </div>
    </div>
</body>

</html>