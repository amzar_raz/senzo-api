==================================================Senzo API using MQTT Protocol==================================================================

Installation
-------------------------------------------------------------------------------------------------------------------------------------------------
Download the folder and paste it in C:\xampp\htdocs

Usage
-------------------------------------------------------------------------------------------------------------------------------------------------
A. Before you be able to use this API properly, you need to adjust 3 config files that located in Senzo-API/WebPageMQTT/configuration/
In this folder you will be find 3 files which are :

	1.databaseConfig: 
	-Change all the variable to match with your mysql database.

	$hostname = "localhost";     // set your host
	$dbname = "iot gateway";     // set your database name
	$username = "root";          // set your database username
	$password = "";		     // set your database password

	$tablename = "tbl_gateway";      // set your database table
	$gatewayId = "id";                   // example column
	$gatewayName = "Name";               // example column
	$gatewaySerial = "Serial";           // example column
	$gatewayLocation = "Location";       // example column
	$gatewayLevel = "Level";             // example column
	$gatewayPassword = "Password";       // example column

	note: If you do not have mysql database, you can use our example database. You can read (B) for further explanation and guideline to
	import the file. 

	2.serverConfig: 
	-Change the host address to gateway.senzo.com.my or 110.4.41.94 to connect to official senzo server.
	You can change this value according to your broker.

	$server = "1.2.3.4";    	  // set your broker address
	$port = "60000" 		  // set your broker port number
	$cafile = NULL ;                  // set your certificate for security measure
	$username = "";                   // set your username or can be left empty
	$password = "";                   // set your password or can be left empty
	$client_id = "";                  // make sure this is unique for connecting to sever - you could use uniqid()
                                  	  // client id sent to brocker or can be left empty
	
	-Change the $baseURL to the path where you put Senzo-API file. 
	-This variable is use to redirect another page within the background in PHP.

	3.directoryUrlConfig.js
	-Same as $baseURL in A(2), change the baseURL to the path where you put Senzo-API file.
	-This variable is use to redirect another page within the background in Javascript.

B. We have provide you example of our database file located in Senzo-API/WebPageMQTT/iot_gateway.sql
You can use this file by 
	i. go to http://localhost/phpmyadmin/
	ii. click on Import
	iii. click on Choose File and select tbl_gateway.sql file
	iv. click Go button.

C. Now you are ready to use this API by load ChooseGateway.php to your browser.

Contribution
-------------------------------------------------------------------------------------------------------------------------------------------------
In this API, to connect using MQTT we use the library from Blue Rhinos Consulting. All the function already be download and store in phpMQTTv2.php