<?php
session_start();
ob_start();
ini_set('max_execution_time', 30);
include '././configuration/serverConfig.php';
include './header/headerAll.php';
?>
<!DOCTYPE html>
<html>



<head>
  <title>Smart Alarm</title>
  <link href="css/SmartAlarm.css" rel="stylesheet">
  <link rel="stylesheet" href="css/Sidebar.css">
  <script type="text/javascript" src="SidebarHandler.js"></script>
</head>
<script type="text/javascript">
  function openOffPop1() {
    document.getElementById("myForm1").style.display = "block";

  }

  function openOffPop2() {
    document.getElementById("myForm2").style.display = "block";

  }

  function off1Function(AlarmInfoOff) {
    //get password from user input
    var password = document.getElementById("pass1").value;
    var fields = AlarmInfoOff.split(" ");
    var serialNo = fields[0];
    var partitionNo = fields[1];
    var status = fields[2];

    //Find empty password
    var boolEmpty = false;
    if (password.length == 0) {
      boolEmpty = true;
    }

    //Find password must not less than 4 number
    var boolLess = false;
    if (password.length > 0 && password.length < 4) {
      boolLess = true;
    }

    //Find password must numeric character only
    var numbers = /^[0-9]+$/;
    var boolNum = false;
    if (!password.match(numbers)) {
      boolNum = true;
    }

    if (!boolEmpty && !boolLess && !boolNum) {
      var hrefOff = baseURL + "/alarmChangeStatusDisarm.php/?SerialNo=" + serialNo + "&PartitionNo=" + partitionNo + "&KeyNumSeq=" + password.split("");
      document.getElementById("myForm1").style.display = "none";
      document.getElementById("myLoader").style.display = "block";

      $.getJSON(hrefOff, function(data) {
        console.log(data);
        if (data.Command == 'ChangeStatusDisarm' && data.Reply == true) {
          document.getElementById("myLoader").style.display = "none";
          setTimeout(function() {
            location.reload();
          }, 2000);
        }
      });
    } else if (boolEmpty) {
      Swal.fire({
        icon: 'error',
        title: 'Failed',
        text: 'Please input your password'
      })

    } else if (boolLess) {
      Swal.fire({
        icon: 'error',
        title: 'Failed',
        text: 'Your password must be 4-6 digit'
      })

    } else if (boolNum) {
      Swal.fire({
        icon: 'error',
        title: 'Failed',
        text: 'Password must numeric character only'
      })
    }
  }

  function off2Function(AlarmInfoOff) {
    //get password from user input
    var password = document.getElementById("pass2").value;
    var fields = AlarmInfoOff.split(" ");
    var serialNo = fields[0];
    var partitionNo = fields[1];
    var status = fields[2];

    //Find empty password
    var boolEmpty = false;
    if (password.length == 0) {
      boolEmpty = true;
    }

    //Find password must not less than 6 number
    var boolLess = false;
    if (password.length > 0 && password.length < 4) {
      boolLess = true;
    }

    //Find password must numeric character only
    var numbers = /^[0-9]+$/;
    var boolNum = false;
    if (!password.match(numbers)) {
      boolNum = true;
    }

    if (!boolEmpty && !boolLess && !boolNum) {
      hrefOff = baseURL + "/alarmChangeStatusDisarm.php/?SerialNo=" + serialNo + "&PartitionNo=" + partitionNo + "&KeyNumSeq=" + password.split("");
      document.getElementById("myForm2").style.display = "none";
      document.getElementById("myLoader").style.display = "block";

      $.getJSON(hrefOff, function(data) {
        if (data.Command == 'ChangeStatusDisarm' && data.Reply == true) {
          document.getElementById("myLoader").style.display = "none";
          setTimeout(function() {
            location.reload();
          }, 2000);
        }
      });
    } else if (boolEmpty) {
      Swal.fire({
        icon: 'error',
        title: 'Failed',
        text: 'Please input your password'
      })

    } else if (boolLess) {
      Swal.fire({
        icon: 'error',
        title: 'Failed',
        text: 'Your password must be 4-6 digit'
      })

    } else if (boolNum) {
      Swal.fire({
        icon: 'error',
        title: 'Failed',
        text: 'Password must numeric character only'
      })
    }
  }

  function stayFunction(AlarmInfoStay) {

    var fields = AlarmInfoStay.split(" ");
    var serialNo = fields[0];
    var partitionNo = fields[1];
    var status = fields[2];

    hrefArm = baseURL + "/alarmChangeStatusArm.php/?SerialNo=" + serialNo + "&PartitionNo=" + partitionNo + "&PartitionStatus=" + status;
    document.getElementById("myCountdown").style.display = "block";
    document.getElementById("bodyCountdown").innerHTML = "Partition " + partitionNo + " is being Armed to Stay mode";

    $.getJSON(hrefArm, function(data) {
      if (data.Command == 'ChangeStatusArm' && data.Reply == true) {
        var timeleft = 10;
        var downloadTimer = setInterval(function() {
          if (timeleft <= 0) {
            clearInterval(downloadTimer);
            document.getElementById("countdown").innerHTML = "Finished";
            setTimeout(function() {
              location.reload();
            }, 2000);
          } else {
            document.getElementById("countdown").innerHTML = timeleft;
          }
          timeleft -= 1;
        }, 1000);
      }
    });
  }

  function sleepFunction(AlarmInfoSleep) {

    var fields = AlarmInfoSleep.split(" ");
    var serialNo = fields[0];
    var partitionNo = fields[1];
    var status = fields[2];

    hrefArm = baseURL + "/alarmChangeStatusArm.php/?SerialNo=" + serialNo + "&PartitionNo=" + partitionNo + "&PartitionStatus=" + status;
    document.getElementById("myCountdown").style.display = "block";
    document.getElementById("bodyCountdown").innerHTML = "Partition " + partitionNo + " is being Armed to Sleep mode";

    $.getJSON(hrefArm, function(data) {
      if (data.Command == 'ChangeStatusArm' && data.Reply == true) {
        var timeleft = 10;
        var downloadTimer = setInterval(function() {
          if (timeleft <= 0) {
            clearInterval(downloadTimer);
            document.getElementById("countdown").innerHTML = "Finished";
            setTimeout(function() {
              location.reload();
            }, 2000);
          } else {
            document.getElementById("countdown").innerHTML = timeleft;
          }
          timeleft -= 1;
        }, 1000);
      }
    });

  }

  function armFunction(AlarmInfoArm) {

    var fields = AlarmInfoArm.split(" ");
    var serialNo = fields[0];
    var partitionNo = fields[1];
    var status = fields[2];

    hrefArm = baseURL + "/alarmChangeStatusArm.php/?SerialNo=" + serialNo + "&PartitionNo=" + partitionNo + "&PartitionStatus=" + status;
    document.getElementById("myCountdown").style.display = "block";
    document.getElementById("bodyCountdown").innerHTML = "Partition " + partitionNo + " is being Armed";

    $.getJSON(hrefArm, function(data) {
      if (data.Command == 'ChangeStatusArm' && data.Reply == true) {
        var timeleft = 10;
        var downloadTimer = setInterval(function() {
          if (timeleft <= 0) {
            clearInterval(downloadTimer);
            document.getElementById("countdown").innerHTML = "Finished";
            setTimeout(function() {
              location.reload();
            }, 2000);
          } else {
            document.getElementById("countdown").innerHTML = timeleft;
          }
          timeleft -= 1;
        }, 1000);
      }
    });
  }

  function closeForm(id) {
    document.getElementById(id).style.display = "none";
  }
</script>

<body>
  <?php
  $SerialGateway = $_SESSION['serialGateway'];
  $Password = $_SESSION['password'];
  $LocationListArray = $_SESSION['location'];
  $SmartSwitchStatus = $_SESSION['SmartSwitchStatus'];
  $CurtainStatus = $_SESSION['CurtainStatus'];
  $AlarmStatus = $_SESSION['AlarmStatus'];
  $ShutterStatus = $_SESSION['ShutterStatus'];
  $IRBlasterStatus = $_SESSION['IRBlasterStatus'];
  $LockStatus = $_SESSION['LockStatus'];
  $TotalDevice = $_SESSION['TotalDevice'];
  $SceneCount = $_SESSION['SceneCount'];
  $AdminPassword = $_SESSION['AdminPassword'];

  echo "<input style='display:none;' id='SerialGateway' value='$SerialGateway'>";
  echo "<input style='display:none;' id='Password' value='$Password'>";
  echo "<input style='display:none;' id='AdminPassword' value='$AdminPassword'>";

  if (isset($_POST['Logout'])) {
    header("Location: ChooseGateway.php");
    header("Refresh:0");
    ob_flush();
    session_destroy();
    exit();
  }

  if (isset($_POST['Home'])) {
    // header("Refresh:0");
    header("Location: Dashboard.php");
    ob_flush();
    exit();
  }

  $opts = array('http' => array('header' => 'Cookie: ' . $_SERVER['HTTP_COOKIE'] . "\r\n"));
  $context = stream_context_create($opts);
  session_write_close(); // unlock the file
  $url = $baseURL . "/alarmSmartAlarm.php";
  $SmartAlarmJson = file_get_contents($url, false, $context);
  $msgJson = json_decode($SmartAlarmJson);

  if (!empty($msgJson->Message)) {
    $Error = $msgJson->Message;
  ?>
    <script type="text/javascript">
      Swal.fire({
        icon: 'error',
        title: 'Failed',
        text: '<?php echo $Error; ?>',
      }).then(function() {
        window.location.href = "ChooseGateway.php";
      });
    </script>
  <?php
    die();
  }

  function getList($msgJson)
  {
    //To decompress all the data from gateway
    $Compression = $msgJson->Compression;
    $decode = base64_decode($Compression);
    $Decompress = gzdecode($decode);
    $msgJson =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));

    if ($msgJson->Command == 'GetSmartAlarmParadoxAPI' && $msgJson->Reply == true) {

      //To decompress all the data from gateway
      $Compress = $msgJson->Compress;
      $decode = base64_decode($Compress);
      $Decompress = gzdecode($decode);
      $msgJson =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));

      $SmartAlarmParadox =  $msgJson->SmartAlarmParadox;
      if (!empty($SmartAlarmParadox)) {

        $PartitionStatus1 = $SmartAlarmParadox->Partitions[0]->PartitionStatus;
        $Partition1Off = "";
        $Partition1Stay = "";
        $Partition1Sleep = "";
        $Partition1Arm = "";

        if ($PartitionStatus1 == 1) {
          $status1 = "Arm";
        } else if ($PartitionStatus1 == 2) {
          $status1 = "Stay";
        } else if ($PartitionStatus1 == 3) {
          $status1 = "Off";
        } else if ($PartitionStatus1 == 4) {
          $status1 = "Sleep";
        }

        $PartitionStatus2 = $SmartAlarmParadox->Partitions[1]->PartitionStatus;
        $Partition2Off = "";
        $Partition2Stay = "";
        $Partition2Sleep = "";
        $Partition2Arm = "";

        if ($PartitionStatus2 == 1) {
          $status2 = "Arm";
        } else if ($PartitionStatus2 == 2) {
          $status2 = "Stay";
        } else if ($PartitionStatus2 == 3) {
          $status2 = "Off";
        } else if ($PartitionStatus2 == 4) {
          $status2 = "Sleep";
        }

        $SerialNo = $SmartAlarmParadox->SerialNo;

        if ($PartitionStatus1 == 3) {
          $Partition1Off = "disabled";
          $Partition1Stay = "";
          $Partition1Sleep = "";
          $Partition1Arm = "";
        } else {
          $Partition1Off = "";
          $Partition1Stay = "disabled";
          $Partition1Sleep = "disabled";
          $Partition1Arm = "disabled";
        }

        if ($PartitionStatus2 == 3) {
          $Partition2Off = "disabled";
          $Partition2Stay = "";
          $Partition2Sleep = "";
          $Partition2Arm = "";
        } else {
          $Partition2Off = "";
          $Partition2Stay = "disabled";
          $Partition2Sleep = "disabled";
          $Partition2Arm = "disabled";
        }

        $AlarmInfoOff1 = $SerialNo . ' ' . 1 . ' ' . 3;
        $AlarmInfoStay1 = $SerialNo . ' ' . 1 . ' ' . 2;
        $AlarmInfoSleep1 = $SerialNo . ' ' . 1 . ' ' . 4;
        $AlarmInfoArm1 = $SerialNo . ' ' . 1 . ' ' . 1;

        $AlarmInfoOff2 = $SerialNo . ' ' . 2 . ' ' . 3;
        $AlarmInfoStay2 = $SerialNo . ' ' . 2 . ' ' . 2;
        $AlarmInfoSleep2 = $SerialNo . ' ' . 2 . ' ' . 4;
        $AlarmInfoArm2 = $SerialNo . ' ' . 2 . ' ' . 1;


        echo "
            <div class='container'>
            <div>
            <div id='myDIV1'>
            <div class='grid-container'>
                    <img class='cardHeader' src='Images/SAC/parti01.svg' alt='Part 1 logo' style='width:50px;height:50px;'>
                    <h3 class='cardHeader' >Partition 1</h3>
                    <h3 class='cardHeader' style='font-size:18px;'>$status1</h3>
            </div>
            <div class='row'>
            <div class='col-xs-5 col-md-3'>
                <div class='card'>
                <input $Partition1Off class='btnAction' type='image' src='Images/SAC/off.svg' alt='off' onclick='openOffPop1()'>
                <p style='font-size:2vh;'>Disarm</p>
                </div>
            </div>

            <div class='col-xs-5 col-md-3'>
                <div class='card'>
                <input $Partition1Stay class='btnAction' type='image' src='Images/SAC/stay.svg' alt='stay' onclick='stayFunction(&#39;$AlarmInfoStay1&#39;)'>
                <p style='font-size:2vh;'>Stay</p>
                </div>
            </div>
            
            <div class='col-xs-5 col-md-3'>
                <div class='card'>
                <input $Partition1Sleep class='btnAction' type='image' src='Images/SAC/sleep.svg' alt='sleep' onclick='sleepFunction(&#39;$AlarmInfoSleep1&#39;)'>
                <p style='font-size:2vh;'>Sleep</p>
                </div>
            </div>
            
            <div class='col-xs-5 col-md-3'>
                <div class='card'>
                <input $Partition1Arm class='btnAction' type='image' src='Images/SAC/arm.svg' alt='arm' onclick='armFunction(&#39;$AlarmInfoArm1&#39;)'>
                <p style='font-size:2vh;'>Arm</p>
                </div>
            </div>
            </div>
            </div>
            </div>
            </div>

            <div class='container'>
            <div>
            <div id='myDIV2'>
            <div class='grid-container'>
                    <img class='cardHeader' src='Images/SAC/parti02.svg' alt='Part 2 logo' style='width:50px;height:50px;'>
                    <h3 class='cardHeader'>Partition 2</h3>
                    <h3 class='cardHeader' style='font-size:18px;'>$status2</h3>
            </div>
            <div class='row'>
            <div class='col-xs-5 col-md-3'>
                <div class='card'>
                <input $Partition2Off class='btnAction' type='image' src='Images/SAC/off.svg' alt='off' onclick='openOffPop2()'>
                <p style='font-size:2vh;'>Disarm</p>
                </div>
            </div>

            <div class='col-xs-5 col-md-3'>
                <div class='card'>
                <input $Partition2Stay class='btnAction' type='image' src='Images/SAC/stay.svg' alt='stay' onclick='stayFunction(&#39;$AlarmInfoStay2&#39;)'>
                <p style='font-size:2vh;'>Stay</p>
                </div>
            </div>
            
            <div class='col-xs-5 col-md-3'>
                <div class='card'>
                <input $Partition2Sleep class='btnAction' type='image' src='Images/SAC/sleep.svg' alt='sleep' onclick='sleepFunction(&#39;$AlarmInfoSleep2&#39;)'>
                <p style='font-size:2vh;'>Sleep</p>
                </div>
            </div>
            
            <div class='col-xs-5 col-md-3'>
                <div class='card'>
                <input $Partition2Arm class='btnAction' type='image' src='Images/SAC/arm.svg' alt='arm' onclick='armFunction(&#39;$AlarmInfoArm2&#39;)'>
                <p style='font-size:2vh;'>Arm</p>
                </div>
            </div>
            </div>
            </div>
            </div>
            </div>";
        echo "<br/><br/>";

        echo "<div class='form-popup' id='myForm1'>
            <div class='form-container' style='width:400px;padding:0px;'>
            <div class='modal-header' style='width:100%;height:60px;'>
            <h3 class='h2form'>Disarmed Partition 1</h3>
            </div>
            <div class='modal-body' style='width:100%;'>
            <fieldset style='width:100%;'>
              <label for='name'>Enter Password:</label>
              <input type='password' id='pass1' maxlength='6' style='width:100%;'>
            </fieldset>
            </div>
            <div class='modal-footer' style='width:100%;text-align:-webkit-center;'>    
            <button type='button' class='all' onclick='off1Function(&#39;$AlarmInfoOff1&#39;)'>Disarm Now</button>
            <button type='button' class='all' onclick='closeForm(&#39;myForm1&#39;)'>Close</button>
            </div>
            </div>
            </div>";

        echo "<div class='form-popup' id='myForm2'>
            <div class='form-container' style='width:400px;padding:0px;'>
            <div class='modal-header' style='width:100%;height:60px;'>
            <h3 class='h2form'>Disarmed Partition 2</h3>
            </div>
            <div class='modal-body' style='width:100%;'>
            <fieldset style='width:100%;'>
              <label for='name'>Enter Password:</label>
              <input type='password' id='pass2' maxlength='6' style='width:100%;'>
            </fieldset>
            </div>
            <div class='modal-footer' style='width:100%;text-align:-webkit-center;'>    
            <button type='button' class='all' onclick='off2Function(&#39;$AlarmInfoOff2&#39;)'>Disarm Now</button>
            <button type='button' class='all' onclick='closeForm(&#39;myForm2&#39;)'>Close</button>
            </div>
            </div>
            </div>";
      } else {
        echo "<h2 class='error'><i class='fas fa-exclamation-circle fa-5x' style='color:red;'></i> </br></br>
        Smart Alarm Not Available For This Selected Gateway </br>
        Please Back To Home Page</h2>";
      }
    } else {
      echo "<h2 class='error'><i class='fas fa-exclamation-circle fa-5x' style='color:red;'></i> </br></br>
      Smart Alarm Not Available For This Selected Gateway </br>
      Please Back To Home Page</h2>";
    }
  }

  ?>
  <input style='display:none;' id='IntervalId' value='none'>

  <!-- Form for sign Door Lock -->
  <div class="form-popup" id="mySiginDoorLockForm">
    <div class="form-container" style="width:400px;padding:0px;">
      <div class="modal-header" style='width:100%;height:70px;'>
        <!-- <a class='back' onclick='closeFormSigin()'><span class='fa fa-arrow-left fa-2x'></a> -->
        <h3 class='h2form'>Manage Door Lock </br> Password</h3>
      </div>
      <div class="modal-body" style='width:100%;'>
        <fieldset style='width:100%;'>
          <label for="name">Enter Admin Password:</label>
          <input type="password" id="password" name="DoorlockPassword" style='width:100%;'>
        </fieldset>
      </div>
      <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
        <button type="submit" class="all" onclick='openGetLock()' name="signin">Ok</button>
        <button type="button" class="all" onclick="cancelFormSigin()">Cancel</button>
      </div>
    </div>
  </div>

  <!-- SideBar + Navbar -->
  <div class="wrapper">
    <!-- Sidebar Holder -->
    <nav id="sidebar">
      <div class="sidebar-header">
        <h3>Senzo</h3>
        <strong>SZ</strong>
      </div>
      <ul class="list-unstyled components">
        <!-- Home Section -->
        <li>
          <form action='SmartAlarm.php' method='POST' id='NavHome' style='margin-block-end: 0'>
            <input style='display:none;' name='Home'>
            <a style='cursor:pointer;' class='Home'>
              <i class="glyphicon glyphicon-home"></i>
              Home
            </a>
          </form>
        </li>
        <li>
          <!-- Device Section -->
          <a href="#pageDevice" data-toggle="collapse" aria-expanded="false">
            <i class="glyphicon glyphicon-tasks"></i>
            <?php echo "Device"; ?>
          </a>
          <ul class="collapse list-unstyled" id="pageDevice">
            <?php
            if ($SmartSwitchStatus) {
              echo '<li><a style="cursor:pointer;" onclick="selectSwitch()">Smart Switch</a></li>';
            }
            if ($CurtainStatus) {
              echo '<li><a style="cursor:pointer;" onclick="selectCurtain()">Smart Curtain</a></li>';
            }
            if ($ShutterStatus) {
              echo '<li><a style="cursor:pointer;" onclick="selectShutter()">Smart Shutter</a></li>';
            }
            if ($LockStatus) {
              echo '<li><a style="cursor:pointer;" onclick="selectDoorLock()">Smart Door Lock</a></li>';
            }
            if ($IRBlasterStatus) {
              echo '<li><a style="cursor:pointer;" onclick="selectIRBlaster()">IR Blaster</a></li>';
            }
            ?>
          </ul>
        </li>
        <li>
          <!-- Scene Section -->
          <a style="cursor:pointer;" onclick='openScenesForm()'>
            <i class="glyphicon glyphicon-film"></i>
            <?php echo "Scene ($SceneCount)"; ?>
          </a>
        </li>
        <li>
          <!-- Security Section -->
          <a href="#pageSecurity" data-toggle="collapse" aria-expanded="false">
            <i class="glyphicon glyphicon-lock"></i>
            <?php echo "Security"; ?>
          </a>
          <ul class="collapse list-unstyled in" id="pageSecurity">
            <?php
             // If smart alarm connected to gateway
            if ($AlarmStatus) {
              echo "<li><a id='cube' style='cursor:pointer;' onclick='selectAlarm(&#39;True&#39;)'>
                            Smart Alarm <img src='Images/Action/security_armed_icon.png' alt='scenes' style='width:20px;height:20px;'>";
            } else {
              echo "<li><a id='cube' style='cursor:pointer;' onclick='selectAlarm(&#39;False&#39;)'>
                            Smart Alarm";
            }
            ?>
            </a>
        </li>
        <li><a style="cursor:pointer;" onclick="selectCamera()">Camera</a></li>
      </ul>
      </li>
      <li>
        <!-- Analytic Section -->
        <a style='cursor:pointer;' onclick='openAnalyticForm()'>
          <i class='glyphicon glyphicon-stats'></i>
          Analytic
        </a>
      </li>
      <li>
        <!-- Setting Section -->
        <a style='cursor:pointer;' onclick='openSettingForm()'>
          <i class='glyphicon glyphicon-cog'></i>
          Setting
        </a>
      </li>
      </ul>

       <!-- Logout button -->
      <ul class="list-unstyled CTAs">
        <li>
          <form method="POST" action="SmartAlarm.php" id='NavLogout' style='margin-block-end: 0'>
            <input style='display:none;' name='Logout'>
            <a style="cursor:pointer;" class="Logout">
              <i class="glyphicon glyphicon-log-out"></i>
              Logout
            </a>
          </form>
        </li>
      </ul>
    </nav>

    <!-- Page Content Holder -->
    <div id="content" style="width: -webkit-fill-available;">

      <nav class="navbar navbar-default">
        <div class="container-fluid">

          <div class="navbar-header">
            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
              <i class="glyphicon glyphicon-align-left"></i>
              <span></span>
            </button>
          </div>

          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
              <li><a></a></li>
            </ul>
          </div>
        </div>
      </nav>

      <h2>Smart Alarm Controller</h2>
      <?php
      $GatewayLevel = $_SESSION['level'];
      echo "<h4 class='Level' >Gateway $GatewayLevel</h4>";
      getList($msgJson);
      ?>
      <div class="line"></div>

    </div>
  </div>

  <!-- Auto Refresh dropdown -->
  <div class='autorefresh'>
    <div class="dropdown">
      <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">
        <img src='Images/Switch/settings_gear.png' alt='switchon' style='width:30px;height:30px;animation: spin 3s linear infinite;'>
      </button>
      <ul class="dropdown-menu">
        <!-- <li><a href="#">Phantom</a></li> -->
        <li>
          <input type='checkbox' id='enable' name='fooby[2][]'>
          <label> Enable</label><br>
          <div id='timeRefresh' style='display:none;margin-left:10px;'>
            <input type='checkbox' value='1-minute' class="slectOne" id='1-minute'>
            <label> 1 minute</label><br>
            <input type='checkbox' value='5-minutes' class="slectOne" id='5-minutes'>
            <label> 5 minutes</label><br>
            <input type='checkbox' value='10-minutes' class="slectOne" id='10-minutes'>
            <label> 10 minutes</label><br>
            <input type='checkbox' value='20-minutes' class="slectOne" id='20-minutes'>
            <label> 20 minutes</label><br>
            <input type='checkbox' value='25-minutes' class="slectOne" id='25-minutes'>
            <label> 25 minutes</label><br>
            <input type='checkbox' value='1-hour' class="slectOne" id='1-hour'>
            <label> 1 hour</label><br>
          </div>
        </li>
        <li>
          <input type='checkbox' id='disable' value='disable'>
          <label> Disable</label><br>
        </li>
      </ul>
    </div>
  </div>

  <!-- Loading animation -->
  <div class="form-popup" id='myLoader'>
    <div class="loading-container">
      <div class="loading"></div>
      <div class="loading-text">loading</div>
    </div>
  </div>

  <!-- Countdown animation -->
  <div class="form-popup" id='myCountdown'>
    <div class="form-container" style="width:400px;padding:0px;">
      <div class="modal-header" style='width:100%;height:60px;'>
        <h3 class='h2form'>Arming</h3>
      </div>
      <div class="modal-body" style='width:100%;'>
        <h4 class='h2form' id='bodyCountdown'></h4>
        <div class="loading-containerAlarm">
          <div class="loading countdown"></div>
        </div>
        <div class='countdown-text'>Please wait...</div>
        <div class='countdown-text' id="countdown"></div>
      </div>
    </div>
  </div>
</body>

</html>