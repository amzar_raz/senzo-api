<?php
//This variable is used to initialize the database using phpmyadmin

//Database connection initialization
$hostname = "localhost";
$dbname = "iot_gateway";
$username = "root";
$password = "";

//Database attribute name based on attribute name declared inside database table
$tablename = "tbl_gateway";
$gatewayId = "id";
$gatewayName = "Name";
$gatewaySerial = "Serial";
$gatewayLocation = "Location";
$gatewayLevel = "Level";
$gatewayPassword = "Password";
?>