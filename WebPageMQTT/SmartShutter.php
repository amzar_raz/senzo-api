<?php
session_start();
ob_start();
ini_set('max_execution_time', 30);
include '././configuration/serverConfig.php';
include './header/headerAll.php';
?>
<!DOCTYPE html>
<html>

<head>
    <title>Smart Shutter</title>
    <link href="css/SmartShutter.css" rel="stylesheet">
    <link rel="stylesheet" href="css/Sidebar.css">
    <script type="text/javascript" src="SidebarHandler.js"></script>
</head>
<script type="text/javascript">
    function openShutterForm(ShutterDetail) {
        var field = ShutterDetail.split("&");
        var shutterSerial = field[0];
        var shutterUserSerialNo = field[1];
        var shutterName = field[2];
        var shutterLocationName = field[3];
        var shutterRSSI = field[4];

        document.getElementsByClassName("actionShutter").value = shutterSerial;
        document.getElementsByClassName("RSSI").value = shutterRSSI;
        document.getElementById("myPopupAction" + shutterSerial).style.display = "block";
    }

    function actionShutter(Switch) {

        var serial = document.getElementsByClassName("actionShutter").value;
        var RSSI = document.getElementsByClassName("RSSI").value;
        var switchno = Switch;
        var loader = document.getElementById("loader");
        var hrefShutterAction = baseURL + "/curtainshutterAction.php/?SerialNo=" + serial + "&SwitchNo=" + switchno;

        if (switchno == 1 || switchno == 3) {
            if (switchno == 1) {
                var text = "Do you want to Open the Shutter?"
            } else {
                var text = "Do you want to Close the Shutter?"
            }
            Swal.fire({
                title: 'Confirmation',
                text: text,
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.value) {
                    loader.style.display = "block";
                    $.getJSON(hrefShutterAction, function(data) {
                        //data is the JSON string
                        loader.style.display = "none";
                        if (data.Command == 'SetCurtainFireLevel' && data.Status == true) {
                            if (switchno == 1) {
                                iziToast.show({
                                    title: 'SHUTTER ACTION',
                                    message: 'Shutter is opening now',
                                    theme: 'dark',
                                    position: 'bottomCenter',
                                    icon: 'icon-person'
                                });
                            } else {
                                iziToast.show({
                                    title: 'SHUTTER ACTION',
                                    message: 'Shutter is closing now',
                                    theme: 'dark',
                                    position: 'bottomCenter',
                                    icon: 'icon-person'
                                });
                            }
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Failed',
                                text: data.Message
                            })
                        }
                    });
                }
            })
        }

        if (switchno == 2) {
            loader.style.display = "block";
            $.getJSON(hrefShutterAction, function(data) {
                //data is the JSON string
                loader.style.display = "none";
                console.log(data);
                if (data.Command == 'SetCurtainFireLevel' && data.Status == true) {
                    iziToast.show({
                        title: 'SHUTTER ACTION',
                        message: 'Shutter is stopped',
                        theme: 'dark',
                        position: 'bottomCenter',
                        icon: 'icon-person'
                    });
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: data.Message
                    })
                }
            });
        }


    }

    function openDelayForm(delayInfo) {
        var serial = document.getElementsByClassName("actionShutter").value;
        document.getElementById("myPopupAction" + serial).style.display = "none";

        var fields = delayInfo.split(" ");
        var timeDelay = JSON.parse(fields[0]);
        var switchno = fields[1];
        var timerEnable = fields[2];

        document.getElementsByClassName("switchno").value = switchno;

        if (switchno == 1) {
            var action = "OPEN"
            var SwitchAction = 'On';
        } else {
            var action = "CLOSE"
            var SwitchAction = 'Off';
        }
        document.getElementById("labelaction").innerHTML = "Action: " + action;

        if (timerEnable == 'true') {
            // document.getElementById("delayTimer").style.color = "red";
            document.getElementById("delayTimer").innerHTML = "Shutter will turn " + SwitchAction + " in about " + timeDelay.h + " hours " + timeDelay.i + " minutes";
            document.getElementById("hours").value = timeDelay.h;
            document.getElementById("minutes").value = timeDelay.i;
        } else {
            document.getElementById("delayTimer").innerHTML = "Timer is not active";
            document.getElementById("hours").value = "";
            document.getElementById("minutes").value = "";
        }
        document.getElementById("myDelayForm").style.display = "block";
    }

    function choose24Timer(switchInfo) {
        var serial = document.getElementsByClassName("actionShutter").value;
        document.getElementById("myPopupAction" + serial).style.display = "none";


        var fields = switchInfo.split(" ");
        var onDays = fields[0];
        var switchno = fields[1];
        var ontime = fields[2];

        document.getElementsByClassName("switchno").value = switchno;
        document.getElementsByClassName("onDays").value = onDays;
        document.getElementsByClassName("ShutterInfo").value = ontime;

        var obj = JSON.parse(ontime);

        if (switchno == 1) {
            OnTimeEnabled_1 = obj[0].OnTimeEnabled;
            OnTimeEnabled_2 = obj[1].OnTimeEnabled;

            document.getElementById("enableTimer").innerHTML = "Enable ON Timer ";
            document.getElementById("setTime").innerHTML = "Set Shutter Open Time";
            document.getElementById("setDay").innerHTML = "Set ON Day";

            if (OnTimeEnabled_1 == true) {
                document.getElementById("t1").style.color = "red";
                document.getElementById("action1").style.color = "red";
                var OnMon = obj[0].OnMon == true ? "Mon" : "";
                var OnTue = obj[0].OnTue == true ? "Tue" : "";
                var OnWed = obj[0].OnWed == true ? "Wed" : "";
                var OnThu = obj[0].OnThu == true ? "Thu" : "";
                var OnFri = obj[0].OnFri == true ? "Fri" : "";
                var OnSat = obj[0].OnSat == true ? "Sat" : "";
                var OnSun = obj[0].OnSun == true ? "Sun" : "";
                if (OnMon == "Mon" && OnTue == "Tue" && OnWed == "Wed" && OnThu == "Thu" && OnFri == "Fri" && OnSat == "Sat" && OnSun == "Sun") {
                    var Onday = "Everyday";
                } else {
                    var Onday = OnMon + OnTue + OnWed + OnThu + OnFri + OnSat + OnSun;
                }
                document.getElementById("t1").innerHTML = "Status : Active";
                document.getElementById("action1").innerHTML = "Turn on at " + tConvert(obj[0].OnTime) + "." + " " + Onday;
            } else {
                document.getElementById("t1").style.color = "black";
                document.getElementById("action1").style.color = "black";
                document.getElementById("t1").innerHTML = "Status : Inactive";
                document.getElementById("action1").innerHTML = "";
            }

            if (OnTimeEnabled_2 == true) {
                document.getElementById("t2").style.color = "red";
                document.getElementById("action2").style.color = "red";
                var OnMon = obj[1].OnMon == true ? "Mon" : "";
                var OnTue = obj[1].OnTue == true ? "Tue" : "";
                var OnWed = obj[1].OnWed == true ? "Wed" : "";
                var OnThu = obj[1].OnThu == true ? "Thu" : "";
                var OnFri = obj[1].OnFri == true ? "Fri" : "";
                var OnSat = obj[1].OnSat == true ? "Sat" : "";
                var OnSun = obj[1].OnSun == true ? "Sun" : "";
                if (OnMon == "Mon" && OnTue == "Tue" && OnWed == "Wed" && OnThu == "Thu" && OnFri == "Fri" && OnSat == "Sat" && OnSun == "Sun") {
                    var Onday = "Everyday";
                } else {
                    var Onday = OnMon + OnTue + OnWed + OnThu + OnFri + OnSat + OnSun;
                }
                document.getElementById("t2").innerHTML = "Status : Active";
                document.getElementById("action2").innerHTML = "Turn on at " + tConvert(obj[1].OnTime) + "." + " " + Onday;
            } else {
                document.getElementById("t2").style.color = "black";
                document.getElementById("action2").style.color = "black";
                document.getElementById("t2").innerHTML = "Status : Inactive";
                document.getElementById("action2").innerHTML = "";
            }
        } else if (switchno == 3) {
            OnTimeEnabled_1 = obj[0].OnTimeEnabled;
            OnTimeEnabled_2 = obj[1].OnTimeEnabled;

            document.getElementById("enableTimer").innerHTML = "Enable OFF Timer ";
            document.getElementById("setTime").innerHTML = "Set Shutter Close Time";
            document.getElementById("setDay").innerHTML = "Set OFF Day";

            if (OnTimeEnabled_1 == true) {
                document.getElementById("t1").style.color = "red";
                document.getElementById("action1").style.color = "red";
                var OnMon = obj[0].OnMon == true ? "Mon" : "";
                var OnTue = obj[0].OnTue == true ? "Tue" : "";
                var OnWed = obj[0].OnWed == true ? "Wed" : "";
                var OnThu = obj[0].OnThu == true ? "Thu" : "";
                var OnFri = obj[0].OnFri == true ? "Fri" : "";
                var OnSat = obj[0].OnSat == true ? "Sat" : "";
                var OnSun = obj[0].OnSun == true ? "Sun" : "";
                if (OnMon == "Mon" && OnTue == "Tue" && OnWed == "Wed" && OnThu == "Thu" && OnFri == "Fri" && OnSat == "Sat" && OnSun == "Sun") {
                    var Onday = "Everyday";
                } else {
                    var Onday = OnMon + OnTue + OnWed + OnThu + OnFri + OnSat + OnSun;
                }
                document.getElementById("t1").innerHTML = "Status : Active";
                document.getElementById("action1").innerHTML = "Turn off at " + tConvert(obj[0].OnTime) + "." + " " + Onday;
            } else {
                document.getElementById("t1").style.color = "black";
                document.getElementById("action1").style.color = "black";
                document.getElementById("t1").innerHTML = "Status : Inactive";
                document.getElementById("action1").innerHTML = "";
            }

            if (OnTimeEnabled_2 == true) {
                document.getElementById("t2").style.color = "red";
                document.getElementById("action2").style.color = "red";
                var OnMon = obj[1].OnMon == true ? "Mon" : "";
                var OnTue = obj[1].OnTue == true ? "Tue" : "";
                var OnWed = obj[1].OnWed == true ? "Wed" : "";
                var OnThu = obj[1].OnThu == true ? "Thu" : "";
                var OnFri = obj[1].OnFri == true ? "Fri" : "";
                var OnSat = obj[1].OnSat == true ? "Sat" : "";
                var OnSun = obj[1].OnSun == true ? "Sun" : "";
                if (OnMon == "Mon" && OnTue == "Tue" && OnWed == "Wed" && OnThu == "Thu" && OnFri == "Fri" && OnSat == "Sat" && OnSun == "Sun") {
                    var Onday = "Everyday";
                } else {
                    var Onday = OnMon + OnTue + OnWed + OnThu + OnFri + OnSat + OnSun;
                }
                document.getElementById("t2").innerHTML = "Status : Active";
                document.getElementById("action2").innerHTML = "Turn off at " + tConvert(obj[1].OnTime) + "." + " " + Onday;
            } else {
                document.getElementById("t2").style.color = "black";
                document.getElementById("action2").style.color = "black";
                document.getElementById("t2").innerHTML = "Status : Inactive";
                document.getElementById("action2").innerHTML = "";
            }
        }

        document.getElementById("choose24Timer").style.display = "block";

    }

    function tConvert(time) {
        // Check correct time format and split into components
        time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

        if (time.length > 1) { // If time format correct
            time = time.slice(1); // Remove full string match value
            time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
            time[0] = +time[0] % 12 || 12; // Adjust hours
        }
        return time.join(''); // return adjusted time or original string
    }

    function open24HForm(timerNo) {

        var ShutterInfo = document.getElementsByClassName("ShutterInfo").value;
        var obj = JSON.parse(ShutterInfo);

        console.log(obj);

        if (timerNo == 1) {
            if (obj[0].OnTimeEnabled == true) {
                var OnMon = obj[0].OnMon == true ? "Mon" : "";
                var OnTue = obj[0].OnTue == true ? "Tue" : "";
                var OnWed = obj[0].OnWed == true ? "Wed" : "";
                var OnThu = obj[0].OnThu == true ? "Thu" : "";
                var OnFri = obj[0].OnFri == true ? "Fri" : "";
                var OnSat = obj[0].OnSat == true ? "Sat" : "";
                var OnSun = obj[0].OnSun == true ? "Sun" : "";
                if (OnMon == "Mon" && OnTue == "Tue" && OnWed == "Wed" && OnThu == "Thu" && OnFri == "Fri" && OnSat == "Sat" && OnSun == "Sun") {
                    var Onday = "Everyday";
                } else {
                    var Onday = OnMon + OnTue + OnWed + OnThu + OnFri + OnSat + OnSun;
                }
                document.getElementById("status").innerHTML = "Turn on at " + tConvert(obj[0].OnTime) + "." + " " + Onday;
                document.getElementById("switch").checked = true;
                document.getElementById("time").value = obj[0].OnTime;
                document.getElementById("day1").checked = obj[0].OnSun;
                document.getElementById("day2").checked = obj[0].OnMon;
                document.getElementById("day3").checked = obj[0].OnTue;
                document.getElementById("day4").checked = obj[0].OnWed;
                document.getElementById("day5").checked = obj[0].OnThu;
                document.getElementById("day6").checked = obj[0].OnFri;
                document.getElementById("day7").checked = obj[0].OnSat;
                document.getElementById("time").disabled = false;
                document.getElementById("day1").disabled = false;
                document.getElementById("day2").disabled = false;
                document.getElementById("day3").disabled = false;
                document.getElementById("day4").disabled = false;
                document.getElementById("day5").disabled = false;
                document.getElementById("day6").disabled = false;
                document.getElementById("day7").disabled = false;
            } else {
                document.getElementById("status").innerHTML = "Timer 1 is not active";
                document.getElementById("switch").checked = false;
                document.getElementById("time").disabled = true;
                document.getElementById("time").value = "00:00";
                document.getElementById("day1").disabled = true;
                document.getElementById("day2").disabled = true;
                document.getElementById("day3").disabled = true;
                document.getElementById("day4").disabled = true;
                document.getElementById("day5").disabled = true;
                document.getElementById("day6").disabled = true;
                document.getElementById("day7").disabled = true;
                document.getElementById("day1").checked = false;
                document.getElementById("day2").checked = false;
                document.getElementById("day3").checked = false;
                document.getElementById("day4").checked = false;
                document.getElementById("day5").checked = false;
                document.getElementById("day6").checked = false;
                document.getElementById("day7").checked = false;
            }
        } else {
            if (obj[1].OnTimeEnabled == true) {
                var OnMon = obj[1].OnMon == true ? "Mon" : "";
                var OnTue = obj[1].OnTue == true ? "Tue" : "";
                var OnWed = obj[1].OnWed == true ? "Wed" : "";
                var OnThu = obj[1].OnThu == true ? "Thu" : "";
                var OnFri = obj[1].OnFri == true ? "Fri" : "";
                var OnSat = obj[1].OnSat == true ? "Sat" : "";
                var OnSun = obj[1].OnSun == true ? "Sun" : "";
                if (OnMon == "Mon" && OnTue == "Tue" && OnWed == "Wed" && OnThu == "Thu" && OnFri == "Fri" && OnSat == "Sat" && OnSun == "Sun") {
                    var Onday = "Everyday";
                } else {
                    var Onday = OnMon + OnTue + OnWed + OnThu + OnFri + OnSat + OnSun;
                }
                document.getElementById("status").innerHTML = "Turn on at " + tConvert(obj[1].OnTime) + "." + " " + Onday;
                document.getElementById("switch").checked = true;
                document.getElementById("time").value = obj[1].OnTime;
                document.getElementById("day1").checked = obj[1].OnSun;
                document.getElementById("day2").checked = obj[1].OnMon;
                document.getElementById("day3").checked = obj[1].OnTue;
                document.getElementById("day4").checked = obj[1].OnWed;
                document.getElementById("day5").checked = obj[1].OnThu;
                document.getElementById("day6").checked = obj[1].OnFri;
                document.getElementById("day7").checked = obj[1].OnSat;
                document.getElementById("time").disabled = false;
                document.getElementById("day1").disabled = false;
                document.getElementById("day2").disabled = false;
                document.getElementById("day3").disabled = false;
                document.getElementById("day4").disabled = false;
                document.getElementById("day5").disabled = false;
                document.getElementById("day6").disabled = false;
                document.getElementById("day7").disabled = false;
            } else {
                document.getElementById("status").innerHTML = "Timer 2 is not active";
                document.getElementById("switch").checked = false;
                document.getElementById("time").disabled = true;
                document.getElementById("time").value = "00:00";
                document.getElementById("day1").disabled = true;
                document.getElementById("day2").disabled = true;
                document.getElementById("day3").disabled = true;
                document.getElementById("day4").disabled = true;
                document.getElementById("day5").disabled = true;
                document.getElementById("day6").disabled = true;
                document.getElementById("day7").disabled = true;
                document.getElementById("day1").checked = false;
                document.getElementById("day2").checked = false;
                document.getElementById("day3").checked = false;
                document.getElementById("day4").checked = false;
                document.getElementById("day5").checked = false;
                document.getElementById("day6").checked = false;
                document.getElementById("day7").checked = false;
            }
        }

        document.getElementById("24Timer").innerHTML = "24 Hour Timer " + timerNo;
        document.getElementsByClassName("timerno").value = timerNo
        document.getElementById("my24HForm").style.display = "block";
        document.getElementById("choose24Timer").style.display = "none";

    }

    function disable() {

        if (document.getElementById("switch").checked) {
            document.getElementById("time").disabled = false;
            document.getElementById("day1").disabled = false;
            document.getElementById("day2").disabled = false;
            document.getElementById("day3").disabled = false;
            document.getElementById("day4").disabled = false;
            document.getElementById("day5").disabled = false;
            document.getElementById("day6").disabled = false;
            document.getElementById("day7").disabled = false;
        } else {
            document.getElementById("time").disabled = true;
            document.getElementById("day1").disabled = true;
            document.getElementById("day2").disabled = true;
            document.getElementById("day3").disabled = true;
            document.getElementById("day4").disabled = true;
            document.getElementById("day5").disabled = true;
            document.getElementById("day6").disabled = true;
            document.getElementById("day7").disabled = true;
        }

    }

    function myDelayTimerFunction() {
        var serial = document.getElementsByClassName("actionShutter").value;
        // var switchno = Switch;
        var switchno = document.getElementsByClassName("switchno").value;
        var timerHours = document.getElementsByName("hours");
        var timerMinutes = document.getElementsByName("minutes");
        var RSSI = document.getElementsByClassName("RSSI").value

        var Minutes = +timerMinutes[0].value + (+timerHours[0].value * 60);

        var boolOn = true;
        if (timerMinutes[0].value == 0 && timerHours[0].value == 0) {
            boolOn = false;
        }
        var hrefDelayTimer = baseURL + "/curtainshutterEnableDelayTimer.php/?SerialNo=" + serial + "&SwitchNo=" + switchno + "&Minutes=" + Minutes;
        var loader = document.getElementById("loader");
        if (boolOn == true) {
            loader.style.display = "block";
            var enable = true;
            $.getJSON(hrefDelayTimer, function(data) {
                //data is the JSON string
                loader.style.display = "none";
                if (data.Command == 'SetCurtainDelayTimer' && data.Status == false) {
                    enable = false;
                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: data.Message
                    })
                } else {
                    Swal.fire(
                        'Good job!',
                        'Delay Timer Has Been Set',
                        'success'
                    )
                    setTimeout(function() {
                        location.reload();
                    }, 3000);
                }
            });
        } else if (!boolOn) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'No time selected'
            })
        }
    }

    function disableDelayTimer() {
        var serial = document.getElementsByClassName("actionShutter").value;
        var switchno = document.getElementsByClassName("switchno").value;
        var RSSI = document.getElementsByClassName("RSSI").value

        var hrefDisableTimer = baseURL + "/curtainshutterDisableDelayTimer.php/?SerialNo=" + serial + "&SwitchNo=" + switchno;
        var loader = document.getElementById("loader");

        loader.style.display = "block";
        var disable = true;
        $.getJSON(hrefDisableTimer, function(data) {
            //data is the JSON string
            loader.style.display = "none";
            if (data.Command == 'CancelCurtainDelayTimer' && data.Status == false) {
                disable = false;
                Swal.fire({
                    icon: 'error',
                    title: 'Failed',
                    text: data.Message
                })
            } else {
                Swal.fire(
                    'Good job!',
                    'Delay Timer Has Been Removed',
                    'success'
                )
                setTimeout(function() {
                    location.reload();
                }, 2000);
            }
        });
    }

    function my24TimerFunction() {
        var serialNo = document.getElementsByClassName("actionShutter").value;
        var switchNo = document.getElementsByClassName("switchno").value;
        var timerNo = document.getElementsByClassName("timerno").value;
        var RSSI = document.getElementsByClassName("RSSI").value

        //For ON Timer
        //toggle button
        var OnTimeEnabled = document.getElementById('switch');
        var OnEnable = OnTimeEnabled.checked ? 'true' : 'false';

        var boolOnCheck = true;
        if (OnEnable == 'false') {
            boolOnCheck = false;
        }

        //Time and date
        var onTime = document.getElementById('time').value;
        var onDay = document.getElementsByName("dayCheck");
        var onDaybinary = 128;

        var boolOn = true;
        onDay.forEach(function(item) {
            if (item.checked) {
                onDaybinary += parseInt(item.value);
            }
        });

        //No day selected
        if (onDaybinary == 128) {
            boolOn = false;
        }

        //For offdays value
        var onDaysInfo = document.getElementsByClassName("onDays").value;
        var obj = JSON.parse(onDaysInfo);
        var offDaybinary = 128;
        if (timerNo == 1) {
            if (obj[0].OnSun == true) {
                offDaybinary = offDaybinary + 1;
            }
            if (obj[0].OnMon == true) {
                offDaybinary = offDaybinary + 2;
            }
            if (obj[0].OnTue == true) {
                offDaybinary = offDaybinary + 4;
            }
            if (obj[0].OnWed == true) {
                offDaybinary = offDaybinary + 8;
            }
            if (obj[0].OnThu == true) {
                offDaybinary = offDaybinary + 16;
            }
            if (obj[0].OnFri == true) {
                offDaybinary = offDaybinary + 32;
            }
            if (obj[0].OnSat == true) {
                offDaybinary = offDaybinary + 64;
            }
        } else {
            if (obj[1].OnSun == true) {
                offDaybinary = offDaybinary + 1;
            }
            if (obj[1].OnMon == true) {
                offDaybinary = offDaybinary + 2;
            }
            if (obj[1].OnTue == true) {
                offDaybinary = offDaybinary + 4;
            }
            if (obj[1].OnWed == true) {
                offDaybinary = offDaybinary + 8;
            }
            if (obj[1].OnThu == true) {
                offDaybinary = offDaybinary + 16;
            }
            if (obj[1].OnFri == true) {
                offDaybinary = offDaybinary + 32;
            }
            if (obj[1].OnSat == true) {
                offDaybinary = offDaybinary + 64;
            }
        }

        var OffEnable = 'false';
        var offTime = '00:00';

        var href24HTimer = baseURL + "/curtainshutterEnable24HTimer.php/?SerialNo=" + serialNo + "&SwitchNo=" + switchNo + "&TimerNo=" + timerNo + "&OnEnable=" + OnEnable +
            "&OffEnable=" + OffEnable + "&OnTime=" + onTime + "&OffTime=" + offTime + "&OnDays=" + onDaybinary + "&OffDays=" + offDaybinary;

        var loader = document.getElementById("loader");
        if (boolOn == true && boolOnCheck == true) {
            loader.style.display = "block";
            var enable = true;
            $.getJSON(href24HTimer, function(data) {
                //data is the JSON string
                console.log(data);
                loader.style.display = "none";
                if (data.Command == 'SetSmartCurtain24HoursTimer' && data.Status == false) {
                    enable = false;
                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: data.Message
                    })
                } else {
                    Swal.fire(
                        'Good job!',
                        '24H Timer Has Been Set',
                        'success'
                    )
                    setTimeout(function() {
                        location.reload();
                    }, 3000);
                }
            });
        } else if (!boolOnCheck) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'No action selected'
            })
        } else if (!boolOn) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'No day selected!'
            })
        }
    }

    function disable24Timer() {
        var serialNo = document.getElementsByClassName("actionShutter").value;
        var switchNo = document.getElementsByClassName("switchno").value;
        var timerNo = document.getElementsByClassName("timerno").value;
        var RSSI = document.getElementsByClassName("RSSI").value

        var hrefDisableTimer = baseURL + "/curtainshutterDisable24HTimer.php/?SerialNo=" + serialNo + "&SwitchNo=" + switchNo + "&TimerNo=" + timerNo;
        var loader = document.getElementById("loader");

        loader.style.display = "block";
        var disable = true;
        $.getJSON(hrefDisableTimer, function(data) {
            console.log(data);
            //data is the JSON string
            loader.style.display = "none";
            if (data.Command == 'CancelSmartCurtain24HoursTimer' && data.Status == false) {
                disable = false;
                Swal.fire({
                    icon: 'error',
                    title: 'Failed',
                    text: data.Message
                })
            } else {
                Swal.fire(
                    'Good job!',
                    '24H Timer Has Been Removed',
                    'success'
                )
                setTimeout(function() {
                    location.reload();
                }, 3000);
            }
        });
    }

    function openLogForm(param) {

        var paramSplit = param.split("&&"); //---- [0]= SN, [2]=switchNo ----//
        var url = baseURL + "/shutterReadLog.php/?SerialNo=" + paramSplit[0] + "&SwitchNo=" + paramSplit[1];
        var loader = document.getElementById("loader");
        document.getElementById("logName").innerHTML = "Device: " + paramSplit[2] + "(" + paramSplit[3] + ")";
        loader.style.display = "block";
        $.getJSON(url, function(data) {
            loader.style.display = "none";
            var LogList = data.DataLogList;
            var logNo = 1;

            if (LogList.length == 0) {
                $("#emptyLog").
                append(
                    '<div class="divNoLog">' +
                    '<img src="Images/smiling_bell.png" alt="smiling bell" width="100" height="100" class="smiley">' +
                    '<h4 style="text-align:center;">No log yet !</h4>' +
                    '<p style="text-align:center;">Check this section to know the activity of this device.</p>' +
                    '</div>'
                );
                document.getElementById("tableLog").style.display = "none";
            } else {
                for (i = LogList.length - 1; i >= 0; i--) {
                    var timeSplit = LogList[i].DateTime.split("T");
                    var statusSplit = LogList[i].Status.split(":");

                    $("#ShutterLogTable").
                    append(
                        '<tr class="trLogTable">' +
                        '<td>' + logNo + '</td>' +
                        '<td>' + timeSplit[0] + "<br/>" + timeSplit[1] + '</td>' +
                        '<td>' + '<b>' + statusSplit[0] + '</b>' + "<br/>" + statusSplit[1] + '</td>' +
                        '</tr>'
                    );
                    logNo++;
                }
                document.getElementById("tableLog").style.display = "block";
            }
        });
        $("#ShutterLogForm").fadeIn("fast", "swing");
    }

    function backLog(id) {
        document.getElementById(id).style.display = 'none';
        if (id == "ShutterLogForm") {
            if ($(".trLogTable")[0]) {
                // Do something if class exists
                $(".trLogTable").remove();
            } else {
                // Do something if class does not exist
                $(".divNoLog").remove();
            }
        }
    }

    function back() {
        var serial = document.getElementsByClassName("actionShutter").value;
        document.getElementById("myDelayForm").style.display = "none";
        document.getElementById("choose24Timer").style.display = "none";
        document.getElementById("myPopupAction" + serial).style.display = "block";
    }

    function back2back() {
        document.getElementById("my24HForm").style.display = "none";
        document.getElementById("choose24Timer").style.display = "block";
    }

    function closeForm(serial) {
        document.getElementById("myPopupAction" + serial).style.display = "none";
    }

    $(function() {
        $('#datetimepicker').datetimepicker({
            format: 'HH:mm'

        });
    });

    function openRSSI(Rssi) {
        // Swal.fire(
        //     'RSSI RESULT',
        //     'The RSSI value for the switch is :' + Rssi,
        //     'info'
        // )
        iziToast.show({
            title: 'RSSI RESULT',
            message: 'The RSSI value for the switch is :' + Rssi,
            theme: 'dark',
            position: 'topCenter',
            icon: 'icon-person'
        });
    }

    //For Show/Hide SN
    $(document).ready(function() {
        $("#showSn").click(function() {

            if ($(this).prop("checked") == true) {
                $(".gatewaySNText").fadeIn("fast", "swing");
                localStorage.setItem("displayGatewaySN", $(this).val());
            } else {
                $(".gatewaySNText").fadeOut("fast", "swing");
                localStorage.setItem("displayGatewaySN", "hideSn");
            }
        });
    });

    $(function() {
        var displayGatewaySN = localStorage.getItem("displayGatewaySN");
        if (displayGatewaySN == null) {
            displayGatewaySN = 'hideSn'
        }

        if (displayGatewaySN == 'showSn') {
            $('#showSn').prop('checked', true);
            $(".gatewaySNText").css("display", "block");
        } else {
            $('#showSn').prop('checked', false);
            $(".gatewaySNText").css("display", "none");
        }
    });

    //For table
    $(document).ready(function() {
        $('#myTable').DataTable();
    });

    //For location
    $(document).ready(function() {
        $('.LocationListItem').click(function() {
            // document.getElementById("LocationListForm" + $(this).text()).submit();
            var table = $('#myTable').DataTable();
            var value = document.getElementById("Location" + $(this).text()).value;
            document.getElementsByClassName("LocationName").value = value;
            table.draw();

            if ($(".data-location").contents().length > 0) {
                $(".data-location").remove();
            }
            $("#collapseFilter").
            append(
                '<div class="data-tag data-location" id="tag' + value + '">' +
                '<span class="data-tag-text" style="margin:0px 5px;">' + value + '</span>' +
                '<span onclick="removeFilter(&#39;' + value + '&#39;)"><i class="fas fa-times"></i></span>' +
                '</div>'
            );
            document.getElementById("collapseFilter").style.display = 'block';
            // window.alert($(this).text());
        });
    });

    function removeFilter(id) {
        var tagId = document.getElementById("tag" + id);
        var table = $('#myTable').DataTable();

        document.getElementsByClassName("LocationName").value = 'Null';

        tagId.remove();
        table.draw();

        if ($(".data-tag").contents().length == 0) {
            document.getElementById("collapseFilter").style.display = "none";
        }
    }

    //For validation hours and minutes
    $(document).ready(function() {
        function setInputFilter(textbox, inputFilter) {
            ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop", "number"].forEach(function(event) {
                textbox.addEventListener(event, function() {
                    if (inputFilter(this.value)) {
                        this.oldValue = this.value;
                        this.oldSelectionStart = this.selectionStart;
                        this.oldSelectionEnd = this.selectionEnd;
                    } else if (this.hasOwnProperty("oldValue")) {
                        this.value = this.oldValue;
                        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                    } else {
                        this.value = "";
                    }
                });
            });
        }

        setInputFilter(document.getElementById("hours"), function(value) {
            return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 23);
        });

        setInputFilter(document.getElementById("minutes"), function(value) {
            return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 59);
        });

    });

    //For search based on location
    $.fn.dataTable.ext.search.push(
        function(settings, data, dataIndex) {
            var LocationName = document.getElementsByClassName("LocationName").value;
            if (LocationName == null) {
                LocationName = 'Null';
            }
            var Location = data[2];

            if (LocationName == Location || LocationName == 'Null') {
                return true;
            }

            return false;

        }
    );
</script>

<body>
    <?php
    $SerialGateway = $_SESSION['serialGateway'];
    $Password = $_SESSION['password'];
    $LocationListArray = $_SESSION['location'];
    $SmartSwitchStatus = $_SESSION['SmartSwitchStatus'];
    $CurtainStatus = $_SESSION['CurtainStatus'];
    $AlarmStatus = $_SESSION['AlarmStatus'];
    $ShutterStatus = $_SESSION['ShutterStatus'];
    $IRBlasterStatus = $_SESSION['IRBlasterStatus'];
    $LockStatus = $_SESSION['LockStatus'];
    $TotalDevice = $_SESSION['TotalDevice'];
    $SceneCount = $_SESSION['SceneCount'];
    $AdminPassword = $_SESSION['AdminPassword'];

    echo "<input style='display:none;' id='SerialGateway' value='$SerialGateway'>";
    echo "<input style='display:none;' id='Password' value='$Password'>";
    echo "<input style='display:none;' id='AdminPassword' value='$AdminPassword'>";



    if (isset($_POST['Logout'])) {
        header("Location: ChooseGateway.php");
        header("Refresh:0");
        ob_flush();
        session_destroy();
        exit();
    }

    if (isset($_POST['Home'])) {
        // header("Refresh:0");
        header("Location: Dashboard.php");
        ob_flush();
        exit();
    }

    $opts = array('http' => array('header' => 'Cookie: ' . $_SERVER['HTTP_COOKIE'] . "\r\n"));
    $context = stream_context_create($opts);
    session_write_close(); // unlock the file
    $url = $baseURL . "/shutterSmartShutter.php";
    $SmartShutterJson = file_get_contents($url, false, $context);
    $msgJson = json_decode($SmartShutterJson);

    if (!empty($msgJson->Message)) {
        $Error = $msgJson->Message;
    ?>
        <script type="text/javascript">
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: '<?php echo $Error; ?>',
            }).then(function() {
                window.location.href = "ChooseGateway.php";
            });
        </script>
    <?php
        die();
    }

    function getList($msgJson)
    {
        //To decompress all the data from gateway
        $Compression = $msgJson->Compression;
        $decode = base64_decode($Compression);
        $Decompress = gzdecode($decode);
        $msgJson =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));

        if ($msgJson->Command == 'GetSmartShutterAPI' && $msgJson->Reply == true) {

            //To decompress all the data from gateway
            $Compress = $msgJson->Compress;
            $decode = base64_decode($Compress);
            $Decompress = gzdecode($decode);
            $msgJson =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));

            $SmartShutterListArray = $msgJson->SmartShutterList;
            if (!empty($SmartShutterListArray)) {

                echo "<div class='table-responsive'>";
                echo "<table id='myTable' class='table-bordered' style='width:100%;margin-top:50px;'>";
                echo "<thead>";
                echo "<tr>";
                echo "<th scope='col'>#</th>";
                echo "<th scope='col'>Name</th>";
                echo "<th scope='col'>Location Name</th>";
                echo "<th scope='col'>Action</th>";
                echo "<th scope='col'>Log</th>";
                echo "<th scope='col'>RSSI</th>";
                echo "</tr>";
                echo "</thead>";
                echo "<tbody id='myBody' >";

                $counter = 1;

                foreach ($SmartShutterListArray as $SmartShutterList) {

                    $ShutterList = $SmartShutterList->ShutterList;

                    $OriRSSI = $ShutterList[0]->RSSI;
                    $RSSI = 100 + $ShutterList[0]->RSSI;
                    $ShutterName = $ShutterList[0]->Name;
                    $ShutterNo = $ShutterList[0]->SwitchNo;

                    echo "<tr class='ShutterTable'>";
                    echo "<td>$counter</td>";
                    echo "<td>
                        <p style='margin:0px;'>$ShutterName</p>
                        <p style='margin-top:5px;' class='gatewaySNText'>$SmartShutterList->UserSerialNo</p>
                        </td>";
                    echo "<td>$SmartShutterList->LocationName</td>";

                    $ShutterDetail = $SmartShutterList->SerialNo . '&' . $SmartShutterList->UserSerialNo . '&' . $ShutterName . '&' . $SmartShutterList->LocationName . '&' . $OriRSSI;
                    echo "<td><button class='btn3' type='button' onclick='openShutterForm(&#39;$ShutterDetail&#39;)'><span class='fa fa-chevron-right'></button></td>";

                    $LogInfo = $SmartShutterList->SerialNo . '&&' . $ShutterNo . '&&' . $ShutterName . '&&' . $SmartShutterList->UserSerialNo;
                    echo "<td><button class='btn3' type='button' onclick='openLogForm(&#39;$LogInfo&#39;)'><span class='fa fa-book-open'></button></td>";

                    if ($RSSI >= 91 && $RSSI <= 99) {
                        echo "<td>
                            <input type='image' onclick='openRSSI(&#39;$OriRSSI&#39;)' src='Images/RSSI/RSSI10.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    } else if ($RSSI >= 81 && $RSSI <= 90) {
                        echo "<td>
                            <input type='image' onclick='openRSSI(&#39;$OriRSSI&#39;)' src='Images/RSSI/RSSI9.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    } else if ($RSSI >= 71 && $RSSI <= 80) {
                        echo "<td>
                            <input type='image' onclick='openRSSI(&#39;$OriRSSI&#39;)' src='Images/RSSI/RSSI8.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    } else if ($RSSI >= 61 && $RSSI <= 70) {
                        echo "<td>
                            <input type='image' onclick='openRSSI(&#39;$OriRSSI&#39;)' src='Images/RSSI/RSSI7.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    } else if ($RSSI >= 51 && $RSSI <= 60) {
                        echo "<td>
                            <input type='image' onclick='openRSSI(&#39;$OriRSSI&#39;)' src='Images/RSSI/RSSI6.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    } else if ($RSSI >= 41 && $RSSI <= 50) {
                        echo "<td>
                            <input type='image' onclick='openRSSI(&#39;$OriRSSI&#39;)'src='Images/RSSI/RSSI5.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    } else if ($RSSI >= 31 && $RSSI <= 40) {
                        echo "<td>
                            <input type='image' onclick='openRSSI(&#39;$OriRSSI&#39;)' src='Images/RSSI/RSSI4.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    } else if ($RSSI >= 21 && $RSSI <= 30) {
                        echo "<td>
                            <input type='image' onclick='openRSSI(&#39;$OriRSSI&#39;)' src='Images/RSSI/RSSI3.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    } else if ($RSSI >= 11 && $RSSI <= 20) {
                        echo "<td>
                            <input type='image' onclick='openRSSI(&#39;$OriRSSI&#39;)'src='Images/RSSI/RSSI2.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    } else if ($RSSI >= 00 && $RSSI <= 10) {
                        echo "<td>
                            <input type='image' onclick='openRSSI(&#39;$OriRSSI&#39;)' src='Images/RSSI/RSSI1.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    } else {
                        echo "<td>
                            <input type='image' onclick='openRSSI(&#39;$OriRSSI&#39;)' src='Images/RSSI/Unknown.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    }
                    echo "</tr>";
                    $counter++;
                }
                echo "</tbody>";
                echo "</table>";
                echo "</div>";


                foreach ($SmartShutterListArray as $SmartShutterList) {

                    $ShutterList = $SmartShutterList->ShutterList;

                    //Get timer delay current status
                    $TimerDelay_Enabled_1 = $ShutterList[0]->TimerDelay_Enabled ? 'true' : 'false';
                    $TimerDelay_Enabled_3 = $ShutterList[2]->TimerDelay_Enabled ? 'true' : 'false';

                    //Get timer delay time
                    $TimerDelayActionTimeOpen = $ShutterList[0]->TimerDelayActionTime;
                    $fromGatewayOpen = new DateTime($TimerDelayActionTimeOpen, new DateTimeZone("Asia/Kuala_Lumpur"));
                    $TimerDelayActionTimeClose = $ShutterList[2]->TimerDelayActionTime;
                    $fromGatewayClose = new DateTime($TimerDelayActionTimeClose, new DateTimeZone("Asia/Kuala_Lumpur"));
                    $current = new DateTime;
                    $current->setTimeZone(new DateTimeZone("Asia/Kuala_Lumpur"));
                    $intervalOpen = $fromGatewayOpen->diff($current);
                    $timeOpen = json_encode($intervalOpen);
                    $intervalClose = $fromGatewayClose->diff($current);
                    $timeClose = json_encode($intervalClose);

                    //Delay timer info
                    $DelayInfo_1 = $timeOpen . ' ' . 1 . ' ' . $TimerDelay_Enabled_1;
                    $DelayInfo_3 = $timeClose . ' ' . 3 . ' ' . $TimerDelay_Enabled_3;

                    //Get 24 hour timer current status
                    $Timer24H_Enabled_1 = $ShutterList[0]->Timer24H_Enabled ? 'true' : 'false';
                    $Timer24H_Enabled_3 = $ShutterList[2]->Timer24H_Enabled ? 'true' : 'false';

                    //Get ondays 
                    $OnDays_1 = $ShutterList[0]->Timer24H;
                    $OnDays_3 = $ShutterList[2]->Timer24H;
                    $jsonOnDays_1 = json_encode($OnDays_1);
                    $jsonOnDays_3 = json_encode($OnDays_3);

                    //First info to get ondays, second info is switch no, and third info to get ontime
                    $Timer24Info_1 = $jsonOnDays_3 . ' ' . 1 . ' ' . $jsonOnDays_1;
                    $Timer24Info_3 = $jsonOnDays_1 . ' ' . 3 . ' ' . $jsonOnDays_3;

                    //------------------------------------------------------------------------------Action Pop UP------------------------------------------------------------------------------------------
                    echo "<div class='form-popup' id='myPopupAction$SmartShutterList->SerialNo'>
                                                    <div class='form-container' style='width:550px;padding:0px 50px;'>
                                                    <div class='modal-header' style='width:100%;height:60px;'>
                                                    <button class='close' onclick='closeForm(&#39;$SmartShutterList->SerialNo&#39;)'></button>
                                                    <h3 class='h2form'>Smart Shutter Remote</h3>
                                                    </div>
                                                    <div class='modal-body' style='width:100%;'>
                                                    <div class='grid-container'>";
                    if ($Timer24H_Enabled_1 == 'true') {
                        echo "<div><input style='width:50px;' class='open24' type='image' src='Images/Shutter/24TimerOn.png' alt='stop' onclick='choose24Timer(&#39;$Timer24Info_1&#39;)'></div>";
                    } else {
                        echo "<div><input style='width:50px;' class='open24' type='image' src='Images/Shutter/24TimerOff.png' alt='stop' onclick='choose24Timer(&#39;$Timer24Info_1&#39;)'></div>";
                    }

                    echo "<div><input class='openShutter' type='image' src='Images/Shutter/open.png' alt='open' onclick='actionShutter(1)'></div>";

                    if ($TimerDelay_Enabled_1 == 'true') {
                        echo "<div><input  style='width:50px;' class='open' type='image' src='Images/Shutter/delayTimerOn.png' alt='open' onclick='openDelayForm(&#39;$DelayInfo_1&#39;)'></div>";
                    } else {
                        echo "<div><input  style='width:50px;' class='open' type='image' src='Images/Shutter/delayTimerOff.png' alt='open' onclick='openDelayForm(&#39;$DelayInfo_1&#39;)'></div>";
                    }
                    echo "</div>
                                    
                                                    <div style='width:100%;'><input class='stopShutter' class='' type='image' src='Images/Shutter/stop.png' alt='stop' onclick='actionShutter(2)'></div>
                                            
                                    
                                                    <div class='grid-container'>";
                    if ($Timer24H_Enabled_3 == 'true') {
                        echo "<div><input style='width:50px;' class='closing24' type='image' src='Images/Shutter/24TimerOn.png' alt='stop' onclick='choose24Timer(&#39;$Timer24Info_3&#39;)'></div>";
                    } else {
                        echo "<div><input style='width:50px;' class='closing24' type='image' src='Images/Shutter/24TimerOff.png' alt='stop' onclick='choose24Timer(&#39;$Timer24Info_3&#39;)'></div>";
                    }

                    echo "<div><input class='closeShutter' type='image' src='Images/Shutter/close.png' alt='close' onclick='actionShutter(3)'></div>";

                    if ($TimerDelay_Enabled_3 == 'true') {
                        echo "<div><input style='width:50px;' class='closing' type='image' src='Images/Shutter/delayTimerOn.png' alt='close' onclick='openDelayForm(&#39;$DelayInfo_3&#39;)'></div>";
                    } else {
                        echo "<div><input style='width:50px;' class='closing' type='image' src='Images/Shutter/delayTimerOff.png' alt='close' onclick='openDelayForm(&#39;$DelayInfo_3&#39;)'></div>";
                    }
                    echo "</div>
                                                    </div>
                                                    </div>
                                                    </div>";
                }
            } else {
                echo "<h2 class='error'><i class='fas fa-exclamation-circle fa-5x' style='color:red;'></i> </br></br>
                Smart Shutter Not Available For This Selected Gateway </br>
                Please Back To Home Page</h2>";
            }
        } else {
            echo "<h2 class='error'><i class='fas fa-exclamation-circle fa-5x' style='color:red;'></i> </br></br>
            Smart Shutter Not Available For This Selected Gateway </br>
            Please Back To Home Page</h2>";
        }
    }

    ?>
    <input style='display:none;' id='IntervalId' value='none'>
    <input style='display:none;' class='Check'>
    <input style='display:none;' class='RSSI'>
    <input style='display:none;' class='actionShutter'>
    <input style='display:none;' class='switchno'>
    <input style='display:none;' class='timerno'>
    <input style='display:none;' class='onDays'>
    <input style='display:none;' class='ShutterInfo'>
    <input style='display:none;' class='LocationName'>

    <!-- Delay Timer popup -->
    <div class="form-popup" id="myDelayForm">
        <div class="form-container" style='width:550px;padding:0px;'>
            <div class="modal-header" style='width:100%;height:60px;'>
                <a class='back' onclick='back()'><span class='fa fa-arrow-left fa-2x'></a>
                <h3 class='h2form'>Shutter Delay Timer</h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <h4 class='title' style='font-weight:700;'>Status</h4>
                <h6 id='delayTimer' class='title' style='margin-bottom: 30px;'></h6>
                <!-- <form method='post' class='form'> -->
                <div class='omrs-input-group' style='margin-left:auto;margin-right:auto;'>
                    <div class='omrs-input-underlined omrs-input-danger'>
                        <label id='labelaction' for='actions'></label>
                        <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'>
                            <path fill='none' d='M0 0h24v24H0V0z' />
                            <circle cx='15.5' cy='9.5' r='1.5' />
                            <circle cx='8.5' cy='9.5' r='1.5' />
                            <path d='M10.01,21.01c0,1.1 0.89,1.99 1.99,1.99s1.99,-0.89 1.99,-1.99h-3.98zM18.88,16.82L18.88,11c0,-3.25 -2.25,-5.97 -5.29,-6.69v-0.72C13.59,2.71 12.88,2 12,2s-1.59,0.71 -1.59,1.59v0.72C7.37,5.03 5.12,7.75 5.12,11v5.82L3,18.94L3,20h18v-1.06l-2.12,-2.12zM16,13.01h-3v3h-2v-3L8,13.01L8,11h3L11,8h2v3h3v2.01z' /></svg>
                    </div>
                </div>
                <div class='omrs-input-group' style='margin-left:auto;margin-right:auto;'>
                    <div class='omrs-input-underlined ' style='width:100%;'>
                        <label for='quantityHours'>Hours :</label>
                        <input type='number' id='hours' name='hours' min='1' max='23' required></br></br>
                        <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'>
                            <path fill='none' d='M0 0h24v24H0V0z' />
                            <circle cx='15.5' cy='9.5' r='1.5' />
                            <circle cx='8.5' cy='9.5' r='1.5' />
                            <path d='M22,5.72l-4.6,-3.86 -1.29,1.53 4.6,3.86L22,5.72zM7.88,3.39L6.6,1.86 2,5.71l1.29,1.53 4.59,-3.85zM12.5,8L11,8v6l4.75,2.85 0.75,-1.23 -4,-2.37L12.5,8zM12,4c-4.97,0 -9,4.03 -9,9s4.02,9 9,9c4.97,0 9,-4.03 9,-9s-4.03,-9 -9,-9zM12,20c-3.87,0 -7,-3.13 -7,-7s3.13,-7 7,-7 7,3.13 7,7 -3.13,7 -7,7z' /></svg>
                    </div>
                </div>
                <div class='omrs-input-group' style='margin-left:auto;margin-right:auto;'>
                    <div class='omrs-input-underlined' style='width:100%;'>
                        <label for='quantityMinutes'>Minutes :</label>
                        <input type='number' id='minutes' name='minutes' min='1' max='59' required></br></br>
                        <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'>
                            <path fill='none' d='M0 0h24v24H0V0z' />
                            <circle cx='15.5' cy='9.5' r='1.5' />
                            <circle cx='8.5' cy='9.5' r='1.5' />
                            <path d='M22,5.72l-4.6,-3.86 -1.29,1.53 4.6,3.86L22,5.72zM7.88,3.39L6.6,1.86 2,5.71l1.29,1.53 4.59,-3.85zM12.5,8L11,8v6l4.75,2.85 0.75,-1.23 -4,-2.37L12.5,8zM12,4c-4.97,0 -9,4.03 -9,9s4.02,9 9,9c4.97,0 9,-4.03 9,-9s-4.03,-9 -9,-9zM12,20c-3.87,0 -7,-3.13 -7,-7s3.13,-7 7,-7 7,3.13 7,7 -3.13,7 -7,7z' /></svg>
                    </div>
                </div>
                <!-- </form> -->
            </div>
            <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
                <button type='button' class='all' onclick='myDelayTimerFunction()'>Enable Timer</button>
                <button type='button' class='all' onclick='disableDelayTimer()'>Disable Timer</button>
            </div>
        </div>
    </div>

    <!-- 24 Hour Timer popup -->
    <div class="form-popup" id="my24HForm">
        <div class="form-container" style='width:550px;padding:0px;'>
            <div class="modal-header" style='width:100%;height:60px;'>
                <a class='back' onclick='back2back()'><span class='fa fa-arrow-left fa-2x'></a>
                <h3 id='24Timer' class='h2form'></h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <h4 class='title' style='font-weight:700;'>Status</h4>
                <h6 id='status' class='title' style='margin-bottom:30px;'></h6>
                <!-- <form method='post' class='form'> -->
                <div class='omrs-input-group' style='margin-left:auto;margin-right:auto;'>
                    <label class='omrs-input-underlined omrs-input-danger' id='enableTimer'></label>
                    <label class='switch'>
                        <?php echo "<input type='checkbox' id='switch' onclick='disable()'>"; ?>
                        <span class='slider round'></span>
                    </label>
                </div>
                <div class='omrs-input-group' style='margin-left:auto;margin-right:auto;'>
                    <label class='omrs-input-underlined ' style='width:100%;'>
                        <label for='quantityHours' id='setTime'></label>
                        <div>
                            <div class='row'>
                                <div class='col-xs-12 col-md-8'>
                                    <div class='form-group'>
                                        <div class='input-group date' id='datetimepicker'>
                                            <input value='00:00' type='text' class='form-control' id='time' />
                                            <span class='input-group-addon'>
                                                <span class='glyphicon glyphicon-time'></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </label>
                </div>
                <div class='omrs-input-group' style='margin-left:auto;margin-right:auto;'>
                    <div class='omrs-input-underlined' style='width:100%;'>
                        <label for='quantityMinutes' id='setDay'></label>
                        <div id='Checkbox'>
                            <input type='checkbox' id='day1' name='dayCheck' value='1'>
                            <label> Every Sunday</label><br>
                            <input type='checkbox' id='day2' name='dayCheck' value='2'>
                            <label> Every Monday</label><br>
                            <input type='checkbox' id='day3' name='dayCheck' value='4'>
                            <label> Every Tuesday</label><br>
                            <input type='checkbox' id='day4' name='dayCheck' value='8'>
                            <label> Every Wednesday</label><br>
                            <input type='checkbox' id='day5' name='dayCheck' value='16'>
                            <label> Every Thursday</label><br>
                            <input type='checkbox' id='day6' name='dayCheck' value='32'>
                            <label> Every Friday</label><br>
                            <input type='checkbox' id='day7' name='dayCheck' value='64'>
                            <label> Every Saturday</label><br>
                        </div>
                    </div>
                </div>
                <!-- </form> -->
            </div>
            <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
                <button type='button' class='all' onclick='my24TimerFunction()'>Enable Timer</button>
                <button type='button' class='all' onclick='disable24Timer()'>Disable Timer</button>
            </div>
        </div>
    </div>

    <!-- Choose Timer popup -->
    <div class="form-popup" id="choose24Timer">
        <div class="form-container" style='width:550px;padding:0px;'>
            <div class="modal-header" style='width:100%;height:60px;'>
                <a class='back' onclick='back()'><span class='fa fa-arrow-left fa-2x'></a>
                <h3 class='h2form'>Set 24 Hour Timer</h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <form method='post' class='form'>
                    <div class="btnTimer">
                        <button id='btnt1' class='all' type="button" onclick='open24HForm(1)'>24 Hour Timer 1</button>
                        <p id='t1' style='text-align: center;'></p>
                        <p id='action1' style='text-align: center;'></p>
                        <button id='btnt2' class='all' type="button" onclick='open24HForm(2)'>24 Hour Timer 2</button>
                        <p id='t2' style='text-align: center;'></p>
                        <p id='action2' style='text-align: center;'></p>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!--Log Form -->
    <div class='form-popup' id='ShutterLogForm'>
        <div class='form-container' style='width:650px;padding:0px;'>
            <div class="modal-header" style='width:100%;height:60px;'>
                <button class='close' onclick='backLog("ShutterLogForm")'></button>
                <h3 class="h2form">Logs</h3>
            </div>
            <div class="modal-body" style='width:100%;max-height: calc(80vh - 210px);overflow-y: auto;'>
                <h4 class="h2form" id='logName'></h4>
                <div id="emptyLog">

                </div>
                <div id="tableLog">
                    <table class="table table-bordered table-hover" style="text-align:center;">
                        <thead>
                            <th>#</th>
                            <th>Date Time</th>
                            <th>Status</th>
                        </thead>
                        <tbody id="ShutterLogTable">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Form for sign Door Lock -->
    <div class="form-popup" id="mySiginDoorLockForm">
        <div class="form-container" style="width:400px;padding:0px;">
            <div class="modal-header" style='width:100%;height:70px;'>
                <!-- <a class='back' onclick='closeFormSigin()'><span class='fa fa-arrow-left fa-2x'></a> -->
                <h3 class='h2form'>Manage Door Lock </br> Password</h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <fieldset style='width:100%;'>
                    <label for="name">Enter Admin Password:</label>
                    <input type="password" id="password" name="DoorlockPassword" style='width:100%;'>
                </fieldset>
            </div>
            <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
                <button type="submit" class="all" onclick='openGetLock()' name="signin">Ok</button>
                <button type="button" class="all" onclick="cancelFormSigin()">Cancel</button>
            </div>
        </div>
    </div>

    <!-- SideBar + Navbar -->
    <div class="wrapper">
        <!-- Sidebar Holder -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>Senzo</h3>
                <strong>SZ</strong>
            </div>
            <ul class="list-unstyled components">
                <!-- Home Section -->
                <li>
                    <form action='SmartShutter.php' method='POST' id='NavHome' style='margin-block-end: 0'>
                        <input style='display:none;' name='Home'>
                        <a style='cursor:pointer;' class='Home'>
                            <i class="glyphicon glyphicon-home"></i>
                            Home
                        </a>
                    </form>
                </li>
                <li>
                    <!-- Device Section -->
                    <a href="#pageDevice" data-toggle="collapse" aria-expanded="false">
                        <i class="glyphicon glyphicon-tasks"></i>
                        <?php echo "Device"; ?>
                    </a>
                    <ul class="collapse list-unstyled in" id="pageDevice">
                        <?php
                        if ($SmartSwitchStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectSwitch()">Smart Switch</a></li>';
                        }
                        if ($CurtainStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectCurtain()">Smart Curtain</a></li>';
                        }
                        if ($ShutterStatus) {
                            echo '<li><a id="cube" style="cursor:pointer;" onclick="selectShutter()">Smart Shutter</a></li>';
                        }
                        if ($LockStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectDoorLock()">Smart Door Lock</a></li>';
                        }
                        if ($IRBlasterStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectIRBlaster()">IR Blaster</a></li>';
                        }
                        ?>
                    </ul>
                </li>
                <li>
                    <!-- Scene Section -->
                    <a style="cursor:pointer;" onclick='openScenesForm()'>
                        <i class="glyphicon glyphicon-film"></i>
                        <?php echo "Scene ($SceneCount)"; ?>
                    </a>
                </li>
                <li>
                    <!-- Security Section -->
                    <a href="#pageSecurity" data-toggle="collapse" aria-expanded="false">
                        <i class="glyphicon glyphicon-lock"></i>
                        <?php echo "Security"; ?>
                    </a>
                    <ul class="collapse list-unstyled" id="pageSecurity">
                        <?php
                        // If smart alarm connected to gateway
                        if ($AlarmStatus) {
                            echo "<li><a style='cursor:pointer;' onclick='selectAlarm(&#39;True&#39;)'>
                            Smart Alarm <img src='Images/Action/security_armed_icon.png' alt='scenes' style='width:20px;height:20px;'>";
                        } else {
                            echo "<li><a style='cursor:pointer;' onclick='selectAlarm(&#39;False&#39;)'>
                            Smart Alarm";
                        }
                        ?>
                        </a>
                </li>
                <li><a style="cursor:pointer;" onclick="selectCamera()">Camera</a></li>
            </ul>
            </li>
            <li>
                <!-- Analytic Section -->
                <a style='cursor:pointer;' onclick='openAnalyticForm()'>
                    <i class='glyphicon glyphicon-stats'></i>
                    Analytic
                </a>
            </li>
            <li>
                <!-- Setting Section -->
                <a style='cursor:pointer;' onclick='openSettingForm()'>
                    <i class='glyphicon glyphicon-cog'></i>
                    Setting
                </a>
            </li>
            </ul>

            <!-- Logout button -->
            <ul class="list-unstyled CTAs">
                <li>
                    <form method="POST" action="SmartShutter.php" id='NavLogout' style='margin-block-end: 0'>
                        <input style='display:none;' name='Logout'>
                        <a style="cursor:pointer;" class="Logout">
                            <i class="glyphicon glyphicon-log-out"></i>
                            Logout
                        </a>
                    </form>
                </li>
            </ul>
        </nav>

        <!-- Page Content Holder -->
        <div id="content" style="width: -webkit-fill-available;">

            <nav class="navbar navbar-default">
                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                            <i class="glyphicon glyphicon-align-left"></i>
                            <span></span>
                        </button>

                        <!-- For responsive part -->
                        <!-- Show and hide content that been targeted-->
                        <button class="btn btn-dark navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <i class="glyphicon glyphicon-align-justify"></i>
                        </button>
                    </div>

                    <!-- Content that have been targeted -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a data-toggle="dropdown" class="dropdown-toggle" style='background: #fff;cursor:pointer;'>Location</a>
                                <ul class="dropdown-menu">
                                    <?php
                                    $LocationListArray = $_SESSION['location'];
                                    if (is_array($LocationListArray) || is_object($LocationListArray)) {
                                        foreach ($LocationListArray as $location) {
                                            echo "<li>";
                                            echo "<input style='display:none;' value='$location->LocationName' id='Location$location->LocationName'>";
                                            echo "<a style='cursor:pointer;' class='LocationListItem' >$location->LocationName</a>";
                                            echo "</li>";
                                        }
                                    }
                                    ?>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div class='filter' id='collapseFilter'>
                <span class='filer-title'> Filtered By : </span>
            </div>

            <h2>List of Available Shutter</h2>
            <?php
            $GatewayLevel = $_SESSION['level'];
            echo "<h4 class='Level' >Gateway $GatewayLevel</h4>";
            getList($msgJson);
            ?>
            <div class="line"></div>

        </div>
    </div>

    <!-- Auto Refresh dropdown -->
    <div class='autorefresh'>
        <div class="dropdown">
            <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">
                <img src='Images/Switch/settings_gear.png' alt='switchon' style='width:30px;height:30px;animation: spin 3s linear infinite;'>
            </button>
            <ul class="dropdown-menu">
                <li>
                    <input type='checkbox' id='showSn' value='showSn'>
                    <label> Show SN</label><br>
                </li>
                <li>
                    <input type='checkbox' id='enable' name='fooby[2][]'>
                    <label> Enable</label><br>
                    <div id='timeRefresh' style='display:none;margin-left:10px;'>
                        <input type='checkbox' value='1-minute' class="slectOne" id='1-minute'>
                        <label> 1 minute</label><br>
                        <input type='checkbox' value='5-minutes' class="slectOne" id='5-minutes'>
                        <label> 5 minutes</label><br>
                        <input type='checkbox' value='10-minutes' class="slectOne" id='10-minutes'>
                        <label> 10 minutes</label><br>
                        <input type='checkbox' value='20-minutes' class="slectOne" id='20-minutes'>
                        <label> 20 minutes</label><br>
                        <input type='checkbox' value='25-minutes' class="slectOne" id='25-minutes'>
                        <label> 25 minutes</label><br>
                        <input type='checkbox' value='1-hour' class="slectOne" id='1-hour'>
                        <label> 1 hour</label><br>
                    </div>
                </li>
                <li>
                    <input type='checkbox' id='disable' value='disable'>
                    <label> Disable</label><br>
                </li>
            </ul>
        </div>
    </div>

    <!-- Loading animation -->
    <div class="form-popup" id='loader'>
        <div class="loading-container">
            <div class="loading"></div>
            <div class="loading-text">loading</div>
        </div>
    </div>
</body>

</html>