<?php
session_start();
ob_start();
ini_set('max_execution_time', 30);
include '././configuration/serverConfig.php';
include './header/headerAll.php';
?>
<!DOCTYPE html>
<html>

<head>
    <title>IR Blaster</title>
    <link href="css/SmartIRBlaster.css" rel="stylesheet">
    <link rel="stylesheet" href="css/Sidebar.css">
    <script type="text/javascript" src="SidebarHandler.js"></script>
</head>
<script type="text/javascript">
    function openIrBlasterForm(IrBlasterDetail) {
        var field = IrBlasterDetail.split("&");
        var irBlasterSerial = field[0];
        var irBlasterUserSerialNo = field[1];
        var irBlasterId = field[2];
        var irBlasterName = field[3];
        var irBlasterLocationName = field[4];
        var irBlasterRoomTemperature = field[5];
        var irBlasterRSSI = field[6];

        document.getElementById("textSerial" + irBlasterId).innerHTML = irBlasterName;
        document.getElementsByClassName("Serial").value = irBlasterSerial;
        document.getElementsByClassName("IRBlasterId").value = irBlasterId;
        document.getElementsByClassName("RSSI").value = irBlasterRSSI;
        document.getElementById("myPopupAction" + irBlasterId).style.display = "block";

    }

    function openAddDevice(IRBlasterId, IRBlasterInfo) {
        var serial = document.getElementsByClassName("Serial").value;
        url = baseURL + "/irBlasterCheckStatus.php/?IRBlasterId=" + IRBlasterId;
        var loader = document.getElementById("checkingStatus");
        loader.style.display = "block";
        $.getJSON(url, function(data) {
            loader.style.display = "none";
            if (data.Command == 'CheckIRBlasterStatus' && data.Status == true) {
                var IRBlasterJson = JSON.parse(IRBlasterInfo);
                if (IRBlasterJson.IRBlasterId == IRBlasterId) {
                    var TotalDevice = IRBlasterJson.AirConditionerList.length + IRBlasterJson.RemoteList.length;
                    if (TotalDevice < 8) {
                        document.getElementsByClassName("IRBlasterInfo").value = IRBlasterInfo;
                        document.getElementById("myPopupAction" + IRBlasterId).style.display = "none";
                        document.getElementById("addDeviceForm").style.display = "block";
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Failed',
                            text: 'Device limit reach'
                        })
                    }
                }

            } else if (data.Status == false) {
                Swal.fire({
                    icon: 'error',
                    title: 'Failed',
                    text: 'The IRBlaster is not responding. Please check device and try again.'
                })
            }
        });
    }

    $(document).ready(function() {
        $('.IRBlasterDeviceList').click(function() {

            //Get value user click from table
            var value = $(this).find("a").attr("value");
            var field = value.split("&");

            var Device = field[0];
            var DeviceInfo = field[1];
            var Location = field[2];

            if (Device == 'AirCond') {
                openAirConditionerForm(DeviceInfo, Location);
            } else if (Device == 'Tv') {
                openTvForm(DeviceInfo);
            } else if (Device == 'Astro') {
                openAstroForm(DeviceInfo);
            }
        });
    });

    function openRenameDevice(DeviceInfo, IRBlasterInfo) {
        var fields = DeviceInfo.split("&&");
        var IRBlasterID = fields[0];
        var DeviceID = fields[1];
        var TypeId = fields[2];
        var DeviceName = fields[3];
        var serial = document.getElementsByClassName("Serial").value;
        var id = document.getElementsByClassName("IRBlasterId").value

        document.getElementsByClassName("IRBlasterId").value = IRBlasterID;
        document.getElementsByClassName("DeviceId").value = DeviceID;
        document.getElementsByClassName("TypeId").value = TypeId;
        document.getElementsByClassName("IRBlasterInfo").value = IRBlasterInfo;
        document.getElementById("deviceRenameName").value = DeviceName;
        document.getElementById("myPopupAction" + id).style.display = "none";
        document.getElementById("renameDeviceForm").style.display = 'block';
    }

    function renameDevice() {
        var IRBlasterID = document.getElementsByClassName("IRBlasterId").value;
        var DeviceID = document.getElementsByClassName("DeviceId").value;
        var TypeId = document.getElementsByClassName("TypeId").value;
        var DeviceName = document.getElementById("deviceRenameName").value;
        var IRBlasterInfo = document.getElementsByClassName("IRBlasterInfo").value;

        var IRBlasterJson = JSON.parse(IRBlasterInfo);

        var hrefRenameDevice = baseURL + "/irBlasterRenameDevice.php/?IRBlasterId=" + IRBlasterID + "&DeviceID=" + DeviceID + "&TypeId=" + TypeId + "&Name=" + DeviceName;
        var loader = document.getElementById("loader");
        // console.log(hrefRenameDevice);

        var boolSame = false;
        var sameName = '';
        for (i = 0; i < IRBlasterJson.AirConditionerList.length; i++) {
            if (DeviceName == IRBlasterJson.AirConditionerList[i].DeviceName) {
                boolSame = true;
                sameName = IRBlasterJson.AirConditionerList[i].DeviceName;
                break;
            }
        }

        for (i = 0; i < IRBlasterJson.RemoteList.length; i++) {
            if (DeviceName == IRBlasterJson.RemoteList[i].DeviceName) {
                boolSame = true;
                sameName = IRBlasterJson.RemoteList[i].DeviceName;
                break;
            }
        }

        var boolEmpty = false;
        if (DeviceName.length == 0) {
            boolEmpty = true;
        }

        if (!boolSame && !boolEmpty) {
            loader.style.display = "block";
            $.getJSON(hrefRenameDevice, function(data) {
                //data is the JSON string
                loader.style.display = "none";
                if (data.Command == 'UpdateIRBlasterDeviceName' && data.Status == true) {
                    Swal.fire(
                        'Good job!',
                        'IR Blaster name has been updated',
                        'success'
                    )
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
                } else if (data.Status == false) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: data.Message
                    })
                }
            });
        } else if (boolSame) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: sameName + ' already exist IRBlaster'
            })
        } else if (boolEmpty) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Name cannot be empty'
            })
        }
    }

    function deleteDevice(DeviceInfo) {
        var fields = DeviceInfo.split(" ");
        var IRBlasterID = fields[0];
        var DeviceID = fields[1];
        var TypeId = fields[2];
        var DeviceName = fields[3];

        var hrefDeleteDevice = baseURL + "/irBlasterDeleteDevice.php/?IRBlasterId=" + IRBlasterID + "&DeviceID=" + DeviceID + "&TypeId=" + TypeId;
        var loader = document.getElementById("loader");
        // console.log(hrefDeleteDevice);

        Swal.fire({
            title: DeviceName,
            text: "Are you sure you want to delete the device?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                loader.style.display = "block";
                $.getJSON(hrefDeleteDevice, function(data) {
                    //data is the JSON string
                    loader.style.display = "none";
                    if (data.Command == 'DeleteDeviceInIRBlaster' && data.Status == false) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Failed',
                            text: data.Message
                        })

                    } else {
                        Swal.fire(
                            'Good job!',
                            DeviceName + ' has been deleted',
                            'success'
                        )
                        setTimeout(function() {
                            location.reload();
                        }, 2000);
                    }
                });
            }
        })
    }

    function openAirConditioner() {
        document.getElementById("addDeviceForm").style.display = "none";
        var myPopupAirCondTypeAndBrand = document.getElementById("myPopupAirCondTypeAndBrand");
        if (myPopupAirCondTypeAndBrand) {
            myPopupAirCondTypeAndBrand.style.display = "block";


        } else {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Error while retrieving type and brand for AC'
            })
        }

    }

    function openModelAirConditioner(BrandId) {
        document.getElementsByClassName("BrandId").value = BrandId;
        document.getElementById("myPopupAirCondTypeAndBrand").style.display = "none";
        var myPopupAirCondModel = document.getElementById("myPopupAirCondModel" + BrandId);
        if (myPopupAirCondModel) {
            myPopupAirCondModel.style.display = "block";
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Error while retrieving model for AC'
            })
        }
    }

    $(document).ready(function() {
        $('.AirConditionerModelList').click(function() {

            //Get value user click from table
            var userClick = $(this).text();

            var airConditionerModel = document.getElementsByName("Model");
            var airConditionerBrandId = document.getElementsByClassName("BrandId").value;
            var IRBlasterID = document.getElementsByClassName("IRBlasterId").value;
            var IRBlasterInfo = document.getElementsByClassName("IRBlasterInfo").value;
            var TypeId = 1;

            var countModel = 0;
            var index = 0;

            airConditionerModel.forEach(function(model) {
                if (model.value == userClick) {
                    index = countModel;
                }
                countModel++;
            });
            var fields = airConditionerModel[index].value.split(" ");
            var IRBlasterJson = JSON.parse(IRBlasterInfo);
            var boolDevice = false;
            for (i = 0; i < IRBlasterJson.AirConditionerList.length; i++) {
                if (IRBlasterJson.AirConditionerList[i].BrandId == airConditionerBrandId && IRBlasterJson.AirConditionerList[i].ModelId == fields[1]) {
                    boolDevice = true;
                    break;
                }
            }

            if (boolDevice) {
                Swal.fire({
                    icon: 'error',
                    title: 'Failed',
                    text: 'This model has already been added to this IRBlaster'
                })
            } else {
                var hrefGetAirConditionerLimit = baseURL + "/irBlasterAirConditionerLimit.php/?IRBlasterId=" + IRBlasterID + "&TypeId=" + TypeId + "&BrandId=" + airConditionerBrandId + "&Model=" + fields[1] + "&Status=New";
                var loader = document.getElementById("fetchingAircon");
                loader.style.display = "block";
                var airConditionerModelId = fields[1];
                $.getJSON(hrefGetAirConditionerLimit, function(data) {
                    console.log(data);
                    loader.style.display = "none";
                    if (data.Command == 'GetAirConditionerLimit' && data.ACModelID == airConditionerModelId && data.ACBrandID == airConditionerBrandId) {
                        document.getElementsByClassName("ModelId").value = airConditionerModelId;
                        document.getElementById("myPopupAirCondModel" + airConditionerBrandId).style.display = "none";
                        document.getElementById("AirConditionerForm" + airConditionerModelId + airConditionerBrandId).style.display = "block"
                        document.getElementById("number" + airConditionerModelId + airConditionerBrandId).value = 21;
                        document.getElementById("number" + airConditionerModelId + airConditionerBrandId).innerHTML = "21";

                        if (data.ModeAutoOption == true) {
                            document.getElementById("auto" + airConditionerModelId + airConditionerBrandId).style.display = "block";
                        }
                        if (data.ModeCoolOption == true) {
                            document.getElementById("cool" + airConditionerModelId + airConditionerBrandId).style.display = "block";
                        }
                        if (data.ModeHeatOption == true) {
                            document.getElementById("heat" + airConditionerModelId + airConditionerBrandId).style.display = "block";
                        }
                        if (data.ModeDryOption == true) {
                            document.getElementById("dry" + airConditionerModelId + airConditionerBrandId).style.display = "block";
                        }
                        if (data.ModeFanOption == true) {
                            document.getElementById("fan" + airConditionerModelId + airConditionerBrandId).style.display = "block";
                        }
                        if (data.FanAutoOption == true) {
                            document.getElementById("fanspeed" + airConditionerModelId + airConditionerBrandId).style.display = "block";
                        }
                        if (data.UpDownSwingAutoOption == true) {
                            if (data.UpDownSwingType == 1) {
                                document.getElementById("swingUD" + airConditionerModelId + airConditionerBrandId).src = "Images/IRBlaster/AirConditioner/icon_swing_horizontal_v3_grey.png";
                            } else {
                                document.getElementById("swingUD" + airConditionerModelId + airConditionerBrandId).src = "Images/IRBlaster/AirConditioner/icon_swing_horizontal_v3_blue.png";
                            }
                            document.getElementById("swingvertical" + airConditionerModelId + airConditionerBrandId).style.display = "block";
                        }
                        if (data.LeftRightSwingAutoOption == true) {
                            if (data.LeftRightSwingType == 1) {
                                document.getElementById("swingLR" + airConditionerModelId + airConditionerBrandId).src = "Images/IRBlaster/AirConditioner/icon_swing_vertical_v3_grey.png";
                            } else {
                                document.getElementById("swingLR" + airConditionerModelId + airConditionerBrandId).src = "Images/IRBlaster/AirConditioner/icon_swing_vertical_v3_blue.png";
                            }
                            document.getElementById("swinghorizontal" + airConditionerModelId + airConditionerBrandId).style.display = "block";
                        }
                        if (data.PowerOption == "ON/OFF") {
                            document.getElementById("on" + airConditionerModelId + airConditionerBrandId).style.display = "block";
                            document.getElementById("off" + airConditionerModelId + airConditionerBrandId).style.display = "block";
                        }
                        if (data.PowerOption == "TOGGLE") {
                            document.getElementById("toggle" + airConditionerModelId + airConditionerBrandId).style.display = "block";
                        }

                        //Air Condtioner Function
                        var info = "false";
                        document.getElementById("modeAuto" + airConditionerModelId + airConditionerBrandId).onclick = function() {
                            var mode = "auto";
                            changeMode(mode, info)
                        };
                        document.getElementById("modeCool" + airConditionerModelId + airConditionerBrandId).onclick = function() {
                            var mode = "cool";
                            changeMode(mode, info)
                        };
                        document.getElementById("modeHeat" + airConditionerModelId + airConditionerBrandId).onclick = function() {
                            var mode = "heat";
                            changeMode(mode, info)
                        };
                        document.getElementById("modeDry" + airConditionerModelId + airConditionerBrandId).onclick = function() {
                            var mode = "dry";
                            changeMode(mode, info)
                        };
                        document.getElementById("modeFan" + airConditionerModelId + airConditionerBrandId).onclick = function() {
                            var mode = "fan";
                            changeMode(mode, info)
                        };
                        document.getElementById("increase" + airConditionerModelId + airConditionerBrandId).onclick = function() {
                            increaseValue(data.TempMax, info)
                        };
                        document.getElementById("decrease" + airConditionerModelId + airConditionerBrandId).onclick = function() {
                            decreaseValue(data.TempMin, info)
                        };
                        document.getElementById("fanlimit" + airConditionerModelId + airConditionerBrandId).onclick = function() {
                            changeFanSpeed(data.FanLimit, info)
                        };
                        document.getElementById("swingLR" + airConditionerModelId + airConditionerBrandId).onclick = function() {
                            swingHorizontal(data.LeftRightSwingType, info)
                        };
                        document.getElementById("swingUD" + airConditionerModelId + airConditionerBrandId).onclick = function() {
                            swingVertical(data.UpDownSwingType, info)
                        };
                        document.getElementById("powerOn" + airConditionerModelId + airConditionerBrandId).onclick = function() {
                            powerOption(data.PowerOption, 1, info)
                        };
                        document.getElementById("powerOff" + airConditionerModelId + airConditionerBrandId).onclick = function() {
                            powerOption(data.PowerOption, 0, info)
                        };
                        document.getElementById("powerToggle" + airConditionerModelId + airConditionerBrandId).onclick = function() {
                            powerOption(data.PowerOption, 1, info)
                        };

                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Failed',
                            text: data.Message
                        })
                    }
                });
            }
        });
    });

    //--------------------------------------------------------------Start Function for Air Conditioner-----------------------------------------------------------------------
    function changeMode(mode, info) {

        //For Test Air Conditioner
        if (info == "false") {
            var IRBlasterID = document.getElementsByClassName("IRBlasterId").value;
            var BrandId = document.getElementsByClassName("BrandId").value;
            var ModelId = document.getElementsByClassName("ModelId").value;
            var ButtonPress = "ModeStatus";

            if (mode == 'auto') {
                var value = 5;
            } else if (mode == 'cool') {
                var value = 1;
            } else if (mode == 'heat') {
                var value = 4;
            } else if (mode == 'dry') {
                var value = 2;
            } else if (mode == 'fan') {
                var value = 3;
            }

            var hrefTestAirConditioner = baseURL + "/irBlasterTestAirConditioner.php/?IRBlasterId=" + IRBlasterID + "&BrandId=" + BrandId + "&ModelId=" + ModelId + "&ButtonPress=" + ButtonPress + "&Value=" + value;
            console.log(hrefTestAirConditioner);
            document.getElementById("signal" + ModelId + BrandId).style.opacity = "1.0";

            //For get signal reply
            $.getJSON(hrefTestAirConditioner, function(data) {
                //data is the JSON string
                loader.style.display = "none";
                document.getElementById("signal" + ModelId + BrandId).style.opacity = "0.5";
                if (data.Command == 'TestAirConditioner' && data.Status == false) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: data.Message
                    })
                } else {
                    iziToast.show({
                        title: 'TEST AC',
                        message: 'Your action is successful',
                        theme: 'dark',
                        position: 'bottomCenter',
                        icon: 'icon-person'
                    });
                    if (mode == 'auto') {
                        document.getElementById("modeAuto" + ModelId + BrandId).src = 'Images/IRBlaster/AirConditioner/auto_icon_blue.png';
                        document.getElementById("modeCool" + ModelId + BrandId).src = 'Images/IRBlaster/AirConditioner/cool_icon_grey.png';
                        document.getElementById("modeHeat" + ModelId + BrandId).src = 'Images/IRBlaster/AirConditioner/heat_icon_grey.png';
                        document.getElementById("modeDry" + ModelId + BrandId).src = 'Images/IRBlaster/AirConditioner/dry_icon_grey.png';
                        document.getElementById("modeFan" + ModelId + BrandId).src = 'Images/IRBlaster/AirConditioner/fan_icon_grey.png';
                    } else if (mode == 'cool') {
                        document.getElementById("modeAuto" + ModelId + BrandId).src = 'Images/IRBlaster/AirConditioner/auto_icon_grey.png';
                        document.getElementById("modeCool" + ModelId + BrandId).src = 'Images/IRBlaster/AirConditioner/cool_icon_blue.png';
                        document.getElementById("modeHeat" + ModelId + BrandId).src = 'Images/IRBlaster/AirConditioner/heat_icon_grey.png';
                        document.getElementById("modeDry" + ModelId + BrandId).src = 'Images/IRBlaster/AirConditioner/dry_icon_grey.png';
                        document.getElementById("modeFan" + ModelId + BrandId).src = 'Images/IRBlaster/AirConditioner/fan_icon_grey.png';
                    } else if (mode == 'heat') {
                        document.getElementById("modeAuto" + ModelId + BrandId).src = 'Images/IRBlaster/AirConditioner/auto_icon_grey.png';
                        document.getElementById("modeCool" + ModelId + BrandId).src = 'Images/IRBlaster/AirConditioner/cool_icon_grey.png';
                        document.getElementById("modeHeat" + ModelId + BrandId).src = 'Images/IRBlaster/AirConditioner/heat_icon_blue.png';
                        document.getElementById("modeDry" + ModelId + BrandId).src = 'Images/IRBlaster/AirConditioner/dry_icon_grey.png';
                        document.getElementById("modeFan" + ModelId + BrandId).src = 'Images/IRBlaster/AirConditioner/fan_icon_grey.png';
                    } else if (mode == 'dry') {
                        document.getElementById("modeAuto" + ModelId + BrandId).src = 'Images/IRBlaster/AirConditioner/auto_icon_grey.png';
                        document.getElementById("modeCool" + ModelId + BrandId).src = 'Images/IRBlaster/AirConditioner/cool_icon_grey.png';
                        document.getElementById("modeHeat" + ModelId + BrandId).src = 'Images/IRBlaster/AirConditioner/heat_icon_grey.png';
                        document.getElementById("modeDry" + ModelId + BrandId).src = 'Images/IRBlaster/AirConditioner/dry_icon_blue.png';
                        document.getElementById("modeFan" + ModelId + BrandId).src = 'Images/IRBlaster/AirConditioner/fan_icon_grey.png';
                    } else if (mode == 'fan') {
                        document.getElementById("modeAuto" + ModelId + BrandId).src = 'Images/IRBlaster/AirConditioner/auto_icon_grey.png';
                        document.getElementById("modeCool" + ModelId + BrandId).src = 'Images/IRBlaster/AirConditioner/cool_icon_grey.png';
                        document.getElementById("modeHeat" + ModelId + BrandId).src = 'Images/IRBlaster/AirConditioner/heat_icon_grey.png';
                        document.getElementById("modeDry" + ModelId + BrandId).src = 'Images/IRBlaster/AirConditioner/dry_icon_grey.png';
                        document.getElementById("modeFan" + ModelId + BrandId).src = 'Images/IRBlaster/AirConditioner/fan_icon_blue.png';
                    }
                }
            });
        }
        //For Set Air Conditioner   
        else {
            var fields = info.split(" ");
            var IRBlasterID = fields[0];
            var DeviceId = fields[1];
            var ButtonPress = "ModeStatus";

            if (mode == 'auto') {
                var value = 5;
            } else if (mode == 'cool') {
                var value = 1;
            } else if (mode == 'heat') {
                var value = 4;
            } else if (mode == 'dry') {
                var value = 2;
            } else if (mode == 'fan') {
                var value = 3;
            }

            var hrefSetAirConditioner = baseURL + "/irBlasterSetAirConditioner.php/?IRBlasterId=" + IRBlasterID + "&DeviceId=" + DeviceId + "&ButtonPress=" + ButtonPress + "&Value=" + value;
            console.log(hrefSetAirConditioner);
            document.getElementById("signal").style.opacity = "1.0";

            //For get signal reply
            $.getJSON(hrefSetAirConditioner, function(data) {
                //data is the JSON string
                document.getElementById("signal").style.opacity = "0.5";
                loader.style.display = "none";
                if (data.Command == 'SetAirConditioner' && data.Status == false) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: data.Message
                    })
                } else {
                    iziToast.show({
                        title: 'SET AC',
                        message: 'Your action is successful',
                        theme: 'dark',
                        position: 'bottomCenter',
                        icon: 'icon-person'
                    });
                    if (mode == 'auto') {
                        document.getElementById("modeAuto").src = 'Images/IRBlaster/AirConditioner/auto_icon_blue.png';
                        document.getElementById("modeCool").src = 'Images/IRBlaster/AirConditioner/cool_icon_grey.png';
                        document.getElementById("modeHeat").src = 'Images/IRBlaster/AirConditioner/heat_icon_grey.png';
                        document.getElementById("modeDry").src = 'Images/IRBlaster/AirConditioner/dry_icon_grey.png';
                        document.getElementById("modeFan").src = 'Images/IRBlaster/AirConditioner/fan_icon_grey.png';
                    } else if (mode == 'cool') {
                        document.getElementById("modeAuto").src = 'Images/IRBlaster/AirConditioner/auto_icon_grey.png';
                        document.getElementById("modeCool").src = 'Images/IRBlaster/AirConditioner/cool_icon_blue.png';
                        document.getElementById("modeHeat").src = 'Images/IRBlaster/AirConditioner/heat_icon_grey.png';
                        document.getElementById("modeDry").src = 'Images/IRBlaster/AirConditioner/dry_icon_grey.png';
                        document.getElementById("modeFan").src = 'Images/IRBlaster/AirConditioner/fan_icon_grey.png';
                    } else if (mode == 'heat') {
                        document.getElementById("modeAuto").src = 'Images/IRBlaster/AirConditioner/auto_icon_grey.png';
                        document.getElementById("modeCool").src = 'Images/IRBlaster/AirConditioner/cool_icon_grey.png';
                        document.getElementById("modeHeat").src = 'Images/IRBlaster/AirConditioner/heat_icon_blue.png';
                        document.getElementById("modeDry").src = 'Images/IRBlaster/AirConditioner/dry_icon_grey.png';
                        document.getElementById("modeFan").src = 'Images/IRBlaster/AirConditioner/fan_icon_grey.png';
                    } else if (mode == 'dry') {
                        document.getElementById("modeAuto").src = 'Images/IRBlaster/AirConditioner/auto_icon_grey.png';
                        document.getElementById("modeCool").src = 'Images/IRBlaster/AirConditioner/cool_icon_grey.png';
                        document.getElementById("modeHeat").src = 'Images/IRBlaster/AirConditioner/heat_icon_grey.png';
                        document.getElementById("modeDry").src = 'Images/IRBlaster/AirConditioner/dry_icon_blue.png';
                        document.getElementById("modeFan").src = 'Images/IRBlaster/AirConditioner/fan_icon_grey.png';
                    } else if (mode == 'fan') {
                        document.getElementById("modeAuto").src = 'Images/IRBlaster/AirConditioner/auto_icon_grey.png';
                        document.getElementById("modeCool").src = 'Images/IRBlaster/AirConditioner/cool_icon_grey.png';
                        document.getElementById("modeHeat").src = 'Images/IRBlaster/AirConditioner/heat_icon_grey.png';
                        document.getElementById("modeDry").src = 'Images/IRBlaster/AirConditioner/dry_icon_grey.png';
                        document.getElementById("modeFan").src = 'Images/IRBlaster/AirConditioner/fan_icon_blue.png';
                    }
                }
            });
        }


    }

    function increaseValue(Max, info) {

        //For Test Air Conditioner
        if (info == "false") {
            var IRBlasterID = document.getElementsByClassName("IRBlasterId").value;
            var BrandId = document.getElementsByClassName("BrandId").value;
            var ModelId = document.getElementsByClassName("ModelId").value;
            var ButtonPress = "Temperature";
            var value = parseInt(document.getElementById('number' + ModelId + BrandId).value, 10);

            value = isNaN(value) ? 0 : value;
            if (value < Max) {
                value++;
            }

            var hrefTestAirConditioner = baseURL + "/irBlasterTestAirConditioner.php/?IRBlasterId=" + IRBlasterID + "&BrandId=" + BrandId + "&ModelId=" + ModelId + "&ButtonPress=" + ButtonPress + "&Value=" + value;
            console.log(hrefTestAirConditioner);
            document.getElementById("signal" + ModelId + BrandId).style.opacity = "1.0";

            //For get signal reply
            $.getJSON(hrefTestAirConditioner, function(data) {
                //data is the JSON string
                document.getElementById("signal" + ModelId + BrandId).style.opacity = "0.5";
                loader.style.display = "none";
                if (data.Command == 'TestAirConditioner' && data.Status == false) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: data.Message
                    })
                } else {
                    iziToast.show({
                        title: 'TEST AC',
                        message: 'Your action is successful',
                        theme: 'dark',
                        position: 'bottomCenter',
                        icon: 'icon-person'
                    });
                    document.getElementById('number' + ModelId + BrandId).value = value;
                    document.getElementById('number' + ModelId + BrandId).innerHTML = value;
                }
            });
        }
        //For Set Air Conditioner  
        else {
            var fields = info.split(" ");
            var IRBlasterID = fields[0];
            var DeviceId = fields[1];
            var ButtonPress = "Temperature";
            var value = parseInt(document.getElementById('number').value, 10);

            value = isNaN(value) ? 0 : value;
            if (value < Max) {
                value++;
            }

            var hrefSetAirConditioner = baseURL + "/irBlasterSetAirConditioner.php/?IRBlasterId=" + IRBlasterID + "&DeviceId=" + DeviceId + "&ButtonPress=" + ButtonPress + "&Value=" + value;
            console.log(hrefSetAirConditioner);
            document.getElementById("signal").style.opacity = "1.0";

            //For get signal reply
            $.getJSON(hrefSetAirConditioner, function(data) {
                //data is the JSON string
                document.getElementById("signal").style.opacity = "0.5";
                loader.style.display = "none";
                if (data.Command == 'SetAirConditioner' && data.Status == false) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: data.Message
                    })
                } else {
                    iziToast.show({
                        title: 'SET AC',
                        message: 'Your action is successful',
                        theme: 'dark',
                        position: 'bottomCenter',
                        icon: 'icon-person'
                    });
                    document.getElementById('number').value = value;
                    document.getElementById('number').innerHTML = value;
                }
            });
        }
    }

    function decreaseValue(Min, info) {

        //For Test Air Conditioner
        if (info == "false") {
            var IRBlasterID = document.getElementsByClassName("IRBlasterId").value;
            var BrandId = document.getElementsByClassName("BrandId").value;
            var ModelId = document.getElementsByClassName("ModelId").value;
            var ButtonPress = "Temperature";
            var value = parseInt(document.getElementById('number' + ModelId + BrandId).value, 10);

            value = isNaN(value) ? 0 : value;
            value < 1 ? value = 1 : '';
            if (value > Min) {
                value--;
            }

            var hrefTestAirConditioner = baseURL + "/irBlasterTestAirConditioner.php/?IRBlasterId=" + IRBlasterID + "&BrandId=" + BrandId + "&ModelId=" + ModelId + "&ButtonPress=" + ButtonPress + "&Value=" + value;
            console.log(hrefTestAirConditioner);
            document.getElementById("signal" + ModelId + BrandId).style.opacity = "1.0";

            //For get signal reply
            $.getJSON(hrefTestAirConditioner, function(data) {
                //data is the JSON string
                document.getElementById("signal" + ModelId + BrandId).style.opacity = "0.5";
                loader.style.display = "none";
                if (data.Command == 'TestAirConditioner' && data.Status == false) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: data.Message
                    })
                } else {
                    iziToast.show({
                        title: 'TEST AC',
                        message: 'Your action is successful',
                        theme: 'dark',
                        position: 'bottomCenter',
                        icon: 'icon-person'
                    });
                    document.getElementById('number' + ModelId + BrandId).value = value;
                    document.getElementById('number' + ModelId + BrandId).innerHTML = value;
                }
            });
        }
        //For Set Air Conditioner  
        else {
            var fields = info.split(" ");
            var IRBlasterID = fields[0];
            var DeviceId = fields[1];
            var ButtonPress = "Temperature";
            var value = parseInt(document.getElementById('number').value, 10);

            value = isNaN(value) ? 0 : value;
            value < 1 ? value = 1 : '';
            if (value > Min) {
                value--;
            }

            var hrefSetAirConditioner = baseURL + "/irBlasterSetAirConditioner.php/?IRBlasterId=" + IRBlasterID + "&DeviceId=" + DeviceId + "&ButtonPress=" + ButtonPress + "&Value=" + value;
            console.log(hrefSetAirConditioner);
            document.getElementById("signal").style.opacity = "1.0";

            //For get signal reply
            $.getJSON(hrefSetAirConditioner, function(data) {
                //data is the JSON string
                document.getElementById("signal").style.opacity = "0.5";
                loader.style.display = "none";
                if (data.Command == 'SetAirConditioner' && data.Status == false) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: data.Message
                    })
                } else {
                    iziToast.show({
                        title: 'SET AC',
                        message: 'Your action is successful',
                        theme: 'dark',
                        position: 'bottomCenter',
                        icon: 'icon-person'
                    });
                    document.getElementById('number').value = value;
                    document.getElementById('number').innerHTML = value;
                }
            });
        }
    }

    function changeFanSpeed(Limit, info) {

        //For Test Air Conditioner
        if (info == "false") {
            var IRBlasterID = document.getElementsByClassName("IRBlasterId").value;
            var BrandId = document.getElementsByClassName("BrandId").value;
            var ModelId = document.getElementsByClassName("ModelId").value;
            var ButtonPress = "FanSpeed";

            if (document.getElementById("fanlimit" + ModelId + BrandId).value == 1 && document.getElementById("fanlimit" + ModelId + BrandId).value <= Limit) {
                if (document.getElementById("fanlimit" + ModelId + BrandId).value == Limit) {
                    var value = Limit + 1;
                } else {
                    var value = 2;
                }
            } else if (document.getElementById("fanlimit" + ModelId + BrandId).value == 2 && document.getElementById("fanlimit" + ModelId + BrandId).value <= Limit) {
                if (document.getElementById("fanlimit" + ModelId + BrandId).value == Limit) {
                    var value = Limit + 1;
                } else {
                    var value = 3;
                }
            } else if (document.getElementById("fanlimit" + ModelId + BrandId).value == 3 && document.getElementById("fanlimit" + ModelId + BrandId).value <= Limit) {
                if (document.getElementById("fanlimit" + ModelId + BrandId).value == Limit) {
                    var value = Limit + 1;
                } else {
                    var value = 4;
                }
            } else if (document.getElementById("fanlimit" + ModelId + BrandId).value == 4 && document.getElementById("fanlimit" + ModelId + BrandId).value <= Limit) {
                if (document.getElementById("fanlimit" + ModelId + BrandId).value == Limit) {
                    var value = Limit + 1;
                } else {
                    var value = 5;
                }
            } else if (document.getElementById("fanlimit" + ModelId + BrandId).value == 5 && document.getElementById("fanlimit" + ModelId + BrandId).value <= Limit) {
                var value = Limit + 1;
            } else if (document.getElementById("fanlimit" + ModelId + BrandId).value == Limit + 1) {
                var value = 1;
            }

            var hrefTestAirConditioner = baseURL + "/irBlasterTestAirConditioner.php/?IRBlasterId=" + IRBlasterID + "&BrandId=" + BrandId + "&ModelId=" + ModelId + "&ButtonPress=" + ButtonPress + "&Value=" + value;
            console.log(hrefTestAirConditioner);
            document.getElementById("signal" + ModelId + BrandId).style.opacity = "1.0";

            //For get signal reply
            $.getJSON(hrefTestAirConditioner, function(data) {
                //data is the JSON string
                document.getElementById("signal" + ModelId + BrandId).style.opacity = "0.5";
                loader.style.display = "none";
                if (data.Command == 'TestAirConditioner' && data.Status == false) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: data.Message
                    })
                } else {
                    iziToast.show({
                        title: 'TEST AC',
                        message: 'Your action is successful',
                        theme: 'dark',
                        position: 'bottomCenter',
                        icon: 'icon-person'
                    });
                    if (document.getElementById("fanlimit" + ModelId + BrandId).value == 1 && document.getElementById("fanlimit" + ModelId + BrandId).value <= Limit) {

                        if (document.getElementById("fanlimit" + ModelId + BrandId).value == Limit) {
                            document.getElementById("fanlimit" + ModelId + BrandId).src = "Images/IRBlaster/AirConditioner/icon_fanspeed_auto_v04.png";
                            document.getElementById("fanlimit" + ModelId + BrandId).value = Limit + 1;
                        } else {
                            document.getElementById("fanlimit" + ModelId + BrandId).src = "Images/IRBlaster/AirConditioner/icon_fanspeed_2_v04.png";
                            document.getElementById("fanlimit" + ModelId + BrandId).value = 2;
                        }

                    } else if (document.getElementById("fanlimit" + ModelId + BrandId).value == 2 && document.getElementById("fanlimit" + ModelId + BrandId).value <= Limit) {

                        if (document.getElementById("fanlimit" + ModelId + BrandId).value == Limit) {
                            document.getElementById("fanlimit" + ModelId + BrandId).src = "Images/IRBlaster/AirConditioner/icon_fanspeed_auto_v04.png";
                            document.getElementById("fanlimit" + ModelId + BrandId).value = Limit + 1;
                        } else {
                            document.getElementById("fanlimit" + ModelId + BrandId).src = "Images/IRBlaster/AirConditioner/icon_fanspeed_3_v04.png";
                            document.getElementById("fanlimit" + ModelId + BrandId).value = 3;
                        }

                    } else if (document.getElementById("fanlimit" + ModelId + BrandId).value == 3 && document.getElementById("fanlimit" + ModelId + BrandId).value <= Limit) {

                        if (document.getElementById("fanlimit" + ModelId + BrandId).value == Limit) {
                            document.getElementById("fanlimit" + ModelId + BrandId).src = "Images/IRBlaster/AirConditioner/icon_fanspeed_auto_v04.png";
                            document.getElementById("fanlimit" + ModelId + BrandId).value = Limit + 1;
                        } else {
                            document.getElementById("fanlimit" + ModelId + BrandId).src = "Images/IRBlaster/AirConditioner/icon_fanspeed_4_v04.png";
                            document.getElementById("fanlimit" + ModelId + BrandId).value = 4;
                        }

                    } else if (document.getElementById("fanlimit" + ModelId + BrandId).value == 4 && document.getElementById("fanlimit" + ModelId + BrandId).value <= Limit) {

                        if (document.getElementById("fanlimit" + ModelId + BrandId).value == Limit) {
                            document.getElementById("fanlimit" + ModelId + BrandId).src = "Images/IRBlaster/AirConditioner/icon_fanspeed_auto_v04.png";
                            document.getElementById("fanlimit" + ModelId + BrandId).value = Limit + 1;
                        } else {
                            document.getElementById("fanlimit" + ModelId + BrandId).src = "Images/IRBlaster/AirConditioner/icon_fanspeed_5_v04.png";
                            document.getElementById("fanlimit" + ModelId + BrandId).value = 5;
                        }

                    } else if (document.getElementById("fanlimit" + ModelId + BrandId).value == 5 && document.getElementById("fanlimit" + ModelId + BrandId).value <= Limit) {

                        document.getElementById("fanlimit" + ModelId + BrandId).src = "Images/IRBlaster/AirConditioner/icon_fanspeed_auto_v04.png";
                        document.getElementById("fanlimit" + ModelId + BrandId).value = Limit + 1;

                    } else if (document.getElementById("fanlimit" + ModelId + BrandId).value == Limit + 1) {

                        document.getElementById("fanlimit" + ModelId + BrandId).src = "Images/IRBlaster/AirConditioner/icon_fanspeed_1_v04.png";
                        document.getElementById("fanlimit" + ModelId + BrandId).value = 1;
                    }
                }
            });
        }
        //For Set Air Conditioner 
        else {
            var fields = info.split(" ");
            var IRBlasterID = fields[0];
            var DeviceId = fields[1];
            var ButtonPress = "FanSpeed";

            if (document.getElementById("fanlimit").value == 1 && document.getElementById("fanlimit").value <= Limit) {
                if (document.getElementById("fanlimit").value == Limit) {
                    var value = Limit + 1;
                } else {
                    var value = 2;
                }
            } else if (document.getElementById("fanlimit").value == 2 && document.getElementById("fanlimit").value <= Limit) {
                if (document.getElementById("fanlimit").value == Limit) {
                    var value = Limit + 1;
                } else {
                    var value = 3;
                }
            } else if (document.getElementById("fanlimit").value == 3 && document.getElementById("fanlimit").value <= Limit) {
                if (document.getElementById("fanlimit").value == Limit) {
                    var value = Limit + 1;
                } else {
                    var value = 4;
                }
            } else if (document.getElementById("fanlimit").value == 4 && document.getElementById("fanlimit").value <= Limit) {
                if (document.getElementById("fanlimit").value == Limit) {
                    var value = Limit + 1;
                } else {
                    var value = 5;
                }
            } else if (document.getElementById("fanlimit").value == 5 && document.getElementById("fanlimit").value <= Limit) {
                var value = Limit + 1;

            } else if (document.getElementById("fanlimit").value == Limit + 1 || document.getElementById("fanlimit").value == 6) {
                var value = 1;
            }

            var hrefSetAirConditioner = baseURL + "/irBlasterSetAirConditioner.php/?IRBlasterId=" + IRBlasterID + "&DeviceId=" + DeviceId + "&ButtonPress=" + ButtonPress + "&Value=" + value;
            console.log(hrefSetAirConditioner);
            document.getElementById("signal").style.opacity = "1.0";

            //For get signal reply
            $.getJSON(hrefSetAirConditioner, function(data) {
                //data is the JSON string
                document.getElementById("signal").style.opacity = "0.5";
                loader.style.display = "none";
                if (data.Command == 'SetAirConditioner' && data.Status == false) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: data.Message
                    })
                } else {
                    iziToast.show({
                        title: 'SET AC',
                        message: 'Your action is successful',
                        theme: 'dark',
                        position: 'bottomCenter',
                        icon: 'icon-person'
                    });
                    if (document.getElementById("fanlimit").value == 1 && document.getElementById("fanlimit").value <= Limit) {

                        if (document.getElementById("fanlimit").value == Limit) {
                            document.getElementById("fanlimit").src = "Images/IRBlaster/AirConditioner/icon_fanspeed_auto_v04.png";
                            document.getElementById("fanlimit").value = Limit + 1;
                        } else {
                            document.getElementById("fanlimit").src = "Images/IRBlaster/AirConditioner/icon_fanspeed_2_v04.png";
                            document.getElementById("fanlimit").value = 2;
                        }

                    } else if (document.getElementById("fanlimit").value == 2 && document.getElementById("fanlimit").value <= Limit) {

                        if (document.getElementById("fanlimit").value == Limit) {
                            document.getElementById("fanlimit").src = "Images/IRBlaster/AirConditioner/icon_fanspeed_auto_v04.png";
                            document.getElementById("fanlimit").value = Limit + 1;
                        } else {
                            document.getElementById("fanlimit").src = "Images/IRBlaster/AirConditioner/icon_fanspeed_3_v04.png";
                            document.getElementById("fanlimit").value = 3;
                        }

                    } else if (document.getElementById("fanlimit").value == 3 && document.getElementById("fanlimit").value <= Limit) {

                        if (document.getElementById("fanlimit").value == Limit) {
                            document.getElementById("fanlimit").src = "Images/IRBlaster/AirConditioner/icon_fanspeed_auto_v04.png";
                            document.getElementById("fanlimit").value = Limit + 1;
                        } else {
                            document.getElementById("fanlimit").src = "Images/IRBlaster/AirConditioner/icon_fanspeed_4_v04.png";
                            document.getElementById("fanlimit").value = 4;
                        }

                    } else if (document.getElementById("fanlimit").value == 4 && document.getElementById("fanlimit").value <= Limit) {

                        if (document.getElementById("fanlimit").value == Limit) {
                            document.getElementById("fanlimit").src = "Images/IRBlaster/AirConditioner/icon_fanspeed_auto_v04.png";
                            document.getElementById("fanlimit").value = Limit + 1;
                        } else {
                            document.getElementById("fanlimit").src = "Images/IRBlaster/AirConditioner/icon_fanspeed_5_v04.png";
                            document.getElementById("fanlimit").value = 5;
                        }

                    } else if (document.getElementById("fanlimit").value == 5 && document.getElementById("fanlimit").value <= Limit) {

                        document.getElementById("fanlimit").src = "Images/IRBlaster/AirConditioner/icon_fanspeed_auto_v04.png";
                        document.getElementById("fanlimit").value = Limit + 1;

                    } else if (document.getElementById("fanlimit").value == Limit + 1 || document.getElementById("fanlimit").value == 6) {

                        document.getElementById("fanlimit").src = "Images/IRBlaster/AirConditioner/icon_fanspeed_1_v04.png";
                        document.getElementById("fanlimit").value = 1;
                    }
                }
            });
        }
    }

    function swingVertical(type, info) {

        //For Test Air Conditioner
        if (info == "false") {
            var IRBlasterID = document.getElementsByClassName("IRBlasterId").value;
            var BrandId = document.getElementsByClassName("BrandId").value;
            var ModelId = document.getElementsByClassName("ModelId").value;
            var ButtonPress = "SwingU/D";

            if (type == 1) {
                if (document.getElementById("swingUD" + ModelId + BrandId).value == 0) {
                    var value = "ON";
                    document.getElementById("swingUD" + ModelId + BrandId).value = 1;
                } else {
                    var value = "OFF";
                    document.getElementById("swingUD" + ModelId + BrandId).value = 0;
                }
            } else if (type == 2) {
                var value = "POSITION";
            } else if (type == 3) {
                var value = "TOGGLE";
            }

            var hrefTestAirConditioner = baseURL + "/irBlasterTestAirConditioner.php/?IRBlasterId=" + IRBlasterID + "&BrandId=" + BrandId + "&ModelId=" + ModelId + "&ButtonPress=" + ButtonPress + "&Value=" + value;
            console.log(hrefTestAirConditioner);
            document.getElementById("signal" + ModelId + BrandId).style.opacity = "1.0";

            //For get signal reply
            $.getJSON(hrefTestAirConditioner, function(data) {
                //data is the JSON string
                document.getElementById("signal" + ModelId + BrandId).style.opacity = "0.5";
                loader.style.display = "none";
                if (data.Command == 'TestAirConditioner' && data.Status == false) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: data.Message
                    })
                } else {
                    iziToast.show({
                        title: 'TEST AC',
                        message: 'Your action is successful',
                        theme: 'dark',
                        position: 'bottomCenter',
                        icon: 'icon-person'
                    });
                    if (type == 1) {
                        if (document.getElementById("swingUD" + ModelId + BrandId).value == 0) {
                            document.getElementById("swingUD" + ModelId + BrandId).src = "Images/IRBlaster/AirConditioner/icon_swing_horizontal_v3_grey.png";
                        } else {
                            document.getElementById("swingUD" + ModelId + BrandId).src = "Images/IRBlaster/AirConditioner/icon_swing_horizontal_v3_blue.png";
                        }
                    }
                }
            });
        }
        //For Set Air Conditioner
        else {
            var fields = info.split(" ");
            var IRBlasterID = fields[0];
            var DeviceId = fields[1];
            var ButtonPress = "SwingU/D";

            if (type == 1) {
                if (document.getElementById("swingUD").value == 0) {
                    var value = "ON";
                    document.getElementById("swingUD").value = 1;
                } else {
                    var value = "OFF";
                    document.getElementById("swingUD").value = 0;
                }
            } else if (type == 2) {
                var value = "POSITION";
            } else if (type == 3) {
                var value = "TOGGLE";
            }

            var hrefSetAirConditioner = baseURL + "/irBlasterSetAirConditioner.php/?IRBlasterId=" + IRBlasterID + "&DeviceId=" + DeviceId + "&ButtonPress=" + ButtonPress + "&Value=" + value;
            console.log(hrefSetAirConditioner);
            document.getElementById("signal").style.opacity = "1.0";

            //For get signal reply
            $.getJSON(hrefSetAirConditioner, function(data) {
                //data is the JSON string
                document.getElementById("signal").style.opacity = "0.5";
                loader.style.display = "none";
                if (data.Command == 'SetAirConditioner' && data.Status == false) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: data.Message
                    })
                } else {
                    iziToast.show({
                        title: 'SET AC',
                        message: 'Your action is successful',
                        theme: 'dark',
                        position: 'bottomCenter',
                        icon: 'icon-person'
                    });
                    if (type == 1) {
                        if (document.getElementById("swingUD").value == 0) {
                            document.getElementById("swingUD").src = "Images/IRBlaster/AirConditioner/icon_swing_horizontal_v3_grey.png";
                        } else {
                            document.getElementById("swingUD").src = "Images/IRBlaster/AirConditioner/icon_swing_horizontal_v3_blue.png";
                        }
                    }
                }
            });
        }
    }

    function swingHorizontal(type, info) {

        //For Test Air Conditioner
        if (info == "false") {
            var IRBlasterID = document.getElementsByClassName("IRBlasterId").value;
            var BrandId = document.getElementsByClassName("BrandId").value;
            var ModelId = document.getElementsByClassName("ModelId").value;
            var ButtonPress = "SwingL/R";

            if (type == 1) {
                if (document.getElementById("swingLR" + ModelId + BrandId).value == 0) {
                    var value = "ON";
                    document.getElementById("swingLR" + ModelId + BrandId).value = 1;
                } else {
                    var value = "OFF";
                    document.getElementById("swingLR" + ModelId + BrandId).value = 0;
                }
            } else if (type == 2) {
                var value = "POSITION";
            } else if (type == 3) {
                var value = "TOGGLE";
            }

            var hrefTestAirConditioner = baseURL + "/irBlasterTestAirConditioner.php/?IRBlasterId=" + IRBlasterID + "&BrandId=" + BrandId + "&ModelId=" + ModelId + "&ButtonPress=" + ButtonPress + "&Value=" + value;
            console.log(hrefTestAirConditioner);
            document.getElementById("signal" + ModelId + BrandId).style.opacity = "1.0";

            //For get signal reply
            $.getJSON(hrefTestAirConditioner, function(data) {
                //data is the JSON string
                document.getElementById("signal" + ModelId + BrandId).style.opacity = "0.5";
                loader.style.display = "none";
                if (data.Command == 'TestAirConditioner' && data.Status == false) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: data.Message
                    })
                } else {
                    iziToast.show({
                        title: 'TEST AC',
                        message: 'Your action is successful',
                        theme: 'dark',
                        position: 'bottomCenter',
                        icon: 'icon-person'
                    });
                    if (type == 1) {
                        if (document.getElementById("swingLR" + ModelId + BrandId).value == 0) {
                            document.getElementById("swingLR" + ModelId + BrandId).src = "Images/IRBlaster/AirConditioner/icon_swing_vertical_v3_blue.png";
                        } else {
                            document.getElementById("swingLR" + ModelId + BrandId).src = "Images/IRBlaster/AirConditioner/icon_swing_vertical_v3_grey.png";
                        }
                    }
                }
            });
        }
        //For Set Air Conditioner
        else {
            var fields = info.split(" ");
            var IRBlasterID = fields[0];
            var DeviceId = fields[1];
            var ButtonPress = "SwingL/R";

            if (type == 1) {
                if (document.getElementById("swingLR").value == 0) {
                    var value = "ON";
                    document.getElementById("swingLR").value = 1;
                } else {
                    var value = "OFF";
                    document.getElementById("swingLR").value = 0;
                }
            } else if (type == 2) {
                var value = "POSITION";
            } else if (type == 3) {
                var value = "TOGGLE";
            }

            var hrefSetAirConditioner = baseURL + "/irBlasterSetAirConditioner.php/?IRBlasterId=" + IRBlasterID + "&DeviceId=" + DeviceId + "&ButtonPress=" + ButtonPress + "&Value=" + value;
            console.log(hrefSetAirConditioner);
            document.getElementById("signal").style.opacity = "1.0";

            //For get signal reply
            $.getJSON(hrefSetAirConditioner, function(data) {
                //data is the JSON string
                document.getElementById("signal").style.opacity = "0.5";
                loader.style.display = "none";
                if (data.Command == 'SetAirConditioner' && data.Status == false) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: data.Message
                    })
                } else {
                    iziToast.show({
                        title: 'SET AC',
                        message: 'Your action is successful',
                        theme: 'dark',
                        position: 'bottomCenter',
                        icon: 'icon-person'
                    });
                    if (type == 1) {
                        if (document.getElementById("swingLR").value == 0) {
                            document.getElementById("swingLR").src = "Images/IRBlaster/AirConditioner/icon_swing_vertical_v3_blue.png";
                        } else {
                            document.getElementById("swingLR").src = "Images/IRBlaster/AirConditioner/icon_swing_vertical_v3_grey.png";
                        }
                    }

                }
            });
        }
    }

    function powerOption(power, value, info) {

        //For Test Air Conditioner
        if (info == "false") {
            var IRBlasterID = document.getElementsByClassName("IRBlasterId").value;
            var BrandId = document.getElementsByClassName("BrandId").value;
            var ModelId = document.getElementsByClassName("ModelId").value;

            if (power == "ON/OFF") {
                if (value == 1) {
                    var ButtonPress = "PowerOn/Toggle";
                } else if (value == 0) {
                    var ButtonPress = "PowerOff";
                }
            } else {
                var ButtonPress = "PowerOn/Toggle";
            }

            var hrefTestAirConditioner = baseURL + "/irBlasterTestAirConditioner.php/?IRBlasterId=" + IRBlasterID + "&BrandId=" + BrandId + "&ModelId=" + ModelId + "&ButtonPress=" + ButtonPress + "&Value=" + value;
            console.log(hrefTestAirConditioner);
            document.getElementById("signal" + ModelId + BrandId).style.opacity = "1.0";

            //For get signal reply
            $.getJSON(hrefTestAirConditioner, function(data) {
                //data is the JSON string
                document.getElementById("signal" + ModelId + BrandId).style.opacity = "0.5";
                loader.style.display = "none";
                if (data.Command == 'TestAirConditioner' && data.Status == false) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: data.Message
                    })
                } else {
                    iziToast.show({
                        title: 'TEST AC',
                        message: 'Your action is successful',
                        theme: 'dark',
                        position: 'bottomCenter',
                        icon: 'icon-person'
                    });
                    if (power == "ON/OFF") {
                        if (value == 1) {
                            document.getElementById("powerOn" + ModelId + BrandId).src = "Images/IRBlaster/AirConditioner/icons_on.png";
                            document.getElementById("powerOff" + ModelId + BrandId).src = "Images/IRBlaster/AirConditioner/icons_off_Grey.png";
                        } else if (value == 0) {
                            document.getElementById("powerOn" + ModelId + BrandId).src = "Images/IRBlaster/AirConditioner/icons_on_Grey.png";
                            document.getElementById("powerOff" + ModelId + BrandId).src = "Images/IRBlaster/AirConditioner/icons_off.png";
                        }
                    }
                }
            });
        }
        //For Set Air Conditioner
        else {
            var fields = info.split(" ");
            var IRBlasterID = fields[0];
            var DeviceId = fields[1];

            if (power == "ON/OFF") {
                if (value == 1) {
                    var ButtonPress = "PowerOn/Toggle";
                } else if (value == 0) {
                    var ButtonPress = "PowerOff";
                }
            } else {
                var ButtonPress = "PowerOn/Toggle";
            }

            var hrefSetAirConditioner = baseURL + "/irBlasterSetAirConditioner.php/?IRBlasterId=" + IRBlasterID + "&DeviceId=" + DeviceId + "&ButtonPress=" + ButtonPress + "&Value=" + value;
            console.log(hrefSetAirConditioner);
            document.getElementById("signal").style.opacity = "1.0";

            //For get signal reply
            $.getJSON(hrefSetAirConditioner, function(data) {
                //data is the JSON string
                document.getElementById("signal").style.opacity = "0.5";
                loader.style.display = "none";
                if (data.Command == 'SetAirConditioner' && data.Status == false) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: data.Message
                    })
                } else {
                    iziToast.show({
                        title: 'SET AC',
                        message: 'Your action is successful',
                        theme: 'dark',
                        position: 'bottomCenter',
                        icon: 'icon-person'
                    });
                    if (power == "ON/OFF") {
                        if (value == 1) {
                            document.getElementById("powerOn").src = "Images/IRBlaster/AirConditioner/icons_on.png";
                            document.getElementById("powerOff").src = "Images/IRBlaster/AirConditioner/icons_off_Grey.png";
                        } else if (value == 0) {
                            document.getElementById("powerOn").src = "Images/IRBlaster/AirConditioner/icons_on_Grey.png";
                            document.getElementById("powerOff").src = "Images/IRBlaster/AirConditioner/icons_off.png";
                        }
                    }
                }
            });
        }
    }

    //--------------------------------------------------------------End Function for Air Conditioner-----------------------------------------------------------------------

    function useAirConditioner(BrandName) {
        var BrandId = document.getElementsByClassName("BrandId").value;
        var ModelId = document.getElementsByClassName("ModelId").value;
        var IRBlasterInfo = document.getElementsByClassName("IRBlasterInfo").value;
        var IRBlasterJson = JSON.parse(IRBlasterInfo);

        document.getElementById("AirConditionerForm" + ModelId + BrandId).style.display = "none"
        document.getElementById("airconditionerDeviceForm").style.display = "block";

        document.getElementById("deviceAirConditionerName").value = BrandName + " " + ModelId + " " + "AC";
        document.getElementById("deviceAirConditionerLocation").value = IRBlasterJson.LocationName;
    }

    function addAirConditioner() {
        var IRBlasterID = document.getElementsByClassName("IRBlasterId").value;
        var TypeID = 1;
        var BrandId = document.getElementsByClassName("BrandId").value;
        var ModelId = document.getElementsByClassName("ModelId").value;
        var DeviceName = document.getElementById("deviceAirConditionerName").value;
        var DeviceLocation = document.getElementById("deviceAirConditionerLocation").value;
        var IRBlasterInfo = document.getElementsByClassName("IRBlasterInfo").value;
        var IRBlasterJson = JSON.parse(IRBlasterInfo);

        var hrefAddDevice = baseURL + "/irBlasterAddAirConditionerDevice.php/?IRBlasterID=" + IRBlasterID + "&TypeID=" + TypeID + "&BrandID=" + BrandId + "&Model=" + ModelId + "&DeviceName=" + DeviceName;
        var loader = document.getElementById("loader");

        var boolSame = false;
        var sameName = '';
        for (i = 0; i < IRBlasterJson.AirConditionerList.length; i++) {
            if (DeviceName == IRBlasterJson.AirConditionerList[i].DeviceName) {
                boolSame = true;
                sameName = IRBlasterJson.AirConditionerList[i].DeviceName;
                break;
            }
        }

        for (i = 0; i < IRBlasterJson.RemoteList.length; i++) {
            if (DeviceName == IRBlasterJson.RemoteList[i].DeviceName) {
                boolSame = true;
                sameName = IRBlasterJson.RemoteList[i].DeviceName;
                break;
            }
        }

        var boolEmpty = false;
        if (DeviceName.length == 0) {
            boolEmpty = true;
        }

        if (!boolSame && !boolEmpty) {
            // document.getElementsByClassName("Check").value = 1;
            loader.style.display = "block";
            $.getJSON(hrefAddDevice, function(data) {
                console.log(data);
                //data is the JSON string
                loader.style.display = "none";
                if (data.Command == 'AddDeviceToIRBlaster' && data.Status == false) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: data.Message
                    })
                } else {
                    Swal.fire(
                        'Good job!',
                        'New AC has been added successfully',
                        'success'
                    )
                    afterAddAirConditioner(DeviceName, DeviceLocation, ModelId, BrandId, IRBlasterID, TypeID)
                }
            });
        } else if (boolSame) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: sameName + ' already exist IRBlaster'
            })
        } else if (boolEmpty) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Name cannot be empty'
            })
        }
    }

    function afterAddAirConditioner(DeviceName, DeviceLocation, ModelId, BrandId, IRBlasterID, TypeID) {
        var hrefGetAirConditionerLimit = baseURL + "/irBlasterAirConditionerLimit.php/?IRBlasterId=" + IRBlasterID + "&TypeId=" + TypeID + "&BrandId=" + BrandId + "&Model=" + ModelId + "&Status=Old";
        $.getJSON(hrefGetAirConditionerLimit, function(data) {
            if (data.ACModelID == ModelId && data.ACBrandID == BrandId) {
                loader.style.display = "none";
                document.getElementById("airconditionerDeviceForm").style.display = "none";
                document.getElementById("AirConditionerFormSet").style.display = "block"
                document.getElementById("deviceName").innerHTML = DeviceName;
                document.getElementById("locationName").innerHTML = DeviceLocation;
                document.getElementById('number').value = 21;
                document.getElementById('number').innerHTML = 21;
                document.getElementById("swingLR").value = 0;
                document.getElementById("swingUD").value = 0;

                if (data.ModeAutoOption == true) {
                    document.getElementById("modeAuto").src = "Images/IRBlaster/AirConditioner/auto_icon_grey.png";
                    document.getElementById("auto").style.display = "block";

                } else {
                    document.getElementById("auto").style.display = "none";
                }

                if (data.ModeCoolOption == true) {
                    document.getElementById("modeCool").src = "Images/IRBlaster/AirConditioner/cool_icon_blue.png";
                    document.getElementById("cool").style.display = "block";

                } else {
                    document.getElementById("cool").style.display = "none";
                }

                if (data.ModeHeatOption == true) {
                    document.getElementById("modeHeat").src = "Images/IRBlaster/AirConditioner/heat_icon_grey.png";
                    document.getElementById("heat").style.display = "block";

                } else {
                    document.getElementById("heat").style.display = "none";
                }

                if (data.ModeDryOption == true) {
                    document.getElementById("modeDry").src = "Images/IRBlaster/AirConditioner/dry_icon_grey.png";
                    document.getElementById("dry").style.display = "block";

                } else {
                    document.getElementById("dry").style.display = "none";
                }

                if (data.ModeFanOption == true) {
                    document.getElementById("modeFan").src = "Images/IRBlaster/AirConditioner/fan_icon_grey.png";
                    document.getElementById("fan").style.display = "block";

                } else {
                    document.getElementById("fan").style.display = "none";
                }

                if (data.FanAutoOption == true) {

                    var temp = 1;
                    document.getElementById("fanlimit").src = "Images/IRBlaster/AirConditioner/icon_fanspeed_" + temp + "_v04.png";
                    document.getElementById("fanspeed").style.display = "block";

                } else {
                    document.getElementById("fanspeed").style.display = "none";
                }

                if (data.UpDownSwingAutoOption == true) {

                    document.getElementById("swingUD").src = "Images/IRBlaster/AirConditioner/icon_swing_horizontal_v3_blue.png";
                    document.getElementById("swingvertical").style.display = "block";

                } else {
                    document.getElementById("swingvertical").style.display = "none";
                }

                if (data.LeftRightSwingAutoOption == true) {

                    document.getElementById("swingLR").src = "Images/IRBlaster/AirConditioner/icon_swing_vertical_v3_blue.png";
                    document.getElementById("swinghorizontal").style.display = "block";

                } else {
                    document.getElementById("swinghorizontal").style.display = "none";
                }

                if (data.PowerOption == "ON/OFF") {

                    document.getElementById("powerOn").src = "Images/IRBlaster/AirConditioner/icons_on_Grey.png";
                    document.getElementById("powerOff").src = "Images/IRBlaster/AirConditioner/icons_off.png";
                    document.getElementById("on").style.display = "block";
                    document.getElementById("off").style.display = "block";

                } else {
                    document.getElementById("on").style.display = "none";
                    document.getElementById("off").style.display = "none";
                }

                if (data.PowerOption == "TOGGLE") {
                    document.getElementById("powerToggle").src = "Images/IRBlaster/AirConditioner/icons_toggle.png";
                    document.getElementById("toggle").style.display = "block";
                } else {
                    document.getElementById("toggle").style.display = "none";
                }

                var IRBlasterID = 0;
                var DeviceId = 0;
                var info = IRBlasterID + " " + DeviceId;
                document.getElementById("modeAuto").onclick = function() {
                    var mode = "auto";
                    changeMode(mode, info)
                };
                document.getElementById("modeCool").onclick = function() {
                    var mode = "cool";
                    changeMode(mode, info)
                };
                document.getElementById("modeHeat").onclick = function() {
                    var mode = "heat";
                    changeMode(mode, info)
                };
                document.getElementById("modeDry").onclick = function() {
                    var mode = "dry";
                    changeMode(mode, info)
                };
                document.getElementById("modeFan").onclick = function() {
                    var mode = "fan";
                    changeMode(mode, info)
                };
                document.getElementById("increase").onclick = function() {
                    increaseValue(data.TempMax, info)
                };
                document.getElementById("decrease").onclick = function() {
                    decreaseValue(data.TempMin, info)
                };
                document.getElementById("fanlimit").onclick = function() {
                    changeFanSpeed(data.FanLimit, info)
                };
                document.getElementById("swingLR").onclick = function() {
                    swingHorizontal(data.LeftRightSwingType, info)
                };
                document.getElementById("swingUD").onclick = function() {
                    swingVertical(data.UpDownSwingType, info)
                };
                document.getElementById("powerOn").onclick = function() {
                    powerOption(data.PowerOption, 1, info)
                };
                document.getElementById("powerOff").onclick = function() {
                    powerOption(data.PowerOption, 0, info)
                };
                document.getElementById("powerToggle").onclick = function() {
                    powerOption(data.PowerOption, 1, info)
                };
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Failed',
                    text: 'Fetch data failed'
                })
                loader.style.display = "none";
            }
        });
    }

    function openAirConditionerForm(AirCondtionerInfo, LocationName) {
        var fields = AirCondtionerInfo.split(" ");
        var AirCondtionerJson = JSON.parse(AirCondtionerInfo);
        var DeviceId = AirCondtionerJson.DeviceId;
        var IRBlasterID = AirCondtionerJson.IRBlasterId;
        var TypeID = AirCondtionerJson.TypeId;
        var BrandId = AirCondtionerJson.BrandId;
        var ModelId = AirCondtionerJson.ModelId;
        var DeviceName = AirCondtionerJson.DeviceName;

        var hrefGetAirConditionerLimit = baseURL + "/irBlasterAirConditionerLimit.php/?IRBlasterId=" + IRBlasterID + "&TypeId=" + TypeID + "&BrandId=" + BrandId + "&Model=" + ModelId + "&Status=Old";
        var loader = document.getElementById("loader");
        loader.style.display = "block";
        $.getJSON(hrefGetAirConditionerLimit, function(data) {
            if (data.ACModelID == ModelId && data.ACBrandID == BrandId) {
                loader.style.display = "none";
                document.getElementById("myPopupAction" + IRBlasterID).style.display = "none";
                document.getElementById("AirConditionerFormSet").style.display = "block"
                document.getElementById("deviceName").innerHTML = DeviceName;
                document.getElementById("locationName").innerHTML = LocationName;
                document.getElementById('number').value = AirCondtionerJson.TemperatureStatus;
                document.getElementById('number').innerHTML = AirCondtionerJson.TemperatureStatus;
                document.getElementById("swingLR").value = AirCondtionerJson.LeftRightSwingStatus;
                document.getElementById("swingUD").value = AirCondtionerJson.UpDownSwingStatus;
                document.getElementById("fanlimit").value = AirCondtionerJson.FanSpeedStatus;

                if (data.ModeAutoOption == true) {

                    if (AirCondtionerJson.ModeAutoStatus == true) {
                        document.getElementById("modeAuto").src = "Images/IRBlaster/AirConditioner/auto_icon_blue.png";
                    } else {
                        document.getElementById("modeAuto").src = "Images/IRBlaster/AirConditioner/auto_icon_grey.png";
                    }
                    document.getElementById("auto").style.display = "block";

                } else {
                    document.getElementById("auto").style.display = "none";
                }

                if (data.ModeCoolOption == true) {

                    if (AirCondtionerJson.ModeCoolStatus == true) {
                        document.getElementById("modeCool").src = "Images/IRBlaster/AirConditioner/cool_icon_blue.png";
                    } else {
                        document.getElementById("modeCool").src = "Images/IRBlaster/AirConditioner/cool_icon_grey.png";
                    }
                    document.getElementById("cool").style.display = "block";

                } else {
                    document.getElementById("cool").style.display = "none";
                }

                if (data.ModeHeatOption == true) {

                    if (AirCondtionerJson.ModeHeatStatus == true) {
                        document.getElementById("modeHeat").src = "Images/IRBlaster/AirConditioner/heat_icon_blue.png";
                    } else {
                        document.getElementById("modeHeat").src = "Images/IRBlaster/AirConditioner/heat_icon_grey.png";
                    }
                    document.getElementById("heat").style.display = "block";

                } else {
                    document.getElementById("heat").style.display = "none";
                }

                if (data.ModeDryOption == true) {

                    if (AirCondtionerJson.ModeDryStatus == true) {
                        document.getElementById("modeDry").src = "Images/IRBlaster/AirConditioner/dry_icon_blue.png";
                    } else {
                        document.getElementById("modeDry").src = "Images/IRBlaster/AirConditioner/dry_icon_grey.png";
                    }
                    document.getElementById("dry").style.display = "block";

                } else {
                    document.getElementById("dry").style.display = "none";
                }

                if (data.ModeFanOption == true) {

                    if (AirCondtionerJson.ModeFanStatus == true) {
                        document.getElementById("modeFan").src = "Images/IRBlaster/AirConditioner/fan_icon_blue.png";
                    } else {
                        document.getElementById("modeFan").src = "Images/IRBlaster/AirConditioner/fan_icon_grey.png";
                    }
                    document.getElementById("fan").style.display = "block";

                } else {
                    document.getElementById("fan").style.display = "none";
                }

                if (data.FanAutoOption == true) {

                    var temp = AirCondtionerJson.FanSpeedStatus;;
                    if (temp == 6) {
                        document.getElementById("fanlimit").src = "Images/IRBlaster/AirConditioner/icon_fanspeed_auto_v04.png";
                    } else {
                        document.getElementById("fanlimit").src = "Images/IRBlaster/AirConditioner/icon_fanspeed_" + temp + "_v04.png";
                    }
                    document.getElementById("fanspeed").style.display = "block";

                } else {
                    document.getElementById("fanspeed").style.display = "none";
                }

                if (data.UpDownSwingAutoOption == true) {

                    if (data.UpDownSwingType == 1) {
                        if (AirCondtionerJson.UpDownSwingStatus == 0) {
                            document.getElementById("swingUD").src = "Images/IRBlaster/AirConditioner/icon_swing_horizontal_v3_grey.png";
                        } else {
                            document.getElementById("swingUD").src = "Images/IRBlaster/AirConditioner/icon_swing_horizontal_v3_blue.png";
                        }
                    } else {
                        document.getElementById("swingUD").src = "Images/IRBlaster/AirConditioner/icon_swing_horizontal_v3_blue.png";
                    }
                    document.getElementById("swingvertical").style.display = "block";

                } else {
                    document.getElementById("swingvertical").style.display = "none";
                }

                if (data.LeftRightSwingAutoOption == true) {

                    if (data.LeftRightSwingType == 1) {
                        if (AirCondtionerJson.UpDownSwingStatus == 0) {
                            document.getElementById("swingLR").src = "Images/IRBlaster/AirConditioner/icon_swing_vertical_v3_grey.png";
                        } else {
                            document.getElementById("swingLR").src = "Images/IRBlaster/AirConditioner/icon_swing_vertical_v3_blue.png";
                        }
                    } else {
                        document.getElementById("swingLR").src = "Images/IRBlaster/AirConditioner/icon_swing_vertical_v3_blue.png";
                    }
                    document.getElementById("swinghorizontal").style.display = "block";

                } else {
                    document.getElementById("swinghorizontal").style.display = "none";
                }

                if (data.PowerOption == "ON/OFF") {

                    if (AirCondtionerJson.Power == true) {
                        document.getElementById("powerOn").src = "Images/IRBlaster/AirConditioner/icons_on.png";
                        document.getElementById("powerOff").src = "Images/IRBlaster/AirConditioner/icons_off_Grey.png";
                    } else {
                        document.getElementById("powerOn").src = "Images/IRBlaster/AirConditioner/icons_on_Grey.png";
                        document.getElementById("powerOff").src = "Images/IRBlaster/AirConditioner/icons_off.png";
                    }
                    document.getElementById("on").style.display = "block";
                    document.getElementById("off").style.display = "block";

                } else {
                    document.getElementById("on").style.display = "none";
                    document.getElementById("off").style.display = "none";
                }

                if (data.PowerOption == "TOGGLE") {
                    document.getElementById("powerToggle").src = "Images/IRBlaster/AirConditioner/icons_toggle.png";
                    document.getElementById("toggle").style.display = "block";
                } else {
                    document.getElementById("toggle").style.display = "none";
                }

                var info = IRBlasterID + " " + DeviceId;
                document.getElementById("modeAuto").onclick = function() {
                    var mode = "auto";
                    changeMode(mode, info)
                };
                document.getElementById("modeCool").onclick = function() {
                    var mode = "cool";
                    changeMode(mode, info)
                };
                document.getElementById("modeHeat").onclick = function() {
                    var mode = "heat";
                    changeMode(mode, info)
                };
                document.getElementById("modeDry").onclick = function() {
                    var mode = "dry";
                    changeMode(mode, info)
                };
                document.getElementById("modeFan").onclick = function() {
                    var mode = "fan";
                    changeMode(mode, info)
                };
                document.getElementById("increase").onclick = function() {
                    increaseValue(data.TempMax, info)
                };
                document.getElementById("decrease").onclick = function() {
                    decreaseValue(data.TempMin, info)
                };
                document.getElementById("fanlimit").onclick = function() {
                    changeFanSpeed(data.FanLimit, info)
                };
                document.getElementById("swingLR").onclick = function() {
                    swingHorizontal(data.LeftRightSwingType, info)
                };
                document.getElementById("swingUD").onclick = function() {
                    swingVertical(data.UpDownSwingType, info)
                };
                document.getElementById("powerOn").onclick = function() {
                    powerOption(data.PowerOption, 1, info)
                };
                document.getElementById("powerOff").onclick = function() {
                    powerOption(data.PowerOption, 0, info)
                };
                document.getElementById("powerToggle").onclick = function() {
                    powerOption(data.PowerOption, 1, info)
                };
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Failed',
                    text: 'Fetch data failed'
                })
                loader.style.display = "none";
            }
        });
    }

    function openTv() {
        document.getElementById("addDeviceForm").style.display = "none";
        var myPopupTvTypeAndBrand = document.getElementById("myPopupTvTypeAndBrand");
        if (myPopupTvTypeAndBrand) {
            myPopupTvTypeAndBrand.style.display = "block";
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Error while retrieving type and brand for TV'
            })
        }
    }

    function openModelTv(Brand) {
        document.getElementsByClassName("Brand").value = Brand;
        document.getElementById("myPopupTvTypeAndBrand").style.display = "none";
        var myPopupTvModel = document.getElementById("myPopupTvModel" + Brand);
        if (myPopupTvModel) {
            myPopupTvModel.style.display = "block";
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Error while retrieving model for TV'
            })
        }
    }

    $(document).ready(function() {
        $('.TvModelList').click(function() {

            //Get value user click from table
            var userClick = $(this).text();

            var tvModel = document.getElementsByName("ModelTv");
            var tvBrand = document.getElementsByClassName("Brand").value;
            var IRBlasterID = document.getElementsByClassName("IRBlasterId").value;
            var IRBlasterInfo = document.getElementsByClassName("IRBlasterInfo").value;
            var TypeId = 2;

            var countModel = 0;
            var index = 0;

            tvModel.forEach(function(model) {
                if (model.value == userClick) {
                    index = countModel;
                }
                countModel++;
            });
            var fields = tvModel[index].value.split(" ");
            var hrefGetIRBlasterDeviceLimit = baseURL + "/irBlasterDeviceLimit.php/?IRBlasterId=" + IRBlasterID + "&TypeId=" + TypeId + "&BrandName=" + tvBrand + "&ModelId=" + fields[1];
            var loader = document.getElementById("fetchingTv");
            var tvModelId = fields[1];
            loader.style.display = "block";
            $.getJSON(hrefGetIRBlasterDeviceLimit, function(data) {
                //data is the JSON string
                if (data.Command == 'GetIRBlasterDeviceLimit' && data.Status == true) {
                    loader.style.display = "none";
                    document.getElementsByClassName("ModelNo").value = tvModelId;
                    document.getElementById("myPopupTvModel" + tvBrand).style.display = "none";
                    document.getElementById("TvForm" + tvBrand).style.display = "block";

                } else {
                    loader.style.display = "none";
                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: data.Message
                    })
                }

            });
        });
    });

    function TestTVButton(ButtonPress) {
        var IRBlasterID = document.getElementsByClassName("IRBlasterId").value;
        var hrefTestTVButton = baseURL + "/irBlasterTestTVButton.php/?IRBlasterId=" + IRBlasterID + "&ButtonPress=" + ButtonPress;
        var loader = document.getElementById("loader");

        console.log(hrefTestTVButton);
        loader.style.display = "block";
        $.getJSON(hrefTestTVButton, function(data) {
            console.log(data);
            //data is the JSON string
            loader.style.display = "none";
            if (data.Command == 'TestTVButton' && data.Status == false) {
                Swal.fire({
                    icon: 'error',
                    title: 'Failed',
                    text: data.Message
                })
            } else {
                iziToast.show({
                    title: 'TEST TV',
                    message: 'Your action is successful',
                    theme: 'dark',
                    position: 'bottomCenter',
                    icon: 'icon-person'
                });
            }
        });
    }

    function selectTv(Brand) {
        document.getElementById("TvForm" + Brand).style.display = "none";
        document.getElementById("tvDeviceForm").style.display = "block";
    }

    function addTv() {
        var IRBlasterID = document.getElementsByClassName("IRBlasterId").value;
        var BrandName = document.getElementsByClassName("Brand").value;
        var ModelId = document.getElementsByClassName("ModelNo").value;
        var DeviceName = document.getElementById("deviceTvName").value;
        var TypeId = 2;
        var IRBlasterInfo = document.getElementsByClassName("IRBlasterInfo").value;
        var IRBlasterJson = JSON.parse(IRBlasterInfo);

        var hrefAddDevice = baseURL + "/irBlasterAddTvDevice.php/?IRBlasterId=" + IRBlasterID + "&TypeId=" + TypeId + "&BrandName=" + BrandName + "&ModelId=" + ModelId + "&DeviceName=" + DeviceName;
        var loader = document.getElementById("loader");

        var boolSame = false;
        var sameName = '';
        for (i = 0; i < IRBlasterJson.AirConditionerList.length; i++) {
            if (DeviceName == IRBlasterJson.AirConditionerList[i].DeviceName) {
                boolSame = true;
                sameName = IRBlasterJson.AirConditionerList[i].DeviceName;
                break;
            }
        }

        for (i = 0; i < IRBlasterJson.RemoteList.length; i++) {
            if (DeviceName == IRBlasterJson.RemoteList[i].DeviceName) {
                boolSame = true;
                sameName = IRBlasterJson.RemoteList[i].DeviceName;
                break;
            }
        }

        var boolEmpty = false;
        if (DeviceName.length == 0) {
            boolEmpty = true;
        }

        if (!boolSame && !boolEmpty) {
            loader.style.display = "block";
            $.getJSON(hrefAddDevice, function(data) {
                //data is the JSON string
                if (data.Command == 'AddDeviceToIRBlasterNew' && data.Status == true) {
                    loader.style.display = "none";
                    Swal.fire(
                        'Good job!',
                        'New TV has been added successfully',
                        'success'
                    )
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
                } else {
                    loader.style.display = "none";
                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: data.Message
                    })
                }
            });
        } else if (boolSame) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: sameName + ' already exist IRBlaster'
            })
        } else if (boolEmpty) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Name cannot be empty'
            })
        }

    }

    function openTvForm(RemoteInfo) {
        var RemoteJson = JSON.parse(RemoteInfo);
        var DeviceId = RemoteJson.DeviceId;
        var IRBlasterID = RemoteJson.IRBlasterId;
        var TypeID = RemoteJson.TypeId;
        var ModelId = RemoteJson.ModelId;
        var DeviceName = RemoteJson.DeviceName;

        document.getElementById("myPopupAction" + IRBlasterID).style.display = "none";
        document.getElementById("TvFormSet").style.display = "block";
        document.getElementById("tvName").innerHTML = DeviceName;
        document.getElementsByClassName("IRBlasterId").value = IRBlasterID;
        document.getElementsByClassName("DeviceId").value = DeviceId;
        document.getElementsByClassName("TypeId").value = TypeID;

        var info = IRBlasterID + " " + TypeID + " " + DeviceId;
        document.getElementById("remoteMute").onclick = function() {
            tvPressButton(info, 21)
        };
        document.getElementById("remotePower").onclick = function() {
            tvPressButton(info, 16)
        };
        document.getElementById("remoteMenu").onclick = function() {
            tvPressButton(info, 34)
        };
        document.getElementById("remoteScreen").onclick = function() {
            tvPressButton(info, 32)
        };
        document.getElementById("remoteSwaps").onclick = function() {
            tvPressButton(info, 22)
        };
        document.getElementById("remoteVolIncrease").onclick = function() {
            tvPressButton(info, 19)
        };
        document.getElementById("remoteVolDecrease").onclick = function() {
            tvPressButton(info, 20)
        };
        document.getElementById("remoteLeft").onclick = function() {
            tvPressButton(info, 26)
        };
        document.getElementById("remoteUp").onclick = function() {
            tvPressButton(info, 24)
        };
        document.getElementById("remoteOk").onclick = function() {
            tvPressButton(info, 23)
        };
        document.getElementById("remoteDown").onclick = function() {
            tvPressButton(info, 25)
        };
        document.getElementById("remoteRight").onclick = function() {
            tvPressButton(info, 27)
        };
        document.getElementById("remoteChIncrease").onclick = function() {
            tvPressButton(info, 17)
        };
        document.getElementById("remoteChDecrease").onclick = function() {
            tvPressButton(info, 18)
        };
        document.getElementById("remoteDash").onclick = function() {
            tvPressButton(info, 35)
        };
        document.getElementById("remoteTvAv").onclick = function() {
            tvPressButton(info, 33)
        };
        document.getElementById("remoteKeyBoard").onclick = function() {
            document.getElementById("keyBoardTv").style.display = "block";
        };
    }

    function PressKeyBoard(ButtonPress) {

        var IRBlasterID = document.getElementsByClassName("IRBlasterId").value;
        var DeviceId = document.getElementsByClassName("DeviceId").value;
        var TypeID = document.getElementsByClassName("TypeId").value;

        var hrefButtonPressTV = baseURL + "/irBlasterButtonPressTV.php/?IRBlasterId=" + IRBlasterID + "&TypeId=" + TypeID + "&DeviceId=" + DeviceId + "&ButtonPress=" + ButtonPress;
        console.log(hrefButtonPressTV);
        loader.style.display = "block";
        $.getJSON(hrefButtonPressTV, function(data) {
            console.log(data);
            //data is the JSON string
            loader.style.display = "none";
            if (data.Command == 'ButtonPressTV' && data.Status == false) {
                Swal.fire({
                    icon: 'error',
                    title: 'Failed',
                    text: data.Message
                })
            } else {
                if (TypeID == 2) {
                    iziToast.show({
                        title: 'SET TV',
                        message: 'Your action is successful',
                        theme: 'dark',
                        position: 'bottomCenter',
                        icon: 'icon-person'
                    });
                } else {
                    iziToast.show({
                        title: 'SET ASTRO',
                        message: 'Your action is successful',
                        theme: 'dark',
                        position: 'bottomCenter',
                        icon: 'icon-person'
                    });
                }

            }
        });
    }

    function tvPressButton(info, ButtonPress) {
        var fields = info.split(" ");
        var IRBlasterID = fields[0];
        var TypeID = fields[1];
        var DeviceId = fields[2];

        var hrefButtonPressTV = baseURL + "/irBlasterButtonPressTV.php/?IRBlasterId=" + IRBlasterID + "&TypeId=" + TypeID + "&DeviceId=" + DeviceId + "&ButtonPress=" + ButtonPress;
        console.log(hrefButtonPressTV);
        loader.style.display = "block";
        $.getJSON(hrefButtonPressTV, function(data) {
            //data is the JSON string
            loader.style.display = "none";
            if (data.Command == 'ButtonPressTV' && data.Status == false) {
                Swal.fire({
                    icon: 'error',
                    title: 'Failed',
                    text: data.Message
                })
            } else {
                iziToast.show({
                    title: 'SET TV',
                    message: 'Your action is successful',
                    theme: 'dark',
                    position: 'bottomCenter',
                    icon: 'icon-person'
                });
            }
        });
    }

    function openAstro() {
        var IRBlasterInfo = document.getElementsByClassName("IRBlasterInfo").value;
        var IRBlasterID = document.getElementsByClassName("IRBlasterId").value;
        var IRBlasterJson = JSON.parse(IRBlasterInfo);

        if (IRBlasterJson.IRBlasterId == IRBlasterID) {

            var boolAstro = false
            for (i = 0; i < IRBlasterJson.RemoteList.length; i++) {
                if (IRBlasterJson.RemoteList[i].TypeId == 3) {
                    boolAstro = true;
                    break;
                }
            }

            if (!boolAstro) {
                document.getElementById("remoteDeviceForm").style.display = "block";
                document.getElementById("addDeviceForm").style.display = "none";
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Failed',
                    text: 'Astro remote already exist in IRBlaster'
                })
            }

        }

    }

    function addAstro() {
        var IRBlasterID = document.getElementsByClassName("IRBlasterId").value;
        var DeviceName = document.getElementById("deviceRemoteName").value;
        var TypeId = 3;
        var BrandName = 'Astro';
        var ModelId = 1;
        var IRBlasterInfo = document.getElementsByClassName("IRBlasterInfo").value;
        var IRBlasterJson = JSON.parse(IRBlasterInfo);

        var hrefAddDevice = baseURL + "/irBlasterDeviceLimit.php/?IRBlasterId=" + IRBlasterID + "&TypeId=" + TypeId + "&BrandName=" + BrandName + "&ModelId=" + ModelId;
        var loader = document.getElementById("fetchingAstro");

        var boolSame = false;
        var sameName = '';
        for (i = 0; i < IRBlasterJson.AirConditionerList.length; i++) {
            if (DeviceName == IRBlasterJson.AirConditionerList[i].DeviceName) {
                boolSame = true;
                sameName = IRBlasterJson.AirConditionerList[i].DeviceName;
                break;
            }
        }

        for (i = 0; i < IRBlasterJson.RemoteList.length; i++) {
            if (DeviceName == IRBlasterJson.RemoteList[i].DeviceName) {
                boolSame = true;
                sameName = IRBlasterJson.RemoteList[i].DeviceName;
                break;
            }
        }

        var boolEmpty = false;
        if (DeviceName.length == 0) {
            boolEmpty = true;
        }

        if (!boolSame && !boolEmpty) {
            loader.style.display = "block";
            $.getJSON(hrefAddDevice, function(data) {
                //data is the JSON string
                if (data.Command == 'GetIRBlasterDeviceLimit' && data.Status == true) {
                    var url = baseURL + "/irBlasterAddTvDevice.php/?IRBlasterId=" + IRBlasterID + "&TypeId=" + TypeId + "&BrandName=" + BrandName + "&ModelId=" + ModelId + "&DeviceName=" + DeviceName;
                    $.getJSON(url, function(info) {
                        //data is the JSON string
                        if (info.Command == 'AddDeviceToIRBlasterNew' && info.Status == true) {
                            loader.style.display = "none";
                            Swal.fire(
                                'Good job!',
                                'New Astro has been added successfully',
                                'success'
                            )
                            setTimeout(function() {
                                location.reload();
                            }, 2000);
                        } else {
                            loader.style.display = "none";
                            Swal.fire({
                                icon: 'error',
                                title: 'Failed',
                                text: info.Message
                            })
                        }

                    });

                } else {
                    loader.style.display = "none";
                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: data.Message
                    })
                }

            });
        } else if (boolSame) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: sameName + ' already exist IRBlaster'
            })
        } else if (boolEmpty) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Name cannot be empty'
            })
        }

    }

    function openAstroForm(AstroInfo) {
        var AstroJson = JSON.parse(AstroInfo);
        var DeviceId = AstroJson.DeviceId;
        var IRBlasterID = AstroJson.IRBlasterId;
        var TypeID = AstroJson.TypeId;
        var ModelId = AstroJson.ModelId;
        var DeviceName = AstroJson.DeviceName;

        document.getElementById("myPopupAction" + IRBlasterID).style.display = "none";
        document.getElementById("AstroFormSet").style.display = "block";
        document.getElementById("astroName").innerHTML = DeviceName;
        document.getElementsByClassName("IRBlasterId").value = IRBlasterID;
        document.getElementsByClassName("DeviceId").value = DeviceId;
        document.getElementsByClassName("TypeId").value = TypeID;

        var info = IRBlasterID + " " + TypeID + " " + DeviceId;
        document.getElementById("astroMute").onclick = function() {
            astroPressButton(info, 21)
        };
        document.getElementById("astroPower").onclick = function() {
            astroPressButton(info, 16)
        };
        document.getElementById("astroHome").onclick = function() {
            astroPressButton(info, 34)
        };
        document.getElementById("astroAstro").onclick = function() {
            astroPressButton(info, 32)
        };
        document.getElementById("astroRadio").onclick = function() {
            astroPressButton(info, 33)
        };
        document.getElementById("astroInfo").onclick = function() {
            astroPressButton(info, 36)
        };
        document.getElementById("astroIII").onclick = function() {
            astroPressButton(info, 37)
        };
        document.getElementById("astroFav").onclick = function() {
            astroPressButton(info, 38)
        };
        document.getElementById("astroRatio").onclick = function() {
            astroPressButton(info, 35)
        };
        document.getElementById("astroVolIncrease").onclick = function() {
            astroPressButton(info, 19)
        };
        document.getElementById("astroVolDecrease").onclick = function() {
            astroPressButton(info, 20)
        };
        document.getElementById("astroLeft").onclick = function() {
            astroPressButton(info, 26)
        };
        document.getElementById("astroUp").onclick = function() {
            astroPressButton(info, 24)
        };
        document.getElementById("astroOk").onclick = function() {
            astroPressButton(info, 23)
        };
        document.getElementById("astroDown").onclick = function() {
            astroPressButton(info, 25)
        };
        document.getElementById("astroRight").onclick = function() {
            astroPressButton(info, 27)
        };
        document.getElementById("astroChIncrease").onclick = function() {
            astroPressButton(info, 17)
        };
        document.getElementById("astroChDecrease").onclick = function() {
            astroPressButton(info, 18)
        };
        document.getElementById("astroBack").onclick = function() {
            astroPressButton(info, 22)
        };
        document.getElementById("astroR").onclick = function() {
            astroPressButton(info, 40)
        };
        document.getElementById("astroG").onclick = function() {
            astroPressButton(info, 41)
        };
        document.getElementById("astroY").onclick = function() {
            astroPressButton(info, 42)
        };
        document.getElementById("astroB").onclick = function() {
            astroPressButton(info, 43)
        };
        document.getElementById("astroKeyBoard").onclick = function() {
            document.getElementById("keyBoardAstro").style.display = "block";
        };
    }

    function astroPressButton(info, ButtonPress) {
        var fields = info.split(" ");
        var IRBlasterID = fields[0];
        var TypeID = fields[1];
        var DeviceId = fields[2];

        var hrefButtonPressAstro = baseURL + "/irBlasterButtonPressAstro.php/?IRBlasterId=" + IRBlasterID + "&TypeId=" + TypeID + "&DeviceId=" + DeviceId + "&ButtonPress=" + ButtonPress;
        console.log(hrefButtonPressAstro);
        loader.style.display = "block";
        $.getJSON(hrefButtonPressAstro, function(data) {
            console.log(data);
            //data is the JSON string
            loader.style.display = "none";
            if (data.Command == 'ButtonPressTV' && data.Status == false) {
                Swal.fire({
                    icon: 'error',
                    title: 'Failed',
                    text: data.Message
                })
            } else {
                iziToast.show({
                    title: 'SET ASTRO',
                    message: 'Your action is successful',
                    theme: 'dark',
                    position: 'bottomCenter',
                    icon: 'icon-person'
                });
            }
        });
    }

    function openRSSI(Rssi) {
        iziToast.show({
            title: 'RSSI RESULT',
            message: 'The RSSI value for the switch is :' + Rssi,
            theme: 'dark',
            position: 'topCenter',
            icon: 'icon-person'
        });
    }

    function closeForm(id) {
        document.getElementById("myPopupAction" + id).style.display = "none";
    }

    function back() {
        var serial = document.getElementsByClassName("Serial").value;
        var id = document.getElementsByClassName("IRBlasterId").value;
        document.getElementById("renameDeviceForm").style.display = "none";
        document.getElementById("addDeviceForm").style.display = "none";
        document.getElementById("TvFormSet").style.display = "none";
        document.getElementById("AstroFormSet").style.display = "none";
        document.getElementById("myPopupAction" + id).style.display = "block";
    }

    function backAstroDevice() {
        document.getElementById("remoteDeviceForm").style.display = "none";
        document.getElementById("addDeviceForm").style.display = "block";
    }

    function backBrandAirCon() {
        document.getElementById("myPopupAirCondTypeAndBrand").style.display = "none";
        document.getElementById("addDeviceForm").style.display = "block";
    }

    function backAirConditionerModel(BrandId) {
        document.getElementById("myPopupAirCondModel" + BrandId).style.display = "none";
        document.getElementById("myPopupAirCondTypeAndBrand").style.display = "block";
    }

    function backAirConditioner(ModelId, BrandId) {
        document.getElementById("AirConditionerForm" + ModelId + BrandId).style.display = "none"
        document.getElementById("myPopupAirCondModel" + BrandId).style.display = "block";
    }

    function backAirConditionerDevice() {
        var ModelId = document.getElementsByClassName("ModelId").value;
        var BrandId = document.getElementsByClassName("BrandId").value;

        document.getElementById("airconditionerDeviceForm").style.display = "none";
        document.getElementById("AirConditionerForm" + ModelId + BrandId).style.display = "block"
    }

    function backBrandTv() {
        document.getElementById("myPopupTvTypeAndBrand").style.display = "none"
        document.getElementById("addDeviceForm").style.display = "block";
    }

    function backTvModel(Brand) {
        document.getElementById("myPopupTvModel" + Brand).style.display = "none";
        document.getElementById("myPopupTvTypeAndBrand").style.display = "block";
    }

    function backTv(Brand) {
        document.getElementById("TvForm" + Brand).style.display = "none";
        document.getElementById("myPopupTvModel" + Brand).style.display = "block";
    }

    function backTvDevice() {
        var BrandName = document.getElementsByClassName("Brand").value;
        document.getElementById("tvDeviceForm").style.display = "none";
        document.getElementById("TvForm" + BrandName).style.display = "block";
    }

    function backKeyboard() {
        document.getElementById("keyBoardTv").style.display = "none";
        document.getElementById("keyBoardAstro").style.display = "none";
    }

    function cancelAircon() {
        var BrandId = document.getElementsByClassName("BrandId").value;
        var loader = document.getElementById("fetchingAircon");
        loader.style.display = "none";
        document.getElementById("myPopupAirCondModel" + BrandId).style.display = "block";
    }

    function cancelTv() {
        var Brand = document.getElementsByClassName("Brand").value;
        var loader = document.getElementById("fetchingTv");
        loader.style.display = "none";
        document.getElementById("myPopupTvModel" + Brand).style.display = "block";
    }

    function cancelAstro() {
        var loader = document.getElementById("fetchingAstro");
        loader.style.display = "none";
        document.getElementById("addDeviceForm").style.display = "block";
    }

    function refresh() {
        location.reload();
    }

    //For Show/Hide SN
    $(document).ready(function() {
        $("#showSn").click(function() {

            if ($(this).prop("checked") == true) {
                $(".gatewaySNText").fadeIn("fast", "swing");
                localStorage.setItem("displayGatewaySN", $(this).val());
            } else {
                $(".gatewaySNText").fadeOut("fast", "swing");
                localStorage.setItem("displayGatewaySN", "hideSn");
            }
        });
    });

    $(function() {
        var displayGatewaySN = localStorage.getItem("displayGatewaySN");
        if (displayGatewaySN == null) {
            displayGatewaySN = 'hideSn'
        }

        if (displayGatewaySN == 'showSn') {
            $('#showSn').prop('checked', true);
            $(".gatewaySNText").css("display", "block");
        } else {
            $('#showSn').prop('checked', false);
            $(".gatewaySNText").css("display", "none");
        }
    });

    //For table
    $(document).ready(function() {
        $('#myTable').DataTable();
    });

    //For location
    $(document).ready(function() {
        $('.LocationListItem').click(function() {
            // document.getElementById("LocationListForm" + $(this).text()).submit();
            var table = $('#myTable').DataTable();
            var value = document.getElementById("Location" + $(this).text()).value;
            document.getElementsByClassName("LocationName").value = value;
            table.draw();

            if ($(".data-location").contents().length > 0) {
                $(".data-location").remove();
            }
            $("#collapseFilter").
            append(
                '<div class="data-tag data-location" id="tag' + value + '">' +
                '<span class="data-tag-text" style="margin:0px 5px;">' + value + '</span>' +
                '<span onclick="removeFilter(&#39;' + value + '&#39;)"><i class="fas fa-times"></i></span>' +
                '</div>'
            );
            document.getElementById("collapseFilter").style.display = 'block';
            // window.alert($(this).text());
        });
    });

    function removeFilter(id) {
        var tagId = document.getElementById("tag" + id);
        var table = $('#myTable').DataTable();

        document.getElementsByClassName("LocationName").value = 'Null';

        tagId.remove();
        table.draw();

        if ($(".data-tag").contents().length == 0) {
            document.getElementById("collapseFilter").style.display = "none";
        }
    }

    //For search based on location
    $.fn.dataTable.ext.search.push(
        function(settings, data, dataIndex) {
            var LocationName = document.getElementsByClassName("LocationName").value;
            if (LocationName == null) {
                LocationName = 'Null';
            }
            var Location = data[2];

            if (LocationName == Location || LocationName == 'Null') {
                return true;
            }

            return false;

        }
    );
</script>

<body>
    <?php
    $SerialGateway = $_SESSION['serialGateway'];
    $Password = $_SESSION['password'];
    $LocationListArray = $_SESSION['location'];
    $SmartSwitchStatus = $_SESSION['SmartSwitchStatus'];
    $CurtainStatus = $_SESSION['CurtainStatus'];
    $AlarmStatus = $_SESSION['AlarmStatus'];
    $ShutterStatus = $_SESSION['ShutterStatus'];
    $IRBlasterStatus = $_SESSION['IRBlasterStatus'];
    $LockStatus = $_SESSION['LockStatus'];
    $TotalDevice = $_SESSION['TotalDevice'];
    $SceneCount = $_SESSION['SceneCount'];
    $AdminPassword = $_SESSION['AdminPassword'];

    echo "<input style='display:none;' id='SerialGateway' value='$SerialGateway'>";
    echo "<input style='display:none;' id='Password' value='$Password'>";
    echo "<input style='display:none;' id='AdminPassword' value='$AdminPassword'>";



    if (isset($_POST['Logout'])) {
        header("Location: ChooseGateway.php");
        header("Refresh:0");
        ob_flush();
        session_destroy();
        exit();
    }

    if (isset($_POST['Home'])) {
        // header("Refresh:0");
        header("Location: Dashboard.php");
        ob_flush();
        exit();
    }

    if (isset($_POST['close'])) {
        header("Refresh:0");
        ob_flush();
        exit();
    }


    // Get the contents of Total Tv JSON file 
    $url = "http://www.senzo.com.my/downloads/IRBlaster/TV/TotalTv.json";
    $strJsonFileContents = file_get_contents($url);
    // var_dump($strJsonFileContents); // show contents
    $GetRemoteBrandListJsonResponseObject = json_decode($strJsonFileContents);

    //-----------------------------------------------------------Select Brand Form Tv--------------------------------------------------------------------
    echo "<div class='form-popup' id='myPopupTvTypeAndBrand'>
    <div class='form-container SameSize' style='width:660px;padding:0px 50px;'>
    <div class='modal-header' style='width:100%;height:60px;'>
    <a class='back' onclick='backBrandTv()'><span class='fa fa-arrow-left fa-2x'></a>
    <h3 class='h2form'>Select Brand</h3>
    </div>";
    echo "<div class='modal-body' style='width:100%;max-height: calc(80vh - 210px);overflow-y: auto;'>";
    // echo "<div class='table-wrapper-scroll-y my-custom-scrollbar'>";
    echo "<table class='table table-bordered table-hover' style='width:100%'>";
    echo "<thead class='thead-dark'>";
    echo "<tr>";
    echo "<th scope='col'>Brand Name</th>";
    echo "</tr>";
    echo "</thead>";
    foreach ($GetRemoteBrandListJsonResponseObject as $RemoteBrandList) {
        echo "<tbody>";
        echo "<tr class='table-light'>";
        if ($RemoteBrandList->Brand == "Sony") {
            echo "<td><input type='image' src='Images/IRBlaster/Brand/Sony.svg' alt='off' onclick='openModelTv(&#39;$RemoteBrandList->Brand&#39;)' style='width:200px;height:100px;'></td>";
        } else if ($RemoteBrandList->Brand == "Sharp") {
            echo "<td><input type='image' src='Images/IRBlaster/Brand/Sharp.svg' alt='off' onclick='openModelTv(&#39;$RemoteBrandList->Brand&#39;)' style='width:120px;height:100px;'></td>";
        } else if ($RemoteBrandList->Brand == "Lg") {
            echo "<td><input type='image' src='Images/IRBlaster/Brand/LG.svg' alt='off' onclick='openModelTv(&#39;$RemoteBrandList->Brand&#39;)' style='width:120px;height:100px;'></td>";
        } else if ($RemoteBrandList->Brand == "Samsung") {
            echo "<td><input type='image' src='Images/IRBlaster/Brand/Samsung.svg' alt='off' onclick='openModelTv(&#39;$RemoteBrandList->Brand&#39;)' style='width:120px;height:100px;'></td>";
        } else if ($RemoteBrandList->Brand == "Panasonic") {
            echo "<td><input type='image' src='Images/IRBlaster/Brand/Panasonic.svg' alt='off' onclick='openModelTv(&#39;$RemoteBrandList->Brand&#39;)' style='width:200px;height:100px;'></td>";
        } else if ($RemoteBrandList->Brand == "Toshiba") {
            echo "<td><input type='image' src='Images/IRBlaster/Brand/Toshiba.svg' alt='off' onclick='openModelTv(&#39;$RemoteBrandList->Brand&#39;)' style='width:120px;height:100px;'></td>";
        } else if ($RemoteBrandList->Brand == "Philips") {
            echo "<td><input type='image' src='Images/IRBlaster/Brand/Philips.svg' alt='off' onclick='openModelTv(&#39;$RemoteBrandList->Brand&#39;)' style='width:200px;height:100px;'></td>";
        } else if ($RemoteBrandList->Brand == "Haier") {
            echo "<td><input type='image' src='Images/IRBlaster/Brand/Haier.svg' alt='off' onclick='openModelTv(&#39;$RemoteBrandList->Brand&#39;)' style='width:120px;height:100px;'></td>";
        } else if ($RemoteBrandList->Brand == "Hisense") {
            echo "<td><input type='image' src='Images/IRBlaster/Brand/Hisense.svg' alt='off' onclick='openModelTv(&#39;$RemoteBrandList->Brand&#39;)' style='width:120px;height:100px;'></td>";
        } else if ($RemoteBrandList->Brand == "Hitachi") {
            echo "<td><input type='image' src='Images/IRBlaster/Brand/Hitachi.svg' alt='off' onclick='openModelTv(&#39;$RemoteBrandList->Brand&#39;)' style='width:120px;height:100px;'></td>";
        } else if ($RemoteBrandList->Brand == "JVC") {
            echo "<td><input type='image' src='Images/IRBlaster/Brand/JVC.svg' alt='off' onclick='openModelTv(&#39;$RemoteBrandList->Brand&#39;)' style='width:120px;height:100px;'></td>";
        }
        echo "</tr>";
        echo "</tbody>";
    }
    echo "</table>";
    echo "</div>";
    echo "</div>";
    echo "</div>";

    //-----------------------------------------------------------Select Model Form Tv--------------------------------------------------------------------
    foreach ($GetRemoteBrandListJsonResponseObject as $RemoteBrandList) {
        echo "<div class='form-popup' id='myPopupTvModel$RemoteBrandList->Brand'>
            <div class='form-container SameSize' style='width:660px;padding:0px 50px;'>
            <div class='modal-header' style='width:100%;height:60px;'>
            <a class='back' onclick='backTvModel(&#39;$RemoteBrandList->Brand&#39;)'><span class='fa fa-arrow-left fa-2x'></a>
            <h3 class='h2form'>Select Model</h3>
            </div>";
        echo "<div class='modal-body' style='width:100%;max-height: calc(80vh - 210px);overflow-y: auto;'>";
        // echo "<div class='table-wrapper-scroll-y my-custom-scrollbar'>";
        echo "<table class='table table-bordered table-hover' style='width:100%'>";
        echo "<thead class='thead-dark'>";
        echo "<tr>";
        echo "<th scope='col'>Model</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        for ($i = 1; $i <= $RemoteBrandList->ModelNo; $i++) {
            echo "<tr class='table-light'>";
            echo "<input style='display:none;' value='Model $i' name='ModelTv'>";
            echo "<td class='TvModelList' style='cursor:pointer;'>Model $i</td>";
            echo "</tr>";
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";
        echo "</div>";
        echo "</div>";
    }

    //-----------------------------------------------------------Tv Model Form--------------------------------------------------------------------
    foreach ($GetRemoteBrandListJsonResponseObject as $RemoteBrandList) {
        echo "<div class='form-popup' id='TvForm$RemoteBrandList->Brand'>
                    <div class='form-container SameSize' style='width:660px;padding:0px 50px;'>
                        <div class='modal-header' style='width:100%;height:60px;'>
                            <a class='back' onclick='backTv(&#39;$RemoteBrandList->Brand&#39;)'><span class='fa fa-arrow-left fa-2x'></a>
                            <h2 class='h2form'>$RemoteBrandList->Brand Remote Test</h2>
                        </div>
                        <div class='modal-body' style='width:100%;'>
                        <p class='h2form'>Test the buttons from available remote models. 
                        <br>Make sure that IR Blaster is within range of the TV</p>
                        <div class = 'remoteTv'>
                            <div class='grid-container remote'>
                                <div>
                                    <div class='container'>
                                        <button class='buttonIncrease' onclick='TestTVButton(19)'><i style='color: #15aabf !important;' class='fas fa-plus fa'></i></button>
                                        <h3 class='text'>Vol</h3>
                                        <button class='buttonDecrease' onclick='TestTVButton(20)'><i style='color: #15aabf !important;' class='fas fa-minus fa'></i></button>
                                    </div>
                                </div>
                                <div><input style='width:50px;height:50px;'  type='image' src='Images/IRBlaster/RemoteButtons/power_remote.svg' alt='power' onclick='TestTVButton(16)'></div>
                                <div>
                                    <div class='container'>
                                        <button class='buttonIncrease' onclick='TestTVButton(17)'><i style='color: #15aabf !important;' class='fas fa-plus fa'></i></button>
                                        <h3 class='text'>Ch</h3>
                                        <button class='buttonDecrease' onclick='TestTVButton(18)'><i style='color: #15aabf !important;' class='fas fa-minus fa'></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class='modal-footer' style='width:100%;text-align:-webkit-center;'>
                        <button type='button' class='all' style='width:49%;background:#4cd137 !important;' onclick='selectTv(&#39;$RemoteBrandList->Brand&#39;)'>Use</button>
                        </div>
                    </div>
                </div>";
    }

    $opts = array('http' => array('header' => 'Cookie: ' . $_SERVER['HTTP_COOKIE'] . "\r\n"));
    $context = stream_context_create($opts);
    session_write_close(); // unlock the file
    $url = $baseURL . "/irBlasterSmartIRBlaster.php";
    $SmartIRBlasterString = file_get_contents($url, false, $context);
    $SmartIRBlasterString = explode("&#&", $SmartIRBlasterString);
    $msgJsonIRBlaster = json_decode($SmartIRBlasterString[0]);

    $msgJson = $msgJsonIRBlaster;

    if (!empty($msgJson->Message)) {
        $Error = $msgJson->Message;
    ?>
        <script type="text/javascript">
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: '<?php echo $Error; ?>',
            }).then(function() {
                window.location.href = "ChooseGateway.php";
            });
        </script>
    <?php
        die();
    } else {
        $msgJsonBrand = json_decode($SmartIRBlasterString[1]);
    }

    function getBrand($msgJson)
    {
        if ($msgJson->Command == 'GetIRBlasterTypeAndBrand') {

            if (!empty($msgJson->IRBlasterTypeAndBrandList)) {
                $IRBlasterTypeAndBrandListArray = $msgJson->IRBlasterTypeAndBrandList;

                //-----------------------------------------------------------Select Brand Form AirConditioner--------------------------------------------------------------------
                echo "<div class='form-popup' id='myPopupAirCondTypeAndBrand'>
                        <div class='form-container SameSize' style='width:660px;padding:0px 50px;'>
                        <div class='modal-header' style='width:100%;height:60px;'>
                        <a class='back' onclick='backBrandAirCon()'><span class='fa fa-arrow-left fa-2x'></a>
                        <h3 class='h2form'>Select Brand</h3>
                        </div>";
                echo "<div class='modal-body' style='width:100%;max-height: calc(80vh - 210px);overflow-y: auto;'>";
                // echo "<div class='table-wrapper-scroll-y my-custom-scrollbar'>";
                echo "<table class='table table-bordered table-hover' style='width:100%'>";
                echo "<thead class='thead-dark'>";
                echo "<tr>";
                echo "<th scope='col'>Brand Name</th>";
                echo "</tr>";
                echo "</thead>";
                foreach ($IRBlasterTypeAndBrandListArray as $IRBlasterTypeAndBrandList) {
                    echo "<tbody>";
                    echo "<tr class='table-light'>";
                    if ($IRBlasterTypeAndBrandList->BrandName == "PANASONIC") {
                        echo "<td><input type='image' src='Images/IRBlaster/Brand/Panasonic.svg' alt='off' onclick='openModelAirConditioner(&#39;$IRBlasterTypeAndBrandList->BrandId&#39;)' style='width:200px;height:100px;'></td>";
                    } else if ($IRBlasterTypeAndBrandList->BrandName == "YORK") {
                        echo "<td><input type='image' src='Images/IRBlaster/Brand/York.svg' alt='off' onclick='openModelAirConditioner(&#39;$IRBlasterTypeAndBrandList->BrandId&#39;)' style='width:120px;height:100px;'></td>";
                    } else if ($IRBlasterTypeAndBrandList->BrandName == "ACSON") {
                        echo "<td><input type='image' src='Images/IRBlaster/Brand/Acson.svg' alt='off' onclick='openModelAirConditioner(&#39;$IRBlasterTypeAndBrandList->BrandId&#39;)' style='width:120px;height:100px;'></td>";
                    } else if ($IRBlasterTypeAndBrandList->BrandName == "DAIKIN") {
                        echo "<td><input type='image' src='Images/IRBlaster/Brand/Daikin.svg' alt='off' onclick='openModelAirConditioner(&#39;$IRBlasterTypeAndBrandList->BrandId&#39;)' style='width:120px;height:100px;'></td>";
                    } else if ($IRBlasterTypeAndBrandList->BrandName == "MITSUBISHI") {
                        echo "<td><input type='image' src='Images/IRBlaster/Brand/Mitsubishi.svg' alt='off' onclick='openModelAirConditioner(&#39;$IRBlasterTypeAndBrandList->BrandId&#39;)' style='width:120px;height:100px;'></td>";
                    } else if ($IRBlasterTypeAndBrandList->BrandName == "TOSHIBA") {
                        echo "<td><input type='image' src='Images/IRBlaster/Brand/Toshiba.svg' alt='off' onclick='openModelAirConditioner(&#39;$IRBlasterTypeAndBrandList->BrandId&#39;)' style='width:120px;height:100px;'></td>";
                    } else if ($IRBlasterTypeAndBrandList->BrandName == "ELECTROLUX") {
                        echo "<td><input type='image' src='Images/IRBlaster/Brand/Electrolux.svg' alt='off' onclick='openModelAirConditioner(&#39;$IRBlasterTypeAndBrandList->BrandId&#39;)' style='width:120px;height:100px;'></td>";
                    } else if ($IRBlasterTypeAndBrandList->BrandName == "LG") {
                        echo "<td><input type='image' src='Images/IRBlaster/Brand/LG.svg' alt='off' onclick='openModelAirConditioner(&#39;$IRBlasterTypeAndBrandList->BrandId&#39;)' style='width:120px;height:100px;'></td>";
                    } else if ($IRBlasterTypeAndBrandList->BrandName == "SHARP") {
                        echo "<td><input type='image' src='Images/IRBlaster/Brand/Sharp.svg' alt='off' onclick='openModelAirConditioner(&#39;$IRBlasterTypeAndBrandList->BrandId&#39;)' style='width:120px;height:100px;'></td>";
                    } else if ($IRBlasterTypeAndBrandList->BrandName == "HITACHI") {
                        echo "<td><input type='image' src='Images/IRBlaster/Brand/Hitachi.svg' alt='off' onclick='openModelAirConditioner(&#39;$IRBlasterTypeAndBrandList->BrandId&#39;)' style='width:120px;height:100px;'></td>";
                    } else if ($IRBlasterTypeAndBrandList->BrandName == "SAMSUNG") {
                        echo "<td><input type='image' src='Images/IRBlaster/Brand/Samsung.svg' alt='off' onclick='openModelAirConditioner(&#39;$IRBlasterTypeAndBrandList->BrandId&#39;)' style='width:120px;height:100px;'></td>";
                    }
                    echo "</tr>";
                    echo "</tbody>";
                }
                echo "</table>";
                echo "</div>";
                echo "</div>";
                echo "</div>";


                //-----------------------------------------------------------Select Model Form AirConditioner--------------------------------------------------------------------
                foreach ($IRBlasterTypeAndBrandListArray as $IRBlasterTypeAndBrandList) {
                    echo "<div class='form-popup' id='myPopupAirCondModel$IRBlasterTypeAndBrandList->BrandId'>
                            <div class='form-container SameSize' style='width:660px;padding:0px 50px;'>
                            <div class='modal-header' style='width:100%;height:60px;'>
                            <a class='back' onclick='backAirConditionerModel(&#39;$IRBlasterTypeAndBrandList->BrandId&#39;)'><span class='fa fa-arrow-left fa-2x'></a>
                            <h3 class='h2form'>Select Model</h3>
                            </div>";
                    echo "<div class='modal-body' style='width:100%;max-height: calc(80vh - 210px);overflow-y: auto;'>";
                    // echo "<div class='table-wrapper-scroll-y my-custom-scrollbar'>";
                    echo "<table class='table table-bordered table-hover' style='width:100%'>";
                    echo "<thead class='thead-dark'>";
                    echo "<tr>";
                    echo "<th scope='col'>Model</th>";
                    echo "</tr>";
                    echo "</thead>";
                    echo "<tbody>";
                    for ($i = 1; $i <= $IRBlasterTypeAndBrandList->AirConditioner; $i++) {
                        echo "<tr class='table-light'>";
                        echo "<input style='display:none;' value='Model $i' name='Model'>";
                        echo "<td class='AirConditionerModelList' style='cursor:pointer;'>Model $i</td>";
                        echo "</tr>";
                    }
                    echo "</tbody>";
                    echo "</table>";
                    echo "</div>";
                    echo "</div>";
                    echo "</div>";
                }

                //-----------------------------------------------------------Air Conditioner Model Form--------------------------------------------------------------------
                foreach ($IRBlasterTypeAndBrandListArray as $IRBlasterTypeAndBrandList) {
                    for ($i = 1; $i <= $IRBlasterTypeAndBrandList->AirConditioner; $i++) {
                        echo "<div class='form-popup' id='AirConditionerForm$i$IRBlasterTypeAndBrandList->BrandId'>
                                                <div class='form-container' style='width:550px;padding:0px;'>
                                                    <div class='modal-header' style='width:100%;height:60px;'>
                                                        <a class='back' onclick='backAirConditioner(&#39;$i&#39; , &#39;$IRBlasterTypeAndBrandList->BrandId&#39;)'><span class='fa fa-arrow-left fa-2x'></a>
                                                        <h3 class='h2form'>Model $i $IRBlasterTypeAndBrandList->BrandName</h3>
                                                    </div>
                                                    <div class='modal-body' style='width:100%;'>
                                                        <div class='name' style='border-top:none;'>
                                                            <div style='display:inline-block;'>
                                                                <h4 style='color:#0097e6;'>Test Remote</h4>
                                                            </div>
                                                            <div style='float:right;margin-right:30px;'>
                                                                <input id='signal$i$IRBlasterTypeAndBrandList->BrandId' style='width:40px;height:40px;opacity:0.5;' type='image' src='Images/IRBlaster/AirConditioner/icon_signal.png' alt='signal'>
                                                            </div>
                                                        </div>
                                                        <div class = 'mode'>
                                                            <h4>Mode :</h4>
                                                            <div class='grid-container modeGrid'>
                                                                <div style='display:none;' id='auto$i$IRBlasterTypeAndBrandList->BrandId'><input id='modeAuto$i$IRBlasterTypeAndBrandList->BrandId' style='width:50px;height:50px;'  type='image' src='Images/IRBlaster/AirConditioner/auto_icon_grey.png' alt='auto'></div>
                                                                <div style='display:none;' id='cool$i$IRBlasterTypeAndBrandList->BrandId'><input id='modeCool$i$IRBlasterTypeAndBrandList->BrandId' style='width:50px;height:50px;'  type='image' src='Images/IRBlaster/AirConditioner/cool_icon_blue.png' alt='cool'></div>
                                                                <div style='display:none;' id='heat$i$IRBlasterTypeAndBrandList->BrandId'><input id='modeHeat$i$IRBlasterTypeAndBrandList->BrandId' style='width:50px;height:50px;'  type='image' src='Images/IRBlaster/AirConditioner/heat_icon_grey.png' alt='heat'></div>
                                                                <div style='display:none;' id='dry$i$IRBlasterTypeAndBrandList->BrandId'><input id='modeDry$i$IRBlasterTypeAndBrandList->BrandId' style='width:50px;height:50px;'  type='image' src='Images/IRBlaster/AirConditioner/dry_icon_grey.png' alt='dry'></div>
                                                                <div style='display:none;' id='fan$i$IRBlasterTypeAndBrandList->BrandId'><input id='modeFan$i$IRBlasterTypeAndBrandList->BrandId' style='width:50px;height:50px;'  type='image' src='Images/IRBlaster/AirConditioner/fan_icon_grey.png' alt='fan'></div>
                                                            </div>
                                                        </div>
                                                        <div class = 'tempfanswing'>
                                                            <h4 style='text-align: center;color:#0097e6;'>Temperature</h4>
                                                            <div class='grid-container tempGrid'>
                                                                <div><input id='decrease$i$IRBlasterTypeAndBrandList->BrandId' style='width:50px;height:50px;'  type='image' src='Images/IRBlaster/AirConditioner/icon_temperature_down.png' alt='decrease'></div>
                                                                <div><p id='number$i$IRBlasterTypeAndBrandList->BrandId' class='number' style='color:#0097e6;font-size:40px;'></p><span style='font-size:20px;margin-bottom:40px;color:#0097e6;'>&#8451;</span></div>
                                                                <div><input id='increase$i$IRBlasterTypeAndBrandList->BrandId' style='width:50px;height:50px;'  type='image' src='Images/IRBlaster/AirConditioner/icon_temperature_up.png' alt='increase'></div>
                                                            </div>
                                                            <div class='grid-container fanswingGrid' style='margin-top:30px;'>
                                                                <div style='display:none;' id='fanspeed$i$IRBlasterTypeAndBrandList->BrandId'><input id='fanlimit$i$IRBlasterTypeAndBrandList->BrandId' style='width:80px;height:80px;'  type='image' src='Images/IRBlaster/AirConditioner/icon_fanspeed_1_v04.png' alt='fanspeed' value='1'></div>
                                                                <div style='display:none;' id='swingvertical$i$IRBlasterTypeAndBrandList->BrandId'><input id='swingUD$i$IRBlasterTypeAndBrandList->BrandId' style='width:80px;height:80px;'  type='image' src='Images/IRBlaster/AirConditioner/icon_swing_horizontal_v3_blue.png' alt='swingvertical' value='0'></div>
                                                                <div style='display:none;' id='swinghorizontal$i$IRBlasterTypeAndBrandList->BrandId'><input id='swingLR$i$IRBlasterTypeAndBrandList->BrandId'style='width:80px;height:80px;'  type='image' src='Images/IRBlaster/AirConditioner/icon_swing_vertical_v3_blue.png' alt='swinghorinzontal' value='0'></div>   
                                                            </div>
                                                        </div>
                                                        <div class = 'power'>
                                                            <div class='grid-container powerGrid'>
                                                                <div style='display:none;' id='on$i$IRBlasterTypeAndBrandList->BrandId'><input id='powerOn$i$IRBlasterTypeAndBrandList->BrandId' style='width:50px;height:50px;'  type='image' src='Images/IRBlaster/AirConditioner/icons_on_Grey.png' alt='on'></div>
                                                                <div style='display:none;' id='off$i$IRBlasterTypeAndBrandList->BrandId'><input id='powerOff$i$IRBlasterTypeAndBrandList->BrandId' style='width:50px;height:50px;'  type='image' src='Images/IRBlaster/AirConditioner/icons_off.png' alt='off'></div>
                                                            </div>
                                                        <div style='display:none;' id='toggle$i$IRBlasterTypeAndBrandList->BrandId' class='toggle' ><input id='powerToggle$i$IRBlasterTypeAndBrandList->BrandId' style='width:50px;height:50px;'  type='image' src='Images/IRBlaster/AirConditioner/icons_toggle.png' alt='toggle'></div>
                                                    </div>
                                                        <div class='modal-footer' style='width:100%;'>
                                                            <button type='button' class='all' style='height:fit-content;width:49%;background:#4cd137 !important;' onclick='useAirConditioner(&#39;$IRBlasterTypeAndBrandList->BrandName&#39;)'>Use</button>
                                                            <button type='button' class='all' style='height:fit-content;width:49%;background:#e84118 !important;' onclick='backAirConditioner(&#39;$i&#39; , &#39;$IRBlasterTypeAndBrandList->BrandId&#39;)'>Reselect</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>";
                    }
                }
            }
        }
    }

    function getList($msgJson)
    {
        //To decompress all the data from gateway
        $Compression = $msgJson->Compression;
        $decode = base64_decode($Compression);
        $Decompress = gzdecode($decode);
        $msgJson =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));

        if ($msgJson->Command == 'GetIRBlasterAPI' && $msgJson->Reply == true) {

            //To decompress all the data from gateway
            $Compress = $msgJson->Compress;
            $decode = base64_decode($Compress);
            $Decompress = gzdecode($decode);
            $msgJson =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));

            $SmartIRBlasterListtArray = $msgJson->IRBlasterList;
            if (!empty($SmartIRBlasterListtArray)) {

                echo "<div class='table-responsive'>";
                echo "<table id='myTable' class='table-bordered' style='width:100%;margin-top:50px;'>";
                echo "<thead>";
                echo "<tr>";
                echo "<th scope='col'>#</th>";
                echo "<th scope='col'>Name</th>";
                echo "<th scope='col'>Location Name</th>";
                echo "<th scope='col'>Room Temperature</th>";
                echo "<th scope='col'>Action</th>	";
                echo "<th scope='col'>RSSI</th>";
                echo "</tr>";
                echo "</thead>";
                echo "<tbody id='myBody' >";

                $counter = 1;

                foreach ($SmartIRBlasterListtArray as $SmarIRBlastertList) {

                    $OriRSSI = $SmarIRBlastertList->RSSI;

                    echo "<tr class='IRBlasterTable' >";
                    echo "<td>$counter</td>";
                    echo "<td>
                        <p style='margin:0px;'>$SmarIRBlastertList->Name</p>
                        <p style='margin-top:5px;' class='gatewaySNText'>$SmarIRBlastertList->UserSerialNo</p>
                        </td>";
                    echo "<td>$SmarIRBlastertList->LocationName</td>";
                    echo "<td>$SmarIRBlastertList->RoomTemperature</td>";

                    $IrBlasterDetail = $SmarIRBlastertList->SerialNo . '&' . $SmarIRBlastertList->UserSerialNo . '&' . $SmarIRBlastertList->IRBlasterId . '&' . $SmarIRBlastertList->Name . '&' . $SmarIRBlastertList->LocationName . '&' . $SmarIRBlastertList->RoomTemperature . '&' . $OriRSSI;
                    echo "<td><button class='btn3' type='button' onclick='openIrBlasterForm(&#39;$IrBlasterDetail&#39;)'><span class='fa fa-chevron-right'></button></td>";

                    $OriRSSI = $SmarIRBlastertList->RSSI;
                    $RSSI = 100 + $SmarIRBlastertList->RSSI;
                    if ($RSSI >= 91 && $RSSI <= 99) {
                        echo "<td>
                            <input type='image' onclick='openRSSI(&#39;$OriRSSI&#39;)' src='Images/RSSI/RSSI10.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    } else if ($RSSI >= 81 && $RSSI <= 90) {
                        echo "<td>
                            <input type='image' onclick='openRSSI(&#39;$OriRSSI&#39;)' src='Images/RSSI/RSSI9.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    } else if ($RSSI >= 71 && $RSSI <= 80) {
                        echo "<td>
                            <input type='image' onclick='openRSSI(&#39;$OriRSSI&#39;)' src='Images/RSSI/RSSI8.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    } else if ($RSSI >= 61 && $RSSI <= 70) {
                        echo "<td>
                            <input type='image' onclick='openRSSI(&#39;$OriRSSI&#39;)' src='Images/RSSI/RSSI7.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    } else if ($RSSI >= 51 && $RSSI <= 60) {
                        echo "<td>
                            <input type='image' onclick='openRSSI(&#39;$OriRSSI&#39;)' src='Images/RSSI/RSSI6.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    } else if ($RSSI >= 41 && $RSSI <= 50) {
                        echo "<td>
                            <input type='image' onclick='openRSSI(&#39;$OriRSSI&#39;)'src='Images/RSSI/RSSI5.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    } else if ($RSSI >= 31 && $RSSI <= 40) {
                        echo "<td>
                            <input type='image' onclick='openRSSI(&#39;$OriRSSI&#39;)' src='Images/RSSI/RSSI4.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    } else if ($RSSI >= 21 && $RSSI <= 30) {
                        echo "<td>
                            <input type='image' onclick='openRSSI(&#39;$OriRSSI&#39;)' src='Images/RSSI/RSSI3.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    } else if ($RSSI >= 11 && $RSSI <= 20) {
                        echo "<td>
                            <input type='image' onclick='openRSSI(&#39;$OriRSSI&#39;)'src='Images/RSSI/RSSI2.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    } else if ($RSSI >= 00 && $RSSI <= 10) {
                        echo "<td>
                            <input type='image' onclick='openRSSI(&#39;$OriRSSI&#39;)' src='Images/RSSI/RSSI1.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    } else {
                        echo "<td>
                            <input type='image' onclick='openRSSI(&#39;$OriRSSI&#39;)' src='Images/RSSI/Unknown.png' alt='1Time' style='width:50px;height:50px;'></td>";
                    }
                    echo "</tr>";
                    $counter++;
                }
                echo "</tbody>";
                echo "</table>";
                echo "</div>";

                //------------------------------------------------------------------------------Action Pop UP------------------------------------------------------------------------------------------
                foreach ($SmartIRBlasterListtArray as $SmarIRBlastertList) {
                    echo "<div class='form-popup' id='myPopupAction$SmarIRBlastertList->IRBlasterId'>
                                <div class='form-container' style='width:700px;padding:0px 50px;'>
                                <div class='modal-header' style='width:100%;height:60px;'>
                                <button class='close' onclick='closeForm(&#39;$SmarIRBlastertList->IRBlasterId&#39;)'></button>
                                <h2 class='h2form' id='textSerial$SmarIRBlastertList->IRBlasterId'></h2>
                                </div>";
                    echo "<div class='modal-body' style='width:100%;max-height: calc(80vh - 210px);overflow-y: auto;'>";
                    // echo "<div class='table-wrapper-scroll-y my-custom-scrollbar'>";
                    echo "<table id='table$SmarIRBlastertList->IRBlasterId' class='table table-hover table-bordered' style='width:100%'>";
                    echo "<thead class='thead-dark'>";
                    echo "<tr>";
                    echo "<th scope='col'>#</th>";
                    echo "<th scope='col'>Name</th>";
                    echo "<th scope='col'>Action</th>";
                    echo "</tr>";
                    echo "</thead>";
                    echo "<tbody>";

                    $AirConditionerList = $SmarIRBlastertList->AirConditionerList;
                    $RemoteList = $SmarIRBlastertList->RemoteList;
                    $DeviceNames = array();

                    foreach ($AirConditionerList as $AirConditioner) {
                        array_push($DeviceNames, $AirConditioner->DeviceName);
                    }
                    foreach ($RemoteList as $Remote) {
                        array_push($DeviceNames, $Remote->DeviceName);
                    }

                    natcasesort($DeviceNames);
                    $DeviceNames = array_values($DeviceNames);

                    $SmarIRBlastertInfo = json_encode($SmarIRBlastertList);
                    $LocationName = $SmarIRBlastertList->LocationName;

                    for ($i = 1; $i <= count($DeviceNames); $i++) {
                        if (!empty($AirConditionerList)) {
                            foreach ($AirConditionerList as $AirConditioner) {
                                if ($AirConditioner->DeviceName == $DeviceNames[$i - 1]) {
                                    $AirConditionerInfo = json_encode($AirConditioner);
                                    $IRBlasterInfo = $AirConditioner->IRBlasterId . '&&' . $AirConditioner->DeviceId . '&&' . $AirConditioner->TypeId . '&&' . $AirConditioner->DeviceName;

                                    echo "<tr class='table-light'>";
                                    echo "<td class='IRBlasterDeviceList' style='cursor:pointer;' scope='row'><a style='color:black;text-decoration:none;' value='AirCond&$AirConditionerInfo&$LocationName'>$i</a></td>";
                                    echo "<td class='IRBlasterDeviceList' style='cursor:pointer;text-align:left;'><a style='color:black;text-decoration:none;' value='AirCond&$AirConditionerInfo&$LocationName'><img src='Images/IRBlaster/IRBDevices/device_aircon.svg' alt='aircon' style='width:50px;height:50px;'> $AirConditioner->DeviceName</a></td>";
                                    echo "<td>        
                                    <button class='btn3' style='background:#00a8ff;' onclick='openRenameDevice(&#39;$IRBlasterInfo&#39;, &#39;$SmarIRBlastertInfo&#39;)'><span class='fa fa-edit'></button>
                                    <button class='btn3' style='background:#eb4d4b;' onclick='deleteDevice(&#39;$IRBlasterInfo&#39;)'><span class='fa fa-trash'></button></td>";
                                    echo "</tr>";
                                }
                            }
                        }

                        if (!empty($RemoteList)) {
                            foreach ($RemoteList as $Remote) {
                                if ($Remote->DeviceName == $DeviceNames[$i - 1]) {
                                    $RemoteInfo = json_encode($Remote);
                                    $IRBlasterInfo = $Remote->IRBlasterId . '&&' . $Remote->DeviceId . '&&' . $Remote->TypeId . '&&' . $Remote->DeviceName;

                                    echo "<tr class='table-light'>";
                                    if ($Remote->TypeId == 2) {
                                        echo "<td class='IRBlasterDeviceList' style='cursor:pointer;' scope='row'><a style='color:black;text-decoration:none;' value='Tv&$RemoteInfo&$LocationName'>$i</a></td>";
                                        echo "<td class='IRBlasterDeviceList' style='cursor:pointer;text-align:left;'><a style='color:black;text-decoration:none;' value='Tv&$RemoteInfo&$LocationName'><img src='Images/IRBlaster/IRBDevices/device_tv.svg' alt='aircon' style='width:50px;height:50px;'> $Remote->DeviceName</a></td>";
                                    } else {
                                        echo "<td class='IRBlasterDeviceList' style='cursor:pointer;' scope='row'><a style='color:black;text-decoration:none;' value='Astro&$RemoteInfo&$LocationName'>$i</a></td>";
                                        echo "<td class='IRBlasterDeviceList' style='cursor:pointer;text-align:left;'><a style='color:black;text-decoration:none;' value='Astro&$RemoteInfo&$LocationName'><img src='Images/IRBlaster/IRBDevices/device_settop.svg' alt='aircon' style='width:50px;height:50px;'> $Remote->DeviceName</a></td>";
                                    }
                                    echo "<td>";
                                    if ($Remote->TypeId == 2) {
                                        echo "
                                        <button class='btn3' style='background:#00a8ff;' onclick='openRenameDevice(&#39;$IRBlasterInfo&#39;, &#39;$SmarIRBlastertInfo&#39;)'><span class='fa fa-edit'></button>
                                        <button class='btn3' style='background:#eb4d4b;' onclick='deleteDevice(&#39;$IRBlasterInfo&#39;)'><span class='fa fa-trash'></button></td>";
                                    } else {
                                        echo "
                                        <button class='btn3' style='background:#00a8ff;' onclick='openRenameDevice(&#39;$IRBlasterInfo&#39;, &#39;$SmarIRBlastertInfo&#39;)'><span class='fa fa-edit'></button>
                                        <button class='btn3' style='background:#eb4d4b;' onclick='deleteDevice(&#39;$IRBlasterInfo&#39;)'><span class='fa fa-trash'></button></td>";
                                    }
                                    echo "</tr>";
                                }
                            }
                        }
                    }
                    echo "
                                </tbody>
                                </table>
                                </div>
                                <div class='modal-footer' style='width:100%;text-align:-webkit-center;'>
                                    <button type='button' class='all' onclick='openAddDevice(&#39;$SmarIRBlastertList->IRBlasterId&#39;, &#39;$SmarIRBlastertInfo&#39;)'>Add Device</button>
                                </div>
                                </div>
                                </div>";
                }
            } else {
                echo "<h2 class='error'><i class='fas fa-exclamation-circle fa-5x' style='color:red;'></i> </br></br>
                Smart IR Blaster Not Available For This Selected Gateway </br>
                Please Back To Home Page</h2>";
            }
        } else {
            echo "<h2 class='error'><i class='fas fa-exclamation-circle fa-5x' style='color:red;'></i> </br></br>
            Smart IR Blaster Not Available For This Selected Gateway </br>
            Please Back To Home Page</h2>";
        }
    }


    ?>

    <input style='display:none;' id='IntervalId' value='none'>
    <input style='display:none;' class='Check'>
    <input style='display:none;' class='RSSI'>
    <input style='display:none;' class='Serial'>
    <input style='display:none;' class='IRBlasterId'>
    <input style='display:none;' class='DeviceId'>
    <input style='display:none;' class='TypeId'>
    <input style='display:none;' class='BrandId'>
    <input style='display:none;' class='BrandName'>
    <input style='display:none;' class='ModelId'>
    <input style='display:none;' class='Brand'>
    <input style='display:none;' class='ModelNo'>
    <input style='display:none;' class='IRBlasterInfo'>
    <input style='display:none;' class='LocationName'>

    <!-- Rename Device Form -->
    <div class='form-popup' id='renameDeviceForm'>
        <div class='form-container' style='width:500px;padding:0px;'>
            <div class="modal-header" style='width:100%;height:60px;'>
                <a class='back' onclick='back()'><span class='fa fa-arrow-left fa-2x'></a>
                <h3 class='h2form'>Rename Device</h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <fieldset style='width:100%;'>
                    <!-- <legend>Enter Device Name</legend> -->
                    <label for="name">Name:</label>
                    <input type="text" id="deviceRenameName" name="deviceName" style='width:100%;'>
                </fieldset>
            </div>
            <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
                <button type="button" class="all" onclick="renameDevice()">Rename Device</button>
            </div>
        </div>
    </div>

    <!-- Add Device Form -->
    <div class='form-popup' id='addDeviceForm'>
        <div class='form-container SameSize' style='width:600px;padding:0px;'>
            <div class="modal-header" style='width:100%;height:60px;'>
                <a class='back' onclick='back()'><span class='fa fa-arrow-left fa-2x'></a>
                <h3 class='h2form'>Devices</h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <div class="btngroup" style='text-align:-webkit-center;'>
                    <button class='all' onclick="openAirConditioner()"><img src='Images/IRBlaster/IRBDevices/device_aircon.svg' alt='aircon' style='width:100px;height:60px;'>Air Conditioner</button>
                    <button class='all' onclick="openTv()"><img src='Images/IRBlaster/IRBDevices/device_tv.svg' alt='aircon' style='width:100px;height:60px;'>Tv</button>
                    <button class='all' onclick="openAstro()"><img src='Images/IRBlaster/IRBDevices/device_settop.svg' alt='aircon' style='width:100px;height:60px;'>Astro</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Air Conditioner Device Form -->
    <div class='form-popup' id='airconditionerDeviceForm'>
        <div class='form-container' style='width:500px;padding:0px;'>
            <div class="modal-header" style='width:100%;height:60px;'>
                <a class='back' onclick='backAirConditionerDevice()'><span class='fa fa-arrow-left fa-2x'></a>
                <h3 class='h2form'>Edit Information</h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <fieldset style='width:100%;'>
                    <!-- <legend>Enter Air Conditioner Information</legend> -->
                    <label for="name">Device Name:</label>
                    <input type="text" id="deviceAirConditionerName" name="deviceName" style='width:100%;'>

                    <label for="name">Location :</label>
                    <input type="text" id="deviceAirConditionerLocation" name="deviceName" disabled style='width:100%;'>
                </fieldset>
            </div>
            <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
                <button type="button" class="all" onclick="addAirConditioner()">Add</button>
            </div>
        </div>
    </div>

    <!-- Astro Device Form -->
    <div class='form-popup' id='remoteDeviceForm'>
        <div class='form-container' style='width:500px;padding:0px;'>
            <div class="modal-header" style='width:100%;height:60px;'>
                <a class='back' onclick='backAstroDevice()'><span class='fa fa-arrow-left fa-2x'></a>
                <h3 class='h2form'>Remote Name</h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <fieldset style='width:100%;'>
                    <!-- <legend>Enter Remote Name</legend> -->
                    <label for="name">Name:</label>
                    <input type="text" id="deviceRemoteName" name="deviceName" style='width:100%;'>
                </fieldset>
            </div>
            <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
                <button type="button" class="all" onclick="addAstro()">Add</button>
            </div>

        </div>
    </div>

    <!-- Tv Device Form -->
    <div class='form-popup' id='tvDeviceForm'>
        <div class='form-container' style='width:500px;padding:0px;'>
            <div class="modal-header" style='width:100%;height:60px;'>
                <a class='back' onclick='backTvDevice()'><span class='fa fa-arrow-left fa-2x'></a>
                <h3 class='h2form'>Device Name</h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <fieldset style='width:100%;'>
                    <!-- <legend>Enter Device Name</legend> -->
                    <label for="name">Name:</label>
                    <input type="text" id="deviceTvName" name="deviceName" style='width:100%;'>
                </fieldset>
            </div>
            <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
                <button type="button" class="all" onclick="addTv()">Add</button>
            </div>
        </div>
    </div>

    <!-- Set Air Conditioner Device Form -->
    <div class='form-popup' id='AirConditionerFormSet'>
        <div class='form-container' style='width:550px;padding:0px;'>
            <div class="modal-header" style='width:100%;height:60px;'>
                <button class='close' onclick='refresh()'></button>
                <h3 class='h2form'>Air Conditioner</h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <div class='name' style='border-top:none;'>
                    <div style='display:inline-block;'>
                        <h4 id='deviceName' style='color:#0097e6;margin-left:30px;'></h4>
                        <h4 id='locationName' style='margin-left:30px;'></h4>
                    </div>
                    <div style='float:right;margin-right:30px;'>
                        <input id='signal' style='width:50px;height:50px;opacity:0.5;' type='image' src='Images/IRBlaster/AirConditioner/icon_signal.png' alt='signal'>
                    </div>
                </div>
                <div class='mode'>
                    <h4 style='color:#0097e6;margin-left:30px;'>Mode :</h4>
                    <div class='grid-container modeGrid'>
                        <div style='display:none;' id='auto'><input id='modeAuto' style='width:50px;height:50px;' type='image' alt='auto'></div>
                        <div style='display:none;' id='cool'><input id='modeCool' style='width:50px;height:50px;' type='image' alt='cool'></div>
                        <div style='display:none;' id='heat'><input id='modeHeat' style='width:50px;height:50px;' type='image' alt='heat'></div>
                        <div style='display:none;' id='dry'><input id='modeDry' style='width:50px;height:50px;' type='image' alt='dry'></div>
                        <div style='display:none;' id='fan'><input id='modeFan' style='width:50px;height:50px;' type='image' alt='fan'></div>
                    </div>
                </div>
                <div class='tempfanswing'>
                    <h4 style='text-align: center;color:#0097e6;'>Temperature</h4>
                    <div class='grid-container tempGrid'>
                        <div><input id='decrease' style='width:50px;height:50px;' type='image' src='Images/IRBlaster/AirConditioner/icon_temperature_down.png' alt='decrease'></div>
                        <div>
                            <p id='number' class='number' style='color:#0097e6;font-size:40px;'></p><span style='font-size:20px;margin-bottom:40px;color:#0097e6;'>&#8451;</span>
                        </div>
                        <div><input id='increase' style='width:50px;height:50px;' type='image' src='Images/IRBlaster/AirConditioner/icon_temperature_up.png' alt='increase'></div>
                    </div>
                    <div class='grid-container fanswingGrid' style='margin-top:30px;'>
                        <div style='display:none;' id='fanspeed'><input id='fanlimit' style='width:80px;height:80px;' type='image' alt='fanspeed' value='1'></div>
                        <div style='display:none;' id='swingvertical'><input id='swingUD' style='width:80px;height:80px;' type='image' alt='swingvertical'></div>
                        <div style='display:none;' id='swinghorizontal'><input id='swingLR' style='width:80px;height:80px;' type='image' alt='swinghorinzontal'></div>
                    </div>
                </div>
                <div class='power'>
                    <div class='grid-container powerGrid'>
                        <div style='display:none;' id='on'><input id='powerOn' style='width:50px;height:50px;' type='image' alt='on'></div>
                        <div style='display:none;' id='off'><input id='powerOff' style='width:50px;height:50px;' type='image' alt='off'></div>
                    </div>
                    <div style='display:none;' id='toggle' class='toggle'><input id='powerToggle' style='width:50px;height:50px;' type='image' alt='toggle'></div>
                </div>
            </div>
        </div>
    </div>

    <!-- Set Tv Device Form -->
    <div class='form-popup' id='TvFormSet'>
        <div class='form-container' style='width:550px;padding:0px;'>
            <div class="modal-header" style='width:100%;height:60px;'>
                <a class='back' onclick='back()'><span class='fa fa-arrow-left fa-2x'></a>
                <h3 class='h2form' id='tvName'></h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <div class='powermute'>
                    <div class='grid-containerTv powermuteGrid'>
                        <div><input id='remoteMute' style='width:50px;height:50px;' type='image' src='Images/IRBlaster/RemoteButtons/mute_remote.svg' alt='mute'></div>
                        <div><input id='remotePower' style='width:50px;height:50px;' type='image' src='Images/IRBlaster/RemoteButtons/power_remote.svg' alt='power'></div>
                    </div>
                </div>
                <div class='menuscreenswap'>
                    <div class='grid-containerTv menuscreenswapGrid'>
                        <div><button type='button' class='all three' id='remoteMenu' style='background: #0097e6;'>MENU</button></div>
                        <div><button type='button' class='all three' id='remoteScreen' style='background: #0097e6;'>SCREEN</button></div>
                        <div><button type='button' class='all three' id='remoteSwaps' style='background: #0097e6;'>SWAP</button></div>
                    </div>
                </div>
                <div class='volarrowch'>
                    <div class='grid-containerTv volarrowchGrid'>
                        <div>
                            <div class='container'>
                                <button class='buttonIncrease' id='remoteVolIncrease'><i style='color: #0097e6 !important;' class='fas fa-plus fa'></i></button>
                                <h3 class='text'>Vol</h3>
                                <button class='buttonDecrease' id='remoteVolDecrease'><i style='color: #0097e6 !important;' class='fas fa-minus fa'></i></button>
                            </div>
                        </div>
                        <div>
                            <div class='grid-containerTv volarrowchGrid bgImgCenter'>
                                <div><input id='remoteLeft' style='width:50px;height:40px;' type='image' src='Images/IRBlaster/RemoteButtons/Cross/left_remote.svg' alt='mute'></div>
                                <div class='containerArrow'>
                                    <input id='remoteUp' style='width:50px;height:40px;position:relative;bottom:10px;' type='image' src='Images/IRBlaster/RemoteButtons/Cross/up_remote.svg' alt='mute'>
                                    <input id='remoteOk' style='width:50px;height:40px;' type='image' src='Images/IRBlaster/RemoteButtons/Cross/ok_remote.svg' alt='mute'>
                                    <input id='remoteDown' style='width:50px;height:40px;position:relative;top:10px;' type='image' src='Images/IRBlaster/RemoteButtons/Cross/down_remote.svg' alt='mute'>
                                </div>
                                <div><input id='remoteRight' style='width:50px;height:40px;' type='image' src='Images/IRBlaster/RemoteButtons/Cross/right_remote.svg' alt='mute'></div>
                            </div>
                        </div>
                        <div>
                            <div class='container'>
                                <button class='buttonIncrease' id='remoteChIncrease'><i style='color: #0097e6 !important;' class='fas fa-plus fa'></i></button>
                                <h3 class='text'>Ch</h3>
                                <button class='buttonDecrease' id='remoteChDecrease'><i style='color: #0097e6 !important;' class='fas fa-minus fa'></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='dashkeyboardtv'>
                    <div class='grid-containerTv dashkeyboardtvGrid'>
                        <div><input id='remoteDash' style='width:50px;height:50px;' type='image' src='Images/IRBlaster/RemoteButtons/dash_remote.svg' alt='dash'></div>
                        <div><input id='remoteKeyBoard' style='width:50px;height:50px;' type='image' src='Images/IRBlaster/RemoteButtons/keyboard_remote.svg' alt='keyboard' value='0'></div>
                        <div><input id='remoteTvAv' style='width:50px;height:50px;' type='image' src='Images/IRBlaster/RemoteButtons/tvav_remote.svg' alt='tvav'></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- KeyBoard Tv Form -->
    <div class='form-popup keyboard' id='keyBoardTv'>
        <div class='form-container' style='width:500px'>
            <button class='close' onclick='backKeyboard()'></button>
            <div class="buttons">
                <div class="button" onclick='PressKeyBoard(1)'>1</div>
                <div class="button" onclick='PressKeyBoard(2)'>2</div>
                <div class="button" onclick='PressKeyBoard(3)'>3</div>
                <div class="button" onclick='PressKeyBoard(4)'>4</div>
                <div class="button" onclick='PressKeyBoard(5)'>5</div>
                <div class="button" onclick='PressKeyBoard(6)'>6</div>
                <div class="button" onclick='PressKeyBoard(7)'>7</div>
                <div class="button" onclick='PressKeyBoard(8)'>8</div>
                <div class="button" onclick='PressKeyBoard(9)'>9</div>
                <div class="button zero" onclick='PressKeyBoard(10)'>0</div>
            </div>
        </div>
    </div>

    <!-- Set Astro Device Form -->
    <div class='form-popup' id='AstroFormSet'>
        <div class='form-container' style='width:550;padding:0px;'>
            <div class="modal-header" style='width:100%;height:60px;'>
                <a class='back' onclick='back()'><span class='fa fa-arrow-left fa-2x'></a>
                <h3 class='h2form' id='astroName'></h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <div class='powermute'>
                    <div class='grid-containerTv powermuteGrid'>
                        <div><input id='astroMute' style='width:50px;height:50px;' type='image' src='Images/IRBlaster/RemoteButtons/mute_remote.svg' alt='mute'></div>
                        <div><input id='astroPower' style='width:50px;height:50px;' type='image' src='Images/IRBlaster/RemoteButtons/power_remote.svg' alt='power'></div>
                    </div>
                </div>
                <div class='menuscreenswap'>
                    <div class='grid-containerTv menuscreenswapGrid'>
                        <div><button type='button' class='all three' id='astroHome' style='background: #0097e6;'>HOME</button></div>
                        <div><input id='astroAstro' style='width:50px;height:50px;' type='image' src='Images/IRBlaster/RemoteButtons/astro_astroremote.svg' alt='astro'></div>
                        <div><button type='button' class='all three' id='astroRadio' style='background: #0097e6;'>RADIO</button></div>
                    </div>
                </div>
                <div class='infofav'>
                    <div class='grid-containerTv infofavGrid'>
                        <div><input id='astroInfo' style='width:50px;height:50px;' type='image' src='Images/IRBlaster/RemoteButtons/info_astroremote.svg' alt='info'></div>
                        <div><input id='astroIII' style='width:50px;height:50px;' type='image' src='Images/IRBlaster/RemoteButtons/iii_astroremote.svg' alt='iii'></div>
                        <div><input id='astroFav' style='width:50px;height:50px;' type='image' src='Images/IRBlaster/RemoteButtons/fav_astroremote.svg' alt='fav'></div>
                        <div><input id='astroRatio' style='width:50px;height:50px;' type='image' src='Images/IRBlaster/RemoteButtons/ratio_astroremote.svg' alt='ratio'></div>
                    </div>
                </div>
                <div class='volarrowch'>
                    <div class='grid-containerTv volarrowchGrid'>
                        <div>
                            <div class='container'>
                                <button class='buttonIncrease' id='astroVolIncrease'><i style='color: #0097e6 !important;' class='fas fa-plus fa'></i></button>
                                <h3 class='text'>Vol</h3>
                                <button class='buttonDecrease' id='astroVolDecrease'><i style='color: #0097e6 !important;' class='fas fa-minus fa'></i></button>
                            </div>
                        </div>
                        <div>
                            <div class='grid-containerTv volarrowchGrid bgImgCenter'>
                                <div><input id='astroLeft' style='width:50px;height:40px;' type='image' src='Images/IRBlaster/RemoteButtons/Cross/left_remote.svg' alt='mute'></div>
                                <div class='containerArrow'>
                                    <input id='astroUp' style='width:50px;height:40px;position:relative;bottom:10px;' type='image' src='Images/IRBlaster/RemoteButtons/Cross/up_remote.svg' alt='mute'>
                                    <input id='astroOk' style='width:50px;height:40px;' type='image' src='Images/IRBlaster/RemoteButtons/Cross/ok_remote.svg' alt='mute'>
                                    <input id='astroDown' style='width:50px;height:40px;position:relative;top:10px' type='image' src='Images/IRBlaster/RemoteButtons/Cross/down_remote.svg' alt='mute'>
                                </div>
                                <div><input id='astroRight' style='width:50px;height:40px;' type='image' src='Images/IRBlaster/RemoteButtons/Cross/right_remote.svg' alt='mute'></div>
                            </div>
                        </div>
                        <div>
                            <div class='container'>
                                <button class='buttonIncrease' id='astroChIncrease'><i style='color: #0097e6 !important;' class='fas fa-plus fa'></i></button>
                                <h3 class='text'>Ch</h3>
                                <button class='buttonDecrease' id='astroChDecrease'><i style='color: #0097e6 !important;' class='fas fa-minus fa'></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='keyboardback'>
                    <div class='grid-containerTv keyboardbackGrid'>
                        <div><input id='astroKeyBoard' style='width:50px;height:50px;' type='image' src='Images/IRBlaster/RemoteButtons/keyboard_remote.svg' alt='keyboard' value='0'></div>
                        <div><input id='astroBack' style='width:50px;height:50px;' type='image' src='Images/IRBlaster/RemoteButtons/back_astroremote.svg' alt='tvav'></div>
                    </div>
                </div>
                <div class='infofav'>
                    <div class='grid-containerTv infofavGrid'>
                        <div><button type='button' class='all four' id='astroR' style='background: #e74c3c;'>R</button></div>
                        <div><button type='button' class='all four' id='astroG' style='background: #27ae60;'>G</button></div>
                        <div><button type='button' class='all four' id='astroY' style='background: #f9ca24;'>Y</button></div>
                        <div><button type='button' class='all four' id='astroB' style='background: #00a8ff;'>B</button></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- KeyBoard Astro Form -->
    <div class='form-popup keyboard' id='keyBoardAstro'>
        <div class='form-container' id='slideUp' style='width:500px'>
            <button class='close' onclick='backKeyboard()'></button>
            <div class="buttons">
                <div class="button" onclick='PressKeyBoard(1)'>1</div>
                <div class="button" onclick='PressKeyBoard(2)'>2</div>
                <div class="button" onclick='PressKeyBoard(3)'>3</div>
                <div class="button" onclick='PressKeyBoard(4)'>4</div>
                <div class="button" onclick='PressKeyBoard(5)'>5</div>
                <div class="button" onclick='PressKeyBoard(6)'>6</div>
                <div class="button" onclick='PressKeyBoard(7)'>7</div>
                <div class="button" onclick='PressKeyBoard(8)'>8</div>
                <div class="button" onclick='PressKeyBoard(9)'>9</div>
                <div class="button zero" onclick='PressKeyBoard(10)'>0</div>
                <div class="button zero" onclick='PressKeyBoard(39)'>@</div>
            </div>
        </div>
    </div>

    <!-- Form for sign Door Lock -->
    <div class="form-popup" id="mySiginDoorLockForm">
        <div class="form-container" style="width:400px;padding:0px;">
            <div class="modal-header" style='width:100%;height:70px;'>
                <!-- <a class='back' onclick='closeFormSigin()'><span class='fa fa-arrow-left fa-2x'></a> -->
                <h3 class='h2form'>Manage Door Lock </br> Password</h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <fieldset style='width:100%;'>
                    <label for="name">Enter Admin Password:</label>
                    <input type="password" id="password" name="DoorlockPassword" style='width:100%;'>
                </fieldset>
            </div>
            <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
                <button type="submit" class="all" onclick='openGetLock()' name="signin">Ok</button>
                <button type="button" class="all" onclick="cancelFormSigin()">Cancel</button>
            </div>
        </div>
    </div>

    <!-- SideBar + Navbar -->
    <div class="wrapper">
        <!-- Sidebar Holder -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>Senzo</h3>
                <strong>SZ</strong>
            </div>
            <ul class="list-unstyled components">
                <!-- Home Section -->
                <li>
                    <form action='SmartIRBlaster.php' method='POST' id='NavHome' style='margin-block-end: 0'>
                        <input style='display:none;' name='Home'>
                        <a style='cursor:pointer;' class='Home'>
                            <i class="glyphicon glyphicon-home"></i>
                            Home
                        </a>
                    </form>
                </li>
                <li>
                    <!-- Device Section -->
                    <a href="#pageDevice" data-toggle="collapse" aria-expanded="false">
                        <i class="glyphicon glyphicon-tasks"></i>
                        <?php echo "Device"; ?>
                    </a>
                    <ul class="collapse list-unstyled in" id="pageDevice">
                        <?php
                        if ($SmartSwitchStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectSwitch()">Smart Switch</a></li>';
                        }
                        if ($CurtainStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectCurtain()">Smart Curtain</a></li>';
                        }
                        if ($ShutterStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectShutter()">Smart Shutter</a></li>';
                        }
                        if ($LockStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectDoorLock()">Smart Door Lock</a></li>';
                        }
                        if ($IRBlasterStatus) {
                            echo '<li><a id="cube" style="cursor:pointer;" onclick="selectIRBlaster()">IR Blaster</a></li>';
                        }
                        ?>
                    </ul>
                </li>
                <li>
                    <!-- Scene Section -->
                    <a style="cursor:pointer;" onclick='openScenesForm()'>
                        <i class="glyphicon glyphicon-film"></i>
                        <?php echo "Scene ($SceneCount)"; ?>
                    </a>
                </li>
                <li>
                    <!-- Security Section -->
                    <a href="#pageSecurity" data-toggle="collapse" aria-expanded="false">
                        <i class="glyphicon glyphicon-lock"></i>
                        <?php echo "Security"; ?>
                    </a>
                    <ul class="collapse list-unstyled" id="pageSecurity">
                        <?php
                        // If smart alarm connected to gateway
                        if ($AlarmStatus) {
                            echo "<li><a style='cursor:pointer;' onclick='selectAlarm(&#39;True&#39;)'>
                            Smart Alarm <img src='Images/Action/security_armed_icon.png' alt='scenes' style='width:20px;height:20px;'>";
                        } else {
                            echo "<li><a style='cursor:pointer;' onclick='selectAlarm(&#39;False&#39;)'>
                            Smart Alarm";
                        }
                        ?>
                        </a>
                </li>
                <li><a style="cursor:pointer;" onclick="selectCamera()">Camera</a></li>
            </ul>
            </li>
            <li>
                <!-- Analytic Section -->
                <a style='cursor:pointer;' onclick='openAnalyticForm()'>
                    <i class='glyphicon glyphicon-stats'></i>
                    Analytic
                </a>
            </li>
            <li>
                <!-- Setting Section -->
                <a style='cursor:pointer;' onclick='openSettingForm()'>
                    <i class='glyphicon glyphicon-cog'></i>
                    Setting
                </a>
            </li>
            </ul>

            <!-- Logout button -->
            <ul class="list-unstyled CTAs">
                <li>
                    <form method="POST" action="SmartIRBlaster.php" id='NavLogout' style='margin-block-end: 0'>
                        <input style='display:none;' name='Logout'>
                        <a style="cursor:pointer;" class="Logout">
                            <i class="glyphicon glyphicon-log-out"></i>
                            Logout
                        </a>
                    </form>
                </li>
            </ul>
        </nav>

        <!-- Page Content Holder -->
        <div id="content" style="width: -webkit-fill-available;">

            <nav class="navbar navbar-default">
                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                            <i class="glyphicon glyphicon-align-left"></i>
                            <span></span>
                        </button>

                        <!-- For responsive part -->
                        <!-- Show and hide content that been targeted-->
                        <button class="btn btn-dark navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <i class="glyphicon glyphicon-align-justify"></i>
                        </button>
                    </div>

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a data-toggle="dropdown" class="dropdown-toggle" style='background: #fff;cursor:pointer;'>Location</a>
                                <ul class="dropdown-menu">
                                    <?php
                                    $LocationListArray = $_SESSION['location'];
                                    if (is_array($LocationListArray) || is_object($LocationListArray)) {
                                        foreach ($LocationListArray as $location) {
                                            echo "<li>";
                                            echo "<input style='display:none;' value='$location->LocationName' id='Location$location->LocationName'>";
                                            echo "<a style='cursor:pointer;' class='LocationListItem' >$location->LocationName</a>";
                                            echo "</li>";
                                        }
                                    }
                                    ?>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div class='filter' id='collapseFilter'>
                <span class='filer-title'> Filtered By : </span>
            </div>

            <h2>List of Available IR Blaster</h2>
            <?php
            $GatewayLevel = $_SESSION['level'];
            echo "<h4 class='Level' >Gateway $GatewayLevel</h4>";
            getList($msgJsonIRBlaster);
            getBrand($msgJsonBrand);
            ?>
            <div class="line"></div>

        </div>
    </div>

    <!-- Auto Refresh dropdown -->
    <div class='autorefresh'>
        <div class="dropdown">
            <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">
                <img src='Images/Switch/settings_gear.png' alt='switchon' style='width:30px;height:30px;animation: spin 3s linear infinite;'>
            </button>
            <ul class="dropdown-menu">
                <li>
                    <input type='checkbox' id='showSn' value='showSn'>
                    <label> Show SN</label><br>
                </li>
                <li>
                    <input type='checkbox' id='enable' name='fooby[2][]'>
                    <label> Enable</label><br>
                    <div id='timeRefresh' style='display:none;margin-left:10px;'>
                        <input type='checkbox' value='1-minute' class="slectOne" id='1-minute'>
                        <label> 1 minute</label><br>
                        <input type='checkbox' value='5-minutes' class="slectOne" id='5-minutes'>
                        <label> 5 minutes</label><br>
                        <input type='checkbox' value='10-minutes' class="slectOne" id='10-minutes'>
                        <label> 10 minutes</label><br>
                        <input type='checkbox' value='20-minutes' class="slectOne" id='20-minutes'>
                        <label> 20 minutes</label><br>
                        <input type='checkbox' value='25-minutes' class="slectOne" id='25-minutes'>
                        <label> 25 minutes</label><br>
                        <input type='checkbox' value='1-hour' class="slectOne" id='1-hour'>
                        <label> 1 hour</label><br>
                    </div>
                </li>
                <li>
                    <input type='checkbox' id='disable' value='disable'>
                    <label> Disable</label><br>
                </li>
            </ul>
        </div>
    </div>

    <!-- Loading animation -->
    <div class="form-popup" id='loader'>
        <div class="loading-container">
            <div class="loading"></div>
            <div class="loading-text">loading</div>
        </div>
    </div>

    <!-- Loading animation -->
    <div class="form-popup" id='checkingStatus'>
        <div class="loading-container">
            <div class="loading"></div>
            <div class="loading-text">Checking IRBlaster Status</div>
        </div>
    </div>

    <!-- Loading animation -->
    <div class="form-popup" id='fetchingAircon'>
        <div class="loading-container">
            <div class="loading"></div>
            <div class="loading-text">Fetching Aircon Model from Server</div>
            <!-- <button class='loading-button btn-block btn-primary' onclick='cancelAircon()'>Cancel</button> -->
        </div>
    </div>

    <!-- Loading animation -->
    <div class="form-popup" id='fetchingTv'>
        <div class="loading-container">
            <div class="loading"></div>
            <div class="loading-text">Fetching Remote Model from Server</div>
            <!-- <button class='loading-button btn-block btn-primary' onclick='cancelTv()'>Cancel</button> -->
        </div>
    </div>

    <!-- Loading animation -->
    <div class="form-popup" id='fetchingAstro'>
        <div class="loading-container">
            <div class="loading"></div>
            <div class="loading-text">Fetching Astro Model from Server</div>
            <!-- <button class='loading-button btn-block btn-primary' onclick='cancelAstro()'>Cancel</button> -->
        </div>
    </div>
</body>

</html>