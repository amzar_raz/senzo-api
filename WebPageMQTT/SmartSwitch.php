<?php
ini_set('max_execution_time', 30);
session_start();
ob_start();
//Include all file needed to use in this page
include '././configuration/serverConfig.php';
include './header/headerAll.php';
?>
<!DOCTYPE html>
<html>

<head>
  <title>Smart Switch</title>
  <link href="css/SmartSwitch.css" rel="stylesheet">
  <link rel="stylesheet" href="css/Sidebar.css">
  <!-- Sidebar and auto refresh function -->
  <script type="text/javascript" src="SidebarHandler.js"></script>
</head>
<script type="text/javascript">
  function openSwitchForm(SwitchDetail) {
    var field = SwitchDetail.split("&");
    var switchSerial = field[0];
    var switchUserSerialNo = field[1];
    var switchName = field[2];
    var switchLocationName = field[3];
    var switchRSSI = field[4];
    var switchSwitchNo = field[5];

    document.getElementsByClassName("serial").value = switchSerial;
    document.getElementsByClassName("switchno").value = switchSwitchNo;
    document.getElementsByClassName("RSSI").value = switchRSSI;
    document.getElementById("myPopupAction" + switchSerial + switchSwitchNo).style.display = "block";
  }

  function actionSwitch(switchInfo, switchName, switchMode) {
    var switchInfoSplit = switchInfo.split(" ");
    var prevfirelevel = document.getElementById("prevfirelevel" + switchInfoSplit[0] + switchInfoSplit[1]).value;
    var current = document.getElementById("switch" + switchInfoSplit[0] + switchInfoSplit[1]).value;

    //Differentiate for dimmer and normal switch
    if (switchMode == 0) {
      var fireLevelValue = document.getElementById("switch" + switchInfoSplit[0] + switchInfoSplit[1]).value == 0 ? '100' : '0';
    } else {
      if (current == 0) {
        var fireLevelValue = prevfirelevel;
      } else {
        var fireLevelValue = 0;
      }

    }

    var loader = document.getElementById("loader");
    var url = baseURL + "/switchAction.php/?SerialNo=" + switchInfoSplit[0] + "&SwitchNo=" + switchInfoSplit[1] + "&fireLevel=" + fireLevelValue;

    if (fireLevelValue != 0) { //To turn on the switch
      loader.style.display = "block";
      $.getJSON(url, function(data) {
        //data is the JSON string
        loader.style.display = "none";
        if (data.Status == true) {

          if (switchMode == 0) {
            document.getElementById("switch" + switchInfoSplit[0] + switchInfoSplit[1]).src = 'Images/Switch/lightbulb_on.svg';
            document.getElementById("switchPop" + switchInfoSplit[0] + switchInfoSplit[1]).src = 'Images/Switch/lightbulb_on.svg';

            document.getElementById("switch" + switchInfoSplit[0] + switchInfoSplit[1]).value = 100;
          } else {
            document.getElementById("switch" + switchInfoSplit[0] + switchInfoSplit[1]).src = 'Images/Switch/dimmerOnTimerOff.png';
            document.getElementById("dimmer" + switchInfoSplit[0] + switchInfoSplit[1]).style.color = '#0097e6';
            document.getElementById("dimmer" + switchInfoSplit[0] + switchInfoSplit[1]).innerHTML = fireLevelValue + "%";

            document.getElementById("switchPop" + switchInfoSplit[0] + switchInfoSplit[1]).src = 'Images/Switch/dimmerOnTimerOff.png';
            document.getElementById("dimmerPop" + switchInfoSplit[0] + switchInfoSplit[1]).style.color = '#0097e6';
            document.getElementById("dimmerPop" + switchInfoSplit[0] + switchInfoSplit[1]).innerHTML = fireLevelValue + "%";

            document.getElementById("switch" + switchInfoSplit[0] + switchInfoSplit[1]).value = fireLevelValue;
          }

          //Change the hidden row value in table
          var table = $('#myTable').DataTable();
          var row = table.row('#row' + switchInfoSplit[0] + switchInfoSplit[1]);
          table.cell(row, 7).data(fireLevelValue).draw();

          iziToast.show({
            title: 'SWITCH ACTION',
            message: 'Your ' + switchName + ' is on',
            theme: 'dark',
            position: 'bottomCenter',
            icon: 'icon-person'
          });
        } else { //If turn on switch Fail
          Swal.fire({
            icon: 'error',
            title: 'Failed',
            text: data.Message
          })
        }
      });
    } else if (fireLevelValue == 0) { //To turn off switch
      loader.style.display = "block";
      $.getJSON(url, function(data) {
        //data is the JSON string
        loader.style.display = "none";
        if (data.Status == true) {

          if (switchMode == 0) {
            document.getElementById("switch" + switchInfoSplit[0] + switchInfoSplit[1]).src = 'Images/Switch/lightbulb_off.svg';

            document.getElementById("switchPop" + switchInfoSplit[0] + switchInfoSplit[1]).src = 'Images/Switch/lightbulb_off.svg';
          } else {
            document.getElementById("switch" + switchInfoSplit[0] + switchInfoSplit[1]).src = 'Images/Switch/dimmerOffTimerOff.png';
            document.getElementById("dimmer" + switchInfoSplit[0] + switchInfoSplit[1]).style.color = '#333';
            document.getElementById("dimmer" + switchInfoSplit[0] + switchInfoSplit[1]).innerHTML = "0%";

            document.getElementById("switchPop" + switchInfoSplit[0] + switchInfoSplit[1]).src = 'Images/Switch/dimmerOffTimerOff.png';
            document.getElementById("dimmerPop" + switchInfoSplit[0] + switchInfoSplit[1]).style.color = '#333';
            document.getElementById("dimmerPop" + switchInfoSplit[0] + switchInfoSplit[1]).innerHTML = "0%";
          }

          document.getElementById("switch" + switchInfoSplit[0] + switchInfoSplit[1]).value = fireLevelValue;

          //Change the hidden row value in table
          var table = $('#myTable').DataTable();
          var row = table.row('#row' + switchInfoSplit[0] + switchInfoSplit[1]);
          table.cell(row, 7).data(fireLevelValue).draw();

          iziToast.show({
            title: 'SWITCH ACTION',
            message: 'Your ' + switchName + ' is off',
            theme: 'dark',
            position: 'bottomCenter',
            icon: 'icon-person'
          });
        } else { //If turn off Fail 
          Swal.fire({
            icon: 'error',
            title: 'Failed',
            text: data.Message
          })
        }
      });
    }
  }

  function openDimmerForm(switchInfo, switchName, from) {
    var switchInfoSplit = switchInfo.split(" ");
    var fireLevelValue = document.getElementById("switch" + switchInfoSplit[0] + switchInfoSplit[1]).value;
    document.getElementById("dimmerInputId").value = fireLevelValue;
    document.getElementById("dimmerOutputId").value = fireLevelValue;
    document.getElementsByClassName("serial").value = switchInfoSplit[0];
    document.getElementsByClassName("switchno").value = switchInfoSplit[1];
    document.getElementsByClassName("name").value = switchName;

    if (from == 'Table') {
      $("#headerDimmer").
      append(
        "<button id='closeDimmer' class='close' onclick='backLog(&#39;DimmerContorlForm&#39;)'></button>"
      );
    } else {
      $("#headerDimmer").
      append(
        "<a id='backDimmer' class='back' onclick='back()'><span class='fa fa-arrow-left fa-2x'></a>"
      );
    }
    document.getElementById("myPopupAction" + switchInfoSplit[0] + switchInfoSplit[1]).style.display = "none";
    document.getElementById("DimmerContorlForm").style.display = 'block';
  }

  function myDimmerFunction() {
    var serial = document.getElementsByClassName("serial").value;
    var switchno = document.getElementsByClassName("switchno").value;
    var fireLevelValue = document.getElementById("dimmerInputId").value; //user input
    var pastfireLevelValue = document.getElementById("prevfirelevel" + serial + switchno).value; //prev
    var current = document.getElementById("switch" + serial + switchno).value; //current
    var switchName = document.getElementsByClassName("name").value;
    var loader = document.getElementById("loader");
    var url = baseURL + "/switchAction.php/?SerialNo=" + serial + "&SwitchNo=" + switchno + "&fireLevel=" + fireLevelValue;

    loader.style.display = "block";
    $.getJSON(url, function(data) {
      loader.style.display = "none";
      if (data.Status == true) {
        if (fireLevelValue != 0) { //To turn on the switch
          document.getElementById("switch" + serial + switchno).src = 'Images/Switch/dimmerOnTimerOff.png';
          document.getElementById("dimmer" + serial + switchno).style.color = '#0097e6';
          document.getElementById("dimmer" + serial + switchno).innerHTML = fireLevelValue + "%";

          document.getElementById("switchPop" + serial + switchno).src = 'Images/Switch/dimmerOnTimerOff.png';
          document.getElementById("dimmerPop" + serial + switchno).style.color = '#0097e6';
          document.getElementById("dimmerPop" + serial + switchno).innerHTML = fireLevelValue + "%";

          document.getElementById("switch" + serial + switchno).value = fireLevelValue;
          document.getElementById("prevfirelevel" + serial + switchno).value = fireLevelValue;
          iziToast.show({
            title: 'SWITCH ACTION',
            message: 'Your ' + switchName + ' is on',
            theme: 'dark',
            position: 'bottomCenter',
            icon: 'icon-person'
          });

        } else if (fireLevelValue == 0) { //To turn off the switch
          document.getElementById("switch" + serial + switchno).src = 'Images/Switch/dimmerOffTimerOff.png';
          document.getElementById("dimmer" + serial + switchno).style.color = '#333';
          document.getElementById("dimmer" + serial + switchno).innerHTML = "0%";

          document.getElementById("switchPop" + serial + switchno).src = 'Images/Switch/dimmerOffTimerOff.png';
          document.getElementById("dimmerPop" + serial + switchno).style.color = '#333';
          document.getElementById("dimmerPop" + serial + switchno).innerHTML = "0%";

          document.getElementById("switch" + serial + switchno).value = fireLevelValue;
          iziToast.show({
            title: 'SWITCH ACTION',
            message: 'Your ' + switchName + ' is off',
            theme: 'dark',
            position: 'bottomCenter',
            icon: 'icon-person'
          });
        }

        //Change the hidden row value in table
        var table = $('#myTable').DataTable();
        var row = table.row('#row' + serial + switchno);
        table.cell(row, 7).data(fireLevelValue).draw();
        document.getElementById("DimmerContorlForm").style.display = 'none';

      } else { //If turn on or turn off Fail
        Swal.fire({
          icon: 'error',
          title: 'Failed',
          text: data.Message
        })
      }
    });
  }

  function openDelayForm(delayInfo) {
    var serial = document.getElementsByClassName("serial").value;
    var switchno = document.getElementsByClassName("switchno").value;
    document.getElementById("myPopupAction" + serial + switchno).style.display = "none";

    var fields = delayInfo.split(" ");
    var timeDelay = JSON.parse(fields[0]);
    var actionno = fields[1];
    var timerEnable = fields[2];

    if (actionno == 1) {
      var SwitchAction = 'On';
    } else {
      var SwitchAction = 'Off';
    }

    if (timerEnable == 'true') {
      document.getElementById("delayTimer").innerHTML = "Switch will turn " + SwitchAction + " in about " + timeDelay.h + " hours " + timeDelay.i + " minutes";
      document.getElementById("actions").value = SwitchAction;
      document.getElementById("hours").value = timeDelay.h;
      document.getElementById("minutes").value = timeDelay.i;
    } else {
      document.getElementById("delayTimer").innerHTML = "Timer is not active";
      document.getElementById("actions").value = 'chooseaction';
      document.getElementById("hours").value = "";
      document.getElementById("minutes").value = "";
    }
    document.getElementById("myDelayForm").style.display = "block";
  }

  function myDelayTimerFunction() {
    var timerActions = document.getElementById("actions");
    var serial = document.getElementsByClassName("serial").value;
    var switchno = document.getElementsByClassName("switchno").value;
    var timerHours = document.getElementsByName("hours");
    var timerMinutes = document.getElementsByName("minutes");
    var RSSI = document.getElementsByClassName("RSSI").value;

    var Minutes = +timerMinutes[0].value + (+timerHours[0].value * 60);

    var boolAction = true;
    if (timerActions.value == null || timerActions.value == 'chooseaction') {
      boolAction = false;
    }
    var boolOn = true;
    if (timerMinutes[0].value == 0 && timerHours[0].value == 0) {
      boolOn = false;
    }

    var hrefDelayTimer = baseURL + "/switchEnableDelayTimer.php/?SerialNo=" + serial + "&SwitchNo=" + switchno + "&Action=" + timerActions.value + "&Minutes=" + Minutes;
    var loader = document.getElementById("loader");
    if (boolOn == true && boolAction == true) {
      loader.style.display = "block";
      var enable = true;
      $.getJSON(hrefDelayTimer, function(data) {
        //data is the JSON string
        loader.style.display = "none";
        if (data.Command == 'SetDelayTimer' && data.Status == false) {
          enable = false;
          Swal.fire({
            icon: 'error',
            title: 'Failed',
            text: data.Message
          })
        } else {
          Swal.fire(
            'Good job!',
            'Delay Timer Has Been Set',
            'success'
          )
          setTimeout(function() {
            location.reload();
          }, 3000);
        }
      });

    } else if (!boolAction) {
      Swal.fire({
        icon: 'error',
        title: 'Failed',
        text: 'Please select action',
      })
    } else if (!boolOn) {
      Swal.fire({
        icon: 'error',
        title: 'Failed',
        text: 'Both hours and minutes cannot be empty',
      })
    } else {
      Swal.fire({
        icon: 'error',
        title: 'Failed',
        text: 'Something went wrong!',
      })
    }
  }

  function disableDelayTimer() {
    var serial = document.getElementsByClassName("serial").value;
    var switchno = document.getElementsByClassName("switchno").value;
    var RSSI = document.getElementsByClassName("RSSI").value

    var hrefDisableTimer = baseURL + "/switchDisableDelayTimer.php/?SerialNo=" + serial + "&SwitchNo=" + switchno;
    var loader = document.getElementById("loader");
    loader.style.display = "block";
    var disable = true;
    $.getJSON(hrefDisableTimer, function(data) {
      //data is the JSON string
      loader.style.display = "none";
      if (data.Command == 'CancelDelayTimer' && data.Status == false) {
        disable = false;
        Swal.fire({
          icon: 'error',
          title: 'Failed',
          text: data.Message
        })
      } else {
        Swal.fire(
          'Good job!',
          'Delay Timer Has Been Removed',
          'success'
        )
        setTimeout(function() {
          location.reload();
        }, 3000);
      }
    });
  }

  function choose24Timer(Timer24HInfo) {
    var serial = document.getElementsByClassName("serial").value;
    var switchno = document.getElementsByClassName("switchno").value;
    document.getElementById("myPopupAction" + serial + switchno).style.display = "none";

    document.getElementsByClassName("SwitchInfo").value = Timer24HInfo;

    var obj = JSON.parse(Timer24HInfo);

    OnTimeEnabled_1 = obj[0].OnTimeEnabled;
    OffTimeEnabled_1 = obj[0].OffTimeEnabled;

    OnTimeEnabled_2 = obj[1].OnTimeEnabled;
    OffTimeEnabled_2 = obj[1].OffTimeEnabled;

    if (OnTimeEnabled_1 == false && OffTimeEnabled_1 == false) {
      document.getElementById("t1").style.color = "black";
      document.getElementById("t1").innerHTML = "Status : Inactive";
    } else {
      document.getElementById("t1").style.color = "red";
      document.getElementById("t1").innerHTML = "Status : Active";
    }

    //On Timer 1
    if (OnTimeEnabled_1 == true) {
      var OnMon = obj[0].OnMon == true ? "Mon" : "";
      var OnTue = obj[0].OnTue == true ? "Tue" : "";
      var OnWed = obj[0].OnWed == true ? "Wed" : "";
      var OnThu = obj[0].OnThu == true ? "Thu" : "";
      var OnFri = obj[0].OnFri == true ? "Fri" : "";
      var OnSat = obj[0].OnSat == true ? "Sat" : "";
      var OnSun = obj[0].OnSun == true ? "Sun" : "";
      if (OnMon == "Mon" && OnTue == "Tue" && OnWed == "Wed" && OnThu == "Thu" && OnFri == "Fri" && OnSat == "Sat" && OnSun == "Sun") {
        var Onday = "Everyday";
      } else {
        var Onday = OnMon + OnTue + OnWed + OnThu + OnFri + OnSat + OnSun;
      }
      document.getElementById("actionOn1").style.color = "red";
      document.getElementById("actionOn1").innerHTML = "Turn on at " + tConvert(obj[0].OnTime) + "." + " " + Onday;
    } else {
      document.getElementById("actionOn1").style.color = "black";
      document.getElementById("actionOn1").innerHTML = "";
    }

    //Off Timer 1
    if (OffTimeEnabled_1 == true) {
      var OffMon = obj[0].OffMon == true ? "Mon" : "";
      var OffTue = obj[0].OffTue == true ? "Tue" : "";
      var OffWed = obj[0].OffWed == true ? "Wed" : "";
      var OffThu = obj[0].OffThu == true ? "Thu" : "";
      var OffFri = obj[0].OffFri == true ? "Fri" : "";
      var OffSat = obj[0].OffSat == true ? "Sat" : "";
      var OffSun = obj[0].OffSun == true ? "Sun" : "";
      if (OffMon == "Mon" && OffTue == "Tue" && OffWed == "Wed" && OffThu == "Thu" && OffFri == "Fri" && OffSat == "Sat" && OffSun == "Sun") {
        var Offday = "Everyday";
      } else {
        var Offday = OffMon + OffTue + OffWed + OffThu + OffFri + OffSat + OffSun;
      }
      document.getElementById("actionOff1").style.color = "red";
      document.getElementById("actionOff1").innerHTML = "Turn off at " + tConvert(obj[0].OffTime) + "." + " " + Offday;
    } else {
      document.getElementById("actionOff1").style.color = "black";
      document.getElementById("actionOff1").innerHTML = "";
    }

    if (OnTimeEnabled_2 == false && OffTimeEnabled_2 == false) {
      document.getElementById("t2").style.color = "black";
      document.getElementById("t2").innerHTML = "Status : Inactive";
    } else {
      document.getElementById("t2").style.color = "red";
      document.getElementById("t2").innerHTML = "Status : Active";
    }

    //On Timer 2
    if (OnTimeEnabled_2 == true) {

      var OnMon = obj[1].OnMon == true ? "Mon" : "";
      var OnTue = obj[1].OnTue == true ? "Tue" : "";
      var OnWed = obj[1].OnWed == true ? "Wed" : "";
      var OnThu = obj[1].OnThu == true ? "Thu" : "";
      var OnFri = obj[1].OnFri == true ? "Fri" : "";
      var OnSat = obj[1].OnSat == true ? "Sat" : "";
      var OnSun = obj[1].OnSun == true ? "Sun" : "";
      if (OnMon == "Mon" && OnTue == "Tue" && OnWed == "Wed" && OnThu == "Thu" && OnFri == "Fri" && OnSat == "Sat" && OnSun == "Sun") {
        var Onday = "Everyday";
      } else {
        var Onday = OnMon + OnTue + OnWed + OnThu + OnFri + OnSat + OnSun;
      }
      document.getElementById("actionOn2").style.color = "red";
      document.getElementById("actionOn2").innerHTML = "Turn on at " + tConvert(obj[1].OnTime) + "." + " " + Onday;
    } else {
      document.getElementById("actionOn2").style.color = "black";
      document.getElementById("actionOn2").innerHTML = "";
    }

    //Off Timer 2
    if (OffTimeEnabled_2 == true) {
      var OffMon = obj[1].OffMon == true ? "Mon" : "";
      var OffTue = obj[1].OffTue == true ? "Tue" : "";
      var OffWed = obj[1].OffWed == true ? "Wed" : "";
      var OffThu = obj[1].OffThu == true ? "Thu" : "";
      var OffFri = obj[1].OffFri == true ? "Fri" : "";
      var OffSat = obj[1].OffSat == true ? "Sat" : "";
      var OffSun = obj[1].OffSun == true ? "Sun" : "";
      if (OffMon == "Mon" && OffTue == "Tue" && OffWed == "Wed" && OffThu == "Thu" && OffFri == "Fri" && OffSat == "Sat" && OffSun == "Sun") {
        var Offday = "Everyday";
      } else {
        var Offday = OffMon + OffTue + OffWed + OffThu + OffFri + OffSat + OffSun;
      }
      document.getElementById("actionOff2").style.color = "red";
      document.getElementById("actionOff2").innerHTML = "Turn off at " + tConvert(obj[1].OffTime) + "." + " " + Offday;
    } else {
      document.getElementById("actionOff2").style.color = "black";
      document.getElementById("actionOff2").innerHTML = "";
    }

    document.getElementById("choose24Timer").style.display = "block";

  }

  function tConvert(time) {
    // Check correct time format and split into components
    time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

    if (time.length > 1) { // If time format correct
      time = time.slice(1); // Remove full string match value
      time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
      time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    return time.join(''); // return adjusted time or original string
  }

  function open24HForm(timerNo) {

    var SwitchInfo = document.getElementsByClassName("SwitchInfo").value;
    var obj = JSON.parse(SwitchInfo);

    if (timerNo == 1) {

      if (obj[0].OnTimeEnabled == false && obj[0].OffTimeEnabled == false) {
        document.getElementById("status").innerHTML = "Timer " + timerNo + " is not active";
      } else {
        document.getElementById("status").innerHTML = "";
      }

      //For ON Timer
      if (obj[0].OnTimeEnabled == true) {
        var OnMon = obj[0].OnMon == true ? "Mon" : "";
        var OnTue = obj[0].OnTue == true ? "Tue" : "";
        var OnWed = obj[0].OnWed == true ? "Wed" : "";
        var OnThu = obj[0].OnThu == true ? "Thu" : "";
        var OnFri = obj[0].OnFri == true ? "Fri" : "";
        var OnSat = obj[0].OnSat == true ? "Sat" : "";
        var OnSun = obj[0].OnSun == true ? "Sun" : "";
        if (OnMon == "Mon" && OnTue == "Tue" && OnWed == "Wed" && OnThu == "Thu" && OnFri == "Fri" && OnSat == "Sat" && OnSun == "Sun") {
          var Onday = "Everyday";
        } else {
          var Onday = OnMon + OnTue + OnWed + OnThu + OnFri + OnSat + OnSun;
        }
        document.getElementById("statusOn").innerHTML = "Turn on at " + tConvert(obj[0].OnTime) + "." + " " + Onday;
        document.getElementById("switch1on").checked = true;
        document.getElementById("onTime").value = obj[0].OnTime;
        document.getElementById("onday1").checked = obj[0].OnSun;
        document.getElementById("onday2").checked = obj[0].OnMon;
        document.getElementById("onday3").checked = obj[0].OnTue;
        document.getElementById("onday4").checked = obj[0].OnWed;
        document.getElementById("onday5").checked = obj[0].OnThu;
        document.getElementById("onday6").checked = obj[0].OnFri;
        document.getElementById("onday7").checked = obj[0].OnSat;
        document.getElementById("switch1on").disabled = false;
        document.getElementById("onday1").disabled = false;
        document.getElementById("onday2").disabled = false;
        document.getElementById("onday3").disabled = false;
        document.getElementById("onday4").disabled = false;
        document.getElementById("onday5").disabled = false;
        document.getElementById("onday6").disabled = false;
        document.getElementById("onday7").disabled = false;
      } else {
        document.getElementById("statusOn").innerHTML = "";
        document.getElementById("switch1on").checked = false;
        document.getElementById("onTime").disabled = true;
        document.getElementById("onTime").value = "00:00";
        document.getElementById("onday1").disabled = true;
        document.getElementById("onday2").disabled = true;
        document.getElementById("onday3").disabled = true;
        document.getElementById("onday4").disabled = true;
        document.getElementById("onday5").disabled = true;
        document.getElementById("onday6").disabled = true;
        document.getElementById("onday7").disabled = true;
        document.getElementById("onday1").checked = false;
        document.getElementById("onday2").checked = false;
        document.getElementById("onday3").checked = false;
        document.getElementById("onday4").checked = false;
        document.getElementById("onday5").checked = false;
        document.getElementById("onday6").checked = false;
        document.getElementById("onday7").checked = false;
      }

      //For OFF Timer
      if (obj[0].OffTimeEnabled == true) {
        var OffMon = obj[0].OffMon == true ? "Mon" : "";
        var OffTue = obj[0].OffTue == true ? "Tue" : "";
        var OffWed = obj[0].OffWed == true ? "Wed" : "";
        var OffThu = obj[0].OffThu == true ? "Thu" : "";
        var OffFri = obj[0].OffFri == true ? "Fri" : "";
        var OffSat = obj[0].OffSat == true ? "Sat" : "";
        var OffSun = obj[0].OffSun == true ? "Sun" : "";
        if (OffMon == "Mon" && OffTue == "Tue" && OffWed == "Wed" && OffThu == "Thu" && OffFri == "Fri" && OffSat == "Sat" && OffSun == "Sun") {
          var Offday = "Everyday";
        } else {
          var Offday = OffMon + OffTue + OffWed + OffThu + OffFri + OffSat + OffSun;
        }
        document.getElementById("statusOff").innerHTML = "Turn off at " + tConvert(obj[0].OffTime) + "." + " " + Offday;

        document.getElementById("switch1off").checked = true;
        document.getElementById("offTime").value = obj[0].OffTime;
        document.getElementById("offday1").checked = obj[0].OffSun;
        document.getElementById("offday2").checked = obj[0].OffMon;
        document.getElementById("offday3").checked = obj[0].OffTue;
        document.getElementById("offday4").checked = obj[0].OffWed;
        document.getElementById("offday5").checked = obj[0].OffThu;
        document.getElementById("offday6").checked = obj[0].OffFri;
        document.getElementById("offday7").checked = obj[0].OffSat;

        document.getElementById("offTime").disabled = false;
        document.getElementById("offday1").disabled = false;
        document.getElementById("offday2").disabled = false;
        document.getElementById("offday3").disabled = false;
        document.getElementById("offday4").disabled = false;
        document.getElementById("offday5").disabled = false;
        document.getElementById("offday6").disabled = false;
        document.getElementById("offday7").disabled = false;
      } else {
        document.getElementById("statusOff").innerHTML = "";
        document.getElementById("switch1off").checked = false;
        document.getElementById("offTime").disabled = true;
        document.getElementById("offTime").value = "00:00";
        document.getElementById("offday1").disabled = true;
        document.getElementById("offday2").disabled = true;
        document.getElementById("offday3").disabled = true;
        document.getElementById("offday4").disabled = true;
        document.getElementById("offday5").disabled = true;
        document.getElementById("offday6").disabled = true;
        document.getElementById("offday7").disabled = true;
        document.getElementById("offday1").checked = false;
        document.getElementById("offday2").checked = false;
        document.getElementById("offday3").checked = false;
        document.getElementById("offday4").checked = false;
        document.getElementById("offday5").checked = false;
        document.getElementById("offday6").checked = false;
        document.getElementById("offday7").checked = false;
      }

    } else {

      if (obj[1].OnTimeEnabled == false && obj[1].OffTimeEnabled == false) {
        document.getElementById("status").innerHTML = "Timer " + timerNo + " is not active";
      } else {
        document.getElementById("status").innerHTML = "";
      }

      //For ON Timer
      if (obj[1].OnTimeEnabled == true) {
        var OnMon = obj[1].OnMon == true ? "Mon" : "";
        var OnTue = obj[1].OnTue == true ? "Tue" : "";
        var OnWed = obj[1].OnWed == true ? "Wed" : "";
        var OnThu = obj[1].OnThu == true ? "Thu" : "";
        var OnFri = obj[1].OnFri == true ? "Fri" : "";
        var OnSat = obj[1].OnSat == true ? "Sat" : "";
        var OnSun = obj[1].OnSun == true ? "Sun" : "";
        if (OnMon == "Mon" && OnTue == "Tue" && OnWed == "Wed" && OnThu == "Thu" && OnFri == "Fri" && OnSat == "Sat" && OnSun == "Sun") {
          var Onday = "Everyday";
        } else {
          var Onday = OnMon + OnTue + OnWed + OnThu + OnFri + OnSat + OnSun;
        }
        document.getElementById("statusOn").innerHTML = "Turn on at " + tConvert(obj[1].OnTime) + "." + " " + Onday;
        document.getElementById("switch1on").checked = true;
        document.getElementById("onTime").value = obj[1].OnTime;
        document.getElementById("onday1").checked = obj[1].OnSun;
        document.getElementById("onday2").checked = obj[1].OnMon;
        document.getElementById("onday3").checked = obj[1].OnTue;
        document.getElementById("onday4").checked = obj[1].OnWed;
        document.getElementById("onday5").checked = obj[1].OnThu;
        document.getElementById("onday6").checked = obj[1].OnFri;
        document.getElementById("onday7").checked = obj[1].OnSat;
        document.getElementById("switch1on").disabled = false;
        document.getElementById("onday1").disabled = false;
        document.getElementById("onday2").disabled = false;
        document.getElementById("onday3").disabled = false;
        document.getElementById("onday4").disabled = false;
        document.getElementById("onday5").disabled = false;
        document.getElementById("onday6").disabled = false;
        document.getElementById("onday7").disabled = false;
      } else {
        document.getElementById("statusOn").innerHTML = "";
        document.getElementById("switch1on").checked = false;
        document.getElementById("onTime").disabled = true;
        document.getElementById("onTime").value = "00:00";
        document.getElementById("onday1").disabled = true;
        document.getElementById("onday2").disabled = true;
        document.getElementById("onday3").disabled = true;
        document.getElementById("onday4").disabled = true;
        document.getElementById("onday5").disabled = true;
        document.getElementById("onday6").disabled = true;
        document.getElementById("onday7").disabled = true;
        document.getElementById("onday1").checked = false;
        document.getElementById("onday2").checked = false;
        document.getElementById("onday3").checked = false;
        document.getElementById("onday4").checked = false;
        document.getElementById("onday5").checked = false;
        document.getElementById("onday6").checked = false;
        document.getElementById("onday7").checked = false;
      }

      //For OFF Timer
      if (obj[1].OffTimeEnabled == true) {
        var OffMon = obj[1].OffMon == true ? "Mon" : "";
        var OffTue = obj[1].OffTue == true ? "Tue" : "";
        var OffWed = obj[1].OffWed == true ? "Wed" : "";
        var OffThu = obj[1].OffThu == true ? "Thu" : "";
        var OffFri = obj[1].OffFri == true ? "Fri" : "";
        var OffSat = obj[1].OffSat == true ? "Sat" : "";
        var OffSun = obj[1].OffSun == true ? "Sun" : "";
        if (OffMon == "Mon" && OffTue == "Tue" && OffWed == "Wed" && OffThu == "Thu" && OffFri == "Fri" && OffSat == "Sat" && OffSun == "Sun") {
          var Offday = "Everyday";
        } else {
          var Offday = OffMon + OffTue + OffWed + OffThu + OffFri + OffSat + OffSun;
        }
        document.getElementById("statusOff").innerHTML = "Turn off at " + tConvert(obj[1].OffTime) + "." + " " + Offday;

        document.getElementById("switch1off").checked = true;
        document.getElementById("offTime").value = obj[1].OffTime;
        document.getElementById("offday1").checked = obj[1].OffSun;
        document.getElementById("offday2").checked = obj[1].OffMon;
        document.getElementById("offday3").checked = obj[1].OffTue;
        document.getElementById("offday4").checked = obj[1].OffWed;
        document.getElementById("offday5").checked = obj[1].OffThu;
        document.getElementById("offday6").checked = obj[1].OffFri;
        document.getElementById("offday7").checked = obj[1].OffSat;

        document.getElementById("offTime").disabled = false;
        document.getElementById("offday1").disabled = false;
        document.getElementById("offday2").disabled = false;
        document.getElementById("offday3").disabled = false;
        document.getElementById("offday4").disabled = false;
        document.getElementById("offday5").disabled = false;
        document.getElementById("offday6").disabled = false;
        document.getElementById("offday7").disabled = false;
      } else {
        document.getElementById("statusOff").innerHTML = "";
        document.getElementById("switch1off").checked = false;
        document.getElementById("offTime").disabled = true;
        document.getElementById("offTime").value = "00:00";
        document.getElementById("offday1").disabled = true;
        document.getElementById("offday2").disabled = true;
        document.getElementById("offday3").disabled = true;
        document.getElementById("offday4").disabled = true;
        document.getElementById("offday5").disabled = true;
        document.getElementById("offday6").disabled = true;
        document.getElementById("offday7").disabled = true;
        document.getElementById("offday1").checked = false;
        document.getElementById("offday2").checked = false;
        document.getElementById("offday3").checked = false;
        document.getElementById("offday4").checked = false;
        document.getElementById("offday5").checked = false;
        document.getElementById("offday6").checked = false;
        document.getElementById("offday7").checked = false;
      }
    }

    document.getElementById("24Timer").innerHTML = "24 Hour Timer " + timerNo;
    document.getElementsByClassName("timerno").value = timerNo
    document.getElementById("my24HForm").style.display = "block";
    document.getElementById("choose24Timer").style.display = "none";

  }

  function my24TimerFunction() {
    var serial = document.getElementsByClassName("serial").value;
    var switchno = document.getElementsByClassName("switchno").value;
    var timerno = document.getElementsByClassName("timerno").value;

    //-----------------------------------------For ON Timer-----------------------------------------------
    //toggle button
    var OnTimeEnabled = document.getElementById('switch1on');
    var OnEnable = OnTimeEnabled.checked ? 'true' : 'false';

    var boolOnCheck = true;
    if (OnEnable == 'false') {
      boolOnCheck = false;
    }

    //Time and date
    var onTime = document.getElementById('onTime').value;
    var onDay = document.getElementsByName("dayOnCheck");
    var onDaybinary = 128;

    var boolOn = true;
    onDay.forEach(function(item) {
      if (item.checked) {
        onDaybinary += parseInt(item.value);
      }
    });

    //No day selected
    if (onDaybinary == 128) {
      boolOn = false;
      // window.alert("No day selected on ON Timer 1");
    }

    //-----------------------------------------For OFF Timer-----------------------------------------------
    //toggle button
    var OffTimeEnabled = document.getElementById('switch1off');
    var OffEnable = OffTimeEnabled.checked ? 'true' : 'false';

    var boolOffCheck = true;
    if (OffEnable == 'false') {
      boolOffCheck = false;
    }

    //Time and date
    var offTime = document.getElementById('offTime').value;
    var offDay = document.getElementsByName("dayOffCheck");
    var offDaybinary = 128;

    var boolOff = true;
    offDay.forEach(function(item) {
      if (item.checked) {
        offDaybinary += parseInt(item.value);
      }
    });

    //No day selected
    if (offDaybinary == 128) {
      boolOff = false;
    }

    var href24HTimer = baseURL + "/switchEnable24HTimer.php/?SerialNo=" + serial + "&SwitchNo=" + switchno + "&TimerNo=" +
      timerno + "&OnEnable=" + OnEnable +
      "&OffEnable=" + OffEnable + "&OnTime=" + onTime + "&OffTime=" + offTime + "&OnDays=" + onDaybinary +
      "&OffDays=" + offDaybinary;
    var loader = document.getElementById("loader");

    var boolValidation = true;
    if (!boolOnCheck && !boolOffCheck) {
      boolValidation = false;
      Swal.fire({
        icon: 'error',
        title: 'Failed',
        text: 'No actions selected',
      })
    }

    if ((!boolOn && boolOnCheck)) {
      boolValidation = false;
      Swal.fire({
        icon: 'error',
        title: 'Failed',
        text: 'No day selected for Switch ON Timer',
      })
    } else if ((!boolOff && boolOffCheck)) {
      boolValidation = false;
      Swal.fire({
        icon: 'error',
        title: 'Failed',
        text: 'No day selected for Switch OFF Timer',
      })
    }

    if (boolOnCheck && boolOffCheck && onTime == offTime) {
      boolValidation = false;
      Swal.fire({
        icon: 'error',
        title: 'Failed',
        text: 'ON Time and OFF Time cannot be the same',
      })
    }

    if (boolValidation) {
      loader.style.display = "block";
      var enable = true;
      $.getJSON(href24HTimer, function(data) {
        //data is the JSON string
        loader.style.display = "none";
        if (data.Command == 'Set24HoursTimer' && data.Status == false) {
          enable = false;
          Swal.fire({
            icon: 'error',
            title: 'Failed',
            text: data.Message
          })
        } else {
          Swal.fire(
            'Good job!',
            '24 Hour Timer Has Been Set',
            'success'
          )
          setTimeout(function() {
            location.reload();
          }, 3000);
        }
      });
    }

  }

  function disable24Timer() {
    var serial = document.getElementsByClassName("serial").value;
    var switchno = document.getElementsByClassName("switchno").value;
    var timerno = document.getElementsByClassName("timerno").value;


    var hrefDisableTimer = baseURL + "/switchDisable24HTimer.php/?SerialNo=" + serial + "&SwitchNo=" + switchno + "&TimerNo=" + timerno;

    //window.alert(hrefDisableTimer);
    var loader = document.getElementById("loader");
    loader.style.display = "block";
    var disable = true
    $.getJSON(hrefDisableTimer, function(data) {
      //data is the JSON string
      loader.style.display = "none";
      if (data.Command == 'Cancel24HoursTimer' && data.Status == false) {
        disable = false;
        Swal.fire({
          icon: 'error',
          title: 'Failed',
          text: data.Message
        })
      } else {
        Swal.fire(
          'Good job!',
          '24 Hour Timer Has Been Removed',
          'success'
        )
        setTimeout(function() {
          location.reload();
        }, 3000);
      }
    });
  }

  function openLogForm(param) {

    var paramSplit = param.split("&&"); //---- [0]= SN, [2]=switchNo ----//
    document.getElementById("myPopupAction" + paramSplit[0] + paramSplit[1]).style.display = "none";
    var url = baseURL + "/switchReadLog.php/?SerialNo=" + paramSplit[0] + "&SwitchNo=" + paramSplit[1];
    var loader = document.getElementById("loader");
    document.getElementById("logName").innerHTML = "Device: " + paramSplit[2] + "(" + paramSplit[3] + ")";
    loader.style.display = "block";

    $.getJSON(url, function(data) {
      loader.style.display = "none";
      var LogList = data.DataLogList;
      var logNo = 1;

      if (LogList.length == 0) {
        $("#emptyLog").
        append(
          '<div class="divNoLog">' +
          '<img src="Images/smiling_bell.png" alt="smiling bell" width="100" height="100" class="smiley">' +
          '<h4 style="text-align:center;">No log yet !</h4>' +
          '<p style="text-align:center;">Check this section to know the activity of this device.</p>' +
          '</div>'
        );
        document.getElementById("tableLog").style.display = "none";
      } else {
        for (i = LogList.length - 1; i >= 0; i--) {
          var timeSplit = LogList[i].DateTime.split("T");
          var statusSplit = LogList[i].Status.split(":");

          $("#SwitchLogTable").
          append(
            '<tr class="trLogTable">' +
            '<td>' + logNo + '</td>' +
            '<td>' + timeSplit[0] + "<br/>" + timeSplit[1] + '</td>' +
            '<td>' + '<b>' + statusSplit[0] + '</b>' + "<br/>" + statusSplit[1] + '</td>' +
            '</tr>'
          );
          logNo++;
        }
        document.getElementById("tableLog").style.display = "block";
      }
    });
    $("#SwitchLogForm").fadeIn("fast", "swing");
  }

  function backLog(id) {
    document.getElementById(id).style.display = 'none';
    if (id == "SwitchLogForm") {
      if ($(".trLogTable")[0]) {
        // Do something if class exists
        $(".trLogTable").remove();
      } else {
        // Do something if class does not exist
        $(".divNoLog").remove();
      }
    }
    if (id == "DimmerContorlForm") {
      $("#closeDimmer").remove();
    }
  }

  function closeForm(serial) {
    document.getElementById("myPopupAction" + serial).style.display = "none";
  }

  function back() {
    var serial = document.getElementsByClassName("serial").value;
    var switchno = document.getElementsByClassName("switchno").value;
    document.getElementById("myDelayForm").style.display = "none";
    document.getElementById("choose24Timer").style.display = "none";
    document.getElementById("DimmerContorlForm").style.display = "none";
    $("#backDimmer").remove();
    document.getElementById("myPopupAction" + serial + switchno).style.display = "block";
  }

  function back2back() {
    document.getElementById("my24HForm").style.display = "none";
    document.getElementById("choose24Timer").style.display = "block";
  }

  function disableOn() {

    var sunday;
    var monday;
    var tuesday;
    var wednesday;
    var thursday;
    var friday;
    var saturday;

    //For on timer
    if (document.getElementById("switch1on").checked) {
      document.getElementById("onTime").disabled = false;
      document.getElementById("onday1").disabled = false;
      document.getElementById("onday2").disabled = false;
      document.getElementById("onday3").disabled = false;
      document.getElementById("onday4").disabled = false;
      document.getElementById("onday5").disabled = false;
      document.getElementById("onday6").disabled = false;
      document.getElementById("onday7").disabled = false;
    }

    if (document.getElementById("switch1on").checked == false) {
      document.getElementById("onTime").disabled = true;
      document.getElementById("onday1").disabled = true;
      document.getElementById("onday2").disabled = true;
      document.getElementById("onday3").disabled = true;
      document.getElementById("onday4").disabled = true;
      document.getElementById("onday5").disabled = true;
      document.getElementById("onday6").disabled = true;
      document.getElementById("onday7").disabled = true;
    }
  }

  function disableOff() {
    //For off timer
    if (document.getElementById("switch1off").checked) {
      document.getElementById("offTime").disabled = false;
      document.getElementById("offday1").disabled = false;
      document.getElementById("offday2").disabled = false;
      document.getElementById("offday3").disabled = false;
      document.getElementById("offday4").disabled = false;
      document.getElementById("offday5").disabled = false;
      document.getElementById("offday6").disabled = false;
      document.getElementById("offday7").disabled = false;
    }

    if (document.getElementById("switch1off").checked == false) {
      document.getElementById("offTime").disabled = true;
      document.getElementById("offday1").disabled = true;
      document.getElementById("offday2").disabled = true;
      document.getElementById("offday3").disabled = true;
      document.getElementById("offday4").disabled = true;
      document.getElementById("offday5").disabled = true;
      document.getElementById("offday6").disabled = true;
      document.getElementById("offday7").disabled = true;
    }
  }

  $(function() {
    $('#datetimepicker1').datetimepicker({
      format: 'HH:mm'

    });
  });

  $(function() {
    $('#datetimepicker2').datetimepicker({
      format: 'HH:mm'
    });
  });

  function openRSSI(Rssi) {
    iziToast.show({
      title: 'RSSI RESULT',
      message: 'The RSSI value for the switch is :' + Rssi,
      theme: 'dark',
      position: 'topCenter',
      icon: 'icon-person'
    });
  }

  //For table
  $(document).ready(function() {
    $('#myTable').DataTable({
      "columnDefs": [{
        "targets": [7],
        "visible": false
      }]
    });
  });

  //For filter location
  $(document).ready(function() {
    $('.LocationListItem').click(function() {
      var table = $('#myTable').DataTable();
      var value = document.getElementById("Location" + $(this).text()).value;
      document.getElementsByClassName("LocationName").value = value;
      table.draw();

      if ($(".data-location").contents().length > 0) {
        $(".data-location").remove();
      }
      $("#collapseFilter").
      append(
        '<div class="data-tag data-location" id="tag' + value + '">' +
        '<span class="data-tag-text" style="margin:0px 5px;">' + value + '</span>' +
        '<span onclick="removeFilter(&#39;' + value + '&#39;)"><i class="fas fa-times"></i></span>' +
        '</div>'
      );
      document.getElementById("collapseFilter").style.display = 'block';
      // window.alert($(this).text());
    });
  });

  //For filter firelevel
  $(document).ready(function() {
    $('.FireLevelItem').click(function() {
      var table = $('#myTable').DataTable();
      var value = document.getElementById("FireLevelValue" + $(this).text()).value;
      document.getElementsByClassName("SwitchFire").value = value;
      table.draw();

      document.getElementById("collapseFilter").style.display = 'block';
      if ($(".data-status").contents().length > 0) {
        $(".data-status").remove();
      }
      $("#collapseFilter").
      append(
        '<div class="data-tag data-status" id="tag' + value + '">' +
        '<span class="data-tag-text" style="margin:0px 5px;">' + $(this).text() + '</span>' +
        '<span onclick="removeFilter(&#39;' + value + '&#39;)"><i class="fas fa-times"></i></span>' +
        '</div>'
      );

    });

  });

  function removeFilter(id) {
    var tagId = document.getElementById("tag" + id);
    var table = $('#myTable').DataTable();

    if (id == '100' || id == '0') {
      document.getElementsByClassName("SwitchFire").value = 'Null'
    } else {
      document.getElementsByClassName("LocationName").value = 'Null';
    }

    tagId.remove();
    table.draw();

    if ($(".data-tag").contents().length == 0) {
      document.getElementById("collapseFilter").style.display = "none";
    }
  }

  //For validation hours and minutes
  $(document).ready(function() {
    function setInputFilter(textbox, inputFilter) {
      ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop", "number"].forEach(function(event) {
        textbox.addEventListener(event, function() {
          if (inputFilter(this.value)) {
            this.oldValue = this.value;
            this.oldSelectionStart = this.selectionStart;
            this.oldSelectionEnd = this.selectionEnd;
          } else if (this.hasOwnProperty("oldValue")) {
            this.value = this.oldValue;
            this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
          } else {
            this.value = "";
          }
        });
      });
    }

    setInputFilter(document.getElementById("hours"), function(value) {
      return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 23);
    });

    setInputFilter(document.getElementById("minutes"), function(value) {
      return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 59);
    });

  });

  //For search based on fire level
  $.fn.dataTable.ext.search.push(
    function(settings, data, dataIndex) {
      var SwitchFire = document.getElementsByClassName("SwitchFire").value;
      if (SwitchFire == null) {
        SwitchFire = 'Null';
      }
      var LocationName = document.getElementsByClassName("LocationName").value;
      if (LocationName == null) {
        LocationName = 'Null';
      }
      var FireLevel = data[7];
      var Location = data[2];

      if ((SwitchFire == 100 && FireLevel > 0 && LocationName == Location) ||
        (SwitchFire == 100 && FireLevel > 0 && LocationName == 'Null') ||
        (SwitchFire == 'Null' && LocationName == 'Null') ||
        (SwitchFire == 'Null' && LocationName == Location) ||
        (SwitchFire == FireLevel && LocationName == 'Null') ||
        (SwitchFire == FireLevel && LocationName == Location)
      ) {
        return true;
      }
      return false;

    }
  );
</script>

<body>
  <?php
  //Get all the session variable from Dashboard.php
  $SerialGateway = $_SESSION['serialGateway'];
  $Password = $_SESSION['password'];
  $LocationListArray = $_SESSION['location'];
  $SmartSwitchStatus = $_SESSION['SmartSwitchStatus'];
  $CurtainStatus = $_SESSION['CurtainStatus'];
  $AlarmStatus = $_SESSION['AlarmStatus'];
  $ShutterStatus = $_SESSION['ShutterStatus'];
  $IRBlasterStatus = $_SESSION['IRBlasterStatus'];
  $LockStatus = $_SESSION['LockStatus'];
  $TotalDevice = $_SESSION['TotalDevice'];
  $SceneCount = $_SESSION['SceneCount'];
  $AdminPassword = $_SESSION['AdminPassword'];

  echo "<input style='display:none;' id='SerialGateway' value='$SerialGateway'>";
  echo "<input style='display:none;' id='Password' value='$Password'>";
  echo "<input style='display:none;' id='AdminPassword' value='$AdminPassword'>";

  if (isset($_POST['Logout'])) {
    header("Location: ChooseGateway.php");
    header("Refresh:0");
    ob_flush();
    session_destroy();
    exit();
  }

  if (isset($_POST['Home'])) {
    // header("Refresh:0");
    header("Location: Dashboard.php");
    ob_flush();
    exit();
  }

  $opts = array('http' => array('header' => 'Cookie: ' . $_SERVER['HTTP_COOKIE'] . "\r\n"));  //Create array to send session id in the cookie
  $context = stream_context_create($opts);   //Creates and returns a stream context
  session_write_close(); // Write session data and end session 
  $url = $baseURL . "/switchSmartSwitch.php"; //Create API url to get command scene
  $SmartSwitchJson = file_get_contents($url, false, $context);  //Reads entire file into a string
  $msgJson = json_decode($SmartSwitchJson); //Convert smart switch or error message from json string 
  if (!empty($msgJson->Message)) {
    $Error = $msgJson->Message;
  ?>
    <script type="text/javascript">
      Swal.fire({
        icon: 'error',
        title: 'Failed',
        text: '<?php echo $Error; ?>',
      }).then(function() {
        window.location.href = "ChooseGateway.php";
      });
    </script>
  <?php
    die();
  }

  function getList($msgJson)
  {
    if ($msgJson->Command == 'GetSmartSwitch' && $msgJson->Reply == true) {

      //To decompress the smart switch list array
      $Compress = $msgJson->Compress;
      $decode = base64_decode($Compress);
      $Decompress = gzdecode($decode);
      $msgJson =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));

      $SmartSwitchListArray = $msgJson->SmartSwitchList;
      if (!empty($SmartSwitchListArray)) {

        echo "<div class='table-responsive'>";
        echo "<table id='myTable' class='table-bordered' style='width:100%;margin-top:50px;'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th scope='col'>#</th>";
        echo "<th scope='col'>Name</th>";
        echo "<th scope='col'>Location</th>";
        echo "<th scope='col'>Switch</th>	";
        echo "<th scope='col'>Action</th>	";
        echo "<th scope='col'>Log</th>	";
        echo "<th scope='col'>RSSI</th>	";
        echo "<th scope='col'>FireLevel</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody id='myBody' >";

        $counter = 1;

        foreach ($SmartSwitchListArray as $SmartSwitch) {

          $SwitchArray = $SmartSwitch->SwitchList;
          foreach ($SwitchArray as $switch) {

            // echo "<tbody id='myBody' >";
            echo "<tr class='SwitchTable' id='row$switch->SerialNo$switch->SwitchNo'>";
            echo "<td>$counter</td>";
            // echo "<td class='SmartSwitchList' style='cursor:pointer;'><a style='color:black;text-decoration:none;' value='$switch->SerialNo&$switch->UserSerialNo&$switch->Name&$switch->LocationName&$switch->RSSI&$switch->SwitchNo'>$switch->UserSerialNo</a></td>";
            echo "<td>
              <p style='margin:0px;'>$switch->Name</p>
              <p style='margin-top:5px;' class='gatewaySNText'>$switch->UserSerialNo</p>
              </td>";
            echo "<td>$switch->LocationName</td>";

            //Check fire level of switch
            $isChecked = $switch->FireLevel == 0 ? "" : "checked";

            //Switch info
            $SwitchInfo = $switch->SerialNo . ' ' . $switch->SwitchNo;
            echo "<input style='display:none;' value='$switch->PrevFirelevel' id='prevfirelevel$switch->SerialNo$switch->SwitchNo'/>";

            echo "<td>";
            if ($isChecked == 'checked') {
              if ($switch->SwitchMode == 0) {
                echo "<input value='$switch->FireLevel' id='switch$switch->SerialNo$switch->SwitchNo' class='switchImage' type='image' onclick='actionSwitch(&#39;$SwitchInfo&#39;, &#39;$switch->Name&#39;, &#39;$switch->SwitchMode&#39;)' src='Images/Switch/lightbulb_on.svg' alt='switchon' style='width:50px;height:50px;'></td>";
              } else {
                echo "<input value='$switch->FireLevel' id='switch$switch->SerialNo$switch->SwitchNo' class='switchImage' type='image' onclick='actionSwitch(&#39;$SwitchInfo&#39;, &#39;$switch->Name&#39;, &#39;$switch->SwitchMode&#39;)' src='Images/Switch/dimmerOnTimerOff.png' alt='switchon' style='width:50px;height:50px;'>
                  </br><p style='color:#0097e6;margin-top:5px;cursor: pointer;' id='dimmer$switch->SerialNo$switch->SwitchNo' onclick='openDimmerForm(&#39;$SwitchInfo&#39;, &#39;$switch->Name&#39;, &#39;Table&#39;)'>$switch->FireLevel%</p></td>";
              }
            } else {
              if ($switch->SwitchMode == 0) {
                echo "<input value='$switch->FireLevel' id='switch$switch->SerialNo$switch->SwitchNo' class='switchImage' type='image' onclick='actionSwitch(&#39;$SwitchInfo&#39;, &#39;$switch->Name&#39;, &#39;$switch->SwitchMode&#39;)' src='Images/Switch/lightbulb_off.svg' alt='switchoff' style='width:50px;height:50px;'></td>";
              } else {
                echo "<input value='$switch->FireLevel' id='switch$switch->SerialNo$switch->SwitchNo' class='switchImage' type='image' onclick='actionSwitch(&#39;$SwitchInfo&#39;, &#39;$switch->Name&#39;, &#39;$switch->SwitchMode&#39;)' src='Images/Switch/dimmerOffTimerOff.png' alt='switchon' style='width:50px;height:50px;'>
                  </br><p style='margin-top:5px;cursor: pointer;' id='dimmer$switch->SerialNo$switch->SwitchNo' onclick='openDimmerForm(&#39;$SwitchInfo&#39;, &#39;$switch->Name&#39;, &#39;Table&#39;)'>$switch->FireLevel%</p></td>";
              }
            }
            echo "</td>";

            $SwitchDetail = $switch->SerialNo . '&' . $switch->UserSerialNo . '&' . $switch->Name . '&' . $switch->LocationName . '&' . $switch->RSSI . '&' . $switch->SwitchNo;
            echo "<td><button class='btn3' type='button' onclick='openSwitchForm(&#39;$SwitchDetail&#39;)'><span class='fa fa-chevron-right'></button></td>";


            $LogInfo = $switch->SerialNo . '&&' . $switch->SwitchNo . '&&' . $switch->Name . '&&' . $switch->UserSerialNo;
            echo "<td><button class='btn3' type='button' onclick='openLogForm(&#39;$LogInfo&#39;)'><span class='fa fa-book-open'></button></td>";

            $RSSI = 100 + $switch->RSSI;
            if ($RSSI >= 91 && $RSSI <= 99) {
              echo "<td>
                <input type='image' onclick='openRSSI(&#39;$switch->RSSI&#39;)' src='Images/RSSI/RSSI10.png' alt='1Time' style='width:50px;height:50px;'></td>";
            } else if ($RSSI >= 81 && $RSSI <= 90) {
              echo "<td>
                <input type='image' onclick='openRSSI(&#39;$switch->RSSI&#39;)' src='Images/RSSI/RSSI9.png' alt='1Time' style='width:50px;height:50px;'></td>";
            } else if ($RSSI >= 71 && $RSSI <= 80) {
              echo "<td>
                <input type='image' onclick='openRSSI(&#39;$switch->RSSI&#39;)' src='Images/RSSI/RSSI8.png' alt='1Time' style='width:50px;height:50px;'></td>";
            } else if ($RSSI >= 61 && $RSSI <= 70) {
              echo "<td>
                <input type='image' onclick='openRSSI(&#39;$switch->RSSI&#39;)' src='Images/RSSI/RSSI7.png' alt='1Time' style='width:50px;height:50px;'></td>";
            } else if ($RSSI >= 51 && $RSSI <= 60) {
              echo "<td>
                <input type='image' onclick='openRSSI(&#39;$switch->RSSI&#39;)' src='Images/RSSI/RSSI6.png' alt='1Time' style='width:50px;height:50px;'></td>";
            } else if ($RSSI >= 41 && $RSSI <= 50) {
              echo "<td>
                <input type='image' onclick='openRSSI(&#39;$switch->RSSI&#39;)'src='Images/RSSI/RSSI5.png' alt='1Time' style='width:50px;height:50px;'></td>";
            } else if ($RSSI >= 31 && $RSSI <= 40) {
              echo "<td>
                <input type='image' onclick='openRSSI(&#39;$switch->RSSI&#39;)' src='Images/RSSI/RSSI4.png' alt='1Time' style='width:50px;height:50px;'></td>";
            } else if ($RSSI >= 21 && $RSSI <= 30) {
              echo "<td>
                <input type='image' onclick='openRSSI(&#39;$switch->RSSI&#39;)' src='Images/RSSI/RSSI3.png' alt='1Time' style='width:50px;height:50px;'></td>";
            } else if ($RSSI >= 11 && $RSSI <= 20) {
              echo "<td>
                <input type='image' onclick='openRSSI(&#39;$switch->RSSI&#39;)'src='Images/RSSI/RSSI2.png' alt='1Time' style='width:50px;height:50px;'></td>";
            } else if ($RSSI >= 00 && $RSSI <= 10) {
              echo "<td>
                <input type='image' onclick='openRSSI(&#39;$switch->RSSI&#39;)' src='Images/RSSI/RSSI1.png' alt='1Time' style='width:50px;height:50px;'></td>";
            } else {
              echo "<td>
                <input type='image' onclick='openRSSI(&#39;$switch->RSSI&#39;)' src='Images/RSSI/Unknown.png' alt='1Time' style='width:50px;height:50px;'></td>";
            }

            echo "<td>$switch->FireLevel</td>";
            echo "</tr>";
            $counter++;
          }
        }
        echo "</tbody>";
        echo "</table>";
        echo "</div>";


        foreach ($SmartSwitchListArray as $SmartSwitch) {

          $SwitchArray = $SmartSwitch->SwitchList;
          foreach ($SwitchArray as $switch) {
            //Check fire level of switch
            $isChecked = $switch->FireLevel == 0 ? "" : "checked";

            //Get delay timer enabled or not
            $TimerDelay_Enabled = $switch->TimerDelay_Enabled == true ? 'true' : 'false';

            //Get 24 hour timer enable or not
            $Timer24H_Enabled = $switch->Timer24H_Enabled == true ? 'true' : 'false';

            //Get 24 Hour on/off enable
            // $OnEnable1 = $switch->Timer24H[0]->OnTimeEnabled ? 'true' : 'false';
            // $OffEnable1 = $switch->Timer24H[0]->OffTimeEnabled ? 'true' : 'false';

            // $OnEnable2 = $switch->Timer24H[1]->OnTimeEnabled ? 'true' : 'false';
            // $OffEnable2 = $switch->Timer24H[1]->OffTimeEnabled ? 'true' : 'false';

            // $Timer1Info = json_encode($switch->Timer24H[0]);
            // $Timer2Info = json_encode($switch->Timer24H[1]);

            //Switch info
            $SwitchInfo = $switch->SerialNo . ' ' . $switch->SwitchNo;

            //Timer delay action
            $TimerDelayAction = $switch->TimerDelayAction;
            // $SwitchAction = $TimerDelayAction == 1 ? 'On' : 'Off';

            //Status delay timer
            $TimerDelayActionTime = $switch->TimerDelayActionTime;
            $fromGateway = new DateTime($TimerDelayActionTime, new DateTimeZone("Asia/Kuala_Lumpur"));
            $current = new DateTime;
            $current->setTimeZone(new DateTimeZone("Asia/Kuala_Lumpur"));
            $interval = $fromGateway->diff($current);
            $time = json_encode($interval);;

            //Delay timer info
            $DelayInfo = $time . ' ' . $TimerDelayAction . ' ' . $TimerDelay_Enabled;

            //24 timer info
            $Timer24Info = json_encode($switch->Timer24H);


            //------------------------------------------------------------------------------Action Pop UP------------------------------------------------------------------------------------------
            echo "<div class='form-popup' id='myPopupAction$switch->SerialNo$switch->SwitchNo'>
            <div class='form-container' style='width:550px;padding:0px 50px;'>
            <div class='modal-header' style='width:100%;height:60px;'>
            <button class='close' onclick='closeForm(&#39;$switch->SerialNo$switch->SwitchNo&#39;)'></button>
            <h3 class='h2form'>Smart Switch Remote</h3>
            </div>
            <div class='modal-body' style='width:100%;'>
            <div class='grid-container'>";
            if ($Timer24H_Enabled == 'true') {
              echo "<div><input style='width:50px;' class='open24' type='image' src='Images/Switch/24TimerOn.png' alt='stop' onclick='choose24Timer(&#39;$Timer24Info&#39;)'></div>";
            } else {
              echo "<div><input style='width:50px;' class='open24' type='image' src='Images/Switch/24TimerOff.png' alt='stop' onclick='choose24Timer(&#39;$Timer24Info&#39;)'></div>";
            }

            if ($isChecked == 'checked') {
              if ($switch->SwitchMode == 0) {
                echo "<div><input id='switchPop$switch->SerialNo$switch->SwitchNo' class='switchImage' type='image' onclick='actionSwitch(&#39;$SwitchInfo&#39;, &#39;$switch->Name&#39;, &#39;$switch->SwitchMode&#39;)' src='Images/Switch/lightbulb_on.svg' alt='switchon'></div>";
              } else {
                echo "<div>
                  <div class='container'>
                  <input id='switchPop$switch->SerialNo$switch->SwitchNo' class='switchImage' type='image' onclick='actionSwitch(&#39;$SwitchInfo&#39;, &#39;$switch->Name&#39;, &#39;$switch->SwitchMode&#39;)' src='Images/Switch/dimmerOnTimerOff.png' alt='switchon'>
                  <p style='color:#0097e6;margin-top:5px;cursor: pointer;' id='dimmerPop$switch->SerialNo$switch->SwitchNo' onclick='openDimmerForm(&#39;$SwitchInfo&#39;, &#39;$switch->Name&#39;, &#39;Modal&#39;)'>$switch->FireLevel%</p>
                  </div>
                  </div>";
              }
            } else {
              if ($switch->SwitchMode == 0) {
                echo "<div><input id='switchPop$switch->SerialNo$switch->SwitchNo' class='switchImage' type='image' onclick='actionSwitch(&#39;$SwitchInfo&#39;, &#39;$switch->Name&#39;, &#39;$switch->SwitchMode&#39;)' src='Images/Switch/lightbulb_off.svg' alt='switchoff'></div>";
              } else {
                echo "<div>
                  <div class='container'>
                  <input id='switchPop$switch->SerialNo$switch->SwitchNo' class='switchImage' type='image' onclick='actionSwitch(&#39;$SwitchInfo&#39;, &#39;$switch->Name&#39;, &#39;$switch->SwitchMode&#39;)' src='Images/Switch/dimmerOffTimerOff.png' alt='switchon'>
                  <p style='margin-top:5px;cursor: pointer;' id='dimmerPop$switch->SerialNo$switch->SwitchNo' onclick='openDimmerForm(&#39;$SwitchInfo&#39;, &#39;$switch->Name&#39;, &#39;Modal&#39;)'>$switch->FireLevel%</p>
                  </div>
                  </div>";
              }
            }

            if ($TimerDelay_Enabled == 'true') {
              echo "<div><input  style='width:50px;' class='open' type='image' src='Images/Switch/delayTimerOn.png' alt='open' onclick='openDelayForm(&#39;$DelayInfo&#39;)'></div>";
            } else {
              echo "<div><input  style='width:50px;' class='open' type='image' src='Images/Switch/delayTimerOff.png' alt='open' onclick='openDelayForm(&#39;$DelayInfo&#39;)'></div>";
            }
            echo "</div>
                  </div>
                  </div>
                  </div>";
          }
        }
      } else {
        echo "<h2 class='error'><i class='fas fa-exclamation-circle fa-5x' style='color:red;'></i> </br></br>
        Smart Switch Not Available For This Selected Gateway </br>
        Please Back To Home Page</h2>";
      }
    } else {
      echo "<h2 class='error'><i class='fas fa-exclamation-circle fa-5x' style='color:red;'></i> </br></br>
      Smart Switch Not Available For This Selected Gateway </br>
      Please Back To Home Page</h2>";
    }
  }

  ?>
  <input style='display:none;' id='IntervalId' value='none'>
  <input style='display:none;' class='RSSI'>
  <input style='display:none;' class='serial'>
  <input style='display:none;' class='switchno'>
  <input style='display:none;' class='timerno'>
  <input style='display:none;' class='SwitchInfo'>
  <input style='display:none;' class='name'>
  <input style='display:none;' class='SwitchFire'>
  <input style='display:none;' class='LocationName'>

  <!-- Delay Timer popup -->
  <div class="form-popup" id="myDelayForm">
    <div class="form-container" style='width:550px;padding:0px;'>
      <div class="modal-header" style='width:100%;height:60px;'>
        <a class='back' onclick='back()'><span class='fa fa-arrow-left fa-2x'></a>
        <h3 class='h2form'>Switch Delay Timer</h3>
      </div>
      <div class="modal-body" style='width:100%;'>
        <h4 class='title' style='font-weight:700;'>Status</h4>
        <h5 id='delayTimer' class='title' style='margin-bottom: 30px;'></h5>
        <!-- <form method='post' class='form'> -->
        <div class='omrs-input-group' style='margin-left:auto;margin-right:auto;'>
          <div class='omrs-input-underlined omrs-input-danger'>
            <label id='labelaction' for='actions'>Action :</label>
            <select id='actions' name='actions'>
              <option value='chooseaction' selected disabled>Choose action</option>
              <option value='On'>On</option>
              <option value='Off'>Off</option>
            </select>
            <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'>
              <path fill='none' d='M0 0h24v24H0V0z' />
              <circle cx='15.5' cy='9.5' r='1.5' />
              <circle cx='8.5' cy='9.5' r='1.5' />
              <path d='M10.01,21.01c0,1.1 0.89,1.99 1.99,1.99s1.99,-0.89 1.99,-1.99h-3.98zM18.88,16.82L18.88,11c0,-3.25 -2.25,-5.97 -5.29,-6.69v-0.72C13.59,2.71 12.88,2 12,2s-1.59,0.71 -1.59,1.59v0.72C7.37,5.03 5.12,7.75 5.12,11v5.82L3,18.94L3,20h18v-1.06l-2.12,-2.12zM16,13.01h-3v3h-2v-3L8,13.01L8,11h3L11,8h2v3h3v2.01z' /></svg>
          </div>
        </div>
        <div class='omrs-input-group' style='margin-left:auto;margin-right:auto;'>
          <div class='omrs-input-underlined ' style='width:100%;'>
            <label for='quantityHours'>Hours :</label>
            <input type='number' id='hours' name='hours' min='1' max='23' required></br></br>
            <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'>
              <path fill='none' d='M0 0h24v24H0V0z' />
              <circle cx='15.5' cy='9.5' r='1.5' />
              <circle cx='8.5' cy='9.5' r='1.5' />
              <path d='M22,5.72l-4.6,-3.86 -1.29,1.53 4.6,3.86L22,5.72zM7.88,3.39L6.6,1.86 2,5.71l1.29,1.53 4.59,-3.85zM12.5,8L11,8v6l4.75,2.85 0.75,-1.23 -4,-2.37L12.5,8zM12,4c-4.97,0 -9,4.03 -9,9s4.02,9 9,9c4.97,0 9,-4.03 9,-9s-4.03,-9 -9,-9zM12,20c-3.87,0 -7,-3.13 -7,-7s3.13,-7 7,-7 7,3.13 7,7 -3.13,7 -7,7z' /></svg>
          </div>
        </div>
        <div class='omrs-input-group' style='margin-left:auto;margin-right:auto;'>
          <div class='omrs-input-underlined' style='width:100%;'>
            <label for='quantityMinutes'>Minutes :</label>
            <input type='number' id='minutes' name='minutes' min='1' max='59' required></br></br>
            <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'>
              <path fill='none' d='M0 0h24v24H0V0z' />
              <circle cx='15.5' cy='9.5' r='1.5' />
              <circle cx='8.5' cy='9.5' r='1.5' />
              <path d='M22,5.72l-4.6,-3.86 -1.29,1.53 4.6,3.86L22,5.72zM7.88,3.39L6.6,1.86 2,5.71l1.29,1.53 4.59,-3.85zM12.5,8L11,8v6l4.75,2.85 0.75,-1.23 -4,-2.37L12.5,8zM12,4c-4.97,0 -9,4.03 -9,9s4.02,9 9,9c4.97,0 9,-4.03 9,-9s-4.03,-9 -9,-9zM12,20c-3.87,0 -7,-3.13 -7,-7s3.13,-7 7,-7 7,3.13 7,7 -3.13,7 -7,7z' /></svg>
          </div>
        </div>
        <!-- </form> -->
      </div>
      <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
        <button type='button' class='all' onclick='myDelayTimerFunction()'>Enable Timer</button>
        <button type='button' class='all' onclick='disableDelayTimer()'>Disable Timer</button>
      </div>
    </div>
  </div>

  <!-- 24 Hour Timer popup -->
  <div class="form-popup" id="my24HForm">
    <div class="form-container" id="my24HContainer" style='width:550px;padding:0px;'>
      <div class="modal-header" style='width:100%;height:60px;'>
        <a class='back' onclick='back2back()'><span class='fa fa-arrow-left fa-2x'></a>
        <h3 id='24Timer' class='h2form'></h3>
      </div>
      <div class="modal-body" style='width:100%;'>
        <h4 class='title' style='font-weight:700;'>Status</h4>
        <h6 id='statusOn' class='title'></h6>
        <h6 id='statusOff' class='title'></h6>
        <h6 id='status' class='title'></h6>
        <!-- <form method='post' class='form'> -->
        <div style="float:left;" class="my24HDiv">
          <h4 style='font-weight:700;' id='onTimer'></h4>
          <div class='omrs-input-group' style='margin-left:auto;margin-right:auto;'>
            <label class='omrs-input-underlined omrs-input-danger' id='enableTimer'>Enable ON Timer :</label>
            <label class='switch'>
              <?php echo "<input type='checkbox' id='switch1on' onclick='disableOn()'>"; ?>
              <span class='slider round'></span>
            </label>
          </div>
          <div class='omrs-input-group' style='margin-left:auto;margin-right:auto;'>
            <label class='omrs-input-underlined ' style='width:100%;'>
              <label for='quantityHours' id='setTime'>Set ON Time :</label>
              <div>
                <div class='row'>
                  <div class='col-xs-12 col-md-8'>
                    <div class='form-group'>
                      <div class='input-group date' id='datetimepicker1'>
                        <input value='00:00' type='text' class='form-control' id='onTime' />
                        <span class='input-group-addon'>
                          <span class='glyphicon glyphicon-time'></span>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </label>
          </div>
          <div class='omrs-input-group' style='margin-left:auto;margin-right:auto;'>
            <div class='omrs-input-underlined' style='width:100%;'>
              <label for='quantityDays' id='setDay'>Set ON Day :</label>
              <div id='onCheckbox'>
                <input type='checkbox' id='onday1' name='dayOnCheck' value='1'>
                <label> Every Sunday</label><br>
                <input type='checkbox' id='onday2' name='dayOnCheck' value='2'>
                <label> Every Monday</label><br>
                <input type='checkbox' id='onday3' name='dayOnCheck' value='4'>
                <label> Every Tuesday</label><br>
                <input type='checkbox' id='onday4' name='dayOnCheck' value='8'>
                <label> Every Wednesday</label><br>
                <input type='checkbox' id='onday5' name='dayOnCheck' value='16'>
                <label> Every Thursday</label><br>
                <input type='checkbox' id='onday6' name='dayOnCheck' value='32'>
                <label> Every Friday</label><br>
                <input type='checkbox' id='onday7' name='dayOnCheck' value='64'>
                <label> Every Saturday</label><br>
              </div>
            </div>
          </div>
          <!-- </form> -->
        </div>

        <!-- <div style="width:200px;display:inline-block">
        </div> -->
        <div style="float:right;" class="my24HDiv">
          <h4 style='font-weight:700;' id='offTimer'></h4>
          <div class='omrs-input-group' style='margin-left:auto;margin-right:auto;'>
            <label class='omrs-input-underlined omrs-input-danger' id='enableTimer'>Enable OFF Timer :</label>
            <label class='switch'>
              <?php echo "<input type='checkbox' id='switch1off' onclick='disableOff()'>"; ?>
              <span class='slider round'></span>
            </label>
          </div>
          <div class='omrs-input-group' style='margin-left:auto;margin-right:auto;'>
            <label class='omrs-input-underlined ' style='width:100%;'>
              <label for='quantityHours' id='setTime'>Set OFF Time :</label>
              <div>
                <div class='row'>
                  <div class='col-xs-12 col-md-8'>
                    <div class='form-group'>
                      <div class='input-group date' id='datetimepicker2'>
                        <input value='00:00' type='text' class='form-control' id='offTime' />
                        <span class='input-group-addon'>
                          <span class='glyphicon glyphicon-time'></span>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </label>
          </div>
          <div class='omrs-input-group' style='margin-left:auto;margin-right:auto;'>
            <div class='omrs-input-underlined' style='width:100%;'>
              <label for='quantityDays' id='setDay'>Set OFF Day :</label>
              <div id='offCheckbox'>
                <input type='checkbox' id='offday1' name='dayOffCheck' value='1'>
                <label> Every Sunday</label><br>
                <input type='checkbox' id='offday2' name='dayOffCheck' value='2'>
                <label> Every Monday</label><br>
                <input type='checkbox' id='offday3' name='dayOffCheck' value='4'>
                <label> Every Tuesday</label><br>
                <input type='checkbox' id='offday4' name='dayOffCheck' value='8'>
                <label> Every Wednesday</label><br>
                <input type='checkbox' id='offday5' name='dayOffCheck' value='16'>
                <label> Every Thursday</label><br>
                <input type='checkbox' id='offday6' name='dayOffCheck' value='32'>
                <label> Every Friday</label><br>
                <input type='checkbox' id='offday7' name='dayOffCheck' value='64'>
                <label> Every Saturday</label><br>
              </div>
            </div>
          </div>
          <!-- </form> -->
        </div>
      </div>
      <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
        <button type='button' class='all' onclick='my24TimerFunction()'>Enable Timer</button>
        <button type='button' class='all' onclick='disable24Timer()'>Disable Timer</button>
      </div>
    </div>
  </div>

  <!-- Choose Timer popup -->
  <div class="form-popup" id="choose24Timer">
    <div class="form-container" style='width:550px;padding:0px;'>
      <div class="modal-header" style='width:100%;height:60px;'>
        <a class='back' onclick='back()'><span class='fa fa-arrow-left fa-2x'></a>
        <h3 class='h2form'>Set 24 Hour Timer</h3>
      </div>
      <div class="modal-body" style='width:100%;'>
        <div class="btnTimer">
          <button id='btnt1' class='all' type="button" onclick='open24HForm(1)'>24 Hour Timer 1</button>
        </div>
        <p id='t1' style='text-align: center;'></p>
        <p id='actionOn1' style='text-align: center;'></p>
        <p id='actionOff1' style='text-align: center;'></p>
      </div>
      <div class="modal-body" style='width:100%;'>
        <div class="btnTimer">
          <button id='btnt2' class='all' type="button" onclick='open24HForm(2)'>24 Hour Timer 2</button>
        </div>
        <p id='t2' style='text-align: center;'></p>
        <p id='actionOn2' style='text-align: center;'></p>
        <p id='actionOff2' style='text-align: center;'></p>
      </div>
    </div>
  </div>

  <!--Log Form -->
  <div class='form-popup' id='SwitchLogForm'>
    <div class='form-container' style='width:650px;padding:0px;'>
      <div class="modal-header" style='width:100%;height:60px;'>
        <button class='close' onclick='backLog("SwitchLogForm")'></button>
        <h3 class="h2form">Logs</h3>
      </div>
      <div class="modal-body" style='width:100%;max-height: calc(80vh - 210px);overflow-y: auto;'>
        <h4 class="h2form" id='logName'></h4>
        <div id="emptyLog">

        </div>
        <div id="tableLog">
          <table class="table table-bordered table-hover" style="text-align:center;">
            <thead>
              <th>#</th>
              <th>Date Time</th>
              <th>Status</th>
            </thead>
            <tbody id="SwitchLogTable">

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <!-- Dimmer Control Form -->
  <div class='form-popup' id='DimmerContorlForm'>
    <div class='form-container' style='width:450px;padding:0px;'>
      <div class="modal-header" id='headerDimmer' style='width:100%;height:60px;'>
        <h3 class="h2form">Dimmer Control</h3>
      </div>
      <div class="modal-body" style='width:100%;'>
        <input type="range" name="dimmerInputName" id="dimmerInputId" min="0" max="100" oninput="dimmerOutputId.value = dimmerInputId.value">
        <output name="dimmerOutputName" id="dimmerOutputId" style='text-align:center;'></output>
      </div>
      <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
        <button type='button' class='all' onclick='myDimmerFunction()'>Ok</button>
      </div>
    </div>
  </div>

  <!-- Form for sign Door Lock -->
  <div class="form-popup" id="mySiginDoorLockForm">
    <div class="form-container" style="width:400px;padding:0px;">
      <div class="modal-header" style='width:100%;height:70px;'>
        <!-- <a class='back' onclick='closeFormSigin()'><span class='fa fa-arrow-left fa-2x'></a> -->
        <h3 class='h2form'>Manage Door Lock </br> Password</h3>
      </div>
      <div class="modal-body" style='width:100%;'>
        <fieldset style='width:100%;'>
          <label for="name">Enter Admin Password:</label>
          <input type="password" id="password" name="DoorlockPassword" style='width:100%;'>
        </fieldset>
      </div>
      <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
        <button type="submit" class="all" onclick='openGetLock()' name="signin">Ok</button>
        <button type="button" class="all" onclick="cancelFormSigin()">Cancel</button>
      </div>
    </div>
  </div>

  <!-- SideBar + Navbar -->
  <div class="wrapper">
    <!-- Sidebar Holder -->
    <nav id="sidebar">
      <div class="sidebar-header">
        <h3>Senzo</h3>
        <strong>SZ</strong>
      </div>
      <ul class="list-unstyled components">
        <!-- Home Section -->
        <li>
          <form action='SmartSwitch.php' method='POST' id='NavHome' style='margin-block-end: 0'>
            <input style='display:none;' name='Home'>
            <a style='cursor:pointer;' class='Home'>
              <i class="glyphicon glyphicon-home"></i>
              Home
            </a>
          </form>
        </li>
        <li>
          <!-- Device Section -->
          <a href="#pageDevice" data-toggle="collapse" aria-expanded="false">
            <i class="glyphicon glyphicon-tasks"></i>
            <?php echo "Device"; ?>
          </a>
          <ul class="collapse list-unstyled in" id="pageDevice">
            <?php
            if ($SmartSwitchStatus) {
              echo '<li><a id="cube" style="cursor:pointer;" onclick="selectSwitch()">Smart Switch</a></li>';
            }
            if ($CurtainStatus) {
              echo '<li><a style="cursor:pointer;" onclick="selectCurtain()">Smart Curtain</a></li>';
            }
            if ($ShutterStatus) {
              echo '<li><a style="cursor:pointer;" onclick="selectShutter()">Smart Shutter</a></li>';
            }
            if ($LockStatus) {
              echo '<li><a style="cursor:pointer;" onclick="selectDoorLock()">Smart Door Lock</a></li>';
            }
            if ($IRBlasterStatus) {
              echo '<li><a style="cursor:pointer;" onclick="selectIRBlaster()">IR Blaster</a></li>';
            }
            ?>
          </ul>
        </li>
        <li>
          <!-- Scene Section -->
          <a style="cursor:pointer;" onclick='openScenesForm()'>
            <i class="glyphicon glyphicon-film"></i>
            <?php echo "Scene ($SceneCount)"; ?>
          </a>
        </li>
        <li>
          <!-- Security Section -->
          <a href="#pageSecurity" data-toggle="collapse" aria-expanded="false">
            <i class="glyphicon glyphicon-lock"></i>
            <?php echo "Security"; ?>
          </a>
          <ul class="collapse list-unstyled" id="pageSecurity">
            <?php
            // If smart alarm connected to gateway
            if ($AlarmStatus) {
              echo "<li><a style='cursor:pointer;' onclick='selectAlarm(&#39;True&#39;)'>
                            Smart Alarm <img src='Images/Action/security_armed_icon.png' alt='scenes' style='width:20px;height:20px;'>";
            } else {
              echo "<li><a style='cursor:pointer;' onclick='selectAlarm(&#39;False&#39;)'>
                            Smart Alarm";
            }
            ?>
            </a>
        </li>
        <li><a style="cursor:pointer;" onclick="selectCamera()">Camera</a></li>
      </ul>
      </li>
      <li>
        <!-- Analytic Section -->
        <a style='cursor:pointer;' onclick='openAnalyticForm()'>
          <i class='glyphicon glyphicon-stats'></i>
          Analytic
        </a>
      </li>
      <li>
        <!-- Setting Section -->
        <a style='cursor:pointer;' onclick='openSettingForm()'>
          <i class='glyphicon glyphicon-cog'></i>
          Setting
        </a>
      </li>
      </ul>

      <!-- Logout button -->
      <ul class="list-unstyled CTAs">
        <li>
          <form method="POST" action="SmartSwitch.php" id='NavLogout' style='margin-block-end: 0'>
            <input style='display:none;' name='Logout'>
            <a style="cursor:pointer;" class="Logout">
              <i class="glyphicon glyphicon-log-out"></i>
              Logout
            </a>
          </form>
        </li>
      </ul>
    </nav>

    <!-- Page Content Holder -->
    <div id="content" style="width: -webkit-fill-available;">

      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
              <i class="glyphicon glyphicon-align-left"></i>
              <span></span>
            </button>

            <!-- For responsive part -->
            <!-- Show and hide content that been targeted-->
            <button class="btn btn-dark navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <i class="glyphicon glyphicon-align-justify"></i>
            </button>
          </div>

          <!-- Content that have been targeted -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" style='background: #fff;cursor:pointer;'>Location</a>
                <ul class="dropdown-menu">
                  <?php
                  $LocationListArray = $_SESSION['location'];
                  if (is_array($LocationListArray) || is_object($LocationListArray)) {
                    foreach ($LocationListArray as $location) {
                      echo "<li>";
                      echo "<input style='display:none;' value='$location->LocationName' id='Location$location->LocationName'>";
                      echo "<a style='cursor:pointer;' class='LocationListItem'>$location->LocationName</a>";
                      echo "</li>";
                    }
                  }
                  ?>
                </ul>
              </li>
              <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" style='background: #fff;cursor:pointer;'>Switch Status Filter</a>
                <ul class="dropdown-menu">
                  <li>
                    <input style='display:none;' value='100' id='FireLevelValueOn'>
                    <a style='cursor:pointer;' class='FireLevelItem'>On</a>
                  </li>
                  <li>
                    <input style='display:none;' value='0' id='FireLevelValueOff'>
                    <a style='cursor:pointer;' class='FireLevelItem'>Off</a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </nav>

      <div class='filter' id='collapseFilter'>
        <span class='filer-title'> Filtered By : </span>
      </div>

      <h2>List of Available Switch</h2>
      <?php
      $GatewayLevel = $_SESSION['level'];
      echo "<h4 class='Level' >Gateway $GatewayLevel</h4>";
      getList($msgJson);
      ?>
      <div class="line"></div>

    </div>
  </div>

  <!-- Auto Refresh dropdown -->
  <div class='autorefresh'>
    <div class="dropdown">
      <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">
        <img src='Images/Switch/settings_gear.png' alt='switchon' style='width:30px;height:30px;animation: spin 3s linear infinite;'>
      </button>
      <ul class="dropdown-menu">
        <li>
          <input type='checkbox' id='showSn' value='showSn'>
          <label> Show SN</label><br>
        </li>
        <li>
          <input type='checkbox' id='enable' name='fooby[2][]'>
          <label> Enable</label><br>
          <div id='timeRefresh' style='display:none;margin-left:10px;'>
            <input type='checkbox' value='1-minute' class="slectOne" id='1-minute'>
            <label> 1 minute</label><br>
            <input type='checkbox' value='5-minutes' class="slectOne" id='5-minutes'>
            <label> 5 minutes</label><br>
            <input type='checkbox' value='10-minutes' class="slectOne" id='10-minutes'>
            <label> 10 minutes</label><br>
            <input type='checkbox' value='20-minutes' class="slectOne" id='20-minutes'>
            <label> 20 minutes</label><br>
            <input type='checkbox' value='25-minutes' class="slectOne" id='25-minutes'>
            <label> 25 minutes</label><br>
            <input type='checkbox' value='1-hour' class="slectOne" id='1-hour'>
            <label> 1 hour</label><br>
          </div>
        </li>
        <li>
          <input type='checkbox' id='disable' value='disable'>
          <label> Disable</label><br>
        </li>
      </ul>
    </div>
  </div>

 <!-- Loading animation -->
  <div class="form-popup" id='loader'>
    <!-- <div id="loadering" class="loader"></div> -->
    <div class="loading-container">
      <div class="loading"></div>
      <div class="loading-text">loading</div>
    </div>
  </div>

</body>

</html>