<?php
session_start();
ini_set('max_execution_time', 30);

//To include all the variable in serverConfig file
include '../configuration/serverConfig.php';

//To use all the function in php MQTT library
require("../phpMQTTv2.php");

$mqtt = new phpMQTT($server, $port, $client_id, $cafile);

//Connect to the server using mqtt
if ($mqtt->connect(true, NULL, $username, $password)) {

    //Session gateway serial number that have been convert using generateSerial.php
	$SerialGateway = $_SESSION['serialGateway'];
	//Session password that have been encrypted for login command
	$key = $_SESSION['key'];

	//Publish and subscribe here
	//Note : All first three command must be include before proceed to next command

	//This command is to identify server
	//Must be use before publish any other command
	//This command do not have server reply, so do need to use subscribeAndWaitForMessage function.
	$mqtt->publish("Senzo/Mobile/Client", "ServerIdentityMQTT {Type:4, Username:'', Password:''}", 0);
	//To prevent from getting error in GatewayInfo command
	sleep(1);

    //This command is use to check whether the gateway is online or not and check gateway version
    $mqtt->publish("Senzo/Mobile/Client", "GatewayInfo {'To':'$SerialGateway'}", 0);
    //Get error message if have one
    getError($mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0));

    //This command is use to authentication the user
    $mqtt->publish("Senzo/Mobile/Client", "loginAPI {'To':'$SerialGateway','key':'$key'}", 0);
    //Get error message if have one
    getError($mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0));

    //This command is use the get smart alarm controller data
    $mqtt->publish("Senzo/Mobile/Client", "GetSmartAlarmParadoxAPI {'To':'$SerialGateway'}", 0);
    //Get server reply
    echo $mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);
} else {
    echo "Time out!\n";
}

function getError($message)
{
    $msgJson = json_decode($message);
    //If there are any message, that mean error have occur
    if (!empty($msgJson->Message)) {
        //Get the error reply
        echo $message;
        die();
    }
}
$mqtt->close();
