<?php
session_start();
include '../configuration/serverConfig.php';
ini_set('max_execution_time', 30);

require("../phpMQTTv2.php");

$mqtt = new phpMQTT($server, $port, $client_id, $cafile);

if ($mqtt->connect(true, NULL, $username, $password)) {

	$SerialGateway = $_SESSION['serialGateway'];
	$Password = $_SESSION['password'];
	$key = $_SESSION['key'];

	$serialno = ($_GET['SerialNo']);
	$switchno = ($_GET['SwitchNo']);
	$timerno = ($_GET['TimerNo']);
	$onenable = ($_GET['OnEnable']);
	$offenable = ($_GET['OffEnable']);
	$ontime = ($_GET['OnTime']);
	$offtime = ($_GET['OffTime']);
	$ondays = ($_GET['OnDays']);
	$offdays = ($_GET['OffDays']);

	filter_var($onenable, FILTER_VALIDATE_BOOLEAN);
	filter_var($offenable, FILTER_VALIDATE_BOOLEAN);

	//Publish
	$mqtt->publish("Senzo/Mobile/Client", "ServerIdentityMQTT {Type:4, Username:'', Password:''}", 0);
	sleep(1);

	//GET SEED
	$mqtt->publish("Senzo/Mobile/Client", "GatewayInfo {'To':'$SerialGateway'}", 0);
	$mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);

	//LOGIN WITH ENCRYPTION
	$mqtt->publish("Senzo/Mobile/Client", "loginAPI {'To':'$SerialGateway','key':'$key'}", 0);
	$mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);

	//SET 24 HOUR TIMER
	$mqtt->publish("Senzo/Mobile/Client", "SetSmartCurtain24HoursTimer {'To':'$SerialGateway', SerialNo: '$serialno', SwitchNo: '$switchno', 
		TimerNo: '$timerno', OnEnable: '$onenable', OffEnable: '$offenable', OnTime: '$ontime', OffTime: '$offtime', OnDays: '$ondays', OffDays: '$offdays' }", 0);
	echo $mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);
} else {
	echo "Time out!\n";
}

$mqtt->close();
