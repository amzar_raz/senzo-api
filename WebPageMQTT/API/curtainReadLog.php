<?php
session_start();
ini_set('max_execution_time', 30);

//To include all the variable in serverConfig file
include '../configuration/serverConfig.php';

//To use all the function in php MQTT library
require("../phpMQTTv2.php");

$mqtt = new phpMQTT($server, $port, $client_id, $cafile);

//Connect to the server using mqtt
if ($mqtt->connect(true, NULL, $username, $password)) {

    //Session gateway serial number that have been convert using generateSerial.php
	$SerialGateway = $_SESSION['serialGateway'];
	//Session password that have been encrypted for login command
	$key = $_SESSION['key'];

    //Get all data from url
    $serialno = $_GET['SerialNo'];
    $switchno = $_GET['SwitchNo'];

	//Publish and subscribe here
	//Note : All first three command must be include before proceed to next command

	//This command is to identify server
	//Must be use before publish any other command
	//This command do not have server reply, so do need to use subscribeAndWaitForMessage function.
	$mqtt->publish("Senzo/Mobile/Client", "ServerIdentityMQTT {Type:4, Username:'', Password:''}", 0);
	//To prevent from getting error in GatewayInfo command
	sleep(1);

    //This command is use to check whether the gateway is online or not and check gateway version
    $mqtt->publish("Senzo/Mobile/Client", "GatewayInfo {'To':'$SerialGateway'}", 0);
    //Get error message if have one
    getError($mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0));

    //This command is use to authentication the user
    $mqtt->publish("Senzo/Mobile/Client", "loginAPI {'To':'$SerialGateway','key':'$key'}", 0);
    //Get error message if have one
    getError($mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0));

    //This command is use to get all log from selected curtain
    $mqtt->publish("Senzo/Mobile/Client", "ReadSmartCurtainLogAPI {'To':'$SerialGateway', SerialNo: '$serialno', SwitchNo: '$switchno'}", 0);
    //Decode the server reply to use for decompression
    $msgJson = json_decode($mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0));

    //To decompress the whole data
    $Compression = $msgJson->Compression;
    $decode = base64_decode($Compression);
    $Decompress = gzdecode($decode);
    $msgJsonCompression =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));

    //To decompress the log data
    $Compress = $msgJsonCompression->DataLogList;
    $decode = base64_decode($Compress);
    $Decompress = gzdecode($decode);
    $msgJsonCompress =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));

    //Create an object to store all the data and arrange it in server reply format
    $object = new \stdClass();
    $object->From = $SerialGateway;
    $object->To = $client_id;
    $object->Command = "ReadSmartCurtainLogAPI";
    $object->TotalDataLog = $msgJsonCompression->TotalDataLog;
    $object->DataLogList = $msgJsonCompress->DataLogList;
    //Display the object in right format
    echo json_encode($object);
} else {
    echo "Time out!\n";
}

function getError($message)
{
    $msgJson = json_decode($message);
    if (!empty($msgJson->Message)) {
        echo $message;
        die();
    }
}
$mqtt->close();
