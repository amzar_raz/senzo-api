<?php
session_start();
include '../configuration/serverConfig.php';
ini_set('max_execution_time', 30);

require("../phpMQTTv2.php");

$mqtt = new phpMQTT($server, $port, $client_id, $cafile);

if ($mqtt->connect(true, NULL, $username, $password)) {

	$SerialGateway = $_SESSION['serialGateway'];
	$Password = $_SESSION['password'];
	$key = $_SESSION['key'];

	$LockId = ($_GET['LockId']);
	$PId = ($_GET['PId']);
	$TP = ($_GET['TP']);
	$TPEndDate = ($_GET['TPEndDate']);
	$TPEndTime = ($_GET['TPEndTime']);

	$date1 = substr($TPEndDate, 0, 6);
	$date2 = substr($TPEndDate, 8, 2);
	$newDate = $date1 . $date2;

	//Publish
	$mqtt->publish("Senzo/Mobile/Client", "ServerIdentityMQTT {Type:4, Username:'', Password:''}", 0);
	sleep(1);

	//GET SEED
	$mqtt->publish("Senzo/Mobile/Client", "GatewayInfo {'To':'$SerialGateway'}", 0);
	$mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);

	//LOGIN WITH ENCRYPTION
	$mqtt->publish("Senzo/Mobile/Client", "loginAPI {'To':'$SerialGateway','key':'$key'}", 0);
	$mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);

	//SET TEMPORARY PASSWORD
	$mqtt->publish("Senzo/Mobile/Client", "SetLockTemporaryPassword {'To':'$SerialGateway','LockId':'$LockId', 'PId':'$PId', 'TP$PId':'$TP', 'TP" . $PId . "EndDate':'$newDate', 'TP" . $PId . "EndTime':'$TPEndTime'}", 0);
	echo $mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);
} else {
	echo "Time out!\n";
}

$mqtt->close();
