<?php
session_start();
include '../configuration/serverConfig.php';
ini_set('max_execution_time', 30);

require("../phpMQTTv2.php");

$mqtt = new phpMQTT($server, $port, $client_id, $cafile);

if ($mqtt->connect(true, NULL, $username, $password)) {

	$SerialGateway = $_SESSION['serialGateway'];
	$Password = $_SESSION['password'];
	$key = $_SESSION['key'];

	$IRBlasterId = $_GET['IRBlasterId'];
	$DeviceID = $_GET['DeviceID'];
	$TypeId = $_GET['TypeId'];

	//Publish
	$mqtt->publish("Senzo/Mobile/Client", "ServerIdentityMQTT {Type:4, Username:'', Password:''}", 0);
	sleep(1);

	//GET SEED
	$mqtt->publish("Senzo/Mobile/Client", "GatewayInfo {'To':'$SerialGateway'}", 0);
	$mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);


	//LOGIN WITH ENCRYPTION
	$mqtt->publish("Senzo/Mobile/Client", "loginAPI {'To':'$SerialGateway','key':'$key'}", 0);
	$mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);

	//DELETE DEVICE
	$mqtt->publish("Senzo/Mobile/Client", "DeleteDeviceInIRBlaster  {'To':'$SerialGateway', IRBlasterID :'$IRBlasterId', DeviceID :'$DeviceID', TypeId :'$TypeId'}", 0);
	echo $mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);
} else {
	echo "Time out!\n";
}

$mqtt->close();
