<?php
session_start();
ini_set('max_execution_time', 30);
include '../configuration/serverConfig.php';

require("../phpMQTTv2.php");

$mqtt = new phpMQTT($server, $port, $client_id, $cafile);

if ($mqtt->connect(true, NULL, $username, $password)) {

    $SerialGateway = $_SESSION['serialGateway'];
    $Password = $_SESSION['password'];
    $key = $_SESSION['key'];

    $PageNo = $_GET['PageNo'];
    $PageSize = $_GET['PageSize'];


    //Publish
    $mqtt->publish("Senzo/Mobile/Client", "ServerIdentityMQTT {Type:4, Username:'', Password:''}", 0);
    sleep(1);

    //GET SEED
    $mqtt->publish("Senzo/Mobile/Client", "GatewayInfo {'To':'$SerialGateway'}", 0);
    $mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);

    //LOGIN WITH ENCRYPTION
    $mqtt->publish("Senzo/Mobile/Client", "loginAPI {'To':'$SerialGateway','key':'$key'}", 0);
    $mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);

    //GET NOTIFICATION
    $mqtt->publish("Senzo/Mobile/Client", "GetMessagesAPI {'To':'$SerialGateway', PageNo: '$PageNo', PageSize: '$PageSize'}", 0);
    // echo $mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);
    $msgJson = json_decode($mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0));

    //To decompress all the data from gateway
    $Compression = $msgJson->Compression;
    $decode = base64_decode($Compression);
    $Decompress = gzdecode($decode);
    $msgJsonCompression =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));

    //To decompress all the data from gateway
    $Compress = $msgJsonCompression->Messages;
    $decode = base64_decode($Compress);
    $Decompress = gzdecode($decode);
    $msgJsonCompress =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));

    $object = new \stdClass();
    $object->From = $SerialGateway;
    $object->To = $client_id;
    $object->Command = "GetMessagesAPI";
    $object->TotalMessages = $msgJsonCompression->TotalMessages;
    $object->Messages = $msgJsonCompress->Messages;
    echo json_encode($object);
} else {
    echo "Time out!\n";
}

$mqtt->close();
