<?php
session_start();
include '../configuration/serverConfig.php';
ini_set('max_execution_time', 30);

require("../phpMQTTv2.php");

$mqtt = new phpMQTT($server, $port, $client_id, $cafile);

if ($mqtt->connect(true, NULL, $username, $password)) {

	$SerialGateway = $_SESSION['serialGateway'];
	$Password = $_SESSION['password'];
	$key = $_SESSION['key'];

	$IRBlasterID = $_GET['IRBlasterID'];
	$TypeID = $_GET['TypeID'];
	$BrandID = $_GET['BrandID'];
	$Model = $_GET['Model'];
	$DeviceName = $_GET['DeviceName'];

	//Publish
	$mqtt->publish("Senzo/Mobile/Client", "ServerIdentityMQTT {Type:4, Username:'', Password:''}", 0);
	sleep(1);

	//GET SEED
	$mqtt->publish("Senzo/Mobile/Client", "GatewayInfo {'To':'$SerialGateway'}", 0);
	$mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);

	//LOGIN WITH ENCRYPTION
	$mqtt->publish("Senzo/Mobile/Client", "loginAPI {'To':'$SerialGateway','key':'$key'}", 0);
	$mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);

	//ADD DEVICE 
	$mqtt->publish("Senzo/Mobile/Client", "AddDeviceToIRBlaster   {'To':'$SerialGateway', IRBlasterID :'$IRBlasterID', TypeID :'$TypeID', BrandID :'$BrandID', Model :'$Model', DeviceName :'$DeviceName'}", 0);
	echo $mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);
} else {
	echo "Time out!\n";
}

$mqtt->close();
