<?php
session_start();
include '../configuration/serverConfig.php';
ini_set('max_execution_time', 30);

require("../phpMQTTv2.php");

$mqtt = new phpMQTT($server, $port, $client_id, $cafile);

if ($mqtt->connect(true, NULL, $username, $password)) {

    $SerialGateway = $_SESSION['serialGateway'];
    $key = $_SESSION['key'];

    //Publish
    $mqtt->publish("Senzo/Mobile/Client", "ServerIdentityMQTT {Type:4, Username:'', Password:''}", 0);
    sleep(1);

    //GET SEED
    $mqtt->publish("Senzo/Mobile/Client", "GatewayInfo {'To':'$SerialGateway'}", 0);
    $mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);

    //LOGIN WITH ENCRYPTION
    $mqtt->publish("Senzo/Mobile/Client", "loginAPI {'To':'$SerialGateway','key':'$key'}", 0);
    $mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);

    //GET LIST SCENE
    $mqtt->publish("Senzo/Mobile/Client", "GetSceneAPI {'To':'$SerialGateway'}", 0);
    $msgJson = json_decode($mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0));

    //To decompress all the data from gateway
    $Compression = $msgJson->Compression;
    $decode = base64_decode($Compression);
    $Decompress = gzdecode($decode);
    $msgJsonCompression =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));

    //To decompress all the data from gateway
    $Compress = $msgJsonCompression->Compress;
    $decode = base64_decode($Compress);
    $Decompress = gzdecode($decode);
    $msgJsonCompress =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));

    $object = new \stdClass();
    $object->From = $SerialGateway;
    $object->To = $client_id;
    $object->Command = "GetSceneAPI";
    $object->Reply = $msgJsonCompression->Reply;
    $object->SceneList = $msgJsonCompress->SceneList;
    echo json_encode($object);
} else {
    echo "Time out!\n";
}

$mqtt->close();
