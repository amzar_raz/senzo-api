<?php
session_start();
include '../configuration/serverConfig.php';
ini_set('max_execution_time', 30);

require("../phpMQTTv2.php");

$mqtt = new phpMQTT($server, $port, $client_id, $cafile);

if ($mqtt->connect(true, NULL, $username, $password)) {

    $key = $_SESSION['key'];
    $SerialGateway = $_SESSION['serialGateway'];

    $DeviceId = $_GET['DeviceId'];
    $Name = $_GET['Name'];
    $LocationName = $_GET['LocationName'];
    $URL = $_GET['URL'];
    $Port = $_GET['Port'];

    //Publish
    $mqtt->publish("Senzo/Mobile/Client", "ServerIdentityMQTT {Type:4, Username:'', Password:''}", 0);
    sleep(1);

    //GET SEED
    $mqtt->publish("Senzo/Mobile/Client", "GatewayInfo {'To':'$SerialGateway'}", 0);
    $mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);

    //LOGIN WITH ENCRYPTION
    $mqtt->publish("Senzo/Mobile/Client", "loginAPI {'To':'$SerialGateway','key':'$key'}", 0);
    $mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);

    //GET LIST SWITCH
    $mqtt->publish("Senzo/Mobile/Client", "UpdateGatewayCamera {'To':'$SerialGateway', 'DeviceId':'$DeviceId', 'Name':'$Name', 'LocationName':'$LocationName', 'URL':'$URL', 'Port':'$Port'}", 0);
    echo $mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);
} else {
    echo "Time out!\n";
}

$mqtt->close();
