<?php
session_start();
ini_set('max_execution_time', 30);
include '../configuration/serverConfig.php';

require("../phpMQTTv2.php");

$mqtt = new phpMQTT($server, $port, $client_id, $cafile);

if ($mqtt->connect(true, NULL, $username, $password)) {

	$SerialGateway = $_SESSION['serialGateway'];
	$key = $_SESSION['key'];

	//Publish
	$mqtt->publish("Senzo/Mobile/Client", "ServerIdentityMQTT {Type:4, Username:'', Password:''}", 0);
	sleep(1);

	//GET SEED
    $mqtt->publish("Senzo/Mobile/Client", "GatewayInfo {'To':'$SerialGateway'}", 0);
    $mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);

	//LOGIN WITH ENCRYPTION
	$mqtt->publish("Senzo/Mobile/Client", "loginAPI {'To':'$SerialGateway','key':'$key'}", 0);
	$mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);

	//ENABLE GOOGLE HOME
	$mqtt->publish("Senzo/Mobile/Client", "EnableGoogleHome  {'To':'$SerialGateway'}", 0);
	echo $mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);
} else {
	echo "Time out!\n";
}

$mqtt->close();
