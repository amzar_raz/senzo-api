<?php
session_start();
include '../configuration/serverConfig.php';
ini_set('max_execution_time', 30);

require("../phpMQTTv2.php");

$mqtt = new phpMQTT($server, $port, $client_id, $cafile);

if ($mqtt->connect(true, NULL, $username, $password)) {

    $key = $_SESSION['key'];

    $SerialGateway = $_GET['serialGateway'];
    $Password = $_GET['password'];
    $SerialNo = ($_GET['SerialNo']);
    $LockId = ($_GET['LockId']);

    //Publish
    $mqtt->publish("Senzo/Mobile/Client", "ServerIdentityMQTT {Type:4, Username:'', Password:''}", 0);
    sleep(1);

    //GET SEED
    $mqtt->publish("Senzo/Mobile/Client", "GatewayInfo {'To':'$SerialGateway'}", 0);
    getError($mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0));

    //LOGIN WITH ENCRYPTION
    $mqtt->publish("Senzo/Mobile/Client", "loginAPI {'To':'$SerialGateway','key':'$key'}", 0);
    getError($mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0));

    //GET TEMPORARY HISTORY
    $mqtt->publish("Senzo/Mobile/Client", "GetLockTemporaryHistory  {'To':'$SerialGateway','SerialNo':'$SerialNo', LockId: '$LockId'}", 0);
    echo $mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);
} else {
    echo "Time out!\n";
}

function getError($message)
{
    $msgJson = json_decode($message);
    if (!empty($msgJson->Message)) {
        echo $message;
        die();
    }
}
$mqtt->close();
