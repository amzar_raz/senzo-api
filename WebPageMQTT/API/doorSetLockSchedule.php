<?php
session_start();
include '../configuration/serverConfig.php';
ini_set('max_execution_time', 30);

require("../phpMQTTv2.php");

$mqtt = new phpMQTT($server, $port, $client_id, $cafile);

if ($mqtt->connect(true, NULL, $username, $password)) {

    $SerialGateway = $_SESSION['serialGateway'];
    $Password = $_SESSION['password'];
    $key = $_SESSION['key'];

    $LockId = ($_GET['LockId']);
    $PId = ($_GET['PId']);
    $SP = ($_GET['SP']);
    $SPDateStart = ($_GET['SPDateStart']);
    $SPTimeStart = ($_GET['SPTimeStart']);
    $SPDateEnd = ($_GET['SPDateEnd']);
    $SPTimeEnd = ($_GET['SPTimeEnd']);

    //Publish
    $mqtt->publish("Senzo/Mobile/Client", "ServerIdentityMQTT {Type:4, Username:'', Password:''}", 0);
    sleep(1);

    //GET SEED
    $mqtt->publish("Senzo/Mobile/Client", "GatewayInfo {'To':'$SerialGateway'}", 0);
    $mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);

    //LOGIN WITH ENCRYPTION
    $mqtt->publish("Senzo/Mobile/Client", "loginAPI {'To':'$SerialGateway','key':'$key'}", 0);
    $mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);

    //SET SCHEDULE PASSWORD
    $mqtt->publish("Senzo/Mobile/Client", "SetLockSchedulingPassword {'To':'$SerialGateway', LockId: '$LockId', PId: '$PId', SP$PId: '$SP', SP" . $PId . "DateStart: '$SPDateStart', SP" . $PId . "TimeStart: '$SPTimeStart', SP" . $PId . "DateEnd: '$SPDateEnd', SP" . $PId . "TimeEnd: '$SPTimeEnd'}", 0);
    echo $mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);
} else {
    echo "Time out!\n";
}

$mqtt->close();
