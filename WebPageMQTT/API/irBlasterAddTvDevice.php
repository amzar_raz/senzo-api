<?php
session_start();
include '../configuration/serverConfig.php';
ini_set('max_execution_time', 30);

require("../phpMQTTv2.php");

$mqtt = new phpMQTT($server, $port, $client_id, $cafile);

if ($mqtt->connect(true, NULL, $username, $password)) {

	$SerialGateway = $_SESSION['serialGateway'];
	$Password = $_SESSION['password'];
	$key = $_SESSION['key'];

	$IRBlasterId = $_GET['IRBlasterId'];
	$TypeId = $_GET['TypeId'];
	$BrandName = $_GET['BrandName'];
	$ModelId = $_GET['ModelId'];
	$DeviceName = $_GET['DeviceName'];

	//Publish
	$mqtt->publish("Senzo/Mobile/Client", "ServerIdentityMQTT {Type:4, Username:'', Password:''}", 0);
	sleep(1);

	//GET SEED
	$mqtt->publish("Senzo/Mobile/Client", "GatewayInfo {'To':'$SerialGateway'}", 0);
	$mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);

	//LOGIN WITH ENCRYPTION
	$mqtt->publish("Senzo/Mobile/Client", "loginAPI {'To':'$SerialGateway','key':'$key'}", 0);
	$mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);

	//ADD DEVICE 
	$mqtt->publish("Senzo/Mobile/Client", "AddDeviceToIRBlasterNew   {'To':'$SerialGateway', IRBlasterId :'$IRBlasterId', TypeId :'$TypeId', BrandName :'$BrandName', ModelId :'$ModelId', DeviceName :'$DeviceName'}", 0);
	echo $mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);
} else {
	echo "Time out!\n";
}

$mqtt->close();
