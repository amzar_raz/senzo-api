<?php
ini_set('max_execution_time', 30);
session_start();
ob_start();
//Include all file needed to use in this page
include '././configuration/serverConfig.php';
include './header/headerAll.php';
?>
<!DOCTYPE HTML>
<html>

<head>
    <title>Senzo Home</title>
    <link href="css/Dashboard.css" rel="stylesheet">
    <link rel="stylesheet" href="css/Sidebar.css">
    <!-- Sidebar and auto refresh function -->
    <script type="text/javascript" src="SidebarHandler.js"></script>
</head>
<script type="text/javascript">
    //Get Device Issue 
    function openIssue(ModuleStatus, Device) {
        var Info = JSON.parse(ModuleStatus);
        //Get the number of lost device per type
        var LostSmartSwitchList = Info.LostSmartSwitchList;
        var LostSmartCurtainList = Info.LostSmartCurtainList;
        var LostSmartShutterList = Info.LostSmartShutterList;
        var LostIRBlasterList = Info.LostIRBlasterList;
        var LostSACList = Info.LostSACList;
        var LowBattery = Info.LowBattery;
        var lostSmartLockList = Info.lostSmartLockList;

        if (Device == 'Switch') {
            if (LostSmartSwitchList.length > 0) {
                document.getElementById("deviceMessage").innerHTML = "Smart Switch is not responding. Make sure to power on the switch before you proceed.";
                document.getElementById("myInput").style.display = "block";
                for (i = 0; i < LostSmartSwitchList.length; i++) {
                    $("#issueBody").
                    append(
                        '<div class="info">' +
                        '<div class="card issueCard" style="border: 1px solid black;background-color:#2f3542;color: floralwhite;">' +
                        '<div class="card-header" style="padding:0px;border-bottom:1px solid #eee;">' +
                        '<p class="itemName" style="display:inline-block;">' + LostSmartSwitchList[i].Name + '</p>' + '<p style="display:inline-block;float:right;">' + 'Device Lost' + '</p>' +
                        '</div>' +
                        '<div class="card-body">' +
                        '<p>Serial: ' + LostSmartSwitchList[i].SerialNo + '</p>' +
                        '<p>Switch: ' + LostSmartSwitchList[i].SwitchNo + '</p>' +
                        '<p>Location: ' + LostSmartSwitchList[i].Location + '</p>' +
                        '</div>' +
                        '</div>' +
                        '</div>'
                    );
                }
            }
        } else if (Device == 'Curtain') {
            if (LostSmartCurtainList.length > 0) {
                document.getElementById("deviceMessage").innerHTML = "Smart Curtain is not responding. Make sure to power on the curtain before you proceed.";
                document.getElementById("myInput").style.display = "block";
                for (i = 0; i < LostSmartCurtainList.length; i++) {
                    $("#issueBody").
                    append(
                        '<div class="info">' +
                        '<div class="card issueCard" style="border: 1px solid black;background-color:#2f3542;color: floralwhite;">' +
                        '<div class="card-header" style="padding:0px;border-bottom:1px solid #eee;">' +
                        '<p class="itemName" style="display:inline-block;">' + LostSmartCurtainList[i].Name + '</p>' + '<p style="display:inline-block;float:right;">' + 'Device Lost' + '</p>' +
                        '</div>' +
                        '<div class="card-body">' +
                        '<p>Serial: ' + LostSmartCurtainList[i].SerialNo + '</p>' +
                        '<p>Location: ' + LostSmartCurtainList[i].Location + '</p>' +
                        '</div>' +
                        '</div>' +
                        '</div>'
                    );
                }
            }
        } else if (Device == 'Shutter') {
            if (LostSmartShutterList.length > 0) {
                document.getElementById("deviceMessage").innerHTML = "Smart Shutter is not responding. Make sure to power on the shutter before you proceed.";
                document.getElementById("myInput").style.display = "block";
                for (i = 0; i < LostSmartShutterList.length; i++) {
                    $("#issueBody").
                    append(
                        '<div class="info">' +
                        '<div class="card issueCard" style="border: 1px solid black;background-color:#2f3542;color: floralwhite;">' +
                        '<div class="card-header" style="padding:0px;border-bottom:1px solid #eee;">' +
                        '<p class="itemName" style="display:inline-block;">' + LostSmartShutterList[i].Name + '</p>' + '<p style="display:inline-block;float:right;">' + 'Device Lost' + '</p>' +
                        '</div>' +
                        '<div class="card-body">' +
                        '<p>Serial: ' + LostSmartShutterList[i].SerialNo + '</p>' +
                        '<p>Location: ' + LostSmartShutterList[i].Location + '</p>' +
                        '</div>' +
                        '</div>' +
                        '</div>'
                    );
                }
            }
        } else if (Device == 'DoorLock') {
            if (LowBattery.length > 0 || lostSmartLockList.length > 0) {
                document.getElementById("deviceMessage").innerHTML = "Smart Door Lock is having some issue. Make sure to fix it before you proceed.";
                document.getElementById("myInput").style.display = "block";
                for (i = 0; i < LowBattery.length; i++) {
                    $("#issueBody").
                    append(
                        '<div class="info">' +
                        '<div class="card issueCard" style="border: 1px solid black;background-color:#2f3542;color: floralwhite;">' +
                        '<div class="card-header" style="width:100%;padding:0px;border-bottom:1px solid #eee;">' +
                        '<p class="itemName" style="display:inline-block;">' + LowBattery[i].Name + '</p>' + '<p style="display:inline-block;float:right;">' + 'Low Battery' + '</p>' +
                        '</div>' +
                        '<div class="card-body">' +
                        '<p>Serial: ' + LowBattery[i].SerialNo + '</p>' +
                        '<p>Location: ' + LowBattery[i].Location + '</p>' +
                        '</div>' +
                        '</div>' +
                        '</div>'
                    );
                }

                for (i = 0; i < lostSmartLockList.length; i++) {
                    $("#issueBody").
                    append(
                        '<div class="info">' +
                        '<div class="card issueCard" style="border: 1px solid black;background-color:#2f3542;color: floralwhite;">' +
                        '<div class="card-header" style="padding:0px;border-bottom:1px solid #eee;">' +
                        '<p class="itemName" style="display:inline-block;">' + lostSmartLockList[i].Name + '</p>' + '<p style="display:inline-block;float:right;">' + 'Device Lost' + '</p>' +
                        '</div>' +
                        '<div class="card-body">' +
                        '<p>Serial: ' + lostSmartLockList[i].SerialNo + '</p>' +
                        '<p>Location: ' + lostSmartLockList[i].Location + '</p>' +
                        '</div>' +
                        '</div>' +
                        '</div>'
                    );
                }
            }
        } else if (Device == 'IrBlaster') {
            if (LostIRBlasterList.length > 0) {
                document.getElementById("deviceMessage").innerHTML = "Smart IR Blaster is not responding. Make sure to power on the ir blaster before you proceed.";
                document.getElementById("myInput").style.display = "block";
                for (i = 0; i < LostIRBlasterList.length; i++) {
                    $("#issueBody").
                    append(
                        '<div class="info">' +
                        '<div class="card issueCard" style="border: 1px solid black;background-color:#2f3542;color: floralwhite;">' +
                        '<div class="card-header" style="padding:0px;border-bottom:1px solid #eee;">' +
                        '<p class="itemName" style="display:inline-block;">' + LostIRBlasterList[i].Name + '</p>' + '<p style="display:inline-block;float:right;">' + 'Device Lost' + '</p>' +
                        '</div>' +
                        '<div class="card-body">' +
                        '<p>Serial: ' + LostIRBlasterList[i].SerialNo + '</p>' +
                        '<p>Location: ' + LostIRBlasterList[i].Location + '</p>' +
                        '</div>' +
                        '</div>' +
                        '</div>'
                    );
                }
            }
        } else if (Device == 'Alarm') {
            if (LostSACList.length > 0) {
                document.getElementById("deviceMessage").innerHTML = "Smart Alarm is not responding. Make sure to power on the SAC before you proceed.";
                document.getElementById("myInput").style.display = "block";
                for (i = 0; i < LostSACList.length; i++) {
                    $("#issueBody").
                    append(
                        '<div class="info">' +
                        '<div class="card issueCard" style="border: 1px solid black;background-color:#2f3542;color: floralwhite;">' +
                        '<div class="card-header" style="padding:0px;border-bottom:1px solid #eee;">' +
                        '<p class="itemName" style="display:inline-block;">' + LostSACList[i].Name + '</p>' + '<p style="display:inline-block;float:right;">' + 'Device Lost' + '</p>' +
                        '</div>' +
                        '<div class="card-body">' +
                        '<p>Serial: ' + LostSACList[i].SerialNo + '</p>' +
                        '<p>Location: ' + LostSACList[i].Location + '</p>' +
                        '</div>' +
                        '</div>' +
                        '</div>'
                    );
                }
            }
        } else if (Device == 'NoIssue') {
            document.getElementById("deviceMessage").innerHTML = "";
            document.getElementById("myInput").style.display = "none";
            $("#issueBody").
            append('<div class="info">' +
                '<img src="Images/smiling_bell.png" alt="smiling bell" width="100" height="100" class="smiley">' +
                '<h4 style="text-align:center;">No issues yet !</h4>' +
                '<p style="text-align:center;">Check this section to know the updates,news and general issues.</p>' +
                '</div>');
        }

        $("#myIssueForm").fadeIn("fast", "swing");
    }

    //Search Issue By Name
    function search() {
        try {
            var input, filter, ul, li, a, i, txtValue;
            input = document.getElementById('myInput');
            filter = input.value.toUpperCase();
            itemName = $(".itemName");
            DeviceIssueCard = $(".issueCard");

            // Loop through all list items, and hide those who don't match the search query
            for (i = 0; i < itemName.length; i++) {

                txtValue = $(itemName[i]).text();

                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    DeviceIssueCard[i].style.display = "";
                } else {
                    DeviceIssueCard[i].style.display = "none";
                }
            }
        } catch (e) {
            console.log(e.message);
        }
    }

    function back(id) {
        document.getElementById(id).style.display = "none"
        $(".info").remove();
        $(".noti").remove();
    }

    //Get Gateway Notification
    function openNotification() {

        var TotalUnreadMessages = document.getElementById("TotalUnreadMessages").value;
        var url = baseURL + "/gatewayNotification.php/?PageNo=" + 1 + "&PageSize=" + 50;
        var loader = document.getElementById("loader");
        loader.style.display = "block";

        $.getJSON(url, function(data) {
            loader.style.display = "none";
            var TotalMessages = data.TotalMessages;
            var TotalPage = TotalMessages / 50;
            TotalPage = Math.ceil(TotalPage);
            var Messages = data.Messages;

            document.getElementById("unreadMessage").innerHTML = TotalUnreadMessages + " Unread Message";
            for (i = 0; i < Messages.length; i++) {
                var field = Messages[i].DateTime.split("T");
                var Date = field[0].split('-');
                var newDate = Date[2] + "/" + Date[1] + "/" + Date[0].slice(2);
                var Time = tConvert(field[1]).split(':');
                var newTime = Time[0] + ":" + Time[1] + " " + Time[2].slice(2);
                $("#notificationBody").
                append(
                    '<div class="noti">' +
                    '<div class="card" style="border: 1px solid black;background-color:#2f3542;color: floralwhite;">' +
                    '<div class="card-body">' +
                    '<div style="display:inline-block;">' +
                    '<p>' + Messages[i].DeviceName + '</p>' +
                    '<p>' + Messages[i].Message + '</p>' +
                    '</div>' +
                    '<div style="display:inline-block;float:right;">' +
                    '<p>' + newTime + '</p>' +
                    '<p>' + newDate + '</p>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>'
                );
            }
            $("#myNotificationForm").fadeIn("fast", "swing");
            document.getElementsByClassName("TotalPage").value = TotalPage;
        });
    }

    //Get More Gateway Notification
    var Page = 2;
    jQuery(
        function($) {
            $('#notificationBody').bind('scroll', function() {
                if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                    var TotalPage = document.getElementsByClassName("TotalPage").value;
                    if (Page <= TotalPage && TotalPage != null) {
                        var loader = document.getElementById("loader");
                        loader.style.display = "block";
                        var url = baseURL + "/gatewayNotification.php/?PageNo=" + Page + "&PageSize=" + 50;
                        $.getJSON(url, function(data) {
                            loader.style.display = "none";
                            Messages = data.Messages;

                            for (i = 0; i < Messages.length; i++) {
                                var field = Messages[i].DateTime.split("T");
                                var Date = field[0].split('-');
                                var newDate = Date[2] + "/" + Date[1] + "/" + Date[0].slice(2);
                                var Time = tConvert(field[1]).split(':');
                                var newTime = Time[0] + ":" + Time[1] + " " + Time[2].slice(2);
                                $("#notificationBody").
                                append(
                                    '<div class="noti">' +
                                    '<div class="card" style="border: 1px solid black;background-color:#2f3542;color: floralwhite;">' +
                                    '<div class="card-body">' +
                                    '<div style="display:inline-block;">' +
                                    '<p>' + Messages[i].DeviceName + '</p>' +
                                    '<p>' + Messages[i].Message + '</p>' +
                                    '</div>' +
                                    '<div style="display:inline-block;float:right;">' +
                                    '<p>' + newTime + '</p>' +
                                    '<p>' + newDate + '</p>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>'
                                );
                            }


                        });
                        Page = Page + 1;
                    }
                }
            })
        }
    );

    //Convert 24 hour to 12 hour
    function tConvert(time) {
        // Check correct time format and split into components
        time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

        if (time.length > 1) { // If time format correct
            time = time.slice(1); // Remove full string match value
            time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
            time[0] = +time[0] % 12 || 12; // Adjust hours
        }
        return time.join(''); // return adjusted time or original string
    }
</script>

<body>
    <?php
    //Get all the session variable from ChooseGateway.php
    $SerialGateway = $_SESSION['serialGateway'];
    $Password = $_SESSION['password'];
    $gName = $_SESSION['name'];

    echo "<input style='display:none;' id='SerialGateway' value='$SerialGateway'>";

    if (isset($_POST['Logout'])) {
        header("Location: ChooseGateway.php");
        header("Refresh:0");
        ob_flush();
        session_destroy();
        exit();
    }

    if (isset($_POST['Home'])) {
        header("Refresh:0");
        ob_flush();
        exit();
    }

    //To use all the function in php MQTT library
    require("phpMQTTv2.php");

    //Define mqtt class
    $mqtt = new phpMQTT($server, $port, $client_id, $cafile);

    //Connect to the server using mqtt
    if ($mqtt->connect(true, NULL, $username, $password)) {

        //Publish and subscribe here

        //This command is to identify server
        //Must be use before publish any other command
        //This command do not have server reply, so do need to use subscribeAndWaitForMessage function.
        $mqtt->publish("Senzo/Mobile/Client", "ServerIdentityMQTT {Type:4, Username:'', Password:''}", 0);
        //To prevent from getting error in GatewayInfo command
        sleep(1);

        //This command is use to check whether the gateway is online or not and check gateway version
        $mqtt->publish("Senzo/Mobile/Client", "GatewayInfo {'To':'$SerialGateway'}", 0);
        $GatewayInfo = $mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0);
        //Get error message if have one
        getError($GatewayInfo);
        //Encrypt the password
        $key = getSeed($GatewayInfo, $Password);
    } else {
        echo "Time out!\n";
    }

    if ($mqtt->connect(true, NULL, $username, $password)) {

        //Publish
        $mqtt->publish("Senzo/Mobile/Client", "ServerIdentityMQTT {Type:4, Username:'', Password:''}", 0);
        sleep(1);

        //This command is use to authentication the user
        $mqtt->publish("Senzo/Mobile/Client", "loginAPI {'To':'$SerialGateway','key':'$key'}", 0);
        //Get error message if have one
        getError($mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0));

        //This command is use to get unreadmessage count
        $mqtt->publish("Senzo/Mobile/Client", "MessageUnreadCount {'To':'$SerialGateway'}", 0);
        $msgJson = json_decode($mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0));

        $TotalUnreadMessages = $msgJson->TotalUnreadMessages;
        echo "<input style='display:none;' id='TotalUnreadMessages' value='$TotalUnreadMessages'>";

        //This command is use to get all location
        $mqtt->publish("Senzo/Mobile/Client", "GetLocation {'To':'$SerialGateway'}", 0);
        getLocation($mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0));

        //This command is use to get smart switch list
        $mqtt->publish("Senzo/Mobile/Client", "GetSmartSwitch {'To':'$SerialGateway'}", 0);
        getDevices($mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0));

       //This command is use to get smart curtain list
        $mqtt->publish("Senzo/Mobile/Client", "GetSmartCurtainAPI {'To':'$SerialGateway'}", 0);
        getDevices($mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0));

        //This command is use to get smart shutter list
        $mqtt->publish("Senzo/Mobile/Client", "GetSmartShutterAPI {'To':'$SerialGateway'}", 0);
        getDevices($mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0));

        //This command is to get module status
        $mqtt->publish("Senzo/Mobile/Client", "GetModuleStatusAPI  {'To':'$SerialGateway'}", 0);
        sleep(1); //use 2 if error
        $msgJson = json_decode($mqtt->subscribeAndWaitForMessage('Senzo/Server/Client/' . $client_id, 0));

        if ($msgJson->Command == 'GetModuleStatusAPI') {

            if (!empty($msgJson->AdminPassword)) {
                $AdminPassword = $msgJson->AdminPassword;
                echo "<input style='display:none;' id='AdminPassword' value='$AdminPassword'>";
                $_SESSION['AdminPassword'] = $AdminPassword;
            }

            $SmartSwitchStatus = $msgJson->SmartSwitchStatus;
            $CurtainStatus = $msgJson->CurtainStatus;
            $SceneStatus = $msgJson->SceneStatus;
            $AlarmStatus = $msgJson->AlarmStatus;
            $ShutterStatus = $msgJson->ShutterStatus;
            $IRBlasterStatus = $msgJson->IRBlasterStatus;
            $LockStatus = $msgJson->LockStatus;

            $TotalDevice = $msgJson->SmartSwitchCount + $msgJson->CurtainCount + $msgJson->PocketRemoteCount + $msgJson->SensorCount + $msgJson->RepeaterCount +
                $msgJson->LockCount + $msgJson->ShutterCount + $msgJson->IRBlasterCount + $msgJson->GreenLightingCount + $msgJson->IDevCount;

            $SceneCount = $msgJson->SceneCount;

            $_SESSION['SmartSwitchStatus'] = $SmartSwitchStatus;
            $_SESSION['CurtainStatus'] = $CurtainStatus;
            $_SESSION['ShutterStatus'] = $ShutterStatus;
            $_SESSION['LockStatus'] = $LockStatus;
            $_SESSION['IRBlasterStatus'] = $IRBlasterStatus;
            $_SESSION['AlarmStatus'] = $AlarmStatus;
            $_SESSION['TotalDevice'] = $TotalDevice;
            $_SESSION['SceneCount'] = $SceneCount;


            $ModuleStatus = json_encode($msgJson);
        } else {
    ?>
            <script type="text/javascript">
                Swal.fire({
                    icon: 'error',
                    title: 'Failed',
                    text: 'Error while retrieving module status',
                }).then(function() {
                    window.location.href = "Dashboard.php";
                });
            </script>
        <?php
        }
    } else {
        echo "Time out!\n";
    }

    function getError($message)
    {

        $msgJson = json_decode($message);
        if (!empty($msgJson->Message)) {
            $Error = $msgJson->Message;
        ?>
            <script type="text/javascript">
                Swal.fire({
                    icon: 'error',
                    title: 'Failed',
                    text: '<?php echo $Error; ?>',
                }).then(function() {
                    window.location.href = "ChooseGateway.php";
                });
            </script>
            <?php
            die();
        }
        if (!empty($msgJson->ServerVersion)) {
            if ($msgJson->ServerVersion < 3.20) {
            ?>
                <script type="text/javascript">
                    Swal.fire({
                        icon: 'error',
                        title: 'Gateway Version Restriction',
                        text: 'Make sure your gateway is version 3.20 and above',
                    }).then(function() {
                        window.location.href = "ChooseGateway.php";
                    });
                </script>
    <?php
                die();
            }
        }
    }

    function getSeed($message, $password)
    {
        $username = "senzoadmin";
        $msgJson = json_decode($message);
        if ($msgJson->Command == 'GatewayInfo') {
            if (empty($msgJson->Message)) {
                if (!empty($msgJson->Seed)) {

                    $encPassword = "kav23EWvk,#2.buja7" . $msgJson->Seed;
                    // $encyptedPassword = $encPassword;

                    //---- compile login credentials to a single Json
                    $object = new \stdClass();
                    $object->User = $username;
                    $object->Password = $password;
                    $JsonObj = json_encode($object);
                    // var_dump($JsonObj);

                    //---- encrypt the login credentials
                    $chiperMethod = "aes-256-cbc";
                    $key = substr(hash('sha256', $encPassword, true), 0, 32);
                    $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0)
                        . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
                    $encrypted = base64_encode(openssl_encrypt($JsonObj, $chiperMethod, $key, OPENSSL_RAW_DATA, $iv));

                    $_SESSION['key'] = $encrypted;
                    return $encrypted;
                }
            }
        }
    }

    function getLocation($message)
    {
        $msgJson = json_decode($message);

        if (!empty($msgJson->LocationList)) {
            $GetLocationList = json_decode($message);
            $_SESSION['location'] = $GetLocationList->LocationList;
        }
    }

    function getDevices($message)
    {
        $msgJson = json_decode($message);

        if (!empty($msgJson->Command)) {
            if ($msgJson->Command == 'GetSmartSwitch' && $msgJson->Reply == true) {
                //To decompress the smart switch list array
                $Compress = $msgJson->Compress;
                $decode = base64_decode($Compress);
                $Decompress = gzdecode($decode);
                $msgJson =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));


                if (!empty($msgJson->SmartSwitchList)) {
                    $SmartSwitchListArray = $msgJson->SmartSwitchList;
                    $_SESSION['SmartSwitch'] = $SmartSwitchListArray;
                }
            }
        } else {
            //To decompress the whole smart curtain/shutter data
            $Compression = $msgJson->Compression;
            $decode = base64_decode($Compression);
            $Decompress = gzdecode($decode);
            $msgJson =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));

            if ($msgJson->Command == 'GetSmartCurtainAPI' && $msgJson->Reply == true) {

                //To decompress smart curtain list array
                $Compress = $msgJson->Compress;
                $decode = base64_decode($Compress);
                $Decompress = gzdecode($decode);
                $msgJson =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));

                if (!empty($msgJson->SmartCurtainList)) {
                    $SmartCurtainListArray = $msgJson->SmartCurtainList;
                    $_SESSION['SmartCurtain'] = $SmartCurtainListArray;
                }
            } else if ($msgJson->Command == 'GetSmartShutterAPI'  && $msgJson->Reply == true) {

                //To decompress smart shutter list array
                $Compress = $msgJson->Compress;
                $decode = base64_decode($Compress);
                $Decompress = gzdecode($decode);
                $msgJson =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));

                if (!empty($msgJson->SmartShutterList)) {
                    $SmartShutterListArray = $msgJson->SmartShutterList;
                    $_SESSION['SmartShutter'] = $SmartShutterListArray;
                }
            }
        }
    }

    $mqtt->close();

    ?>

    <input style='display:none;' id='IntervalId' value='none'>
    <input style='display:none;' class='TotalPage'>

    <!-- Form for issue -->
    <div class="form-popup" id="myIssueForm">
        <div class="form-container" style="width:500px;padding:0px;">
            <div class="modal-header" style='width:100%;height:60px;'>
                <button class='close' onclick='back("myIssueForm")'></button>
                <h3 class='h2form' id='issueDevice'>Issue</h3>
            </div>
            <div class="modal-body" id="issueBody" style='width:100%;max-height: calc(70vh - 210px);overflow-y: auto;'>
                <h4 id="deviceMessage" style="text-align:center;"></h4>
                <input type="text" id="myInput" onkeyup="search()" placeholder="Search by name" class="form-control" style="margin:10px;width: -webkit-fill-available;">

            </div>
            <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
            </div>
        </div>
    </div>

    <!-- Form for notification -->
    <div class="form-popup" id="myNotificationForm">
        <div class="form-container" style="width:500px;padding:0px;">
            <div class="modal-header" style='width:100%;height:60px;'>
                <button class='close' onclick='back("myNotificationForm")'></button>
                <h3 class='h2form'>Notifications</h3>
            </div>
            <div class="modal-body" style="height:40px;padding:0px;margin-left:10px;">
                <h4 id="unreadMessage"></h4>
            </div>
            <div class="modal-body" id="notificationBody" style='width:100%;max-height: calc(70vh - 210px);overflow-y: auto;'>

            </div>
            <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
            </div>
        </div>
    </div>

    <!-- Form for sign Door Lock -->
    <div class="form-popup" id="mySiginDoorLockForm">
        <div class="form-container" style="width:400px;padding:0px;">
            <div class="modal-header" style='width:100%;height:70px;'>
                <!-- <a class='back' onclick='closeFormSigin()'><span class='fa fa-arrow-left fa-2x'></a> -->
                <h3 class='h2form'>Manage Door Lock </br> Password</h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <fieldset style='width:100%;'>
                    <label for="name">Enter Admin Password:</label>
                    <input type="password" id="password" name="DoorlockPassword" style='width:100%;'>
                </fieldset>
            </div>
            <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
                <button type="submit" class="all" onclick='openGetLock()' name="signin">Ok</button>
                <button type="button" class="all" onclick="cancelFormSigin()">Cancel</button>
            </div>
        </div>
    </div>

    <!-- SideBar + Navbar -->
    <div class="wrapper">
        <!-- Sidebar Holder -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>Senzo</h3>
                <strong>SZ</strong>
            </div>
            <ul class="list-unstyled components">
                 <!-- Home Section -->
                <li>
                    <form action='Dashboard.php' method='POST' id='NavHome' style='margin-block-end: 0'>
                        <input style='display:none;' name='Home'>
                        <a id="cube" style='cursor:pointer;' class='Home'>
                            <i class="glyphicon glyphicon-home"></i>
                            Home
                        </a>
                    </form>
                </li>
                <li>
                    <!-- Device Section -->
                    <a href="#pageDevice" data-toggle="collapse" aria-expanded="false">
                        <i class="glyphicon glyphicon-tasks"></i>
                        <?php echo "Device"; ?>
                    </a>
                    <ul class="collapse list-unstyled" id="pageDevice">
                        <?php
                        if ($SmartSwitchStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectSwitch()">Smart Switch</a></li>';
                        }
                        if ($CurtainStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectCurtain()">Smart Curtain</a></li>';
                        }
                        if ($ShutterStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectShutter()">Smart Shutter</a></li>';
                        }
                        if ($LockStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectDoorLock()">Smart Door Lock</a></li>';
                        }
                        if ($IRBlasterStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectIRBlaster()">IR Blaster</a></li>';
                        }
                        ?>
                    </ul>
                </li>
                <li>
                     <!-- Scene Section -->
                    <a style='cursor:pointer;' onclick='openScenesForm()'>
                        <i class='glyphicon glyphicon-film'></i>
                        <?php echo "Scene ($SceneCount)"; ?>
                    </a>
                </li>
                <li> 
                    <!-- Security Section -->
                    <a href="#pageSecurity" data-toggle="collapse" aria-expanded="false">
                        <i class="glyphicon glyphicon-lock"></i>
                        <?php echo "Security"; ?>
                    </a>
                    <ul class="collapse list-unstyled" id="pageSecurity">
                        <?php
                        // If smart alarm connected to gateway
                        if ($AlarmStatus) {
                            echo "<li><a style='cursor:pointer;' onclick='selectAlarm(&#39;True&#39;)'>
                            Smart Alarm <img src='Images/Action/security_armed_icon.png' alt='scenes' style='width:20px;height:20px;'>";
                        } else {
                            echo "<li><a style='cursor:pointer;' onclick='selectAlarm(&#39;False&#39;)'>
                            Smart Alarm";
                        }
                        ?>
                        </a>
                </li>
                <li><a style="cursor:pointer;" onclick="selectCamera()">Camera</a></li>
            </ul>
            </li>
            <li>
                <!-- Analytic Section -->
                <a style='cursor:pointer;' onclick='openAnalyticForm()'>
                    <i class='glyphicon glyphicon-stats'></i>
                    Analytic
                </a>
            </li>
            <li>
                <!-- Setting Section -->
                <a style='cursor:pointer;' onclick='openSettingForm()'>
                    <i class='glyphicon glyphicon-cog'></i>
                    Setting
                </a>
            </li>
            </ul>

             <!-- Logout button -->
            <ul class="list-unstyled CTAs">
                <li>
                    <form method="POST" action="Dashboard.php" id='NavLogout' style='margin-block-end: 0'>
                        <input style='display:none;' name='Logout'>
                        <a style="cursor:pointer;" class="Logout">
                            <i class="glyphicon glyphicon-log-out"></i>
                            Logout
                        </a>
                    </form>
                </li>
            </ul>
        </nav>

        <!-- Page Content Holder -->
        <div id="content" style="width: -webkit-fill-available;">

            <nav class="navbar navbar-default">
                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                            <i class="glyphicon glyphicon-align-left"></i>
                            <span></span>
                        </button>

                        <!-- For responsive part -->
                        <!-- Show and hide content that been targeted-->
                        <button class="btn btn-dark navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <i class="glyphicon glyphicon-align-justify"></i>
                        </button>
                    </div>

                    <!-- Content that have been targeted -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <?php
                                if ($TotalUnreadMessages > 0) {
                                    echo "<a style='cursor:pointer;' onclick='openNotification()'><i class='glyphicon glyphicon-bell'><span class='badge'>.</span></i>";
                                } else {
                                    echo "<a style='cursor:pointer;' onclick='openNotification()'><i class='glyphicon glyphicon-bell'></i>";
                                }
                                ?>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <h2>Welcome to <?php echo "$gName"; ?></h2>
            <div class="line"></div>
            <div class="row">
                <h4 style='margin-left:20px;'>All available devices</h4>
                <?php
                if ($SmartSwitchStatus) {
                    $LostSmartSwitchList = $msgJson->LostSmartSwitchList;
                    echo '<div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-body ">
                            <div class="row">
                                <div class="col-5 col-md-4">
                                    <div class="icon-big text-center icon-warning">
                                        <!-- <i class="nc-icon nc-globe text-warning"></i> -->
                                        <img class="card-img-top" src="Images/DevicesView/switch.svg" alt="SmartSwitch" style="width:100px;">
                                    </div>
                                </div>
                                <div class="col-7 col-md-8">
                                    <div class="numbers">
                                        <p class="card-category">Smart Switch</p>
                                        <p class="card-title">';
                    echo "$msgJson->SmartSwitchCount";
                    echo '</p>
                                        <p>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer ">
                            <hr>
                            <div class="stats">';
                    echo "<a style='cursor:pointer;' onclick='selectSwitch()'>";
                    echo '<i class="fa fa-eye"></i>
                                View Now
                            </a>';
                    if (count($LostSmartSwitchList) > 0) {
                        echo '<div class="animate-flicker">';
                        echo "<i class='fas fa-exclamation-triangle' onclick='openIssue(&#39;$ModuleStatus&#39;, &#39;Switch&#39;)'></i>";
                        echo '</div>';
                    } else {
                        echo '<div class="noIssue">';
                        echo "<i class='fas fa-exclamation-triangle' onclick='openIssue(&#39;$ModuleStatus&#39;, &#39;NoIssue&#39;)'></i>";
                        echo '</div>';
                    }
                    echo '</div>
                        </div>
                    </div>
                </div>';
                }
                if ($CurtainStatus) {
                    $LostSmartCurtainList = $msgJson->LostSmartCurtainList;
                    echo '<div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-body ">
                            <div class="row">
                                <div class="col-5 col-md-4">
                                    <div class="icon-big text-center icon-warning">
                                        <img class="card-img-top" src="Images/DevicesView/curtain.svg" alt="SmartCurtain" style="width:100px;">
                                    </div>
                                </div>
                                <div class="col-7 col-md-8">
                                    <div class="numbers">
                                        <p class="card-category">Smart Curtain</p>
                                        <p class="card-title">';
                    echo "$msgJson->CurtainCount";
                    echo '</p>
                                        <p>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer ">
                            <hr>
                            <div class="stats">';
                    echo "<a style='cursor:pointer;' onclick='selectCurtain()'>";
                    echo '<i class="fa fa-eye"></i>
                                View Now
                            </a>';
                    if (count($LostSmartCurtainList) > 0) {
                        echo '<div class="animate-flicker">';
                        echo "<i class='fas fa-exclamation-triangle' onclick='openIssue(&#39;$ModuleStatus&#39;, &#39;Curtain&#39;)'></i>";
                        echo '</div>';
                    } else {
                        echo '<div class="noIssue">';
                        echo "<i class='fas fa-exclamation-triangle' onclick='openIssue(&#39;$ModuleStatus&#39;, &#39;NoIssue&#39;)'></i>";
                        echo '</div>';
                    }
                    echo '</div>
                        </div>
                    </div>
                </div>';
                }
                if ($ShutterStatus) {
                    $LostSmartShutterList = $msgJson->LostSmartShutterList;
                    echo '<div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-body ">
                            <div class="row">
                                <div class="col-5 col-md-4">
                                    <div class="icon-big text-center icon-warning">
                                        <img class="card-img-top" src="Images/DevicesView/shutter.svg" alt="SmartShutter" style="width:100px;">
                                    </div>
                                </div>
                                <div class="col-7 col-md-8">
                                    <div class="numbers">
                                        <p class="card-category">Smart Shutter</p>
                                        <p class="card-title">';
                    echo "$msgJson->ShutterCount";
                    echo '</p>
                                        <p>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer ">
                            <hr>
                            <div class="stats">';
                    echo "<a style='cursor:pointer;' onclick='selectShutter()'>";
                    echo '<i class="fa fa-eye"></i>
                                View Now
                                </a>';
                    if (count($LostSmartShutterList) > 0) {
                        echo '<div class="animate-flicker">';
                        echo "<i class='fas fa-exclamation-triangle' onclick='openIssue(&#39;$ModuleStatus&#39;, &#39;Shutter&#39;)'></i>";
                        echo '</div>';
                    } else {
                        echo '<div class="noIssue">';
                        echo "<i class='fas fa-exclamation-triangle' onclick='openIssue(&#39;$ModuleStatus&#39;, &#39;NoIssue&#39;)'></i>";
                        echo '</div>';
                    }
                    echo '</div>
                        </div>
                    </div>
                </div>';
                }
                if ($LockStatus) {
                    $LowBattery = $msgJson->LowBattery;
                    $lostSmartLockList = $msgJson->lostSmartLockList;
                    echo '<div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-body ">
                            <div class="row">
                                <div class="col-5 col-md-4">
                                    <div class="icon-big text-center icon-warning">
                                        <img class="card-img-top" src="Images/DevicesView/lock.svg" alt="SmartDoorLock" style="width:100px;">
                                    </div>
                                </div>
                                <div class="col-7 col-md-8">
                                    <div class="numbers">
                                        <p class="card-category">Smart Door Lock</p>
                                        <p class="card-title">';
                    echo "$msgJson->LockCount";
                    echo '</p>
                                        <p>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer ">
                            <hr>
                            <div class="stats">';
                    echo "<a style='cursor:pointer;' onclick='selectDoorLock()'>";
                    echo '<i class="fa fa-eye"></i>
                                View Now
                            </a>';
                    if (count($LowBattery) > 0 || count($lostSmartLockList) > 0) {
                        echo '<div class="animate-flicker">';
                        echo "<i class='fas fa-exclamation-triangle' onclick='openIssue(&#39;$ModuleStatus&#39;, &#39;DoorLock&#39;)'></i>";
                        echo '</div>';
                    } else {
                        echo '<div class="noIssue">';
                        echo "<i class='fas fa-exclamation-triangle' onclick='openIssue(&#39;$ModuleStatus&#39;, &#39;NoIssue&#39;)'></i>";
                        echo '</div>';
                    }
                    echo '</div>
                        </div>
                    </div>
                </div>';
                }
                if ($IRBlasterStatus) {
                    $LostIRBlasterList = $msgJson->LostIRBlasterList;
                    echo '<div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-body ">
                            <div class="row">
                                <div class="col-5 col-md-4">
                                    <div class="icon-big text-center icon-warning">
                                        <img class="card-img-top" src="Images/DevicesView/ir_blaster.svg" alt="IrBlaster" style="width:100px;">
                                    </div>
                                </div>
                                <div class="col-7 col-md-8">
                                    <div class="numbers">
                                        <p class="card-category">IR Blaster</p>
                                        <p class="card-title">';
                    echo "$msgJson->IRBlasterCount";
                    echo '</p>
                                        <p>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer ">
                            <hr>
                            <div class="stats">';
                    echo "<a style='cursor:pointer;' onclick='selectIRBlaster()'>";
                    echo '<i class="fa fa-eye"></i>
                                View Now
                                </a>';
                    if (count($LostIRBlasterList) > 0) {
                        echo '<div class="animate-flicker">';
                        echo "<i class='fas fa-exclamation-triangle' onclick='openIssue(&#39;$ModuleStatus&#39;, &#39;IrBlaster&#39;)'></i>";
                        echo '</div>';
                    } else {
                        echo '<div class="noIssue">';
                        echo "<i class='fas fa-exclamation-triangle' onclick='openIssue(&#39;$ModuleStatus&#39;, &#39;NoIssue&#39;)'></i>";
                        echo '</div>';
                    }
                    echo '</div>
                        </div>
                    </div>
                </div>';
                }
                ?>
            </div>

            <div class="line"></div>
            <div class="row">
                <h4 style='margin-left:20px;'>Scene</h4>
                <?php
                echo '<div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-body ">
                            <div class="row">
                                <div class="col-5 col-md-4">
                                    <div class="icon-big text-center icon-warning">
                                         <!-- <i class="nc-icon nc-globe text-warning"></i> -->
                                        <img class="card-img-top" src="Images/Action/scenes_icon.png" alt="Scenes" style="width:100px;">
                                    </div>
                                </div>
                                <div class="col-7 col-md-8">
                                    <div class="numbers">
                                        <p class="card-category">Scene</p>
                                        <p class="card-title">';
                echo "$msgJson->SceneCount";
                echo '</p>
                                    <p>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer ">
                        <hr>
                        <div class="stats">';
                echo "<a style='cursor:pointer;' onclick='openScenesForm()'>";
                echo '<i class="fa fa-eye"></i>
                            View Now
                            </a>';
                echo '<div class="noIssue">';
                echo "<i class='fas fa-exclamation-triangle' onclick='openIssue(&#39;$ModuleStatus&#39;, &#39;NoIssue&#39;)'></i>";
                echo '</div>';
                echo '</div>
                    </div>
                </div>
            </div>';
                ?>
            </div>


            <div class="line"></div>
            <div class="row">
                <h4 style="margin-left:20px;">Security</h4>
                <?php
                echo '<div class="col-lg-3 col-md-6 col-sm-6"> 
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-5 col-md-4">
                                <div class="icon-big text-center icon-warning">
                                    <!-- <i class="nc-icon nc-globe text-warning"></i> -->
                                    <img class="card-img-top" src="Images/DevicesView/sac.png" alt="SmartAlarm" style="width:100px;">
                                </div>
                            </div>
                            <div class="col-7 col-md-8">
                                <div class="numbers">
                                    <p class="card-category">Smart Alarm</p>
                                    <p class="card-title">';
                if ($AlarmStatus) {
                    echo "$msgJson->SACType";
                }
                echo '</p>
                                    <p>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer ">
                        <hr>
                        <div class="stats">';
                if ($AlarmStatus) {
                    $LostSACList = $msgJson->LostSACList;
                    echo "<a style='cursor:pointer;' onclick='selectAlarm(&#39;True&#39;)'>";
                } else {
                    echo "<a style='cursor:pointer;' onclick='selectAlarm(&#39;False&#39;)'>";
                }
                echo '<i class="fa fa-eye"></i>
                            View Now
                            </a>';
                if (count($LostSACList) > 0) {
                    echo '<div class="animate-flicker">';
                    echo "<i class='fas fa-exclamation-triangle' onclick='openIssue(&#39;$ModuleStatus&#39;, &#39;Alarm&#39;)'></i>";
                    echo '</div>';
                } else {
                    echo '<div class="noIssue">';
                    echo "<i class='fas fa-exclamation-triangle' onclick='openIssue(&#39;$ModuleStatus&#39;, &#39;NoIssue&#39;)'></i>";
                    echo '</div>';
                }
                echo '</div>
                </div>
            </div>
        </div>';

                echo '<div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-5 col-md-4">
                            <div class="icon-big text-center icon-warning">
                                <img class="card-img-top" src="Images/DevicesView/cctv.svg" alt="IrBlaster" style="width:100px;">
                            </div>
                        </div>
                        <div class="col-7 col-md-8">
                            <div class="numbers">
                                <p class="card-category">Camera</p>
                                <p class="card-title">';
                echo "3";
                echo '</p>
                                <p>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer ">
                    <hr>
                    <div class="stats">';
                echo "<a style='cursor:pointer;' onclick='selectCamera()'>";
                echo '<i class="fa fa-eye"></i>
                        View Now
                        </a>';
                echo '<div class="noIssue">';
                echo "<i class='fas fa-exclamation-triangle' onclick='openIssue(&#39;$ModuleStatus&#39;, &#39;NoIssue&#39;)'></i>";
                echo '</div>';
                echo '</div>
                </div>
            </div>
        </div>';
                ?>
            </div>
        </div>
    </div>

    <!-- Auto Refresh dropdown -->
    <div class='autorefresh'>
        <div class="dropdown">
            <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">
                <img src='Images/Switch/settings_gear.png' alt='switchon' style='width:30px;height:30px;animation: spin 3s linear infinite;'>
            </button>
            <ul class="dropdown-menu">
                <!-- <li><a href="#">Phantom</a></li> -->
                <li>
                    <input type='checkbox' id='enable' name='fooby[2][]'>
                    <label> Enable</label><br>
                    <div id='timeRefresh' style='display:none;margin-left:10px;'>
                        <input type='checkbox' value='1-minute' class="slectOne" id='1-minute'>
                        <label> 1 minute</label><br>
                        <input type='checkbox' value='5-minutes' class="slectOne" id='5-minutes'>
                        <label> 5 minutes</label><br>
                        <input type='checkbox' value='10-minutes' class="slectOne" id='10-minutes'>
                        <label> 10 minutes</label><br>
                        <input type='checkbox' value='20-minutes' class="slectOne" id='20-minutes'>
                        <label> 20 minutes</label><br>
                        <input type='checkbox' value='25-minutes' class="slectOne" id='25-minutes'>
                        <label> 25 minutes</label><br>
                        <input type='checkbox' value='1-hour' class="slectOne" id='1-hour'>
                        <label> 1 hour</label><br>
                    </div>
                </li>
                <li>
                    <input type='checkbox' id='disable' value='disable'>
                    <label> Disable</label><br>
                </li>
            </ul>
        </div>
    </div>

    <!-- Loader -->
    <div class="form-popup" id='loader'>
        <!-- <div id="loadering" class="loader"></div> -->
        <div class="loading-container">
            <div class="loading"></div>
            <div class="loading-text">loading</div>
        </div>
    </div>
</body>

</html>