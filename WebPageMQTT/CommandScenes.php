<?php
ini_set('max_execution_time', 30);
session_start();
ob_start();
//Include all file needed to use in this page
include '././configuration/serverConfig.php';
include './header/headerAll.php';
?>
<!DOCTYPE html>
<html>

<head>
    <title>Scenes Command</title>
    <link href="css/CommandScenes.css" rel="stylesheet">
    <link rel="stylesheet" href="css/Sidebar.css">
    <!-- Sidebar and auto refresh function -->
    <script type="text/javascript" src="SidebarHandler.js"></script>
</head>
<script type="text/javascript">
    function openDelayTimer(DeviceInfo, StateInfo) {
        var fields = DeviceInfo.split(" ");
        var serial = fields[0];
        var switchno = fields[1];
        var info = StateInfo.split(" ");
        var states = info[0];

        //Set the current value in input form;
        if (states == 'setswitchdelaytimer') {
            var minutes = info[1];
            var action = info[2];
            var hours = (minutes / 60);
            var rhours = Math.floor(hours);
            var minutes = (hours - rhours) * 60;
            var rminutes = Math.round(minutes);
            document.getElementById("delayactions").value = 'setswitchdelaytimer';
            document.getElementById("hours").value = rhours;
            document.getElementById("minutes").value = rminutes;
            document.getElementById("switchactions").value = action;
            document.getElementById("hours").disabled = false;
            document.getElementById("minutes").disabled = false;
            document.getElementById("switchactions").disabled = false;
            document.getElementById("delayTimerStatus").innerHTML = "Switch will turn on in about " + rhours + " hours " + rminutes + " minutes";
        } else if (states == 'cancelswitchdelaytimer' || states == 'chooseaction') {
            document.getElementById("delayactions").value = states;
            document.getElementById("switchactions").value = 'chooseaction';
            document.getElementById("hours").value = '';
            document.getElementById("minutes").value = '';
            document.getElementById("hours").disabled = true;
            document.getElementById("minutes").disabled = true;
            document.getElementById("switchactions").disabled = true;
            document.getElementById("delayTimerStatus").innerHTML = "";
        }

        //After apply timer
        delayactionDevice = document.getElementById("delayAction" + serial + " " + switchno).value;
        switchactionDevice = document.getElementById("switchAction" + serial + " " + switchno).value;
        minutesDevice = document.getElementById("minutes" + serial + " " + switchno).value;
        if (delayactionDevice == 'setswitchdelaytimer') {
            var hours = (minutesDevice / 60);
            var rhours = Math.floor(hours);
            var minutes = (hours - rhours) * 60;
            var rminutes = Math.round(minutes);
            document.getElementById("delayactions").value = delayactionDevice;
            document.getElementById("switchactions").value = switchactionDevice;
            document.getElementById("hours").value = rhours;
            document.getElementById("minutes").value = rminutes;
            document.getElementById("hours").disabled = false;
            document.getElementById("minutes").disabled = false;
            document.getElementById("switchactions").disabled = false;
            document.getElementById("delayTimerStatus").innerHTML = "Switch will turn on in about " + rhours + " hours " + rminutes + " minutes";
        } else if (delayactionDevice == 'cancelswitchdelaytimer' || delayactionDevice == '') {
            document.getElementById("delayactions").value = delayactionDevice;
            document.getElementById("switchactions").value = 'chooseaction';
            document.getElementById("hours").value = '';
            document.getElementById("minutes").value = '';
            document.getElementById("hours").disabled = true;
            document.getElementById("minutes").disabled = true;
            document.getElementById("switchactions").disabled = true;
            document.getElementById("delayTimerStatus").innerHTML = "";
        }

        document.getElementById("myDelayForm").style.display = "block";
        document.getElementsByClassName("serial").value = serial;
        document.getElementsByClassName("switchno").value = switchno;
    }

    function myDelayTimerFunction() {
        var serial = document.getElementsByClassName("serial").value;
        var switchno = document.getElementsByClassName("switchno").value;
        var delayAction = document.getElementById("delayactions");
        var switchAction = document.getElementById("switchactions");
        var hours = document.getElementsByName("hours");
        var minutes = document.getElementsByName("minutes");

        var Minutes = +minutes[0].value + (+hours[0].value * 60);

        var boolDelayAction = true;
        if (delayAction.value == null || delayAction.value == 'chooseaction') {
            boolDelayAction = false;
        }

        var boolSwitchAction = true;
        if ((switchAction.value == null || switchAction.value == 'chooseaction') && delayAction.value == 'setswitchdelaytimer') {
            boolSwitchAction = false;
        }

        var boolTime = true;
        if (hours[0].value == 0 && minutes[0].value == 0 && delayAction.value == 'setswitchdelaytimer') {
            boolTime = false;
        }

        if (!boolDelayAction) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Please check your delay action'
            })
        } else if (!boolSwitchAction) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Please check your switch action'
            })
        } else if (!boolTime) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Both hours and minutes cannot be empty'
            })
        } else if (boolDelayAction && boolSwitchAction && boolTime) {
            document.getElementById("serial" + serial + " " + switchno).value = serial;
            document.getElementById("switchno" + serial + " " + switchno).value = switchno;
            document.getElementById("delayAction" + serial + " " + switchno).value = delayAction.value;

            if (delayAction.value == 'setswitchdelaytimer') {
                document.getElementById("switchAction" + serial + " " + switchno).value = switchAction.value;
                document.getElementById("minutes" + serial + " " + switchno).value = Minutes;
                document.getElementById("delay" + serial + " " + switchno).src = "Images/Scene/delayTimerOn.png";
                document.getElementById("myDelayForm").style.display = "none";
            } else if (delayAction.value == 'cancelswitchdelaytimer') {
                Swal.fire({
                    title: 'Are you sure want to cancel delay timer?',
                    text: "Click Yes to cancel delay timer",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes!'
                }).then((result) => {
                    if (result.value) {
                        document.getElementById("delay" + serial + " " + switchno).src = "Images/Scene/delayTimerCancel.png";
                        document.getElementById("myDelayForm").style.display = "none";
                    }
                })
            } else {
                document.getElementById("delay" + serial + " " + switchno).src = "Images/Scene/delayTimerOff.png";
                document.getElementById("myDelayForm").style.display = "none";
            }

        }


    }

    function saveScenes(SwitchInfo, CurtainInfo, ShutterInfo, SceneId, StateInfo) {

        var SceneStringArray = [];
        var CompareArray = [];
        var CompareToggle = [];
        var ToggleStringArray1 = [];
        var FindIndex1 = [];
        var count1 = 0;
        var StatesSerialNoToggleArray = [];
        var StatesSerialNoAlarmArray = [];
        var check = false;
        var toggleallswitches = [];
        var setallalarmtriggerfirelevel = [];

        var TempInfo = StateInfo.split(",");
        for (i = 0; i < TempInfo.length; i++) {
            if (TempInfo[i] != "") {
                //Store command already exist in an array
                var State = TempInfo[i].split(" ");
                var StatesDelayAction = State[0];
                if (StatesDelayAction == 'toggleallswitches') {
                    var StatesSerialNo = State[1];
                    StatesSerialNoToggleArray.push(StatesSerialNo);
                } else if (StatesDelayAction == 'setallalarmtriggerfirelevel') {
                    var StatesSerialNo = State[1];
                    StatesSerialNoAlarmArray.push(StatesSerialNo);
                }

            } else {
                check = true;
            }
        }

        ////---------------------------------------------------------New switch command---------------------------------------------------------------------------
        var switchInfo = JSON.parse(SwitchInfo);
        if (switchInfo != "Empty") {
            for (i = 0; i < switchInfo.length; i++) {
                for (j = 0; j < switchInfo[i].SwitchList.length; j++) {
                    //Get new switch delay timer value
                    var switchserial = document.getElementById("serial" + switchInfo[i].SwitchList[j].SerialNo + " " + switchInfo[i].SwitchList[j].SwitchNo).value;
                    var switchno = document.getElementById("switchno" + switchInfo[i].SwitchList[j].SerialNo + " " + switchInfo[i].SwitchList[j].SwitchNo).value;
                    var delayAction = document.getElementById("delayAction" + switchInfo[i].SwitchList[j].SerialNo + " " + switchInfo[i].SwitchList[j].SwitchNo).value;
                    var switchAction = document.getElementById("switchAction" + switchInfo[i].SwitchList[j].SerialNo + " " + switchInfo[i].SwitchList[j].SwitchNo).value;
                    var minutes = document.getElementById("minutes" + switchInfo[i].SwitchList[j].SerialNo + " " + switchInfo[i].SwitchList[j].SwitchNo).value;

                    if (switchserial != "" && switchno != "") {
                        if (delayAction == 'cancelswitchdelaytimer') {
                            var StringScene = delayAction + " " + switchserial + " " + switchno + " \\n";
                            SceneStringArray.push(StringScene);
                        } else if (delayAction == 'setswitchdelaytimer') {
                            var StringScene = delayAction + " " + switchserial + " " + switchno + " " + minutes + " " + switchAction + " \\n";
                            SceneStringArray.push(StringScene);
                        } else {
                            //doing nothing
                        }
                        var CompareString = switchserial + " " + switchno;
                        CompareArray.push(CompareString);
                    }

                    //Get new switch toggle value
                    var serialToggle = document.getElementById("serialToggle" + switchInfo[i].SwitchList[j].SerialNo + " " + switchInfo[i].SwitchList[j].SwitchNo).value;
                    var switchToggle = document.getElementById("switchToggle" + switchInfo[i].SwitchList[j].SerialNo + " " + switchInfo[i].SwitchList[j].SwitchNo).value;
                    var toggleType = document.getElementById("switch" + switchInfo[i].SwitchList[j].SerialNo + " " + switchInfo[i].SwitchList[j].SwitchNo).value;
                    var switchnum = switchInfo[i].SwitchList.length;

                    if (serialToggle != "" && switchToggle != "") {
                        if (toggleType == 1 || toggleType == 2 || toggleType == 100) {
                            if (toggleType == 1 || toggleType == 2) {
                                var typeToggle = 'toggleallswitches';
                            } else {
                                var typeToggle = 'setallalarmtriggerfirelevel';
                            }

                            var SwitchNoArray1 = [];
                            for (k = 1; k <= switchnum; k++) {
                                if (k == switchToggle) {
                                    var toggleNum = toggleType;
                                    SwitchNoArray1.push(toggleNum);
                                } else {
                                    var toggleNum = 255;
                                    SwitchNoArray1.push(toggleNum);
                                }
                            }

                            var SwitchNoString = SwitchNoArray1.join(" ");
                            var StringSceneToggle = switchToggle + " " + toggleType + " \\n";

                            //For state toggleallswitches
                            var boolSerialToggle = true;
                            if (typeToggle == 'toggleallswitches') {
                                var boolSerialToggle = false;
                                if (StatesSerialNoToggleArray.length != 0) {
                                    for (k = 0; k < StatesSerialNoToggleArray.length; k++) {
                                        if (serialToggle == StatesSerialNoToggleArray[k]) {
                                            boolSerialToggle = true;
                                            break;
                                        }
                                    }
                                } else {
                                    boolSerialToggle = false;
                                }
                            }

                            //For state setallalarmtriggerfirelevel
                            var boolSerialAlarm = true;
                            if (typeToggle == 'setallalarmtriggerfirelevel') {
                                var boolSerialAlarm = false;
                                if (StatesSerialNoAlarmArray.length != 0) {
                                    for (k = 0; k < StatesSerialNoAlarmArray.length; k++) {
                                        if (serialToggle == StatesSerialNoAlarmArray[k]) {
                                            boolSerialAlarm = true;
                                            break;
                                        }
                                    }
                                } else {
                                    boolSerialAlarm = false;
                                }
                            }

                            //First time selected switch input state switch toggle
                            if (boolSerialToggle == false) {
                                StatesSerialNoToggleArray.push(serialToggle);
                                if (typeToggle == 'toggleallswitches') {
                                    StateInfo = StateInfo + "," + typeToggle + " " + serialToggle + " " + switchnum + " " + SwitchNoString + ",";
                                    // console.log(StateInfo);
                                }
                            }

                            //First time selected switch input state switch set alarm
                            if (boolSerialAlarm == false) {
                                StatesSerialNoAlarmArray.push(serialToggle);
                                if (typeToggle == 'setallalarmtriggerfirelevel') {
                                    StateInfo = StateInfo + "," + typeToggle + " " + serialToggle + " " + switchnum + " " + SwitchNoString + " " + 0 + ",";
                                    // console.log(StateInfo);
                                }
                            }

                            if (typeToggle == 'toggleallswitches') {
                                ToggleStringArray1.push(StringSceneToggle);
                                FindIndex1.push(serialToggle + count1);
                                count1++;
                                var CompareString = serialToggle;
                                toggleallswitches.push(CompareString);
                            } else if (typeToggle == 'setallalarmtriggerfirelevel') {
                                ToggleStringArray1.push(StringSceneToggle);
                                FindIndex1.push(serialToggle + count1);
                                count1++;
                                var CompareString = serialToggle;
                                setallalarmtriggerfirelevel.push(CompareString);
                            }

                            var CompareString = serialToggle;
                            CompareToggle.push(CompareString);

                        } else if (toggleType == '') {

                            toggleType = 255;
                            var StringSceneToggle = switchToggle + " " + toggleType + " \\n";
                            ToggleStringArray1.push(StringSceneToggle);
                            FindIndex1.push(serialToggle + count1);
                            count1++;

                            var CompareString = serialToggle;
                            CompareToggle.push(CompareString);
                            toggleallswitches.push(CompareString);
                            setallalarmtriggerfirelevel.push(CompareString);
                        }


                    }
                }

            }
        }
        //---------------------------------------------------------End of New switch command---------------------------------------------------------------------------

        //---------------------------------------------------------New curtain command---------------------------------------------------------------------------
        var curtainInfo = JSON.parse(CurtainInfo);
        if (curtainInfo != "Empty") {
            for (i = 0; i < curtainInfo.length; i++) {
                for (j = 0; j < curtainInfo[i].CurtainList.length; j++) {
                    //Get new curtain delay timer value
                    var switchserial = document.getElementById("serial" + curtainInfo[i].CurtainList[j].SerialNo + " " + curtainInfo[i].CurtainList[j].SwitchNo).value;
                    var switchno = document.getElementById("switchno" + curtainInfo[i].CurtainList[j].SerialNo + " " + curtainInfo[i].CurtainList[j].SwitchNo).value;
                    var delayAction = document.getElementById("delayAction" + curtainInfo[i].CurtainList[j].SerialNo + " " + curtainInfo[i].CurtainList[j].SwitchNo).value;
                    var switchAction = document.getElementById("switchAction" + curtainInfo[i].CurtainList[j].SerialNo + " " + curtainInfo[i].CurtainList[j].SwitchNo).value;
                    var minutes = document.getElementById("minutes" + curtainInfo[i].CurtainList[j].SerialNo + " " + curtainInfo[i].CurtainList[j].SwitchNo).value;

                    if (switchserial != "" && switchno != "") {

                        if (delayAction == 'cancelswitchdelaytimer') {
                            var StringScene = delayAction + " " + switchserial + " " + switchno + " \\n";
                            SceneStringArray.push(StringScene);
                        } else if (delayAction == 'setswitchdelaytimer') {
                            var StringScene = delayAction + " " + switchserial + " " + switchno + " " + minutes + " " + switchAction + " \\n";
                            SceneStringArray.push(StringScene);
                        } else {
                            //doing nothing
                        }

                        var CompareString = switchserial + " " + switchno;
                        CompareArray.push(CompareString);
                    }

                    //Get new curtain toggle value
                    var serialToggle = document.getElementById("serialToggle" + curtainInfo[i].CurtainList[j].SerialNo + " " + curtainInfo[i].CurtainList[j].SwitchNo).value;
                    var switchToggle = document.getElementById("switchToggle" + curtainInfo[i].CurtainList[j].SerialNo + " " + curtainInfo[i].CurtainList[j].SwitchNo).value;
                    var toggleType = document.getElementById("switch" + curtainInfo[i].CurtainList[j].SerialNo + " " + curtainInfo[i].CurtainList[j].SwitchNo).value;
                    var switchnum = curtainInfo[i].CurtainList.length;

                    if (serialToggle != "" && switchToggle != "") {
                        if (toggleType == 1) {
                            var typeToggle = 'toggleallswitches';
                            var SwitchNoArray1 = [];
                            for (k = 1; k <= switchnum; k++) {
                                if (k == switchToggle) {
                                    var toggleNum = toggleType;
                                    SwitchNoArray1.push(toggleNum);
                                } else {
                                    var toggleNum = 255;
                                    SwitchNoArray1.push(toggleNum);
                                }
                            }

                            var SwitchNoString = SwitchNoArray1.join(" ");
                            var StringSceneToggle = switchToggle + " " + toggleType + " \\n";

                            var boolSerial = false;
                            for (k = 0; k < StatesSerialNoToggleArray.length; k++) {
                                if (serialToggle == StatesSerialNoToggleArray[k]) {
                                    boolSerial = true;
                                    break;
                                }
                            }

                            if (boolSerial == false) {
                                StatesSerialNoToggleArray.push(serialToggle);
                                if (typeToggle == 'toggleallswitches') {
                                    StateInfo = StateInfo + "," + typeToggle + " " + serialToggle + " " + switchnum + " " + SwitchNoString + ",";
                                }
                            }

                            ToggleStringArray1.push(StringSceneToggle);
                            FindIndex1.push(serialToggle + count1);
                            count1++;
                            var CompareString = serialToggle;
                            toggleallswitches.push(CompareString);

                        } else if (toggleType == '') {

                            toggleType = 255;
                            var StringSceneToggle = switchToggle + " " + toggleType + " \\n";
                            ToggleStringArray1.push(StringSceneToggle);
                            FindIndex1.push(serialToggle + count1);
                            count1++;

                            var CompareString = serialToggle;
                            toggleallswitches.push(CompareString);
                        }

                        var CompareString = serialToggle;
                        CompareToggle.push(CompareString);

                    }
                }
            }
        }
        //---------------------------------------------------------End of New curtain command---------------------------------------------------------------------------

        //---------------------------------------------------------New shutter command---------------------------------------------------------------------------
        var shutterInfo = JSON.parse(ShutterInfo);
        if (shutterInfo != "Empty") {
            for (i = 0; i < shutterInfo.length; i++) {
                for (j = 0; j < shutterInfo[i].ShutterList.length; j++) {
                    //Get new shutter delay timer value
                    var switchserial = document.getElementById("serial" + shutterInfo[i].ShutterList[j].SerialNo + " " + shutterInfo[i].ShutterList[j].SwitchNo).value;
                    var switchno = document.getElementById("switchno" + shutterInfo[i].ShutterList[j].SerialNo + " " + shutterInfo[i].ShutterList[j].SwitchNo).value;
                    var delayAction = document.getElementById("delayAction" + shutterInfo[i].ShutterList[j].SerialNo + " " + shutterInfo[i].ShutterList[j].SwitchNo).value;
                    var switchAction = document.getElementById("switchAction" + shutterInfo[i].ShutterList[j].SerialNo + " " + shutterInfo[i].ShutterList[j].SwitchNo).value;
                    var minutes = document.getElementById("minutes" + shutterInfo[i].ShutterList[j].SerialNo + " " + shutterInfo[i].ShutterList[j].SwitchNo).value;

                    if (switchserial != "" && switchno != "") {
                        if (delayAction == 'cancelswitchdelaytimer') {
                            var StringScene = delayAction + " " + switchserial + " " + switchno + " \\n";
                            SceneStringArray.push(StringScene);
                        } else if (delayAction == 'setswitchdelaytimer') {
                            var StringScene = delayAction + " " + switchserial + " " + switchno + " " + minutes + " " + switchAction + " \\n";
                            SceneStringArray.push(StringScene);
                        } else {
                            //doing nothing
                        }

                        var CompareString = switchserial + " " + switchno;
                        CompareArray.push(CompareString);
                    }

                    //Get new shutter toggle value
                    var serialToggle = document.getElementById("serialToggle" + shutterInfo[i].ShutterList[j].SerialNo + " " + shutterInfo[i].ShutterList[j].SwitchNo).value;
                    var switchToggle = document.getElementById("switchToggle" + shutterInfo[i].ShutterList[j].SerialNo + " " + shutterInfo[i].ShutterList[j].SwitchNo).value;
                    var toggleType = document.getElementById("switch" + shutterInfo[i].ShutterList[j].SerialNo + " " + shutterInfo[i].ShutterList[j].SwitchNo).value;
                    var switchnum = shutterInfo[i].ShutterList.length;

                    if (serialToggle != "" && switchToggle != "") {
                        if (toggleType == 1) {
                            var typeToggle = 'toggleallswitches';
                            var SwitchNoArray1 = [];
                            for (k = 1; k <= switchnum; k++) {
                                if (k == switchToggle) {
                                    var toggleNum = toggleType;
                                    SwitchNoArray1.push(toggleNum);
                                } else {
                                    var toggleNum = 255;
                                    SwitchNoArray1.push(toggleNum);
                                }
                            }

                            var SwitchNoString = SwitchNoArray1.join(" ");
                            var StringSceneToggle = switchToggle + " " + toggleType + " \\n";

                            var boolSerial = false;
                            for (k = 0; k < StatesSerialNoToggleArray.length; k++) {
                                if (serialToggle == StatesSerialNoToggleArray[k]) {
                                    boolSerial = true;
                                    break;
                                }
                            }

                            if (boolSerial == false) {
                                StatesSerialNoToggleArray.push(serialToggle);
                                if (typeToggle == 'toggleallswitches') {
                                    StateInfo = StateInfo + "," + typeToggle + " " + serialToggle + " " + switchnum + " " + SwitchNoString + ",";
                                }
                            }

                            ToggleStringArray1.push(StringSceneToggle);
                            FindIndex1.push(serialToggle + count1);
                            count1++;
                            var CompareString = serialToggle;
                            toggleallswitches.push(CompareString);

                        } else if (toggleType == '') {

                            toggleType = 255;
                            var StringSceneToggle = switchToggle + " " + toggleType + " \\n";
                            ToggleStringArray1.push(StringSceneToggle);
                            FindIndex1.push(serialToggle + count1);
                            count1++;

                            var CompareString = serialToggle;
                            toggleallswitches.push(CompareString);
                        }

                        var CompareString = serialToggle;
                        CompareToggle.push(CompareString);

                    }
                }
            }
        }
        //---------------------------------------------------------End of New shutter command---------------------------------------------------------------------------

        //---------------------------------------------------------Existing command---------------------------------------------------------------------------
        var TempState = StateInfo.split(",");
        for (i = 0; i < TempState.length; i++) {
            if (TempState[i] != "") {
                //Command already exist
                var State = TempState[i].split(" ");
                var StatesDelayAction = State[0];
                if (StatesDelayAction == 'setswitchdelaytimer') {
                    var StatesSerialNo = State[1];
                    var StatesSWitchNo = State[2];
                    var StatesMinutes = State[3];
                    var StatesSwitchAction = State[4];
                } else if (StatesDelayAction == 'cancelswitchdelaytimer') {
                    var StatesSerialNo = State[1];
                    var StatesSWitchNo = State[2];
                } else if (StatesDelayAction == 'toggleallswitches') {
                    var StatesSerialNo = State[1];
                    var StatesNumSWitch = State[2];
                    var StateNumToggleArray = [];
                    if (StatesNumSWitch > 1) {
                        for (j = 1; j <= StatesNumSWitch; j++) {
                            var StatesNumToggle = State[2 + j];
                            StateNumToggleArray.push(StatesNumToggle);
                        }
                    } else {
                        StateNumToggleArray.push(State[3]);
                    }
                } else if (StatesDelayAction == 'setallalarmtriggerfirelevel') {
                    var StatesSerialNo = State[1];
                    var StatesNumSWitch = State[2];
                    var StateNumToggleArray = [];
                    if (StatesNumSWitch > 1) {
                        for (j = 1; j <= StatesNumSWitch; j++) {
                            var StatesNumToggle = State[2 + j];
                            StateNumToggleArray.push(StatesNumToggle);
                        }
                    } else {
                        StateNumToggleArray.push(State[3]);
                    }
                    var StateZero = 0;
                }

                CompareString = StatesSerialNo + " " + StatesSWitchNo;

                //Delay Timer
                if (!CompareArray.includes(CompareString)) {

                    if (StatesDelayAction == 'cancelswitchdelaytimer') {
                        var StringScene = StatesDelayAction + " " + StatesSerialNo + " " + StatesSWitchNo + " \\n";
                        SceneStringArray.push(StringScene);
                    } else if (StatesDelayAction == 'setswitchdelaytimer') {
                        var StringScene = StatesDelayAction + " " + StatesSerialNo + " " + StatesSWitchNo + " " + StatesMinutes + " " + StatesSwitchAction + " \\n";
                        SceneStringArray.push(StringScene);
                    }
                }

                //Toggle Switch
                if (CompareToggle.includes(StatesSerialNo)) {
                    if (StatesDelayAction == 'toggleallswitches') {
                        if (FindIndex1.length != 0) {
                            for (a = 0; a < FindIndex1.length; a++) {
                                // console.log(FindIndex1);
                                var index = FindIndex1.indexOf(StatesSerialNo + a);
                                if (index >= 0) {
                                    // console.log(index);
                                    // console.log(ToggleStringArray1);
                                    ToggleString = ToggleStringArray1[index].split(" ");
                                    var toggleSwitch = ToggleString[0];
                                    var toggleType = ToggleString[1];
                                    if (toggleType == 100) {
                                        toggleType = 255;
                                    }
                                    // console.log(toggleSwitch + ":" + toggleType);
                                    StateNumToggleArray[toggleSwitch - 1] = toggleType;
                                }
                            }
                            // console.log(StateNumToggleArray);
                            var StateNumToggleString = StateNumToggleArray.join(" ");
                            // console.log(StateNumToggleString);

                            boolNothing = false;
                            if (StateNumToggleArray.length >= 1) {
                                for (x = 0; x < StateNumToggleArray.length; x++) {
                                    if (StateNumToggleArray[x] != 255) {
                                        boolNothing = true;
                                        break;
                                    }
                                }
                            }
                            var StringSceneToggle = StatesDelayAction + " " + StatesSerialNo + " " + StatesNumSWitch + " " + StateNumToggleString + " \\n";
                            if (boolNothing == true) {
                                SceneStringArray.push(StringSceneToggle);
                            }
                        }
                    } else if (StatesDelayAction == 'setallalarmtriggerfirelevel') {
                        if (FindIndex1.length != 0) {
                            for (a = 0; a < FindIndex1.length; a++) {
                                // console.log(FindIndex1);
                                var index = FindIndex1.indexOf(StatesSerialNo + a);
                                if (index >= 0) {
                                    // console.log(index);
                                    // console.log(ToggleStringArray1);
                                    ToggleString = ToggleStringArray1[index].split(" ");
                                    var toggleSwitch = ToggleString[0];
                                    var toggleType = ToggleString[1];
                                    if (toggleType == 1 || toggleType == 2) {
                                        toggleType = 255;
                                    }
                                    // console.log(toggleSwitch + ":" + toggleType);
                                    StateNumToggleArray[toggleSwitch - 1] = toggleType;
                                }
                            }

                            var StateNumToggleString = StateNumToggleArray.join(" ");
                            // console.log(StateNumToggleString);

                            boolNothing = false;
                            if (StateNumToggleArray.length >= 1) {
                                for (y = 0; y < StateNumToggleArray.length; y++) {
                                    if (StateNumToggleArray[y] != 255) {
                                        boolNothing = true;
                                        break;
                                    }
                                }
                            }
                            var StringSceneToggle = StatesDelayAction + " " + StatesSerialNo + " " + StatesNumSWitch + " " + StateNumToggleString + " " + StateZero + " \\n";
                            if (boolNothing == true) {
                                SceneStringArray.push(StringSceneToggle);
                            }
                        }
                    }
                }

                if ((toggleallswitches.length == 0 && setallalarmtriggerfirelevel.length == 0) || (!toggleallswitches.includes(StatesSerialNo) && !setallalarmtriggerfirelevel.includes(StatesSerialNo))) {
                    if (StatesDelayAction == 'toggleallswitches') {
                        var StateNumToggleString = StateNumToggleArray.join(" ");
                        var StringScene = StatesDelayAction + " " + StatesSerialNo + " " + StatesNumSWitch + " " + StateNumToggleString + " \\n";
                        SceneStringArray.push(StringScene);
                    } else if (StatesDelayAction == 'setallalarmtriggerfirelevel') {
                        var StateNumToggleString = StateNumToggleArray.join(" ");
                        var StringScene = StatesDelayAction + " " + StatesSerialNo + " " + StatesNumSWitch + " " + StateNumToggleString + " " + StateZero + " \\n";
                        SceneStringArray.push(StringScene);
                    }
                    // console.log(StringScene + "Nothing" + i);
                }
            }
        }
        //---------------------------------------------------------End of Existing command---------------------------------------------------------------------------

        // //For remove duplicate
        var uniqueSceneStringArray = [];
        $.each(SceneStringArray, function(i, el) {
            if ($.inArray(el, uniqueSceneStringArray) === -1) uniqueSceneStringArray.push(el);
        });

        var SceneString = uniqueSceneStringArray.join("");
        // console.log(SceneString);
        hrefSceneCommand = baseURL + "/sceneCommand.php/?Id=" + SceneId + "&Commands=" + SceneString;
        var loader = document.getElementById("loader");
        Swal.fire({
            title: 'Save Commands Now?',
            text: "Click Yes to save Commands",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                loader.style.display = "block";
                $.getJSON(hrefSceneCommand, function(data) {
                    //data is the JSON string
                    loader.style.display = "none";
                    if (data.Command == 'UpdateSceneCommands' && data.Reply == true) {
                        Swal.fire(
                            'Good job!',
                            'You have save the scene command',
                            'success'
                        )
                        setTimeout(function() {
                            window.location.href = "SceneList.php";
                        }, 2000);
                    } else if (data.Status == false) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Failed',
                            text: data.Message
                        })
                    }
                });
            }
        })

    }

    function changeToggleSwitch(DeviceInfo) {
        var fields = DeviceInfo.split(" ");
        if (document.getElementById("switch" + fields[0] + " " + fields[1]).value == '') {
            //Blue
            document.getElementById("img" + fields[0] + " " + fields[1]).src = "Images/Scene/onOnTimerOff.png";
            document.getElementById("switch" + fields[0] + " " + fields[1]).value = 1;

        } else if (document.getElementById("switch" + fields[0] + " " + fields[1]).value == 1) {
            //Grey
            document.getElementById("img" + fields[0] + " " + fields[1]).src = "Images/Scene/onOffTimerOff.png";
            document.getElementById("switch" + fields[0] + " " + fields[1]).value = 2;

        } else if (document.getElementById("switch" + fields[0] + " " + fields[1]).value == 2) {
            //Alarm
            document.getElementById("img" + fields[0] + " " + fields[1]).src = "Images/Scene/onAlarm.png";
            document.getElementById("switch" + fields[0] + " " + fields[1]).value = 100;

        } else if (document.getElementById("switch" + fields[0] + " " + fields[1]).value == 100) {
            //Default
            document.getElementById("img" + fields[0] + " " + fields[1]).src = "Images/Scene/onNoAction.png";
            document.getElementById("switch" + fields[0] + " " + fields[1]).value = '';
        }
        document.getElementById("serialToggle" + fields[0] + " " + fields[1]).value = fields[0];
        document.getElementById("switchToggle" + fields[0] + " " + fields[1]).value = fields[1];
    }

    function changeToggleCurtain(DeviceInfo) {
        var fields = DeviceInfo.split(" ");
        if (document.getElementById("switch" + fields[0] + " " + fields[1]).value == '') {
            //Blue
            document.getElementById("img" + fields[0] + " " + fields[1]).src = "Images/Scene/curtainshutterOnTimerOff.png";
            document.getElementById("switch" + fields[0] + " " + fields[1]).value = 1;

        } else if (document.getElementById("switch" + fields[0] + " " + fields[1]).value == 1) {
            //Default
            document.getElementById("img" + fields[0] + " " + fields[1]).src = "Images/Scene/onNoAction.png";
            document.getElementById("switch" + fields[0] + " " + fields[1]).value = '';
        }

        document.getElementById("serialToggle" + fields[0] + " " + fields[1]).value = fields[0];
        document.getElementById("switchToggle" + fields[0] + " " + fields[1]).value = fields[1];
    }

    function closeForm(id) {
        document.getElementById(id).style.display = "none";
    }

    function disableTextBox(obj) {
        var selected = document.delayForm.delayactions.options[obj.selectedIndex].value;
        // invalid selection
        if (selected == 'setswitchdelaytimer') {
            document.delayForm.hours.disabled = false;
            document.delayForm.minutes.disabled = false;
            document.delayForm.switchactions.disabled = false;
        }
        // ------------
        // valid selection
        if (selected == '' || selected == 'cancelswitchdelaytimer') {
            document.delayForm.hours.disabled = true;
            document.delayForm.minutes.disabled = true;
            document.delayForm.switchactions.disabled = true;

            document.delayForm.hours.value = '';
            document.delayForm.minutes.value = '';
            document.delayForm.switchactions.value = 'chooseaction';
        }
    }

    $(document).ready(function() {
        $('#mySmartSwitchTable').DataTable();
        $('#mySmartCurtainTable').DataTable();
        $('#mySmartShutterTable').DataTable();
    });
</script>

<body>
    <?php
    //Get all the session variable from Dashboard.php
    $SerialGateway = $_SESSION['serialGateway'];
    $Password = $_SESSION['password'];
    $LocationListArray = $_SESSION['location'];
    $SmartSwitchStatus = $_SESSION['SmartSwitchStatus'];
    $CurtainStatus = $_SESSION['CurtainStatus'];
    $AlarmStatus = $_SESSION['AlarmStatus'];
    $ShutterStatus = $_SESSION['ShutterStatus'];
    $IRBlasterStatus = $_SESSION['IRBlasterStatus'];
    $LockStatus = $_SESSION['LockStatus'];
    $TotalDevice = $_SESSION['TotalDevice'];
    $SceneCount = $_SESSION['SceneCount'];
    $AdminPassword = $_SESSION['AdminPassword'];

    //Get Id and Name from Scene page
    $SceneId = ($_POST['SceneId']);
    $SceneName = ($_POST['SceneName']);

    echo "<input style='display:none;' id='SerialGateway' value='$SerialGateway'>";
    echo "<input style='display:none;' id='Password' value='$Password'>";
    echo "<input style='display:none;' id='AdminPassword' value='$AdminPassword'>";

    if (isset($_POST['Logout'])) {
        header("Location: ChooseGateway.php");
        header("Refresh:0");
        ob_flush();
        session_destroy();
        exit();
    }

    if (isset($_POST['Home'])) {
        // header("Refresh:0");
        header("Location: Dashboard.php");
        ob_flush();
        exit();
    }

    $opts = array('http' => array('header' => 'Cookie: ' . $_SERVER['HTTP_COOKIE'] . "\r\n"));  //Create array to send session id in the cookie
    $context = stream_context_create($opts);    //Creates and returns a stream context
    session_write_close();  // Write session data and end session 
    $url = $baseURL . "/sceneSceneCommand.php/?Id=$SceneId"; //Create API url to get command scene
    $CommandSceneJson = file_get_contents($url, false, $context); //Reads entire file into a string
    $msgJson = json_decode($CommandSceneJson); //Convert command scene or error message from json string 

    //If there any error message
    if (!empty($msgJson->Message)) {
        $Error = $msgJson->Message;
    ?>
        <script type="text/javascript">
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: '<?php echo $Error; ?>',
            }).then(function() {
                window.location.href = "ChooseGateway.php";
            });
        </script>
    <?php
        die();
    } else {
        //To decompress the whole command scene data
        $Compression = $msgJson->Compression;
        $decode = base64_decode($Compression);
        $Decompress = gzdecode($decode);
        $msgJson =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));
    }

    function getList($msgJson)
    {

        if ($msgJson->Command == 'GetSceneCommandsAPI') {

            //To decompress state list array
            $Compress = $msgJson->Compress;
            $decode = base64_decode($Compress);
            $Decompress = gzdecode($decode);
            $msgJson =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));

            if (!empty($msgJson->States)) {
                $StateArray = $msgJson->States;
                $fields = explode("\n", $StateArray[0]);
            } else {
                $StateArray = array();
                array_push($StateArray, " ");
                $fields = explode("\n", $StateArray[0]);
            }

            if (isset($_SESSION['SmartSwitch'])) {
                $SmartSwitchListArray = $_SESSION['SmartSwitch'];
            } else {
                $SmartSwitchListArray = "Empty";
            }
            if (isset($_SESSION['SmartCurtain'])) {
                $SmartCurtainListArray = $_SESSION['SmartCurtain'];
            } else {
                $SmartCurtainListArray = "Empty";
            }
            if (isset($_SESSION['SmartShutter'])) {
                $SmartShutterListArray = $_SESSION['SmartShutter'];
            } else {
                $SmartShutterListArray = "Empty";
            }

            //-------------------------------------Smart Switch Table--------------------------------------------------
            if ($SmartSwitchListArray != 'Empty') {
                echo "<h4 style='margin-top: 30px;'>Smart Switch</h4>";
                echo "<div class='table-responsive'>";
                echo '<table class="table table-bordered table-hover" id="mySmartSwitchTable" style="width:100%">';
                echo "<thead class='thead-dark'>";
                echo "<tr>";
                echo "<th scope='col'>No.</th>";
                echo "<th scope='col'>Name</th>";
                echo "<th scope='col'>Timer</th>";
                echo "<th scope='col'>Switch</th>";
                echo "</tr>";
                echo "</thead>";
                echo "<tbody id='myBodySwitch'>";

                $count = 1;
                $no = 1;
                foreach ($SmartSwitchListArray as $SmartSwitch) {

                    $SwitchArray = $SmartSwitch->SwitchList;
                    $SmartSwitchType = $SmartSwitch->Model == 37 ? "(Senzo Touch 1 On/Off)" : ($SmartSwitch->Model == 38 ? "(Senzo Touch 2 On/Off)" : ($SmartSwitch->Model == 39 ? "(Senzo Touch 3 On/Off)" : ($SmartSwitch->Model == 42 ? "(Senzo Touch 1 High Power)" : "")));

                    foreach ($SwitchArray as $switch) {

                        $SwitchInfo = $switch->SerialNo . " " . $switch->SwitchNo;

                        $NewStateArray = array();
                        foreach ($fields as $states) {
                            if (!empty($states)) {
                                array_push($NewStateArray, $states);
                            }
                        }

                        echo "<tr class='table-light'>";
                        echo "<td style='width:5%;'>$no</td>";
                        echo "
                    <input style='display:none;' id='serial$SwitchInfo' class='serial'>
                    <input style='display:none;' id='switchno$SwitchInfo' class='switchno'>
                    <input style='display:none;' id='delayAction$SwitchInfo' class='delayAction' value='chooseoption'>
                    <input style='display:none;' id='switchAction$SwitchInfo' class='switchAction' value='chooseoption'>
                    <input style='display:none;' id='minutes$SwitchInfo' class='minutes'>

                    <input style='display:none;' id='serialToggle$SwitchInfo' class='serialToggle'>
                    <input style='display:none;' id='switchToggle$SwitchInfo' class='switchToggle'>";

                        echo "<td>$switch->Name $SmartSwitchType</td>";

                        //Display delay timer state based on state list
                        if (!empty($NewStateArray)) {
                            $BoolState = false;
                            for ($i = 0; $i < count($NewStateArray); $i++) {
                                $StatesString = explode(" ", $NewStateArray[$i]);
                                $StatesStatus = $StatesString[0];
                                if ($StatesStatus == 'setswitchdelaytimer' || $StatesStatus == 'cancelswitchdelaytimer') {
                                    $StatesSerialNo = $StatesString[1];
                                    $StatesNo = $StatesString[2];

                                    if ($StatesStatus == 'setswitchdelaytimer' && $StatesSerialNo == $switch->SerialNo && $StatesNo == $switch->SwitchNo) {
                                        $StatesInfo = $StatesStatus . ' ' . $StatesString[3] . ' ' . $StatesString[4];
                                        echo "<td style='width:10%;'><input style='height:40px;width:40px;' id='delay$SwitchInfo' type='image' src='Images/Scene/delayTimerOn.png' alt='delayTimerOn' onclick='openDelayTimer(&#39;$SwitchInfo&#39;, &#39;$StatesInfo&#39;)'></td>";
                                        $BoolState = false;
                                        break;
                                    } else if ($StatesStatus == 'cancelswitchdelaytimer' && $StatesSerialNo == $switch->SerialNo && $StatesNo == $switch->SwitchNo) {
                                        echo "<td style='width:10%;'><input style='height:40px;width:40px;' id='delay$SwitchInfo' type='image' src='Images/Scene/delayTimerCancel.png' alt='delayTimerCancel' onclick='openDelayTimer(&#39;$SwitchInfo&#39;, &#39;$StatesStatus&#39;)'></td>";
                                        $BoolState = false;
                                        break;
                                    } else {
                                        $BoolState = true;
                                    }
                                } else {
                                    $BoolState = true;
                                }
                            }
                            if ($BoolState == true) {
                                $StatesStatus = 'chooseaction';
                                echo "<td style='width:10%;'><input style='height:40px;width:40px;' id='delay$SwitchInfo' type='image' src='Images/Scene/delayTimerOff.png' alt='delayTimerOff' onclick='openDelayTimer(&#39;$SwitchInfo&#39;, &#39;$StatesStatus&#39;)'></td>";
                            }
                        } else {
                            $StatesStatus = 'chooseaction';
                            echo "<td style='width:10%;'><input style='height:40px;width:40px;' id='delay$SwitchInfo' type='image' src='Images/Scene/delayTimerOff.png' alt='delayTimerOFf' onclick='openDelayTimer(&#39;$SwitchInfo&#39;, &#39;$StatesStatus&#39;)'></td>";
                        }

                        //Display toggle switch state based on state list
                        if (!empty($NewStateArray)) {
                            $BoolState = false;
                            for ($i = 0; $i < count($NewStateArray); $i++) {
                                $StatesString = explode(" ", $NewStateArray[$i]);
                                $StatesStatus = $StatesString[0];

                                if ($StatesStatus == 'toggleallswitches' || $StatesStatus == 'setallalarmtriggerfirelevel') {
                                    $StatesSerialNo = $StatesString[1];
                                    $StatesSwitchNum = $StatesString[2];
                                    $StateSwithNoArray = array();

                                    for ($j = 0; $j < $StatesSwitchNum; $j++) {
                                        array_push($StateSwithNoArray, $StatesString[3 + $j]);
                                    }

                                    if ($StatesStatus == 'toggleallswitches' && $StatesSerialNo == $switch->SerialNo && $StateSwithNoArray[$switch->SwitchNo - 1] == 1) {
                                        echo "<td style='width:10%;'><input style='height:40px;width:40px;' id='img$SwitchInfo' type='image' src='Images/Scene/onOnTimerOff.png' alt='onNoAction' onclick='changeToggleSwitch(&#39;$SwitchInfo&#39;)' value='1'></td>";
                                        echo "<input style='display:none;' id='switch$SwitchInfo' value='1' class='typeToggle'>";
                                        $BoolState = false;
                                        break;
                                    } else if ($StatesStatus == 'toggleallswitches' && $StatesSerialNo == $switch->SerialNo && $StateSwithNoArray[$switch->SwitchNo - 1] == 2) {
                                        echo "<td style='width:10%;'><input style='height:40px;width:40px;' id='img$SwitchInfo' type='image' src='Images/Scene/onOffTimerOff.png' alt='onNoAction' onclick='changeToggleSwitch(&#39;$SwitchInfo&#39;)' value='2'></td>";
                                        echo "<input style='display:none;' id='switch$SwitchInfo' value='2' class='typeToggle'>";
                                        $BoolState = false;
                                        break;
                                    } else if ($StatesStatus == 'setallalarmtriggerfirelevel' && $StatesSerialNo == $switch->SerialNo && $StateSwithNoArray[$switch->SwitchNo - 1] == 100) {
                                        echo "<td style='width:10%;'><input style='height:40px;width:40px;' id='img$SwitchInfo' type='image' src='Images/Scene/onAlarm.png' alt='onNoAction' onclick='changeToggleSwitch(&#39;$SwitchInfo&#39;)' value='100'></td>";
                                        echo "<input style='display:none;' id='switch$SwitchInfo' value='100' class='typeToggle'>";
                                        $BoolState = false;
                                        break;
                                    } else {
                                        $BoolState = true;
                                    }
                                } else {
                                    $BoolState = true;
                                }
                            }
                            if ($BoolState == true) {
                                echo "<td style='width:10%;'><input style='height:40px;width:40px;' id='img$SwitchInfo' type='image' src='Images/Scene/onNoAction.png' alt='onNoAction' onclick='changeToggleSwitch(&#39;$SwitchInfo&#39;)'></td>";
                                echo "<input style='display:none;' id='switch$SwitchInfo' value='' class='typeToggle' >";
                            }
                        } else {
                            echo "<td style='width:10%;'><input style='height:40px;width:40px;' id='img$SwitchInfo' type='image' src='Images/Scene/onNoAction.png' alt='onNoAction' onclick='changeToggleSwitch(&#39;$SwitchInfo&#39;)'></td>";
                            echo "<input style='display:none;' id='switch$SwitchInfo' value='' class='typeToggle'>";
                        }
                        echo "</tr>";
                        $no++;
                    }
                    $count++;
                }
                echo "</tbody>";
                echo "</table>";
                echo "</div>";
            }


            //-------------------------------------Smart Curtain Table--------------------------------------------------
            if ($SmartCurtainListArray != "Empty") {
                echo "<h4 style='margin-top: 30px;'>Smart Curtain</h4>";
                echo "<div class='table-responsive'>";
                echo '<table class="table table-bordered table-hover" id="mySmartCurtainTable" style="width:100%">';
                echo "<thead class='thead-dark'>";
                echo "<tr>";
                echo "<th scope='col'>No.</th>";
                echo "<th scope='col'>Name</th>";
                echo "<th scope='col'>Timer</th>";
                echo "<th scope='col'>Switch</th>";
                echo "</tr>";
                echo "</thead>";
                echo "<tbody id='myTableCurtain'>";

                $count = 1;
                $no = 1;
                foreach ($SmartCurtainListArray as $SmartCurtain) {

                    $CurtainArray = $SmartCurtain->CurtainList;

                    foreach ($CurtainArray as $curtain) {

                        $CurtainInfo = $curtain->SerialNo . " " . $curtain->SwitchNo;

                        $NewStateArray = array();
                        foreach ($fields as $states) {
                            if (!empty($states)) {
                                array_push($NewStateArray, $states);
                            }
                        }

                        echo "<tr class='table-light'>";
                        echo "<td style='width:5%;'>$no</td>";
                        echo "
                    <input style='display:none;' id='serial$CurtainInfo' class='serial'>
                    <input style='display:none;' id='switchno$CurtainInfo' class='switchno'>
                    <input style='display:none;' id='delayAction$CurtainInfo' class='delayAction'>
                    <input style='display:none;' id='switchAction$CurtainInfo' class='switchAction'>
                    <input style='display:none;' id='minutes$CurtainInfo' class='minutes'>     

                    <input style='display:none;' id='serialToggle$CurtainInfo' class='serialToggle'>
                    <input style='display:none;' id='switchToggle$CurtainInfo' class='switchToggle'>";
                        echo "<td>$curtain->Name</td>";

                        //Display delay timer state based on state list
                        if (!empty($NewStateArray)) {
                            $BoolState = false;
                            for ($i = 0; $i < count($NewStateArray); $i++) {
                                $StatesString = explode(" ", $NewStateArray[$i]);
                                $StatesStatus = $StatesString[0];
                                if ($StatesStatus == 'setswitchdelaytimer' || $StatesStatus == 'cancelswitchdelaytimer') {
                                    $StatesSerialNo = $StatesString[1];
                                    $StatesNo = $StatesString[2];

                                    if ($StatesStatus == 'setswitchdelaytimer' && $StatesSerialNo == $curtain->SerialNo && $StatesNo == $curtain->SwitchNo) {
                                        $StatesInfo = $StatesStatus . ' ' . $StatesString[3] . ' ' . $StatesString[4];
                                        echo "<td style='width:10%;'><input style='height:40px;width:40px;' id='delay$CurtainInfo' type='image' src='Images/Scene/delayTimerOn.png' alt='stop' onclick='openDelayTimer(&#39;$CurtainInfo&#39;, &#39;$StatesInfo&#39;)'></td>";
                                        $BoolState = false;
                                        break;
                                    } else if ($StatesStatus == 'cancelswitchdelaytimer' && $StatesSerialNo == $curtain->SerialNo && $StatesNo == $curtain->SwitchNo) {
                                        echo "<td style='width:10%;'><input style='height:40px;width:40px;' id='delay$CurtainInfo' type='image' src='Images/Scene/delayTimerCancel.png' alt='stop' onclick='openDelayTimer(&#39;$CurtainInfo&#39;, &#39;$StatesStatus&#39;)'></td>";
                                        $BoolState = false;
                                        break;
                                    } else {
                                        $BoolState = true;
                                    }
                                } else {
                                    $BoolState = true;
                                }
                            }
                            if ($BoolState == true) {
                                $StatesStatus = 'chooseaction';
                                echo "<td style='width:10%;'><input style='height:40px;width:40px;' id='delay$CurtainInfo' type='image' src='Images/Scene/delayTimerOff.png' alt='stop' onclick='openDelayTimer(&#39;$CurtainInfo&#39;, &#39;$StatesStatus&#39;)'></td>";
                            }
                        } else {
                            $StatesStatus = 'chooseaction';
                            echo "<td style='width:10%;'><input style='height:40px;width:40px;' id='delay$CurtainInfo' type='image' src='Images/Scene/delayTimerOff.png' alt='stop' onclick='openDelayTimer(&#39;$CurtainInfo&#39;, &#39;$StatesStatus&#39;)'></td>";
                        }

                        //Display toggle switch state based on state list
                        if (!empty($NewStateArray)) {
                            $BoolState = false;
                            for ($i = 0; $i < count($NewStateArray); $i++) {
                                $StatesString = explode(" ", $NewStateArray[$i]);
                                $StatesStatus = $StatesString[0];

                                if ($StatesStatus == 'toggleallswitches' || $StatesStatus == 'setallalarmtriggerfirelevel') {
                                    $StatesSerialNo = $StatesString[1];
                                    $StatesSwitchNum = $StatesString[2];
                                    $StateSwithNoArray = array();

                                    for ($j = 0; $j < $StatesSwitchNum; $j++) {
                                        array_push($StateSwithNoArray, $StatesString[3 + $j]);
                                    }

                                    if ($StatesStatus == 'toggleallswitches' && $StatesSerialNo == $curtain->SerialNo && $StateSwithNoArray[$curtain->SwitchNo - 1] == 1) {
                                        echo "<td style='width:10%;'><input style='height:40px;width:40px;' id='img$CurtainInfo' type='image' src='Images/Scene/curtainshutterOnTimerOff.png' alt='onNoAction' onclick='changeToggleCurtain(&#39;$CurtainInfo&#39;)' value='1'></td>";
                                        echo "<input style='display:none;' id='switch$CurtainInfo' value='1' class='typeToggle'>";
                                        $BoolState = false;
                                        break;
                                    } else {
                                        $BoolState = true;
                                    }
                                } else {
                                    $BoolState = true;
                                }
                            }
                            if ($BoolState == true) {
                                echo "<td style='width:10%;'><input style='height:40px;width:40px;' id='img$CurtainInfo' type='image' src='Images/Scene/onNoAction.png' alt='onNoAction' onclick='changeToggleCurtain(&#39;$CurtainInfo&#39;)'></td>";
                                echo "<input style='display:none;' id='switch$CurtainInfo' class='typeToggle'>";
                            }
                        } else {
                            echo "<td style='width:10%;'><input style='height:40px;width:40px;' id='img$CurtainInfo' type='image' src='Images/Scene/onNoAction.png' alt='onNoAction' onclick='changeToggleCurtain(&#39;$CurtainInfo&#39;)'></td>";
                            echo "<input style='display:none;' id='switch$CurtainInfo' class='typeToggle'>";
                        }
                        echo "</tr>";
                        $no++;
                    }
                    $count++;
                }
                echo "</tbody>";
                echo "</table>";
                echo "</div>";
            }

            //-------------------------------------Smart Shutter Table--------------------------------------------------
            if ($SmartShutterListArray != "Empty") {
                echo "<h4 style='margin-top: 30px;'>Smart Shutter</h4>";
                echo "<div class='table-responsive'>";
                echo '<table class="table table-bordered table-hover" id="mySmartShutterTable" style="width:100%">';
                echo "<thead class='thead-dark'>";
                echo "<tr>";
                echo "<th scope='col'>No.</th>";
                echo "<th scope='col'>Name</th>";
                echo "<th scope='col'>Timer</th>";
                echo "<th scope='col'>Switch</th>";
                echo "</tr>";
                echo "</thead>";
                echo "<tbody id='myBodyShutter'>";

                $count = 1;
                $no = 1;
                foreach ($SmartShutterListArray as $SmartShutter) {

                    $ShutterArray = $SmartShutter->ShutterList;

                    foreach ($ShutterArray as $shutter) {

                        $ShutterInfo = $shutter->SerialNo . " " . $shutter->SwitchNo;

                        $NewStateArray = array();
                        foreach ($fields as $states) {
                            if (!empty($states)) {
                                array_push($NewStateArray, $states);
                            }
                        }

                        echo "<tr class='table-light'>";
                        echo "<td style='width:5%;'>$no</td>";
                        echo "
                    <input style='display:none;' id='serial$ShutterInfo' class='serial'>
                    <input style='display:none;' id='switchno$ShutterInfo' class='switchno'>
                    <input style='display:none;' id='delayAction$ShutterInfo' class='delayAction'>
                    <input style='display:none;' id='switchAction$ShutterInfo' class='switchAction'>
                    <input style='display:none;' id='minutes$ShutterInfo' class='minutes'>

                    <input style='display:none;' id='serialToggle$ShutterInfo' class='serialToggle'>
                    <input style='display:none;' id='switchToggle$ShutterInfo' class='switchToggle'>";
                        echo "<td>$shutter->Name</td>";

                        //Display delay timer state based on state list
                        if (!empty($NewStateArray)) {
                            $BoolState = false;
                            for ($i = 0; $i < count($NewStateArray); $i++) {
                                $StatesString = explode(" ", $NewStateArray[$i]);
                                $StatesStatus = $StatesString[0];

                                if ($StatesStatus == 'setswitchdelaytimer' || $StatesStatus == 'cancelswitchdelaytimer') {
                                    $StatesSerialNo = $StatesString[1];
                                    $StatesNo = $StatesString[2];
                                    if ($StatesStatus == 'setswitchdelaytimer' && $StatesSerialNo == $shutter->SerialNo && $StatesNo == $shutter->SwitchNo) {
                                        $StatesInfo = $StatesStatus . ' ' . $StatesString[3] . ' ' . $StatesString[4];
                                        echo "<td style='width:10%;'><input style='height:40px;width:40px;' id='delay$ShutterInfo' type='image' src='Images/Scene/delayTimerOn.png' alt='stop' onclick='openDelayTimer(&#39;$ShutterInfo&#39;, &#39;$StatesInfo&#39;)'></td>";
                                        $BoolState = false;
                                        break;
                                    } else if ($StatesStatus == 'cancelswitchdelaytimer' && $StatesSerialNo == $shutter->SerialNo && $StatesNo == $shutter->SwitchNo) {
                                        echo "<td style='width:10%;'><input style='height:40px;width:40px;' id='delay$ShutterInfo' type='image' src='Images/Scene/delayTimerCancel.png' alt='stop' onclick='openDelayTimer(&#39;$ShutterInfo&#39;, &#39;$StatesStatus&#39;)'></td>";
                                        $BoolState = false;
                                        break;
                                    } else {
                                        $BoolState = true;
                                    }
                                } else {
                                    $BoolState = true;
                                }
                            }
                            if ($BoolState == true) {
                                $StatesStatus = 'chooseaction';
                                echo "<td style='width:10%;'><input style='height:40px;width:40px;' id='delay$ShutterInfo' type='image' src='Images/Scene/delayTimerOff.png' alt='stop' onclick='openDelayTimer(&#39;$ShutterInfo&#39;, &#39;$StatesStatus&#39;)'></td>";
                            }
                        } else {
                            $StatesStatus = 'chooseaction';
                            echo "<td style='width:10%;'><input style='height:40px;width:40px;' id='delay$ShutterInfo' type='image' src='Images/Scene/delayTimerOff.png' alt='stop' onclick='openDelayTimer(&#39;$ShutterInfo&#39;, &#39;$StatesStatus&#39;)'></td>";
                        }

                        //Display toggle switch state based on state list
                        if (!empty($NewStateArray)) {
                            $BoolState = false;
                            for ($i = 0; $i < count($NewStateArray); $i++) {
                                $StatesString = explode(" ", $NewStateArray[$i]);
                                $StatesStatus = $StatesString[0];

                                if ($StatesStatus == 'toggleallswitches' || $StatesStatus == 'setallalarmtriggerfirelevel') {
                                    $StatesSerialNo = $StatesString[1];
                                    $StatesSwitchNum = $StatesString[2];
                                    $StateSwithNoArray = array();

                                    for ($j = 0; $j < $StatesSwitchNum; $j++) {
                                        array_push($StateSwithNoArray, $StatesString[3 + $j]);
                                    }

                                    if ($StatesStatus == 'toggleallswitches' && $StatesSerialNo == $shutter->SerialNo && $StateSwithNoArray[$shutter->SwitchNo - 1] == 1) {
                                        echo "<td style='width:10%;'><input style='height:40px;width:40px;' id='img$ShutterInfo' type='image' src='Images/Scene/curtainshutterOnTimerOff.png' alt='onNoAction' onclick='changeToggleCurtain(&#39;$ShutterInfo&#39;)' value='1'></td>";
                                        echo "<input style='display:none;' id='switch$ShutterInfo' value='1' class='typeToggle'>";
                                        $BoolState = false;
                                        break;
                                    } else {
                                        $BoolState = true;
                                    }
                                } else {
                                    $BoolState = true;
                                }
                            }
                            if ($BoolState == true) {
                                echo "<td style='width:10%;'><input style='height:40px;width:40px;' id='img$ShutterInfo' type='image' src='Images/Scene/onNoAction.png' alt='onNoAction' onclick='changeToggleCurtain(&#39;$ShutterInfo&#39;)'></td>";
                                echo "<input style='display:none;' id='switch$ShutterInfo' class='typeToggle'>";
                            }
                        } else {
                            echo "<td style='width:10%;'><input style='height:40px;width:40px;' id='img$ShutterInfo' type='image' src='Images/Scene/onNoAction.png' alt='onNoAction' onclick='changeToggleCurtain(&#39;$ShutterInfo&#39;)'></td>";
                            echo "<input style='display:none;' id='switch$ShutterInfo' class='typeToggle'>";
                        }
                        echo "</tr>";
                        $no++;
                    }
                    $count++;
                }
                echo "</tbody>";
                echo "</table>";
                echo "</div>";
            }
        }
    }


    ?>
    <input style='display:none;' id='IntervalId' value='none'>
    <input style='display:none;' class='Minutes'>
    <input style='display:none;' class='Hours'>
    <input style='display:none;' class='serial'>
    <input style='display:none;' class='switchno'>

    <!-- Delay Timer popup -->
    <div class='form-popup' id='myDelayForm'>
        <div class="form-container" style='width:450px;padding:0px;'>
            <div class="modal-header" style='width:100%;height:60px;'>
                <button class='close' onclick='closeForm("myDelayForm")'></button>
                <h3 id='delayTimer' class='h2form'>Delay Timer</h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <h5 style='margin-bottom:30px;text-align: center;' id='delayTimerStatus'></h5>
                <form method='post' class='form' name='delayForm'>
                    <div class='omrs-input-group'>
                        <div class='omrs-input-underlined omrs-input-danger' style="margin-bottom: 1px;">
                            <label for='actions'>Delay Actions :</label>
                            <select id='delayactions' name='delayactions' onchange='disableTextBox(this)' style='width:100%;'>
                                <option value='chooseaction' selected disabled>Choose action</option>
                                <option value=''>Nothing</option>
                                <option value='setswitchdelaytimer'>Set delay timer</option>
                                <option value='cancelswitchdelaytimer'>Cancel delay timer</option>
                            </select></br></br>
                            <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'>
                                <path fill='none' d='M0 0h24v24H0V0z' />
                                <circle cx='15.5' cy='9.5' r='1.5' />
                                <circle cx='8.5' cy='9.5' r='1.5' />
                                <path d='M10.01,21.01c0,1.1 0.89,1.99 1.99,1.99s1.99,-0.89 1.99,-1.99h-3.98zM18.88,16.82L18.88,11c0,-3.25 -2.25,-5.97 -5.29,-6.69v-0.72C13.59,2.71 12.88,2 12,2s-1.59,0.71 -1.59,1.59v0.72C7.37,5.03 5.12,7.75 5.12,11v5.82L3,18.94L3,20h18v-1.06l-2.12,-2.12zM16,13.01h-3v3h-2v-3L8,13.01L8,11h3L11,8h2v3h3v2.01z' /></svg>
                        </div>
                    </div>
                    <div class='omrs-input-group'>
                        <div class='omrs-input-underlined omrs-input-danger' style="margin-bottom: 1px;">
                            <label for='actions'>Switch Action :</label>
                            <select id='switchactions' name='switchactions' style='width:100%;'>
                                <option value='chooseaction' selected disabled>Choose action</option>
                                <option value='1'>On</option>
                                <option value='0'>Off</option>
                            </select></br></br>
                            <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'>
                                <path fill='none' d='M0 0h24v24H0V0z' />
                                <circle cx='15.5' cy='9.5' r='1.5' />
                                <circle cx='8.5' cy='9.5' r='1.5' />
                                <path d='M10.01,21.01c0,1.1 0.89,1.99 1.99,1.99s1.99,-0.89 1.99,-1.99h-3.98zM18.88,16.82L18.88,11c0,-3.25 -2.25,-5.97 -5.29,-6.69v-0.72C13.59,2.71 12.88,2 12,2s-1.59,0.71 -1.59,1.59v0.72C7.37,5.03 5.12,7.75 5.12,11v5.82L3,18.94L3,20h18v-1.06l-2.12,-2.12zM16,13.01h-3v3h-2v-3L8,13.01L8,11h3L11,8h2v3h3v2.01z' /></svg>
                        </div>
                    </div>
                    <div class='omrs-input-group'>
                        <div class='omrs-input-underlined ' style='width:100%;'>
                            <label for='quantityHours'>Hours :</label>
                            <input type='number' id='hours' name='hours' min='1' max='59' required></br></br>
                            <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'>
                                <path fill='none' d='M0 0h24v24H0V0z' />
                                <circle cx='15.5' cy='9.5' r='1.5' />
                                <circle cx='8.5' cy='9.5' r='1.5' />
                                <path d='M22,5.72l-4.6,-3.86 -1.29,1.53 4.6,3.86L22,5.72zM7.88,3.39L6.6,1.86 2,5.71l1.29,1.53 4.59,-3.85zM12.5,8L11,8v6l4.75,2.85 0.75,-1.23 -4,-2.37L12.5,8zM12,4c-4.97,0 -9,4.03 -9,9s4.02,9 9,9c4.97,0 9,-4.03 9,-9s-4.03,-9 -9,-9zM12,20c-3.87,0 -7,-3.13 -7,-7s3.13,-7 7,-7 7,3.13 7,7 -3.13,7 -7,7z' /></svg>
                        </div>
                    </div>
                    <div class='omrs-input-group'>
                        <div class='omrs-input-underlined' style='width:100%;'>
                            <label for='quantityMinutes'>Minutes :</label>
                            <input type='number' id='minutes' name='minutes' min='1' max='60' required></br></br>
                            <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'>
                                <path fill='none' d='M0 0h24v24H0V0z' />
                                <circle cx='15.5' cy='9.5' r='1.5' />
                                <circle cx='8.5' cy='9.5' r='1.5' />
                                <path d='M22,5.72l-4.6,-3.86 -1.29,1.53 4.6,3.86L22,5.72zM7.88,3.39L6.6,1.86 2,5.71l1.29,1.53 4.59,-3.85zM12.5,8L11,8v6l4.75,2.85 0.75,-1.23 -4,-2.37L12.5,8zM12,4c-4.97,0 -9,4.03 -9,9s4.02,9 9,9c4.97,0 9,-4.03 9,-9s-4.03,-9 -9,-9zM12,20c-3.87,0 -7,-3.13 -7,-7s3.13,-7 7,-7 7,3.13 7,7 -3.13,7 -7,7z' /></svg>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
                <button type='button' class='all' onclick='myDelayTimerFunction()'>Apply Timer</button>
            </div>
        </div>
    </div>

    <!-- Form for sign Door Lock -->
    <div class="form-popup" id="mySiginDoorLockForm">
        <div class="form-container" style="width:400px;padding:0px;">
            <div class="modal-header" style='width:100%;height:70px;'>
                <!-- <a class='back' onclick='closeFormSigin()'><span class='fa fa-arrow-left fa-2x'></a> -->
                <h3 class='h2form'>Manage Door Lock </br> Password</h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <fieldset style='width:100%;'>
                    <label for="name">Enter Admin Password:</label>
                    <input type="password" id="password" name="DoorlockPassword" style='width:100%;'>
                </fieldset>
            </div>
            <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
                <button type="submit" class="all" onclick='openGetLock()' name="signin">Ok</button>
                <button type="button" class="all" onclick="cancelFormSigin()">Cancel</button>
            </div>
        </div>
    </div>

    <!-- SideBar + Navbar -->
    <div class="wrapper">
        <!-- Sidebar Holder -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>Senzo</h3>
                <strong>SZ</strong>
            </div>
            <ul class="list-unstyled components">
                <!-- Home Section -->
                <li>
                    <form action='CommandScenes.php' method='POST' id='NavHome' style='margin-block-end: 0'>
                        <input style='display:none;' name='Home'>
                        <a style='cursor:pointer;' class='Home'>
                            <i class="glyphicon glyphicon-home"></i>
                            Home
                        </a>
                    </form>
                </li>
                <li>
                    <!-- Device Section -->
                    <a href="#pageDevice" data-toggle="collapse" aria-expanded="false">
                        <i class="glyphicon glyphicon-tasks"></i>
                        <?php echo "Device"; ?>
                    </a>
                    <ul class="collapse list-unstyled" id="pageDevice">
                        <?php
                        if ($SmartSwitchStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectSwitch()">Smart Switch</a></li>';
                        }
                        if ($CurtainStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectCurtain()">Smart Curtain</a></li>';
                        }
                        if ($ShutterStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectShutter()">Smart Shutter</a></li>';
                        }
                        if ($LockStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectDoorLock()">Smart Door Lock</a></li>';
                        }
                        if ($IRBlasterStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectIRBlaster()">IR Blaster</a></li>';
                        }
                        ?>
                    </ul>
                </li>
                <li>
                    <!-- Scene Section -->
                    <a id="cube" style="cursor:pointer;" onclick='openScenesForm()'>
                        <i class="glyphicon glyphicon-film"></i>
                        <?php echo "Scene ($SceneCount)"; ?>
                    </a>
                </li>
                <li>
                    <!-- Security Section -->
                    <a href="#pageSecurity" data-toggle="collapse" aria-expanded="false">
                        <i class="glyphicon glyphicon-lock"></i>
                        <?php echo "Security"; ?>
                    </a>
                    <ul class="collapse list-unstyled" id="pageSecurity">
                        <?php
                        // If smart alarm connected to gateway
                        if ($AlarmStatus) {
                            echo "<li><a style='cursor:pointer;' onclick='selectAlarm(&#39;True&#39;)'>
                            Smart Alarm <img src='Images/Action/security_armed_icon.png' alt='scenes' style='width:20px;height:20px;'>";
                        } else {
                            echo "<li><a style='cursor:pointer;' onclick='selectAlarm(&#39;False&#39;)'>
                            Smart Alarm";
                        }
                        ?>
                        </a>
                </li>
                <li><a style="cursor:pointer;" onclick="selectCamera()">Camera</a></li>
            </ul>
            </li>
            <li>
                <!-- Analytic Section -->
                <a style='cursor:pointer;' onclick='openAnalyticForm()'>
                    <i class='glyphicon glyphicon-stats'></i>
                    Analytic
                </a>
            </li>
            <li>
                <!-- Setting Section -->
                <a style='cursor:pointer;' onclick='openSettingForm()'>
                    <i class='glyphicon glyphicon-cog'></i>
                    Setting
                </a>
            </li>
            </ul>

            <!-- Logout button -->
            <ul class="list-unstyled CTAs">
                <li>
                    <form method="POST" action="CommandScenes.php" id='NavLogout' style='margin-block-end: 0'>
                        <input style='display:none;' name='Logout'>
                        <a style="cursor:pointer;" class="Logout">
                            <i class="glyphicon glyphicon-log-out"></i>
                            Logout
                        </a>
                    </form>
                </li>
            </ul>
        </nav>

        <!-- Page Content Holder -->
        <div id="content" style="width: -webkit-fill-available;">

            <nav class="navbar navbar-default">
                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                            <i class="glyphicon glyphicon-align-left"></i>
                            <span></span>
                        </button>

                        <!-- For responsive part -->
                        <!-- Show and hide content that been targeted-->
                        <button class="btn btn-dark navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <i class="glyphicon glyphicon-align-justify"></i>
                        </button>
                    </div>

                    <!-- Content that have been targeted -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li><?php
                                //To decompress all the data from gateway
                                $Compress = $msgJson->Compress;
                                $decode = base64_decode($Compress);
                                $Decompress = gzdecode($decode);
                                $msgJsonCommandScene =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));

                                if (!empty($msgJsonCommandScene->States)) {
                                    $StateArray = $msgJsonCommandScene->States;
                                    $fields = explode("\n", $StateArray[0]);
                                    // print_r($StateArray);
                                } else {
                                    $StateArray = array();
                                    array_push($StateArray, " ");
                                    $fields = explode("\n", $StateArray[0]);
                                }

                                $SceneId = ($_POST['SceneId']);
                                if (isset($_SESSION['SmartSwitch'])) {
                                    $SmartSwitchListArray = $_SESSION['SmartSwitch'];
                                } else {
                                    $SmartSwitchListArray = "Empty";
                                }
                                if (isset($_SESSION['SmartCurtain'])) {
                                    $SmartCurtainListArray = $_SESSION['SmartCurtain'];
                                } else {
                                    $SmartCurtainListArray = "Empty";
                                }
                                if (isset($_SESSION['SmartShutter'])) {
                                    $SmartShutterListArray = $_SESSION['SmartShutter'];
                                } else {
                                    $SmartShutterListArray = "Empty";
                                }

                                $SwitchInfo = json_encode($SmartSwitchListArray);
                                $CurtainInfo = json_encode($SmartCurtainListArray);
                                $ShutterInfo = json_encode($SmartShutterListArray);
                                $StateInfo = implode(",", $fields);
                                echo "<a style='cursor:pointer;' onclick='saveScenes(&#39;$SwitchInfo&#39;, &#39;$CurtainInfo&#39;, &#39;$ShutterInfo&#39;, &#39;$SceneId&#39;, &#39;$StateInfo&#39;)'><i class='fas fa-cloud'></i> Save Command</a>";
                                ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <?php
            echo "<h2>$SceneName</h2>";
            $GatewayLevel = $_SESSION['level'];
            echo "<h4 class='Level' >Gateway $GatewayLevel</h4>";
            getList($msgJson);
            ?>
            <div class="line"></div>

        </div>
    </div>

    <!-- Auto Refresh dropdown -->
    <div class='autorefresh'>
        <div class="dropdown">
            <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">
                <img src='Images/Switch/settings_gear.png' alt='switchon' style='width:30px;height:30px;animation: spin 3s linear infinite;'>
            </button>
            <ul class="dropdown-menu">
                <!-- <li><a href="#">Phantom</a></li> -->
                <li>
                    <input type='checkbox' id='enable' name='fooby[2][]'>
                    <label> Enable</label><br>
                    <div id='timeRefresh' style='display:none;margin-left:10px;'>
                        <input type='checkbox' value='1-minute' class="slectOne" id='1-minute'>
                        <label> 1 minute</label><br>
                        <input type='checkbox' value='5-minutes' class="slectOne" id='5-minutes'>
                        <label> 5 minutes</label><br>
                        <input type='checkbox' value='10-minutes' class="slectOne" id='10-minutes'>
                        <label> 10 minutes</label><br>
                        <input type='checkbox' value='20-minutes' class="slectOne" id='20-minutes'>
                        <label> 20 minutes</label><br>
                        <input type='checkbox' value='25-minutes' class="slectOne" id='25-minutes'>
                        <label> 25 minutes</label><br>
                        <input type='checkbox' value='1-hour' class="slectOne" id='1-hour'>
                        <label> 1 hour</label><br>
                    </div>
                </li>
                <li>
                    <input type='checkbox' id='disable' value='disable'>
                    <label> Disable</label><br>
                </li>
            </ul>
        </div>
    </div>

    <!-- Loading animation -->
    <div class="form-popup" id='loader'>
        <!-- <div id="loadering" class="loader"></div> -->
        <div class="loading-container">
            <div class="loading"></div>
            <div class="loading-text">loading</div>
        </div>
    </div>

</body>

</html>