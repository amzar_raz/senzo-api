<?php
ini_set('max_execution_time', 30);
session_start();
ob_start();
//Include all file needed to use in this page
include '././configuration/serverConfig.php';
include './header/headerAll.php';;
?>
<!DOCTYPE html>
<html>

<head>
    <title>Scene</title>
    <link href="css/Scene.css" rel="stylesheet">
    <link rel="stylesheet" href="css/Sidebar.css">
    <!-- Sidebar and auto refresh function -->
    <script type="text/javascript" src="SidebarHandler.js"></script>
</head>
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTableScene').DataTable();
    });

    function openAddSceneForm(SceneArray) {

        var parsedSceneArray = JSON.parse(SceneArray);
        if (parsedSceneArray.length <= 100) {
            document.getElementsByClassName("SceneArray").value = SceneArray;
            document.getElementById("addSceneForm").style.display = 'block';
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Scene limit reach'
            })
        }

    }

    function addScene() {
        var name = document.getElementById("sceneAddName").value;
        var SceneArrayJson = document.getElementsByClassName("SceneArray").value;

        var parsedSceneArray = JSON.parse(SceneArrayJson);
        var boolValidName = false;
        var sameName = '';
        if (parsedSceneArray.length != 0) {
            for (i = 0; i < parsedSceneArray.length; i++) {
                if (name == parsedSceneArray[i].Name) {
                    boolValidName = true;
                    sameName = parsedSceneArray[i].Name;
                    break;
                }
            }
        }

        var boolEmptyName = false;
        if (name == null || name == '') {
            boolEmptyName = true;
        }

        var letters = /^[0-9a-zA-Z \_]+$/;

        if (!boolValidName && !boolEmptyName && name.match(letters)) {
            hrefAddScene = baseURL + "/sceneAdd.php/?Name=" + name;
            var loader = document.getElementById("loader");
            loader.style.display = "block";
            $.getJSON(hrefAddScene, function(data) {
                //data is the JSON string
                loader.style.display = "none";
                if (data.Command == 'AddSceneCommands' && data.Reply == true) {
                    Swal.fire(
                        'Good job!',
                        'You have create a scene',
                        'success'
                    )
                    setTimeout(function() {
                        window.location.href = "Dashboard.php";
                    }, 2000);

                } else if (data.Status == false) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: data.Message
                    })
                }
            });
        } else if (boolValidName) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: sameName + ' already exist in Scene Management List'
            })
        } else if (boolEmptyName) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Name cannot be empty'
            })
        } else if (!name.match(letters)) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Name must be alphanumeric characters only'
            })
        }


    }

    function launchCommand(SceneId) {
        var SerialGateway = document.getElementById('SerialGateway').value;
        var Password = document.getElementById('Password').value;
        hrefLaunchScene = baseURL + "/sceneLaunch.php/?Id=" + SceneId;
        hrefGetSceneList = baseURL + "/sceneGetSceneCommand.php/?Id=" + SceneId;
        var loader = document.getElementById("loader");

        Swal.fire({
            title: 'Are you sure want to launch this Scene?',
            text: "Click Yes to launch the Scene Command",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                loader.style.display = "block";
                $.getJSON(hrefLaunchScene, function(data) {
                    //data is the JSON string
                    loader.style.display = "none";
                    if (data.Command == 'RunSceneCommands' && data.Reply == true) {
                        Swal.fire(
                            'Good job!',
                            'The Scene Is Launched Successfully',
                            'success'
                        )
                        //Get current scene command
                        $.getJSON(hrefGetSceneList, function(scene) {
                            if (scene.States.length == 1) {
                                iziToast.show({
                                    title: 'NOTICE',
                                    message: 'No Scene Commands Has Been Set',
                                    theme: 'dark',
                                    position: 'bottomCenter',
                                    icon: 'icon-person'
                                });
                            }

                        });
                    } else if (data.Status == false) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Failed',
                            text: data.Message
                        })
                    }
                });
            }
        })


    }

    function openRenameScene(SceneInfo, SceneArray) {
        var fields = SceneInfo.split("&&");
        var id = fields[0];
        var name = fields[1];

        document.getElementsByClassName("SceneId").value = id;
        document.getElementById("sceneRenameName").value = name;
        document.getElementsByClassName("SceneArray").value = SceneArray;
        document.getElementById("renameSceneForm").style.display = 'block';
    }

    function renameScene() {
        var id = document.getElementsByClassName("SceneId").value;
        var name = document.getElementById("sceneRenameName").value;
        var SceneArrayJson = document.getElementsByClassName("SceneArray").value;

        var parsedSceneArray = JSON.parse(SceneArrayJson);
        // console.log(parsedSceneArray);
        var boolValidName = false;
        var sameName = '';
        for (i = 0; i < parsedSceneArray.length; i++) {
            if (name == parsedSceneArray[i].Name) {
                boolValidName = true;
                sameName = parsedSceneArray[i].Name;
                break;
            }
        }

        var boolEmptyName = false;
        if (name == null || name == '') {
            boolEmptyName = true;
        }

        var letters = /^[0-9a-zA-Z \_]+$/;

        if (!boolValidName && !boolEmptyName && name.match(letters)) {
            hrefRenameScene = baseURL + "/sceneRename.php/?Id=" + id + "&Name=" + name;
            var loader = document.getElementById("loader");
            loader.style.display = "block";
            $.getJSON(hrefRenameScene, function(data) {
                //data is the JSON string
                loader.style.display = "none";
                if (data.Command == 'UpdateSceneProp' && data.Reply == true) {
                    Swal.fire(
                        'Good job!',
                        'Scene name has been successfully updated',
                        'success'
                    )
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
                } else if (data.Status == false) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: data.Message
                    })
                }
            });
        } else if (boolValidName) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: sameName + ' already exist in Scene Management List'
            })
        } else if (boolEmptyName) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Name cannot be empty'
            })
        } else if (!name.match(letters)) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Name must be alphanumeric characters only'
            })
        }

    }

    function deleteScene(SceneId) {
        hrefDeleteScene = baseURL + "/sceneDelete.php/?Id=" + SceneId;

        var loader = document.getElementById("loader");

        Swal.fire({
            title: 'Are you sure want to delete this Scene?',
            text: "Note: All Commands belong to This Scene will be deleted if you proceed",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                loader.style.display = "block";
                $.getJSON(hrefDeleteScene, function(data) {
                    //data is the JSON string
                    loader.style.display = "none";
                    if (data.Command == 'DeleteSceneCommands' && data.Reply == true) {
                        Swal.fire(
                            'Good job!',
                            'Dialogues Scene Successfully Deleted',
                            'success'
                        )
                        setTimeout(function() {
                            window.location.href = "Dashboard.php";
                        }, 2000);

                    } else if (data.Status == false) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Failed',
                            text: data.Message
                        })
                    }
                });
            }
        })
    }

    function closeForm(id) {
        document.getElementById(id).style.display = 'none';
    }
</script>

<body>
    <?php
    //Get all the session variable from Dashboard.php
    $SerialGateway = $_SESSION['serialGateway'];
    $Password = $_SESSION['password'];
    $LocationListArray = $_SESSION['location'];
    $SmartSwitchStatus = $_SESSION['SmartSwitchStatus'];
    $CurtainStatus = $_SESSION['CurtainStatus'];
    $AlarmStatus = $_SESSION['AlarmStatus'];
    $ShutterStatus = $_SESSION['ShutterStatus'];
    $IRBlasterStatus = $_SESSION['IRBlasterStatus'];
    $LockStatus = $_SESSION['LockStatus'];
    $TotalDevice = $_SESSION['TotalDevice'];
    $SceneCount = $_SESSION['SceneCount'];
    $AdminPassword = $_SESSION['AdminPassword'];

    echo "<input style='display:none;' id='SerialGateway' value='$SerialGateway'>";
    echo "<input style='display:none;' id='Password' value='$Password'>";
    echo "<input style='display:none;' id='AdminPassword' value='$AdminPassword'>";

    if (isset($_POST['Logout'])) {
        header("Location: ChooseGateway.php");
        header("Refresh:0");
        ob_flush();
        session_destroy();
        exit();
    }

    if (isset($_POST['Home'])) {
        // header("Refresh:0");
        header("Location: Dashboard.php");
        ob_flush();
        exit();
    }

    $opts = array('http' => array('header' => 'Cookie: ' . $_SERVER['HTTP_COOKIE'] . "\r\n"));  //Create array to send session id in the cookie
    $context = stream_context_create($opts);    //Creates and returns a stream context
    session_write_close();  // Write session data and end session 
    $url = $baseURL . "/sceneSceneList.php";    //Create API url to get scene list
    $SceneJson = file_get_contents($url, false, $context);  //Reads entire file into a string
    $msgJson = json_decode($SceneJson); //Convert scene list from json string 

    //If there any error message
    if (!empty($msgJson->Message)) {
        $Error = $msgJson->Message;
    ?>
        <script type="text/javascript">
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: '<?php echo $Error; ?>',
            }).then(function() {
                window.location.href = "ChooseGateway.php";
            });
        </script>
    <?php
        die();
    } else {
        //To decompress the whole scene list data
        $Compression = $msgJson->Compression;
        $decode = base64_decode($Compression);
        $Decompress = gzdecode($decode);
        $msgJson =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));
    }

    function getList($msgJson)
    {
        if ($msgJson->Command == 'GetSceneAPI' && $msgJson->Reply == true) {

            //To decompress scene list array
            $Compress = $msgJson->Compress;
            $decode = base64_decode($Compress);
            $Decompress = gzdecode($decode);
            $msgJson =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));

            $SceneListArray = $msgJson->SceneList;
            if (!empty($SceneListArray)) {

                $SceneArray = json_encode($SceneListArray);

                echo "<div class='table-responsive'>";
                echo "<table id='myTableScene' class='table-bordered' style='width:100%;margin-top:50px;'>";
                echo "<thead>";
                echo "<tr>";
                echo "<th style='width:10%;'>No</th>";
                echo "<th style='width:50%;'>Name</th>";
                echo "<th style='width:20%;'>Launch</th>";
                echo "<th style='width:20%;'>Action</th>";
                echo "</tr>";
                echo "</thead>";
                echo "<tbody id='myBody' >";

                foreach ($SceneListArray as $SceneList) {

                    echo "<tr class='SceneTable' >";
                    echo "<td>$SceneList->SceneNo</td>";
                    echo "<td>$SceneList->Name</td>";
                    echo "<td><button class='btn3 launch' style='background:#eb4d4b;color:white;' onclick='launchCommand(&#39;$SceneList->Id&#39;)'><span class='fas fa-play'>Launch</button></td>";

                    $SceneId = $SceneList->Id;
                    $SceneName = $SceneList->Name;
                    $SceneInfo = $SceneId . '&&' . $SceneName;
                    echo "<td><form action='CommandScenes.php' method='post' style='display:inline-block;'>
                        <button type='submit' class='btn3' style='background:#f1c40f;'><span class='fa fa-mouse-pointer'></button>
                        <input style='display:none;' value='$SceneId' name='SceneId'  **/>
                        <input style='display:none;' value='$SceneName' name='SceneName' **/>
                        </form>
                        <button class='btn3' style='background:#00a8ff;' onclick='openRenameScene(&#39;$SceneInfo&#39; , &#39;$SceneArray&#39;)'><span class='fa fa-edit'></button>
                        <button class='btn3' style='background:#eb4d4b;' onclick='deleteScene(&#39;$SceneId&#39;)'><span class='fa fa-trash'></button>
                        </td>";
                    echo "</tr>";
                }
                echo "</tbody>";
                echo "</table>";
                echo "</div>";
            } else {
                echo "<h2 class='error'><i class='fas fa-info-circle fa-5x' style='color:#45aaf2;'></i> </br></br>
                Scene Not Available</br>
                Click + at the top to add a scene</h2>";
            }
        } else {
            echo "<h2 class='error'><i class='fas fa-info-circle fa-5x' style='color:#45aaf2;'></i> </br></br>
            Scene Not Available</br>
            Click + at the top to add a scene</h2>";
        }
    }

    ?>

    <input style='display:none;' id='IntervalId' value='none'>
    <input style='display:none;' class='SceneId'>
    <input style='display:none;' class='Check'>
    <input style='display:none;' class='SceneArray'>

    <!-- Add Scene Form -->
    <div class='form-popup' id='addSceneForm'>
        <div class='form-container' style='width:400px;padding:0px;'>
            <div class="modal-header" style='width:100%;height:60px;'>
                <button class='close' onclick='closeForm("addSceneForm")'></button>
                <h3 class='h2form'>Scene Management</h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <fieldset style='width:100%;'>
                    <!-- <legend>Enter Scene Name</legend> -->
                    <label for="name">Name:</label>
                    <input type="text" id="sceneAddName" name="sceneName" style='width:100%;'>

                </fieldset>
            </div>
            <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
                <button type="button" class="all" onclick="addScene()">Add Scene</button>
            </div>
        </div>
    </div>

    <!-- Rename Scene Form -->
    <div class='form-popup' id='renameSceneForm'>
        <div class='form-container' style='width:400px;padding:0px;'>
            <div class="modal-header" style='width:100%;height:60px;'>
                <button class='close' onclick='closeForm("renameSceneForm")'></button>
                <h3 class='h2form'>Rename Scene</h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <fieldset style='width:100%;'>
                    <!-- <legend>Enter Scene Name</legend> -->
                    <label for="name">Name:</label>
                    <input type="text" id="sceneRenameName" name="sceneRename" style='width:100%;'>
                </fieldset>
            </div>
            <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
                <button type="button" class="all" onclick="renameScene()" style='width:160px;'>Rename Scene</button>
            </div>
        </div>
    </div>

    <!-- Form for sign Door Lock -->
    <div class="form-popup" id="mySiginDoorLockForm">
        <div class="form-container" style="width:400px;padding:0px;">
            <div class="modal-header" style='width:100%;height:70px;'>
                <!-- <a class='back' onclick='closeFormSigin()'><span class='fa fa-arrow-left fa-2x'></a> -->
                <h3 class='h2form'>Manage Door Lock </br> Password</h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <fieldset style='width:100%;'>
                    <label for="name">Enter Admin Password:</label>
                    <input type="password" id="password" name="DoorlockPassword" style='width:100%;'>
                </fieldset>
            </div>
            <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
                <button type="submit" class="all" onclick='openGetLock()' name="signin">Ok</button>
                <button type="button" class="all" onclick="cancelFormSigin()">Cancel</button>
            </div>
        </div>
    </div>

    <!-- SideBar + Navbar -->
    <div class="wrapper">
        <!-- Sidebar Holder -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>Senzo</h3>
                <strong>SZ</strong>
            </div>
            <ul class="list-unstyled components">
                <!-- Home Section -->
                <li>
                    <form action='SceneList.php' method='POST' id='NavHome' style='margin-block-end: 0'>
                        <input style='display:none;' name='Home'>
                        <a style='cursor:pointer;' class='Home'>
                            <i class="glyphicon glyphicon-home"></i>
                            Home
                        </a>
                    </form>
                </li>
                <li>
                    <!-- Device Section -->
                    <a href="#pageDevice" data-toggle="collapse" aria-expanded="false">
                        <i class="glyphicon glyphicon-tasks"></i>
                        <?php echo "Device"; ?>
                    </a>
                    <ul class="collapse list-unstyled" id="pageDevice">
                        <?php
                        if ($SmartSwitchStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectSwitch()">Smart Switch</a></li>';
                        }
                        if ($CurtainStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectCurtain()">Smart Curtain</a></li>';
                        }
                        if ($ShutterStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectShutter()">Smart Shutter</a></li>';
                        }
                        if ($LockStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectDoorLock()">Smart Door Lock</a></li>';
                        }
                        if ($IRBlasterStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectIRBlaster()">IR Blaster</a></li>';
                        }
                        ?>
                    </ul>
                </li>
                <li>
                    <!-- Scene Section -->
                    <a id="cube" style="cursor:pointer;" onclick='openScenesForm()'>
                        <i class="glyphicon glyphicon-film"></i>
                        <?php echo "Scene ($SceneCount)"; ?>
                    </a>
                </li>
                <li>
                    <!-- Security Section -->
                    <a href="#pageSecurity" data-toggle="collapse" aria-expanded="false">
                        <i class="glyphicon glyphicon-lock"></i>
                        <?php echo "Security"; ?>
                    </a>
                    <ul class="collapse list-unstyled" id="pageSecurity">
                        <?php
                        // If smart alarm connected to gateway
                        if ($AlarmStatus) {
                            echo "<li><a style='cursor:pointer;' onclick='selectAlarm(&#39;True&#39;)'>
                            Smart Alarm <img src='Images/Action/security_armed_icon.png' alt='scenes' style='width:20px;height:20px;'>";
                        } else {
                            echo "<li><a style='cursor:pointer;' onclick='selectAlarm(&#39;False&#39;)'>
                            Smart Alarm";
                        }
                        ?>
                        </a>
                </li>
                <li><a style="cursor:pointer;" onclick="selectCamera()">Camera</a></li>
            </ul>
            </li>
            <li>
                <!-- Analytic Section -->
                <a style='cursor:pointer;' onclick='openAnalyticForm()'>
                    <i class='glyphicon glyphicon-stats'></i>
                    Analytic
                </a>
            </li>
            <li>
                <!-- Setting Section -->
                <a style='cursor:pointer;' onclick='openSettingForm()'>
                    <i class='glyphicon glyphicon-cog'></i>
                    Setting
                </a>
            </li>
            </ul>

            <!-- Logout button -->
            <ul class="list-unstyled CTAs">
                <li>
                    <form method="POST" action="SceneList.php" id='NavLogout' style='margin-block-end: 0'>
                        <input style='display:none;' name='Logout'>
                        <a style="cursor:pointer;" class="Logout">
                            <i class="glyphicon glyphicon-log-out"></i>
                            Logout
                        </a>
                    </form>
                </li>
            </ul>
        </nav>

        <!-- Page Content Holder -->
        <div id="content" style="width: -webkit-fill-available;">

            <nav class="navbar navbar-default">
                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                            <i class="glyphicon glyphicon-align-left"></i>
                            <span></span>
                        </button>

                        <!-- For responsive part -->
                        <!-- Show and hide content that been targeted-->
                        <button class="btn btn-dark navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <i class="glyphicon glyphicon-align-justify"></i>
                        </button>
                    </div>

                    <!-- Content that have been targeted -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li> <?php
                                    if ($msgJson->Reply == true) {
                                        //To decompress the scene list array
                                        $Compress = $msgJson->Compress;
                                        $decode = base64_decode($Compress);
                                        $Decompress = gzdecode($decode);
                                        $msgJsonGetScene =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));

                                        $SceneListArray = $msgJsonGetScene->SceneList;
                                        $SceneArray = json_encode($SceneListArray);
                                        echo "<a style='cursor:pointer;' onclick='openAddSceneForm(&#39;$SceneArray&#39;)'><i class='fas fa-plus'></i> Add Scene</a>";
                                    } else {
                                        $SceneListArray = array();
                                        $SceneArray = json_encode($SceneListArray);
                                        echo "<a style='cursor:pointer;' onclick='openAddSceneForm(&#39;$SceneArray&#39;)'><i class='fas fa-plus'></i> Add Scene</a>";
                                    }
                                    ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <h2>List of Available Scene</h2>
            <?php
            $GatewayLevel = $_SESSION['level'];
            echo "<h4 class='Level' >Gateway $GatewayLevel</h4>";
            getList($msgJson);
            ?>
            <div class="line"></div>
            </div>
    </div>

    <!-- Auto Refresh dropdown -->
    <div class='autorefresh'>
        <div class="dropdown">
            <button class="btnRefresh dropdown-toggle" type="button" data-toggle="dropdown">
                <img src='Images/Switch/settings_gear.png' alt='switchon' style='width:30px;height:30px;animation: spin 3s linear infinite;'>
            </button>
            <ul class="dropdown-menu">
                <!-- <li><a href="#">Phantom</a></li> -->
                <li>
                    <input type='checkbox' id='enable' name='fooby[2][]'>
                    <label> Enable</label><br>
                    <div id='timeRefresh' style='display:none;margin-left:10px;'>
                        <input type='checkbox' value='1-minute' class="slectOne" id='1-minute'>
                        <label> 1 minute</label><br>
                        <input type='checkbox' value='5-minutes' class="slectOne" id='5-minutes'>
                        <label> 5 minutes</label><br>
                        <input type='checkbox' value='10-minutes' class="slectOne" id='10-minutes'>
                        <label> 10 minutes</label><br>
                        <input type='checkbox' value='20-minutes' class="slectOne" id='20-minutes'>
                        <label> 20 minutes</label><br>
                        <input type='checkbox' value='25-minutes' class="slectOne" id='25-minutes'>
                        <label> 25 minutes</label><br>
                        <input type='checkbox' value='1-hour' class="slectOne" id='1-hour'>
                        <label> 1 hour</label><br>
                    </div>
                </li>
                <li>
                    <input type='checkbox' id='disable' value='disable'>
                    <label> Disable</label><br>
                </li>
            </ul>
        </div>
    </div>

    <!-- Loader -->
    <div class="form-popup" id='loader'>
        <!-- <div id="loadering" class="loader"></div> -->
        <div class="loading-container">
            <div class="loading"></div>
            <div class="loading-text">loading</div>
        </div>
    </div>
</body>

</html>