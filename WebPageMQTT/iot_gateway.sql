-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 08, 2020 at 11:19 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `iot gateway`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gateway`
--

CREATE TABLE `tbl_gateway` (
  `id` int(11) NOT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `Serial` varchar(100) DEFAULT NULL,
  `Location` varchar(100) DEFAULT NULL,
  `Level` varchar(100) DEFAULT NULL,
  `Password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_gateway`
--

INSERT INTO `tbl_gateway` (`id`, `Name`, `Serial`, `Location`, `Level`, `Password`) VALUES
(33, 'Gateway Test', 'ID01-081523-1073', 'Newict', '3', 'Senzo@1073'),
(34, 'Gateway Sir', 'ID01-1E1523-1216', 'Shahiran Cubic', '3', 'Senzo@1216'),
(35, 'Gateway Old', 'ID01-1E1523-1213', 'Newict', '3', 'Senzo@1213');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_gateway`
--
ALTER TABLE `tbl_gateway`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_gateway`
--
ALTER TABLE `tbl_gateway`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
