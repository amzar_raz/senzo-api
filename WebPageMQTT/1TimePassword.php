<?php
ini_set('max_execution_time', 30);
session_start();
ob_start();
//Include all file needed to use in this page
include '././configuration/serverConfig.php';
include './header/headerAll.php';
?>

<!DOCTYPE html>
<html>



<head>
    <title>Smart Door Lock</title>
    <link href="css/1TimePassword.css" rel="stylesheet">
    <link rel="stylesheet" href="css/Sidebar.css">
    <!-- Sidebar and auto refresh function -->
    <script type="text/javascript" src="SidebarHandler.js"></script>
</head>
<script type="text/javascript">
    function open1TimeForm(TemporaryInfoWithId) {

        var fields = TemporaryInfoWithId.split(" ");
        var id = fields[0];
        var TemporaryInfo = fields[1];

        var temp = JSON.parse(TemporaryInfo);
        var tempLength = temp.length;
        //To find number of available temporary password 
        var avail = tempLength == 0 ? "2" : tempLength == 1 ? "1" : tempLength == 2 ? "0" : "";
        var bool = true;
        if (avail == 0) {
            bool = false;
        }

        if (bool == true) {
            document.getElementsByClassName("1TimePass").value = TemporaryInfo;
            document.getElementsByClassName("1TimeId").value = id;
            document.getElementById("my1TimeForm").style.display = "block";
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Full',
                text: 'All password utilised'
            })
        }

    }

    function add1TimePassword() {

        var tempInfo = document.getElementsByClassName("1TimePass").value;
        var LockId = document.getElementsByClassName("1TimeId").value;
        var password = document.getElementById("pass").value;
        var date = document.getElementById("date").value;
        var time = document.getElementById("time").value + ":00";

        var fields = date.split("-")
        //Convert to mm/dd/yyyy for comparison
        var newDate = fields[1] + "/" + fields[0] + "/" + fields[2];
        //Convert to dd/mm/yyyy to send into url
        var inDate = fields[0] + "/" + fields[1] + "/" + fields[2];

        parsedtempInfo = JSON.parse(tempInfo);

        //To determine password id
        if (parsedtempInfo.length == 0) {
            var PId = 1;
        } else {
            var PId = parsedtempInfo[0].PId == 1 ? 2 : 1;
        }

        //Find empty password
        var boolEmpty = false;
        if (password.length == 0) {
            boolEmpty = true;
        }

        //Find password must not less than 6 number
        var boolLess = false;
        if (password.length > 0 && password.length < 6) {
            boolLess = true;
        }

        //Find password must numeric character only
        var numbers = /^[0-9]+$/;
        var boolNum = false;
        if (!password.match(numbers)) {
            boolNum = true;
        }

        //Find password overlapping
        var boolPass = false;
        for (i = 0; i < parsedtempInfo.length; i++) {
            if (parsedtempInfo[i].TP == password) {
                boolPass = true;
                break;
            }
        }

        //Find end date same or not
        var boolDate = false;
        for (i = 0; i < parsedtempInfo.length; i++) {
            if (parsedtempInfo[i].TPEndDate == date && parsedtempInfo[i].TPEndTime == document.getElementById("time").value) {
                boolDate = true;
                break;
            }
        }

        //Set start datetime
        var index = time.split(":");
        var hours = index[0];
        var minutes = index[1];
        var inputDate = new Date(newDate);
        inputDate.setHours(hours);
        inputDate.setMinutes(minutes);

        //Current date
        var today = new Date();

        var loader = document.getElementById("loader");
        if (inputDate >= today && !boolDate && !boolPass && !boolEmpty && !boolLess && !boolNum) {
            var hrefSetLockTemporaryPassword = baseURL + "/doorSetLockTemporary.php/?LockId=" + LockId + "&PId=" + PId + "&TP=" + password + "&TPEndDate=" + inDate + "&TPEndTime=" + time;
            loader.style.display = "block";
            $.getJSON(hrefSetLockTemporaryPassword, function(data) {
                loader.style.display = "none";
                if (data.Command == 'SetLockTemporaryPassword' && data.Status == false) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: data.Message
                    })
                } else {
                    Swal.fire(
                        'Good job!',
                        'Temporary password is successfully set',
                        'success'
                    )
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
                }
            });
        } else if (boolEmpty) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Please input your password'
            })
        } else if (boolLess) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Your password must be 6 digit'
            })
        } else if (boolPass) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Temporary password cannot be same'
            })
        } else if (boolDate) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Both temporary password time must not be same'
            })
        } else if (boolNum) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Password must numeric character only'
            })
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Please select latest time'
            })
        }

    }

    function edit1TimeForm(LockListInfo) {
        var fields = LockListInfo.split(" ");
        var LockId = fields[0];
        var PId = fields[1];
        var TemporaryInfo = fields[2];

        parsedtempInfo = JSON.parse(TemporaryInfo);
        //Set the value in input form
        for (i = 0; i < parsedtempInfo.length; i++) {
            if (LockId == parsedtempInfo[i].LockId && PId == parsedtempInfo[i].PId) {
                document.getElementById("passEdit").value = parsedtempInfo[i].TP;
                document.getElementById("dateEdit").value = parsedtempInfo[i].TPEndDate;
                document.getElementById("timeEdit").value = parsedtempInfo[i].TPEndTime;

            }
        }

        document.getElementsByClassName("1TimePId").value = PId;
        document.getElementsByClassName("1TimeId").value = LockId;
        document.getElementsByClassName("1TimePass").value = TemporaryInfo;
        document.getElementById("edit1TimeForm").style.display = "block";

    }

    function edit1TimePassword() {

        var tempInfo = document.getElementsByClassName("1TimePass").value;
        var PId = document.getElementsByClassName("1TimePId").value;
        var LockId = document.getElementsByClassName("1TimeId").value;
        var password = document.getElementById("passEdit").value;
        var date = document.getElementById("dateEdit").value;
        var time = document.getElementById("timeEdit").value + ":00";


        var fields = date.split("-")
        //Convert to mm/dd/yyyy for comparison
        var newDate = fields[1] + "/" + fields[0] + "/" + fields[2];
        //Convert to dd/mm/yyyy to send into url
        var inDate = fields[0] + "/" + fields[1] + "/" + fields[2];

        parsedtempInfo = JSON.parse(tempInfo);

        //Set start datetime
        var index = time.split(":");
        var hours = index[0];
        var minutes = index[1];
        var inputDate = new Date(newDate);
        inputDate.setHours(hours);
        inputDate.setMinutes(minutes);

        //Current date
        var today = new Date();

        //Find empty password
        var boolEmpty = false;
        if (password.length == 0) {
            boolEmpty = true;
        }

        //Find password must not less than 6 number
        var boolLess = false;
        if (password.length > 0 && password.length < 6) {
            boolLess = true;
        }

        //Find password must numeric character only
        var numbers = /^[0-9]+$/;
        var boolNum = false;
        if (!password.match(numbers)) {
            boolNum = true;
        }

        //Find password overlapping
        var boolPass = false;
        for (i = 0; i < parsedtempInfo.length; i++) {
            if (parsedtempInfo[i].TP == password) {
                boolPass = true;
                break;
            }
        }

        //Find end date same or not
        var boolDate = false;
        for (i = 0; i < parsedtempInfo.length; i++) {
            if (parsedtempInfo[i].TPEndDate == date && parsedtempInfo[i].TPEndTime == document.getElementById("time").value) {
                boolDate = true;
                break;
            }
        }

        var loader = document.getElementById("loader");
        if (inputDate >= today && !boolPass && !boolDate && !boolEmpty && !boolLess && !boolNum) {
            var hrefSetLockTemporaryPassword = baseURL + "/doorSetLockTemporary.php/?LockId=" + LockId + "&PId=" + PId + "&TP=" + password + "&TPEndDate=" + inDate + "&TPEndTime=" + time;
            loader.style.display = "block";
            $.getJSON(hrefSetLockTemporaryPassword, function(data) {
                loader.style.display = "none";
                if (data.Command == 'SetLockTemporaryPassword' && data.Status == false) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Failed',
                        text: data.Message
                    })
                } else {
                    Swal.fire(
                        'Good job!',
                        'Temporary password is successfully set',
                        'success'
                    )
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
                }
            });
        } else if (boolEmpty) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Please input your password'
            })
        } else if (boolLess) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Your password must be 6 digit'
            })
        } else if (boolPass) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Temporary password cannot be same'
            })
        } else if (boolDate) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Both temporary password time must not be same'
            })
        } else if (boolDate) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Password must numeric character only'
            })
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Please select latest time'
            })
        }
    }

    function delete1TimePassword(LockListInfo) {
        var fields = LockListInfo.split(" ");
        var LockId = fields[0];
        var PId = fields[1];

        var hrefDeleteTemporaryPassword = baseURL + "/doorDeleteTemporary.php/?LockId=" + LockId + "&PId=" + PId;
        var loader = document.getElementById("loader");

        Swal.fire({
            title: 'Confirmation',
            text: "Are you sure you want to delete this password?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                loader.style.display = "block";
                $.getJSON(hrefDeleteTemporaryPassword, function(data) {
                    loader.style.display = "none";
                    if (data.Command == 'DeleteTemporaryPassword' && data.Status == false) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Failed',
                            text: data.Message
                        })
                    } else {
                        Swal.fire(
                            'Good job!',
                            'Password Successfully Deleted',
                            'success'
                        )
                        setTimeout(function() {
                            location.reload();
                        }, 2000);
                    }

                });
            }
        })

    }

    function openHistoryForm() {
        var historyForm = document.getElementById("historyForm");
        if (historyForm) {
            historyForm.style.display = "block";
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Error while retrieving temporary history'
            })
        }
    }

    //Add Timepicker function
    $(function() {
        $('#datetimepicker').datetimepicker({
            format: 'HH:mm'

        });
    });

    //Add Datepicker function
    $(function() {
        $("#datepicker").datepicker({
            autoclose: true,
            todayHighlight: true,
            // format: 'mm-dd-yyyy'
            format: 'dd-mm-yyyy'
        }).datepicker('update', new Date());
    });

    //Edit Timepicker function
    $(function() {
        $('#datetimepickerEdit').datetimepicker({
            format: 'HH:mm'
        });
    });

    //Edit Datepicker function
    $(function() {
        $("#datepickerEdit").datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd-mm-yyyy'
        }).datepicker('update', new Date());
    });


    function closeForm(id) {
        document.getElementById(id).style.display = "none";
    }

    //For additional info in the beginning
    $(function() {
        var myLock = document.getElementById("lockEmpty");
        var myHistory = document.getElementById("historyEmpty");

        //If myHistory is exist
        if (myHistory) {
            $("#historyEmpty").append(
                '<div class="alert alert-info" role="alert" style="text-align:center;">' +
                '<h4 class="alert-heading">Additional Info!</h4>' +
                '<p>To create new password, click on ADD PASSWORD</p>' +
                '</div>'
            );
        } else if (myLock) {
            $("#lockEmpty").append(
                '<div class="alert alert-info" role="alert" style="text-align:center;">' +
                '<h4 class="alert-heading">Additional Info!</h4>' +
                '<p>The used password now located in PASSWORD HISTORY</p>' +
                '</div>'
            );
        }
    });
</script>

<body>
    <?php
    //Get all the session variable from Dashboard.php
    $SerialGateway = $_SESSION['serialGateway'];
    $Password = $_SESSION['password'];
    $LocationListArray = $_SESSION['location'];
    $SmartSwitchStatus = $_SESSION['SmartSwitchStatus'];
    $CurtainStatus = $_SESSION['CurtainStatus'];
    $AlarmStatus = $_SESSION['AlarmStatus'];
    $ShutterStatus = $_SESSION['ShutterStatus'];
    $IRBlasterStatus = $_SESSION['IRBlasterStatus'];
    $LockStatus = $_SESSION['LockStatus'];
    $TotalDevice = $_SESSION['TotalDevice'];
    $SceneCount = $_SESSION['SceneCount'];
    $AdminPassword = $_SESSION['AdminPassword'];

    //Get Serial and Lock id from SmartDoorLock page
    $SerialNo = ($_POST['SerialNo']);
    $LockId = ($_POST['LockId']);

    echo "<input style='display:none;' id='SerialGateway' value='$SerialGateway'>";
    echo "<input style='display:none;' id='Password' value='$Password'>";
    echo "<input style='display:none;' id='AdminPassword' value='$AdminPassword'>";

    if (isset($_POST['Logout'])) {
        header("Location: ChooseGateway.php");
        header("Refresh:0");
        ob_flush();
        session_destroy();
        exit();
    }

    if (isset($_POST['Home'])) {
        header("Location: Dashboard.php");
        ob_flush();
        exit();
    }

    $opts = array('http' => array('header' => 'Cookie: ' . $_SERVER['HTTP_COOKIE'] . "\r\n"));  //Create array to send session id in the cookie
    $context = stream_context_create($opts);    //Creates and returns a stream context
    session_write_close();  // Write session data and end session 
    $url = $baseURL . "/doorTemporaryPassword.php/?LockId=$LockId&SerialNo=$SerialNo";  //Create API url to get temporary password and temporary history
    $TemporaryString = file_get_contents($url, false, $context);    //Reads entire file into a string
    $TemporaryString = explode("&#&", $TemporaryString);    //Split the string
    $msgJsonTemporaryPassword = json_decode($TemporaryString[0]); //Convert temporary password or error message from json string 
    $msgJson = $msgJsonTemporaryPassword;

    //If there any error message
    if (!empty($msgJson->Message)) {
        $Error = $msgJson->Message;
    ?>
        <script type="text/javascript">
            setTimeout(function() {
                Swal.fire({
                    icon: 'error',
                    title: 'Failed',
                    text: '<?php echo $Error; ?>',
                }).then(function() {
                    window.location.href = "ChooseGateway.php";
                });
            }, 1000);
        </script>
    <?php
    } else {
        $msgJsonTemporaryHistory = json_decode($TemporaryString[1]); //Convert temporary history from json string
        //To decompress the whole temporary history data
        $Compression = $msgJsonTemporaryHistory->Compression;
        $decode = base64_decode($Compression);
        $Decompress = gzdecode($decode);
        $msgJsonTemporaryHistory =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));
    }

    function getList($msgJsonPassword, $TotalTemporaryHistoryLog)
    {
        //To decompress the whole temporary password data
        $Compression = $msgJsonPassword->Compression;
        $decode = base64_decode($Compression);
        $Decompress = gzdecode($decode);
        $msgJsonPassword =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));

        if ($msgJsonPassword->Command == 'GetLockTemporaryPasswordAPI' && $msgJsonPassword->Reply == true) {
            
            //To decompress the temporary password list array
            $Compress = $msgJsonPassword->Compress;
            $decode = base64_decode($Compress);
            $Decompress = gzdecode($decode);
            $msgJsonPassword =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));

            $SerialNo = ($_POST['SerialNo']);
            $LockId = ($_POST['LockId']);
            $Name = ($_POST['LockName']);

            $TemporaryPasswordListArray = $msgJsonPassword->LockTemporaryPasswordList;

            echo "<h4 class='title nameDevice'>Device : $Name</h4>";
            $count = count($TemporaryPasswordListArray);
            if ($count == 0) {
                echo "<h4 class='title' style='margin-top: 30px;'>Available : 2 </h4>";
            } else if ($count == 1) {
                echo "<h4 class='title' style='margin-top: 30px;'>Available : 1 </h4>";
            } else {
                echo "<h4 class='title' style='margin-top: 30px;'>Available : 0 </h4>";
            }

            $TemporaryInfo = json_encode($TemporaryPasswordListArray);
            $TemporaryInfoWithId = $LockId . ' ' . $TemporaryInfo;

            foreach ($TemporaryPasswordListArray as $TemporaryPassword) {

                $LockListInfo = $TemporaryPassword->LockId . ' ' . $TemporaryPassword->PId;
                $LockListInfoEdit = $TemporaryPassword->LockId . ' ' . $TemporaryPassword->PId . ' ' . $TemporaryInfo;

                echo "<div class='container'>
            <div style='margin-left:auto;margin-right:auto;margin-top: 30px;'>
            <p class='pass'><img src='Images/Door/Unused.png' alt='used' height='30px' width='120px'></p>
            <p class='pass'>Password : $TemporaryPassword->TP</p>
            <div class='grid-container'>
            <div class='center'><p style='color:green;'>Start Date : </p><p>$TemporaryPassword->TPStartDate</p></div>
            <div style='width:100px;'></div>
            <div class='center'><p style='color:green;'>Start Time : </p><p>$TemporaryPassword->TPStartTime</p></div>
            </div>
            <div class='grid-container'>
            <div class='center'><p style='color:red;'>End Date : </p><p>$TemporaryPassword->TPEndDate</p></div>
            <div style='width:100px;'></div>
            <div class='center'><p style='color:red;'>End Time : </p><p>$TemporaryPassword->TPEndTime</p></div>
            </div>
            </div>
            <div class='wrap1'>
            <button class='btn3' style='background:#00a8ff;' onclick='edit1TimeForm(&#39;$LockListInfoEdit&#39;)'><span class='fa fa-edit'></button>
            </div>
            <div class='wrap2'>
            <button class='btn3' style='background:#eb4d4b;' onclick='delete1TimePassword(&#39;$LockListInfo&#39;)'><span class='fa fa-trash'></button>
            </div>
            </div>";
            }

            echo "<div style='text-align:center;'>";
            echo "<button onclick='open1TimeForm(&#39;$TemporaryInfoWithId&#39;)' class='all gap' id='btnAddPassword' >Add Password</button>";
            echo "<div class='notification-badges'>";
            echo "<button data-badge='$TotalTemporaryHistoryLog' onclick='openHistoryForm()' class='all gap' id='btnHistory' >Password History </button>";
            echo "</div></div>";

            if ($count == 0) {
                echo "<div id='historyEmpty'></div>";
            } else if ($count == 1) {
                echo "<div id='lockEmpty'></div>";
            } else {
                echo "<div id='lockEmpty'></div>";
            }
        }
    }

    function getHistory($msgJson)
    {

        if ($msgJson->Command == 'GetLockTemporaryHistoryAPI') {

            //To decompress the temporary history list array
            $Compress = $msgJson->Compress;
            $decode = base64_decode($Compress);
            $Decompress = gzdecode($decode);
            $msgJson =  json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $Decompress));

            $DataTemporaryPasswordListArray = $msgJson->DataTemporaryPasswordList;
            $count = 1;

            echo "<div class='form-popup' id='historyForm'>
                    <div class='form-container' style='width: 600px;padding:0px;'>
                        <div class='modal-header' style='width:100%;height:60px;'>
                            <button class='close' onclick='closeForm(&#39;historyForm&#39;)'></button>
                            <h3 class='h2form'>Temporary Password History</h3>
                        </div>
                        <div class='modal-body' style='width:100%;max-height: calc(80vh - 210px);overflow-y: auto;'>";
            foreach ($DataTemporaryPasswordListArray as $DataTemporary) {

                echo "<div class='container' style='width:max-content;'>
                        <div class='numHistory'>
                            <span class='step'>$count</span>
                        </div>
                        <div style='margin-left:auto;margin-right:auto;margin-top: 30px;'>
                            <div class='grid-container'>
                                <div class='center'><p>Password : </p><p>$DataTemporary->Password</p></div>
                                <div style='width:50px;'></div>";
                if ($DataTemporary->TPStatus == 1) {
                    echo "<div class='center'><img src='Images/Door/Used.png' alt='used' height='30px' width='120px'></div>";
                } else if ($DataTemporary->TPStatus == 2) {
                    echo "<div class='center'><img src='Images/Door/Expired.png' alt='expired' height='30px' width='120px'></div>";
                } else {
                    echo "<div class='center'><img src='Images/Door/Cancel.png' alt='cancel' height='30px' width='120px'></div>";
                }
                echo "</div>";
                echo "<div class='grid-container'>
                                <div class='center'><p style='color:green;'>Start Date : </p><p>$DataTemporary->TPStartDate</p></div>
                                <div style='width:50px;'></div>
                                <div class='center'><p style='color:green;'>Start Time : </p><p>$DataTemporary->TPStartTime</p></div>
                        </div>
                        <div class='grid-container'>
                                <div class='center'><p style='color:red;'>End Date : </p><p>$DataTemporary->TPEndDate</p></div>
                                <div style='width:50px;'></div>
                                <div class='center'><p style='color:red;'>End Time : </p><p>$DataTemporary->TPEndTime</p></div>
                        </div>
                    </div>
                </div>";
                $count++;
            }
            echo "
                    </div>
                </div>
            </div>";
        }
    }

    ?>

    <input style='display:none;' id='IntervalId' value='none'>
    <input style='display:none;' class='1TimePass'>
    <input style='display:none;' class='1TimePId'>
    <input style='display:none;' class='1TimeId'>

    <!-- 1 Time Password Form -->
    <div class="form-popup" id="my1TimeForm">
        <div class="form-container" style='width: 450px;padding:0px;'>
            <div class="modal-header" style='width:100%;height:60px;'>
                <h3 class='h2form'>Create 1 Time Use Password</h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <!-- <form method='post' class='form'> -->
                <div class='omrs-input-group'>
                    <label class='omrs-input-underlined' style='width:100%;'>
                        <label for='password'>Password :</label>
                        </br>
                        <input type='text' id="pass" name="password" maxlength="6" required style='width:100%;'>
                    </label>
                </div>
                <div class='omrs-input-group'>
                    <label class='omrs-input-underlined ' style='width:100%;'>
                        <div class='row'>
                            <div class='col-xs-12 col-md-10'>
                                <label for='quantityDate'>Date :</label>
                                <div id="datepicker" class="input-group date" data-date-format="mm-dd-yyyy">
                                    <input id="date" class="form-control" type="text" readonly />
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                    </label>
                </div>
                <div class='omrs-input-group'>
                    <label class='omrs-input-underlined ' style='width:100%;'>
                        <div class='row'>
                            <div class='col-xs-12 col-md-10'>
                                <div class='form-group'>
                                    <label for='quantityHours'>Time :</label>
                                    <div class='input-group date' id='datetimepicker'>
                                        <input type='text' class='form-control' id='time' />
                                        <span class='input-group-addon'>
                                            <span class='glyphicon glyphicon-time'></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </label>
                </div>
                <!-- </form> -->
            </div>
            <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
                <button type='button' class='all' onclick='add1TimePassword()'>Ok</button>
                <button type='button' class='all' onclick='closeForm("my1TimeForm")'>Cancel</button>
            </div>
        </div>
    </div>

    <!-- Edit 1 Time Password Form -->
    <div class="form-popup" id="edit1TimeForm">
        <div class="form-container" style='width: 450px;padding:0px;'>
            <div class="modal-header" style='width:100%;height:60px;'>
                <h3 class='h2form'>Edit 1 Time Use Password</h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <!-- <form method='post' class='form'> -->
                <div class='omrs-input-group'>
                    <label class='omrs-input-underlined' style='width:100%;'>
                        <label for='password'>Password :</label>
                        </br>
                        <input type="text" id="passEdit" name="password" maxlength="6" required style='width:100%;'>
                    </label>
                </div>
                <div class='omrs-input-group'>
                    <label class='omrs-input-underlined ' style='width:100%;'>
                        <div class='row'>
                            <div class='col-xs-12 col-md-10'>
                                <label for='quantityDate'>Date :</label>
                                <div id="datepickerEdit" class="input-group date" data-date-format="mm-dd-yyyy">
                                    <input id="dateEdit" class="form-control" type="text" readonly />
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                    </label>
                </div>
                <div class='omrs-input-group'>
                    <label class='omrs-input-underlined ' style='width:100%;'>
                        <div class='row'>
                            <div class='col-xs-12 col-md-10'>
                                <div class='form-group'>
                                    <label for='quantityHours'>Time :</label>
                                    <div class='input-group date' id='datetimepickerEdit'>
                                        <input type='text' class='form-control' id='timeEdit' />
                                        <span class='input-group-addon'>
                                            <span class='glyphicon glyphicon-time'></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </label>
                </div>
                <!-- </form> -->
            </div>
            <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
                <button type='button' class='all' onclick='edit1TimePassword()'>Ok</button>
                <button type='button' class='all' onclick='closeForm("edit1TimeForm")'>Cancel</button>
            </div>
        </div>
    </div>

    <!-- Form for sign Door Lock -->
    <div class="form-popup" id="mySiginDoorLockForm">
        <div class="form-container" style="width:400px;padding:0px;">
            <div class="modal-header" style='width:100%;height:70px;'>
                <!-- <a class='back' onclick='closeFormSigin()'><span class='fa fa-arrow-left fa-2x'></a> -->
                <h3 class='h2form'>Manage Door Lock </br> Password</h3>
            </div>
            <div class="modal-body" style='width:100%;'>
                <fieldset style='width:100%;'>
                    <label for="name">Enter Admin Password:</label>
                    <input type="password" id="password" name="DoorlockPassword" style='width:100%;'>
                </fieldset>
            </div>
            <div class="modal-footer" style='width:100%;text-align:-webkit-center;'>
                <button type="submit" class="all" onclick='openGetLock()' name="signin">Ok</button>
                <button type="button" class="all" onclick="cancelFormSigin()">Cancel</button>
            </div>
        </div>
    </div>

    <!-- SideBar + Navbar -->
    <div class="wrapper">
        <!-- Sidebar Holder -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>Senzo</h3>
                <strong>SZ</strong>
            </div>
            <ul class="list-unstyled components">
                <!-- Home Section -->
                <li>
                    <form action='1TimePassword.php' method='POST' id='NavHome' style='margin-block-end: 0'>
                        <input style='display:none;' name='Home'>
                        <a style='cursor:pointer;' class='Home'>
                            <i class="glyphicon glyphicon-home"></i>
                            Home
                        </a>
                    </form>
                </li>
                <li>
                    <!-- Device Section -->
                    <a href="#pageDevice" data-toggle="collapse" aria-expanded="false">
                        <i class="glyphicon glyphicon-tasks"></i>
                        <?php echo "Device"; ?>
                    </a>
                    <ul class="collapse list-unstyled in" id="pageDevice">
                        <?php
                        if ($SmartSwitchStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectSwitch()">Smart Switch</a></li>';
                        }
                        if ($CurtainStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectCurtain()">Smart Curtain</a></li>';
                        }
                        if ($ShutterStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectShutter()">Smart Shutter</a></li>';
                        }
                        if ($LockStatus) {
                            echo '<li><a id="cube" style="cursor:pointer;" onclick="openSmartDoorLock()">Smart Door Lock</a></li>';
                        }
                        if ($IRBlasterStatus) {
                            echo '<li><a style="cursor:pointer;" onclick="selectIRBlaster()">IR Blaster</a></li>';
                        }
                        ?>
                    </ul>
                </li>
                <li>
                    <!-- Scene Section -->
                    <a style="cursor:pointer;" onclick='openScenesForm()'>
                        <i class="glyphicon glyphicon-film"></i>
                        <?php echo "Scene ($SceneCount)"; ?>
                    </a>
                </li>
                <li>
                    <!-- Security Section -->
                    <a href="#pageSecurity" data-toggle="collapse" aria-expanded="false">
                        <i class="glyphicon glyphicon-lock"></i>
                        <?php echo "Security"; ?>
                    </a>
                    <ul class="collapse list-unstyled" id="pageSecurity">
                        <?php
                        // If smart alarm connected to gateway
                        if ($AlarmStatus) {
                            echo "<li><a style='cursor:pointer;' onclick='selectAlarm(&#39;True&#39;)'>
                            Smart Alarm <img src='Images/Action/security_armed_icon.png' alt='scenes' style='width:20px;height:20px;'>";
                        } else {
                            echo "<li><a style='cursor:pointer;' onclick='selectAlarm(&#39;False&#39;)'>
                            Smart Alarm";
                        }
                        ?>
                        </a>
                </li>
                <li><a style="cursor:pointer;" onclick="selectCamera()">Camera</a></li>
            </ul>
            </li>
            <li>
                <!-- Analytic Section -->
                <a style='cursor:pointer;' onclick='openAnalyticForm()'>
                    <i class='glyphicon glyphicon-stats'></i>
                    Analytic
                </a>
            </li>
            <li>
                <!-- Setting Section -->
                <a style='cursor:pointer;' onclick='openSettingForm()'>
                    <i class='glyphicon glyphicon-cog'></i>
                    Setting
                </a>
            </li>
            </ul>

            <!-- Logout button -->
            <ul class="list-unstyled CTAs">
                <li>
                    <form method="POST" action="1TimePassword.php" id='NavLogout' style='margin-block-end: 0'>
                        <input style='display:none;' name='Logout'>
                        <a style="cursor:pointer;" class="Logout">
                            <i class="glyphicon glyphicon-log-out"></i>
                            Logout
                        </a>
                    </form>
                </li>
            </ul>
        </nav>

        <!-- Page Content Holder -->
        <div id="content" style="width: -webkit-fill-available;">

            <nav class="navbar navbar-default">
                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                            <i class="glyphicon glyphicon-align-left"></i>
                            <span></span>
                        </button>

                        <!-- For responsive part -->
                        <!-- Show and hide content that been targeted-->
                        <button class="btn btn-dark navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <i class="glyphicon glyphicon-align-justify"></i>
                        </button>
                    </div>

                    <!-- Content that have been targeted -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a style="cursor:pointer;" onclick='openSmartDoorLock()'>Smart Door Lock</a></li>
                        </ul>
                    </div>
                </div>
            </nav>

            <h2>List of 1 Time Password</h2>
            <?php
            $GatewayLevel = $_SESSION['level'];
            echo "<h4 class='Level' >Gateway $GatewayLevel</h4>";
            $TotalTemporaryHistoryLog = $msgJsonTemporaryHistory->TotalTemporaryHistoryLog;
            getList($msgJsonTemporaryPassword, $TotalTemporaryHistoryLog);
            getHistory($msgJsonTemporaryHistory);
            ?>
            <div class="line"></div>
        </div>
    </div>

    <!-- Auto Refresh dropdown -->
    <div class='autorefresh'>
        <div class="dropdown">
            <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">
                <img src='Images/Switch/settings_gear.png' alt='switchon' style='width:30px;height:30px;animation: spin 3s linear infinite;'>
            </button>
            <ul class="dropdown-menu refreshdrop">
                <!-- <li><a href="#">Phantom</a></li> -->
                <li>
                    <input type='checkbox' id='enable' name='fooby[2][]'>
                    <label> Enable</label><br>
                    <div id='timeRefresh' style='display:none;margin-left:10px;'>
                        <input type='checkbox' value='1-minute' class="slectOne" id='1-minute'>
                        <label> 1 minute</label><br>
                        <input type='checkbox' value='5-minutes' class="slectOne" id='5-minutes'>
                        <label> 5 minutes</label><br>
                        <input type='checkbox' value='10-minutes' class="slectOne" id='10-minutes'>
                        <label> 10 minutes</label><br>
                        <input type='checkbox' value='20-minutes' class="slectOne" id='20-minutes'>
                        <label> 20 minutes</label><br>
                        <input type='checkbox' value='25-minutes' class="slectOne" id='25-minutes'>
                        <label> 25 minutes</label><br>
                        <input type='checkbox' value='1-hour' class="slectOne" id='1-hour'>
                        <label> 1 hour</label><br>
                    </div>
                </li>
                <li>
                    <input type='checkbox' id='disable' value='disable'>
                    <label> Disable</label><br>
                </li>
            </ul>
        </div>
    </div>

    <!-- Loading animation -->
    <div class="form-popup" id='loader'>
        <div class="loading-container">
            <div class="loading"></div>
            <div class="loading-text">loading</div>
        </div>
    </div>
</body>

</html>