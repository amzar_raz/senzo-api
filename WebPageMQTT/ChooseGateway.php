<?php
ini_set('max_execution_time', 30);
session_start();
ob_start();
//Include all file needed to use in this page
include 'generateSerial.php';
include './header/headerAll.php';
include '././configuration/serverConfig.php';
?>
<!DOCTYPE HTML>

<head>
    <title>Senzo Switch</title>
    <link href="css/ChooseGateway.css" rel="stylesheet">
    <link href="css/Home.css" rel="stylesheet">
</head>
<script type="text/javascript">
    //Clickable row
    $(document).ready(function() {
        $('#example tr').click(function() {

            //Get value user click from table
            var value = $(this).find("a").attr("value");
            var field = value.split("&");

            var gatewayId = field[0];
            var gatewayName = field[1];
            var gatewaySerial = field[2];
            var gatewayLocation = field[3];
            var gatewayLevel = field[4];
            var gatewayPassword = field[5];

            document.getElementById("name_upd").value = gatewayName;
            document.getElementById("serial_upd").value = gatewaySerial;
            document.getElementById("location_upd").value = gatewayLocation;
            document.getElementById("level_upd").value = gatewayLevel;
            document.getElementById("id_upd").value = gatewayId;
            document.getElementById("password_upd").value = gatewayPassword;

            document.getElementById("btnDelete").value = gatewayId;
            document.getElementById("btnSubmit").value = gatewaySerial + " " + gatewayPassword;
            document.getElementById("gLevel").value = gatewayLevel;
            document.getElementById("gName").value = gatewayName;

            document.getElementById("mypopupUpdateDeleteForm").style.display = "block";
        });
    });

    function openGatewayForm() {
        document.getElementById("myAddForm").style.display = "block";
    }

    function addGateway(List) {
        var serial = document.getElementById("serial").value;
        parsedtempInfo = JSON.parse(List);
        //If gateway serial already exist
        for (i = 0; i < parsedtempInfo.length; i++) {
            if (parsedtempInfo[i].Serial == serial) {
                Swal.fire({
                    icon: 'error',
                    title: 'Failed to Add The Gateway',
                    text: 'Gateway serial number already exist'
                })
                return false;
            }
        }
        return true;
    }

    function closeForm(id) {
        document.getElementById(id).style.display = "none";
    }

    function btnUpdateForm() {
        document.getElementById("mypopupUpdateDeleteForm").style.display = "none";
        document.getElementById("myUpdateForm").style.display = "block";
    }

    function btnDeleteForm() {
        document.getElementById("mypopupUpdateDeleteForm").style.display = "none";
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                document.getElementById("id").value = document.getElementById("btnDelete").value;
                document.getElementById("myForm").submit();
            }
        })
    }

    //For table
    $(document).ready(function() {
        $('#example').DataTable();
    });

    //Show/Hide SN
    $(document).ready(function() {
        $("#showSn").click(function() {
            if ($(this).prop("checked") == true) {
                $(".gatewaySNText").fadeIn("fast", "swing");
                localStorage.input = $(this).val();
            } else {
                $(".gatewaySNText").fadeOut("fast", "swing");
                localStorage.input = 'hideSn';
            }
        });
    });

    $(function() {
        var statusSn = localStorage.input;
        if (statusSn == 'showSn') {
            $('#showSn').prop('checked', true);
            $(".gatewaySNText").css("display", "block");
        } else {
            $('#showSn').prop('checked', false);
            $(".gatewaySNText").css("display", "none");
        }
    });
</script>
<html>

<body>
    <div id="wrapper">
        <header>
            <h1><span>Senzo</span><br>IoT Smart Building</h1>
        </header>
        <main>
            <div style="position: absolute; left: 5%; top: 20%;">
                <button class='add gw' onclick='openGatewayForm()'><i class="fas fa-plus-circle"></i> Gateway</button>
            </div>
            <h3>List of Available Gateway</h3>
            <!-- Show gateway information in table -->
            <div class="conttable">
                <div class="table-responsive">
                    <table class="table table-bordered  table-hover" id="example" style="width:100%">
                        <thead class='thead-dark'>
                            <tr>
                                <th scope="col">#</th>
                                <!-- <th scope="col">Id</th> -->
                                <th scope="col">Gateway Name</th>
                                <!-- <th scope="col">Gateway Serial</th> -->
                                <th scope="col">Location</th>
                                <th scope="col">Level</th>
                            </tr>
                        </thead>
                        <tbody id="myTable">

                            <?php
                            $List = getAllGatewayfromDB();
                            $array = json_decode($List, true);
                            $counter = 1;
                            foreach ($array as $item) {
                                echo "<tr class='table-light'>
                                        <td scope='row'><a style='color:black;' value='$item[id]&$item[Name]&$item[Serial]&$item[Location]&$item[Level]&$item[password]'>$counter</a></td>
                                        <td style='cursor:pointer;'>
                                            <p>$item[Name]</p>
                                            <p style='margin-top:5px;' class='gatewaySNText'>$item[Serial]</p>
                                        </td>
                                        <td style='cursor:pointer;'>$item[Location]</td>
                                        <td style='cursor:pointer;'>$item[Level]</td>
                                    </tr>";
                                // <td class='SerialIdList' style='cursor:pointer;' value='$item[id]'>$item[Serial]</td>
                                $counter++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </main>
        <!-- <footer>
            This is the footer
        </footer> -->
    </div>

    <!-- Submit delete form -->
    <form action='ChooseGateway.php' method='POST' id='myForm' style='display:none;'>
        <input type="text" style="display:none;" id="id" name="Id">
    </form>

    <!-- Show SN dropdown -->
    <div class='autorefresh'>
        <div class="dropdown">
            <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">
                <img src='Images/Switch/settings_gear.png' alt='switchon' style='width:30px;height:30px;animation: spin 3s linear infinite;'>
            </button>
            <ul class="dropdown-menu">
                <li>
                    <input type='checkbox' id='showSn' value='showSn'>
                    <label> Show SN</label><br>
                </li>
            </ul>
        </div>
    </div>

    <!-- Form show update and delete popup -->
    <div class='form-popupUpdateDelete' id='mypopupUpdateDeleteForm'>
        <form action='ChooseGateway.php' method='POST'>

            <div class='menu'>
                <div class='menulist'>
                    <button class='gw' type='submit' id='btnSubmit' name='Submit'><span class='fa fa-mouse-pointer'></span>Select</button>
                    <button class='gw' type='button' id='btnUpdate' onclick='btnUpdateForm()' name='update'><span class='fa fa-edit'></span>Edit</button>
                    <button class='gw' type='button' id='btnDelete' onclick='btnDeleteForm()' name='delete'><span class='fa fa-trash'></span>Delete</button>
                    <button class='gw' type='button' onclick='closeForm("mypopupUpdateDeleteForm")'><span class='fa fa-times'></span>Close</button>
                    <input style="display:none;" id="gLevel" name="level">
                    <input style="display:none;" id="gName" name="name">
                </div>
            </div>
        </form>
    </div>

    <!-- Form for Add new gateway information -->
    <div class="form-popup" id="myAddForm">
        <?php
        $List = getAllGatewayfromDB();
        echo "<form action='ChooseGateway.php' method='POST' class='form-container' onsubmit='return addGateway(&#39;$List&#39;)'>"; ?>
        <h2>Add Gateway</h2>

        <fieldset>
            <legend>Your Gateway Info</legend>
            <label for="name">Name:</label>
            <input type="text" id="name" name="GatewayName" required>

            <label for="serial">Serial:</label>
            <input type="text" id="serial" name="GatewaySerial" pattern="[I][D][0][1][\-][A-Z0-9]{6}[\-][A-Z0-9]{4}" required>

            <label for="location">Location:</label>
            <input type="text" id="location" name="GatewayLocation" required>

            <label for="level">Level:</label>
            <input type="text" id="level" name="GatewayLevel" required>

        </fieldset>

        <button type='submit' class='btnForm'>Submit</button>
        <button type="button" class="btnForm" onclick="closeForm('myAddForm')">Close</button>
        </form>
    </div>

    <!-- Form for update gateway information -->
    <div class="form-popup" id="myUpdateForm">
        <form action="ChooseGateway.php" method="POST" class="form-container">
            <h2>Update Gateway</h2>

            <fieldset>
                <legend>Your Gateway Info</legend>
                <label for="name">Name:</label>
                <input type="text" id="name_upd" name="GatewayUpdName" required>

                <label for="serial">Serial:</label>
                <input type="text" id="serial_upd" name="GatewayUpdSerial" pattern="[I][D][0][1][\-][A-Z0-9]{6}[\-][A-Z0-9]{4}">

                <label for="location">Location:</label>
                <input type="text" id="location_upd" name="GatewayUpdLocation" required>

                <label for="level">Level:</label>
                <input type="text" id="level_upd" name="GatewayUpdLevel" required>

                <label for="password">Password:</label>
                <input type="text" id="password_upd" name="GatewayUpdPassword">

                <input type="text" style="display:none;" id="id_upd" name="GatewayUpdId">

            </fieldset>

            <button type="submit" class="btnForm" name='updButton'>Submit</button>
            <button type="button" class="btnForm" onclick="closeForm('myUpdateForm')">Close</button>
        </form>
    </div>
    <?php

    //Select gateway
    if (isset($_POST['Submit'])) {
        $serialGatewayPasswordBefore = $_POST['Submit'];
        $gatewayLevel = $_POST['level'];
        $gatewayName = $_POST['name'];

        $fields = explode(" ", $serialGatewayPasswordBefore);
        $serialGatewayBefore = $fields[0];
        $serialPasswordBefore = $fields[1];

        $serialGateway = generateUserSerial($serialGatewayBefore);
        // $serialPassword = "Senzo@" . substr($serialGatewayBefore, 12, 4);
        $serialPassword = $serialPasswordBefore;

        $_SESSION['serialNo'] = $serialGatewayBefore;
        $_SESSION['serialGateway'] = $serialGateway;
        $_SESSION['password'] = $serialPassword;
        $_SESSION['level'] = $gatewayLevel;
        $_SESSION['name'] = $gatewayName;

        header("Location: Dashboard.php");
        header("Refresh:0");
        ob_flush();
        exit();
    }

    //Create gateway
    if (isset($_POST['GatewayName']) && isset($_POST['GatewaySerial']) && isset($_POST['GatewayLocation']) && isset($_POST['GatewayLevel'])) {
        $defaultPassword = "Senzo@" . substr($_POST['GatewaySerial'], 12, 4);
        if (createGateway($_POST['GatewayName'], $_POST['GatewaySerial'], $_POST['GatewayLocation'], $_POST['GatewayLevel'], $defaultPassword)) {
    ?>
            <script type="text/javascript">
                Swal.fire(
                    'Good job!',
                    'You add new gateway',
                    'success'
                )
                setTimeout(function() {
                    location.reload();
                }, 2000);
                if (window.history.replaceState) {
                    window.history.replaceState(null, null, window.location.href);
                }
            </script>
        <?php
        } else {
        ?>
            <script type="text/javascript">
                Swal.fire({
                    icon: 'error',
                    title: 'Failed',
                    text: 'Error while adding new gateway'
                })
                setTimeout(function() {
                    location.reload();
                }, 2000);
                if (window.history.replaceState) {
                    window.history.replaceState(null, null, window.location.href);
                }
            </script>
        <?php
        }
    }

    //Update gateway
    if (isset($_POST['updButton'])) {
        if (updateGateway($_POST['GatewayUpdName'], $_POST['GatewayUpdSerial'], $_POST['GatewayUpdLocation'], $_POST['GatewayUpdLevel'], $_POST['GatewayUpdId'], $_POST['GatewayUpdPassword'])) {
        ?>
            <script type="text/javascript">
                Swal.fire(
                    'Good job!',
                    'Your gateway information successfully updated',
                    'success'
                )
                setTimeout(function() {
                    location.reload();
                }, 2000);
                if (window.history.replaceState) {
                    window.history.replaceState(null, null, window.location.href);
                }
            </script>
        <?php
        } else {
        ?>
            <script type="text/javascript">
                Swal.fire({
                    icon: 'error',
                    title: 'Failed',
                    text: 'Error while updating gateway information'
                })
                setTimeout(function() {
                    location.reload();
                }, 2000);
                if (window.history.replaceState) {
                    window.history.replaceState(null, null, window.location.href);
                }
            </script>
        <?php
        }
    }


    //Delete gateway
    if (isset($_POST['Id'])) {
        if (deleteGateway($_POST['Id'])) {
        ?>
            <script type="text/javascript">
                Swal.fire(
                    'Good job!',
                    'You have delete gateway',
                    'success'
                )
                setTimeout(function() {
                    location.reload();
                }, 2000);
                if (window.history.replaceState) {
                    window.history.replaceState(null, null, window.location.href);
                }
            </script>
        <?php
        } else {
        ?>
            <script type="text/javascript">
                Swal.fire({
                    icon: 'error',
                    title: 'Failed',
                    text: 'Error while deleting gateway gateway'
                })
                setTimeout(function() {
                    location.reload();
                }, 2000);
                if (window.history.replaceState) {
                    window.history.replaceState(null, null, window.location.href);
                }
            </script>
    <?php
        }
    }

    //Get all gateway information
    function getAllGatewayfromDB()
    {
        include '././configuration/databaseConfig.php';

        $link = mysqli_connect($hostname, $username, $password, $dbname);

        // Check connection
        if ($link === false) {
            die("ERROR: Could not connect. " . mysqli_connect_error());
        }

        $sql_query = "SELECT * from $tablename";
        $std_obj = $link->prepare($sql_query);

        //execute query
        $std_obj->execute();
        $data = $std_obj->get_result();
        $gateways = array();

        while ($row = $data->fetch_assoc()) {
            array_push($gateways, array(
                "id" => $row[$gatewayId],
                "Name" => $row[$gatewayName],
                "Serial" => $row[$gatewaySerial],
                "Location" => $row[$gatewayLocation],
                "Level" => $row[$gatewayLevel],
                "password" => $row[$gatewayPassword]
            ));
        }

        $json = json_encode($gateways);

        return $json;
    }

    //Create new gateway info
    function createGateway($Name, $Serial, $Location, $Level, $defaultPassword)
    {
        include '././configuration/databaseConfig.php';
        $link = mysqli_connect($hostname, $username, $password, $dbname);

        // Check connection
        if ($link === false) {
            die("ERROR: Could not connect. " . mysqli_connect_error());
        }

        $sql = "INSERT INTO `$tablename` (`$gatewayId`, `$gatewayName`, `$gatewaySerial`, `$gatewayLocation`, `$gatewayLevel`, `$gatewayPassword`) VALUES (NULL, '$Name', '$Serial', '$Location', '$Level', '$defaultPassword')";
        if (mysqli_query($link, $sql)) {
            return true;
        } else {
            return false;
        }
        mysqli_close($link);
    }

    //Update gateway info
    function updateGateway($Name, $Serial, $Location, $Level, $id, $Password)
    {
        include '././configuration/databaseConfig.php';
        $link = mysqli_connect($hostname, $username, $password, $dbname);

        // Check connection
        if ($link === false) {
            die("ERROR: Could not connect. " . mysqli_connect_error());
        }

        $sql = "UPDATE `$tablename` SET `$gatewayName`='$Name', `$gatewaySerial`='$Serial', `$gatewayLocation`='$Location',`$gatewayLevel`='$Level', `$gatewayPassword`='$Password' WHERE `$gatewayId`= '$id'";
        if (mysqli_query($link, $sql)) {
            return true;
        } else {
            return false;
        }
        mysqli_close($link);
    }

    //Delete gateway info
    function deleteGateway($id)
    {
        include '././configuration/databaseConfig.php';
        $link = mysqli_connect($hostname, $username, $password, $dbname);

        // Check connection
        if ($link === false) {
            die("ERROR: Could not connect. " . mysqli_connect_error());
        }

        $sql = "DELETE FROM `$tablename` WHERE $gatewayId = $id";
        if (mysqli_query($link, $sql)) {
            return true;
        } else {
            return false;
        }
        mysqli_close($link);
    }
    ?>
</body>

</html>